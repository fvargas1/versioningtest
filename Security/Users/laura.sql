IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'laura')
CREATE LOGIN [laura] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [laura] FOR LOGIN [laura]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Consumo_FechaValuacion_fecha]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_Consumo_FechaValuacion_fecha';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Garantia_Consumo
SET errorFormato = 1
WHERE LEN(FechaValuacion)>0 AND ISDATE(FechaValuacion) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoGarantia,
	'FechaValuacion',
	FechaValuacion,
	1,
	@Detalle
FROM dbo.FILE_Garantia_Consumo
WHERE LEN(FechaValuacion)>0 AND ISDATE(FechaValuacion) = 0;
GO

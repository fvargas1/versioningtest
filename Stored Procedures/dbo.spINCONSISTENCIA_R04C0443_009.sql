SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_009]
AS

BEGIN

-- El Codigo del Crédito dato deberá ser único e irrepetible dentro del archivo que se reporta.

SELECT CodigoCredito, COUNT(CodigoCredito) AS Repetidos
FROM dbo.RW_R04C0443
GROUP BY CodigoCredito
HAVING COUNT(CodigoCredito) > 1;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Rel_Group_Group_Ins]
	@IdGroupParent BAJAWARE_utID,
	@IdGroupChild BAJAWARE_utID
AS
INSERT INTO dbo.BAJAWARE_Rel_Group_Group (IdGroupParent, IdGroupChild)
VALUES (@IdGroupParent, @IdGroupChild);
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_User_HasPwdIteration]
	@userId AS INTEGER,
	@password AS BAJAWARE_utBinary
AS
SELECT COUNT(IdPasswordIteration)
FROM dbo.BAJAWARE_UserPasswordIteration
WHERE IdUser = @userId AND OldPassword = @password;
GO

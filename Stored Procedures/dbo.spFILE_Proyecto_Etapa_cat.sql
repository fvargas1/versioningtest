SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Proyecto_Etapa_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Proyecto_Etapa_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Proyecto f
LEFT OUTER JOIN dbo.SICC_Etapa cat ON LTRIM(ISNULL(f.Etapa,'')) = cat.Codigo
WHERE cat.IdEtapa IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Proyecto_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoProyecto,
 'Etapa',
 f.Etapa,
 2,
 @Detalle
FROM dbo.FILE_Proyecto f
LEFT OUTER JOIN dbo.SICC_Etapa cat ON LTRIM(ISNULL(f.Etapa,'')) = cat.Codigo
WHERE cat.IdEtapa IS NULL;
GO

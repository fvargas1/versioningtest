SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Cedula_Anexo20]
AS
DECLARE @NombreInstitucion VARCHAR(150);
DECLARE @FechaPeriodo VARCHAR(10);

SELECT @NombreInstitucion = Nombre FROM dbo.SICC_Institucion WHERE Codigo = (SELECT [Value] FROM dbo.BAJAWARE_Config WHERE CodeName='CodigoInstitucion');
SELECT @FechaPeriodo = CONVERT(CHAR(10), Fecha,126) FROM dbo.SICC_Periodo WHERE Activo = 1;

SELECT DISTINCT
p.IdPersona AS IdPersona,
c.IdMetodologia,
p.Nombre AS 'Description',
dbo.fnFormat_Name(p.Nombre) AS NombrePersona,
dbo.fnFormat_Name(p.Codigo) AS CodigoPersona,
m.Nombre AS Metodologia,
m.Codigo AS CodigoMetodologia,
'' AS Division,
0 as FirmaPromotor,
@NombreInstitucion AS Institucion,
@FechaPeriodo AS FechaPeriodo
FROM dbo.SICCMX_Persona p
INNER JOIN dbo.SICCMX_VW_Credito_NMC c ON p.IdPersona = c.IdPersona
INNER JOIN dbo.SICCMX_Anexo20 anx ON c.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Metodologia m ON c.IdMetodologia = m.IdMetodologia
WHERE c.MontoValorizado > 0
ORDER BY p.Nombre;
GO

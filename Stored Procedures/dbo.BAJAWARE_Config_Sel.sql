SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Config_Sel]
	@IdConfig BAJAWARE_utID
AS
SELECT IdConfig, [Name], Description, CodeName, Value
FROM dbo.BAJAWARE_Config
WHERE IdConfig = @IdConfig;
GO

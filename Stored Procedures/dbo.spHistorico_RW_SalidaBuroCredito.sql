SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_SalidaBuroCredito]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_SalidaBuroCredito);

DELETE FROM Historico.RW_SalidaBuroCredito WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_SalidaBuroCredito (
	IdPeriodoHistorico, Periodo, BP, Cuenta, Calificacion, Cartera
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	BP,
	Cuenta,
	Calificacion,
	Cartera	
FROM dbo.RW_SalidaBuroCredito
WHERE IdReporteLog = @IdReporteLog;
GO

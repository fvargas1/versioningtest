SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_032_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si la Rotación de Capital de Trabajo (dat_rotac_capital_trabajo) es = 0 y , entonces el Puntaje
-- Asignado por Rotación de Capital de Trabajo (cve_ptaje_rotac_capit_trabajo) debe ser = 55

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(RotCapTrabajo AS DECIMAL(10,6)) = 0 AND ISNULL(P_RotCapTrabajo,'') <> '55';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO

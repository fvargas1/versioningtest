SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Entidad_GetEntidades]
AS
SELECT
	ep.IdEntidad,
	ep.Codigo AS CodigoBajaware,
	ep.Nombre,
	ep.Codigo,
	ep.Codigo + ' - ' + ep.Nombre + 
	CASE
	WHEN te.Codigo='1' THEN '(Estado)'
	WHEN te.Codigo='2' THEN '(Municipio)'
	WHEN te.Codigo='3' THEN '(Organismo Descentralizado)'
	ELSE '( )' END AS DisplayText
FROM dbo.SICCMX_Entidad ep
LEFT OUTER JOIN dbo.SICC_TipoEntidad te ON ep.IdTipoEntidad=te.IdTipoEntidad
WHERE ep.Codigo IS NOT NULL
ORDER BY ep.Nombre ASC;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_005]
AS
BEGIN
-- Validar que el Puntaje Crediticio Cualitativo (dat_puntaje_cred_cualitativo) sea >= 281 pero <= 835.

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PuntajeCualitativo
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(PuntajeCualitativo,''),'-1') AS DECIMAL) NOT BETWEEN 281 AND 835;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_038]
AS

BEGIN

-- Si el Tipo de Operación es 254, el Destino de Crédito deberá ser 330, 332, 333, 430, 432 ó 433

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoOperacion,
	DestinoCredito
FROM dbo.RW_VW_R04C0478_INC
WHERE ISNULL(TipoOperacion,'') = '254' AND ISNULL(DestinoCredito,'') NOT IN ('330', '332', '333', '430', '432', '433');

END


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0419Datos_CalculandoConceptos]
AS

TRUNCATE TABLE R04.[0419Conceptos];

-- Calculamos los conceptos historicos.
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139401000000',
	(ReservasAdicOrdenadasCNBVHistorico+SaldoReservaCubHistorico+SaldoReservaExpHistorico+InteresCarteraVencidaHistorico)
FROM R04.[0419Datos] dat
WHERE (ReservasAdicOrdenadasCNBVHistorico+SaldoReservaCubHistorico+SaldoReservaExpHistorico+InteresCarteraVencidaHistorico) <> 0
AND dat.CodigoProducto IS NOT NULL;

 -- Calculamos los conceptos totales actuales.
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139402000000',
	(dat.ReservasAdicOrdenadasCNBV+dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.InteresCarteraVencida)
FROM R04.[0419Datos] dat
WHERE (dat.ReservasAdicOrdenadasCNBV+dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.InteresCarteraVencida) <> 0
AND dat.CodigoProducto IS NOT NULL;

-- Movimientos creditos comerciales
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	cre.Codigo,
	conf.Concepto,
	SUM(mov.Monto)
FROM dbo.SICCMX_Credito_Movimientos mov
INNER JOIN dbo.SICCMX_Credito cre ON mov.IdCredito = cre.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0419Datos] dat ON dat.Codigo = cre.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo AND conf.TipoMovimiento IS NOT NULL
WHERE tmov.Codigo IN ('ADJU','BONIF','CAST')
GROUP BY cre.Codigo, conf.Concepto;


-- Movimientos para creditos de consumo
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	con.Codigo,
	conf.Concepto,
	SUM(mov.Monto)
FROM dbo.SICCMX_Consumo_Movimientos mov
INNER JOIN dbo.SICCMX_Consumo con ON mov.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0419Datos] dat ON dat.Codigo = con.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo AND conf.TipoMovimiento IS NOT NULL
WHERE tmov.Codigo IN ('ADJU','BONIF','CAST')
GROUP BY con.Codigo, conf.Concepto;


-- Movimientos para creditos hipotecarios
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	hip.Codigo,
	conf.Concepto,
	SUM(mov.Monto)
FROM dbo.SICCMX_Hipotecario_Movimientos mov
INNER JOIN dbo.SICCMX_Hipotecario hip ON mov.IdHipotecario = hip.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0419Datos] dat ON dat.Codigo = hip.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo AND conf.TipoMovimiento IS NOT NULL
WHERE tmov.Codigo IN ('ADJU','BONIF','CAST')
GROUP BY hip.Codigo, conf.Concepto;


-- Calculo de diferencias de reserva cuando el credito tiene castigos o bonificaciones y el credito esta en ceros
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139211' + conf.Concepto,
	cargos.ReservaHistorico - (cargos.ReservaActual + cargos.CargoMovimiento)
FROM R04.[0419Datos] dat
INNER JOIN R04.RW_VW_CargosMovimiento cargos ON dat.Codigo = cargos.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE cargos.ReservaHistorico > (cargos.ReservaActual + cargos.CargoMovimiento) AND dat.SaldoActual = 0 AND dat.Moneda = '0'; -- Moneda Nacional


-- Calculo de conceptos Movimientos otros cargos
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139290000000',
	(dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico) - (dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida)
FROM R04.[0419Datos] dat
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico > dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida
AND conf.TipoMovimiento IS NULL AND dat.Moneda = '0'; -- Moneda Nacional


/* ABONOS */

-- Aumento de las reservas
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139301' + conf.Concepto, -- Creacion de estimaciones preventivas para riesgo crediticio por calificacion de cartera
	(dat.SaldoReservaCubActual + dat.SaldoReservaExpActual) - (dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico)
FROM R04.[0419Datos] dat
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
LEFT OUTER JOIN R04.VW_0419Abonos_CalculoDiferente dif ON dat.Codigo = dif.Codigo
WHERE SaldoReservaExpHistorico + SaldoReservaCubHistorico < SaldoReservaCubActual + SaldoReservaExpActual AND SaldoHistorico > 0 AND SaldoActual > 0
	AND dif.Codigo IS NULL AND dat.Moneda = '0'; -- Moneda Nacional


/*Aumento de reservas cuando se mantiene la misma calificacion pero anterior reserva era cero*/
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139301' + conf.Concepto,
	dat.SaldoReservaCubActual + dat.SaldoReservaExpActual
FROM R04.[0419Datos] dat
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE (CalifCubHistorico IS NULL AND CalifCubActual IS NULL)
	AND (CalifExpHistorico IS NOT NULL AND CalifExpActual IS NOT NULL ) 
	AND CalifExpHistorico = CalifExpActual 
	AND (SaldoReservaCubActual + SaldoReservaExpActual) > 0
	AND (SaldoReservaCubHistorico + SaldoReservaExpHistorico) = 0
	AND dat.Moneda = '0' -- Moneda Nacional
UNION ALL
SELECT
	dat.Codigo,
	'139301' + conf.Concepto,
	dat.SaldoReservaCubActual + dat.SaldoReservaExpActual
FROM R04.[0419Datos] dat
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE (CalifExpHistorico IS NULL AND CalifExpActual IS NULL)
	AND (CalifCubHistorico IS NOT NULL AND CalifCubActual IS NOT NULL)
	AND CalifCubHistorico = CalifCubActual
	AND (SaldoReservaCubActual+SaldoReservaExpActual) > 0
	AND (SaldoReservaCubHistorico+SaldoReservaExpHistorico) = 0
	AND dat.Moneda = '0'; -- Moneda Nacional


-- Reservas adicionales
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139302000000',
	(dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida) - (dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico)
FROM R04.[0419Datos] dat
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico < dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida
	AND dat.Moneda = '0'; -- Moneda Nacional


-- Reservas de creditos nuevos
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139301' + conf.Concepto,
	dat.SaldoReservaCubActual + dat.SaldoReservaExpActual + ISNULL(cargos.CargoMovimiento,0)
FROM R04.[0419Datos] dat
LEFT OUTER JOIN R04.RW_VW_CargosMovimiento cargos ON cargos.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
LEFT OUTER JOIN (
	SELECT dat.Codigo
	FROM R04.[0419Datos] dat
	INNER JOIN RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo = dat.Codigo
	INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
	WHERE dat.CalifCubHistorico IS NULL AND dat.CalifExpHistorico IS NULL
) AS tmp ON dat.Codigo = tmp.Codigo
WHERE dat.SaldoHistorico = 0 AND dat.CalifCubHistorico IS NULL AND dat.CalifExpHistorico IS NULL AND tmp.Codigo IS NULL;


-- Calculo de diferencias de reserva cuando el credito tiene castigos o bonificaciones y el credito esta en ceros
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139301' + conf.Concepto,
	(cargos.ReservaActual + cargos.CargoMovimiento) - cargos.ReservaHistorico
FROM R04.[0419Datos] dat
INNER JOIN r04.RW_VW_CargosMovimiento cargos ON cargos.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE cargos.ReservaHistorico < (cargos.ReservaActual + cargos.CargoMovimiento) AND dat.SaldoActual = 0
	AND dat.Moneda = '0' --Moneda Nacional
	AND cargos.ReservaHistorico > 0; -- Evitar los creditos nuevos


/***
Para los conceptos donde se compara la calificacion historica con la actual se toma
como base 2 calificaciones: calificacion expuesta y cubierta.
Para el caso donde un credito el mes pasado solo presente una reserva (ya sea expuesta o cubierta),
y en el presente mes tenga dos calificaciones. La disminucion del monto de reserva
(mientras mantenga la misma calificación) se registrara como cancelación de estimaciones por cobro de cartera.
Mientras que la reserva creada por la nueva calificación del credito se registrara como estimación de reservas
derivadas de adquisiciones de cartera.
*/
-- Select del cargo por cancelacion de estimacion por cobro EXP
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	CASE
	WHEN (dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico) - (dat.SaldoReservaExpActual + dat.SaldoReservaCubActual) > 0 AND dat.SaldoActual = 0 THEN '139211'
	WHEN (dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico) - (dat.SaldoReservaExpActual + dat.SaldoReservaCubActual) > 0 AND dat.SaldoActual > 0 THEN '139212'
	ELSE '139301' END + conf.Concepto,
	ABS((dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico) - (dat.SaldoReservaExpActual + dat.SaldoReservaCubActual))
FROM R04.[0419Datos] dat
INNER JOIN dbo.RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE ((dat.CalifCubHistorico IS NULL AND dat.CalifCubActual IS NOT NULL AND dat.CalifExpHistorico = dat.CalifExpActual)
	OR(dat.CalifExpHistorico IS NULL AND dat.CalifExpActual IS NOT NULL AND dat.CalifCubActual = dat.CalifCubHistorico))
	AND dat.Moneda = '0' -- Moneda Nacional
	AND ABS((dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico) - (dat.SaldoReservaExpActual + dat.SaldoReservaCubActual)) > 0;


/**
Caso 2: Cuando la calificacion Expuesta Actual es diferente de la anterior y En este periodo
se crea una calificaicon Cubierta
**/
-- Select de cargo Cancelacion de estimacion por calificacion
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	CASE
	WHEN (dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico) - (dat.SaldoReservaExpActual + dat.SaldoReservaCubActual) > 0 AND dat.SaldoActual = 0 THEN '139211'
	WHEN (dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico) - (dat.SaldoReservaExpActual + dat.SaldoReservaCubActual) > 0 AND dat.SaldoActual > 0 THEN '139212'
	ELSE '139301' END + conf.Concepto,
	ABS((dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico) - (dat.SaldoReservaExpActual + dat.SaldoReservaCubActual))
FROM R04.[0419Datos] dat
INNER JOIN dbo.RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE ((dat.CalifExpHistorico <> dat.CalifExpActual AND dat.CalifCubHistorico IS NULL AND dat.CalifCubActual IS NOT NULL)
	OR (dat.CalifCubHistorico <> dat.CalifCubActual AND dat.CalifExpHistorico IS NULL AND dat.CalifExpActual IS NOT NULL))
	AND ABS((dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico) - (dat.SaldoReservaExpActual + dat.SaldoReservaCubActual)) > 0
	AND dat.Moneda = '0'; -- Moneda Nacional


/**
Caso 3: Cuando en el periodo anterior se tenia una calificacion la cual no existe en este periodo
porque se creo una calificacion diferente.
Ej: Anterior: Exp=A, Cub=NA; Actual: Exp=NA, Cub
**/
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	CASE WHEN (dat.SaldoReservaCubActual + dat.SaldoReservaExpActual) > (dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico) THEN '139301' ELSE '139211' END + conf.Concepto,
	ABS((dat.SaldoReservaCubActual + dat.SaldoReservaExpActual) - (dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico))
FROM R04.[0419Datos] dat
INNER JOIN RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE (dat.CalifCubHistorico IS NULL AND dat.CalifCubActual IS NOT NULL AND dat.CalifExpHistorico IS NOT NULL AND dat.CalifExpActual IS NULL)
	OR (dat.CalifExpHistorico IS NULL AND dat.CalifExpActual IS NOT NULL AND dat.CalifCubHistorico IS NOT NULL AND dat.CalifCubActual IS NULL)
	AND dat.Moneda = '0'; -- Moneda Nacional


-- Dollar, UDIS
/*** Cargo Deslizamiento cambiario ***/
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	CASE
	WHEN(rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) * ra.ValorActual - (rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) * rh.ValorHistorico > 0 THEN '139304'
	ELSE '139213' END + conf.Concepto,
	ABS((rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) * ra.ValorActual - (rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) * rh.ValorHistorico)
FROM R04.[0419Datos] dat 
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL 
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo = dat.Codigo
LEFT OUTER JOIN (
	SELECT Codigo
	FROM R04.[0419Datos]
	WHERE SaldoHistorico = 0 AND SaldoActual > 0 AND CalifCubHistorico IS NULL AND CalifExpHistorico IS NULL
) AS tmp ON dat.Codigo = tmp.Codigo
WHERE (rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) * ra.ValorActual - (rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) * rh.ValorHistorico <> 0
	AND tmp.Codigo IS NULL; -- Creditos Nuevos


-- Cargo Disminuciones de la reserva por calificacion de cartera ara todas las monedas
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	CASE
	WHEN dat.IdTipoBaja IS NULL THEN '139212' --Cancelacion de estimaciones por calificacion de cartera
	ELSE '139211' --Cancelacion de estimaciones por cobro de cartera, adjudicacion o recepcion de bienes como dacion en pago
	END + conf.Concepto,
	((rh.SaldoReservaExpHistorico + rh.SaldoReservaCubHistorico) - (ra.SaldoReservaCubActual + ra.SaldoReservaExpActual)) * ra.ValorActual 
FROM R04.[0419Datos] dat
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
INNER JOIN SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo = dat.Codigo
LEFT OUTER JOIN R04.VW_0419Cargos_CalculoDiferente dif ON dat.Codigo = dif.Codigo
WHERE (dat.SaldoReservaExpHistorico + dat.SaldoReservaCubHistorico) / rh.ValorHistorico > (dat.SaldoReservaCubActual + dat.SaldoReservaExpActual) / ra.ValorActual
	AND dif.Codigo IS NULL;


-- Cargo Calculo de conceptos Movimientos otros cargos
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139290000000',
	(((dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida)/ValorActual) - (dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida) / rh.ValorHistorico) * ra.ValorActual
FROM R04.[0419Datos] dat
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo = dat.Codigo
WHERE (dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico) / rh.ValorHistorico > (dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida ) / ra.ValorActual
	AND conf.TipoMovimiento IS NULL AND dat.Moneda <> '0';


-- Cargo Calculo de diferencias de reserva cuando el credito tiene castigos o bonificaciones y el credito esta en ceros
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139211' + conf.Concepto,
	(cargos.ReservaHistorico / rh.ValorHistorico) - ((cargos.ReservaActual + cargos.CargoMovimiento) / ra.ValorActual) * ra.ValorActual
FROM R04.[0419Datos] dat
INNER JOIN R04.RW_VW_CargosMovimiento cargos ON cargos.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
INNER JOIN SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo=dat.Codigo
WHERE (cargos.ReservaHistorico) / rh.ValorHistorico > (cargos.ReservaActual + cargos.CargoMovimiento) / ra.ValorActual AND dat.SaldoActual = 0
	AND dat.Moneda <> '0';


-- Abono Calculo de diferencias de reserva cuando el credito tiene castigos o bonificaciones y el credito esta en ceros
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139301' + conf.Concepto,
	(((cargos.ReservaActual + cargos.CargoMovimiento) / ra.ValorActual) - (cargos.ReservaHistorico / rh.ValorHistorico)) * ra.ValorActual
FROM R04.[0419Datos] dat
INNER JOIN R04.RW_VW_CargosMovimiento cargos ON cargos.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
INNER JOIN SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo = dat.Codigo
WHERE cargos.ReservaHistorico / rh.ValorHistorico < (cargos.ReservaActual + cargos.CargoMovimiento) / ra.ValorActual
	AND dat.SaldoActual = 0 AND dat.Moneda <> '0';


-- Abono Reservas adicionales
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139302000000',
	(((dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida) / ra.ValorActual) - ((dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico) / rh.ValorHistorico)) * ra.ValorActual
FROM R04.[0419Datos] dat
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
INNER JOIN SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo = dat.Codigo
WHERE (dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico) / rh.ValorHistorico < (dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida) / ra.ValorActual
	AND dat.Moneda <> '0';


-- Aumento de las reservas
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139301' + conf.Concepto,
	((ra.SaldoReservaCubActual + ra.SaldoReservaExpActual) - (rh.SaldoReservaExpHistorico + rh.SaldoReservaCubHistorico)) * ra.ValorActual
FROM R04.[0419Datos] dat
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo = dat.Codigo
LEFT OUTER JOIN R04.VW_0419Abonos_CalculoDiferente dif ON dat.Codigo = dif.Codigo
WHERE (rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) < (ra.SaldoReservaCubActual + ra.SaldoReservaExpActual) AND dat.SaldoHistorico > 0
	AND dat.SaldoActual > 0
	AND ((ra.SaldoReservaCubActual + ra.SaldoReservaExpActual) - (rh.SaldoReservaExpHistorico + rh.SaldoReservaCubHistorico)) * ra.ValorActual <> 0
	AND dif.Codigo IS NULL
	AND dat.Moneda <> '0';


-- Aumento de las reservas cuando en el periodo anterior no tenia saldotodas las monedas
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	'139301' + conf.Concepto,
	(dat.SaldoReservaCubActual + dat.SaldoReservaExpActual)
FROM R04.[0419Datos] dat
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
LEFT OUTER JOIN R04.VW_0419Abonos_CalculoDiferente dif ON dat.Codigo = dif.Codigo
WHERE (dat.CalifCubHistorico IS NOT NULL AND dat.CalifExpHistorico IS NOT NULL)
	AND dat.SaldoHistorico = 0
	AND dat.SaldoActual > 0
	AND dif.Codigo IS NULL


/***
Para los conceptos donde se compara la calificacion historica con la actual se toma
como base 2 calificaciones: calificacion expuesta y cubierta.
Para el caso donde un credito el mes pasado solo presente una reserva (ya sea expuesta o cubierta),
y en el presente mes tenga dos calificaciones. La disminucion del monto de reserva
(mientras mantenga la misma calificación) se registrara como cancelación de estimaciones por cobro de cartera.
Mientras que la reserva creada por la nueva calificación del credito se registrara como estimación de reservas
derivadas de adquisiciones de cartera.
*/
-- Select del cargo por cancelacion de estimacion por cobro EXP
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	CASE WHEN (rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) - (ra.SaldoReservaCubActual + ra.SaldoReservaExpActual) > 0 THEN '139211' ELSE '139301' END + conf.Concepto,
	ABS((rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) - (ra.SaldoReservaCubActual + ra.SaldoReservaExpActual)) * ra.ValorActual
FROM R04.[0419Datos] dat
INNER JOIN dbo.RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo = dat.Codigo
WHERE ((dat.CalifCubHistorico IS NULL AND dat.CalifCubActual IS NOT NULL AND dat.CalifExpHistorico = dat.CalifExpActual)
	OR (dat.CalifExpHistorico IS NULL AND dat.CalifExpActual IS NOT NULL AND dat.CalifCubActual = dat.CalifCubHistorico))
	AND (rh.SaldoReservaExpHistorico + rh.SaldoReservaCubHistorico) - (ra.SaldoReservaExpActual + ra.SaldoReservaCubActual) <> 0
	AND dat.Moneda <> '0';


/**
Caso 2: Cuando la calificacion Expuesta Actual es diferente de la anterior y En este periodo se crea una calificaicon Cubierta
**/
-- Select de cargo Cancelacion de estimacion por calificacion
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	CASE WHEN (rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) - (ra.SaldoReservaExpActual + ra.SaldoReservaCubActual) > 0 THEN '139211' ELSE '139301' END + conf.Concepto,
	ABS((rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) - (ra.SaldoReservaExpActual + ra.SaldoReservaCubActual)) * ra.ValorActual
FROM R04.[0419Datos] dat
INNER JOIN RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo = dat.Codigo
WHERE ((dat.CalifExpHistorico <> dat.CalifExpActual AND dat.CalifCubHistorico IS NULL AND dat.CalifCubActual IS NOT NULL)
	OR (dat.CalifCubHistorico <> dat.CalifCubActual AND dat.CalifExpHistorico IS NULL AND dat.CalifExpActual IS NOT NULL))
	AND (rh.SaldoReservaExpHistorico + rh.SaldoReservaCubHistorico) - (ra.SaldoReservaExpActual + ra.SaldoReservaCubActual) <> 0
	AND dat.Moneda <> '0';


/**
Caso 3: Cuando en el periodo anterior se tenia una calificacion la cual no existe en este periodo porque se creo una calificacion diferente.
Ej: Anterior: Exp=A, Cub=NA; Actual: Exp=NA, Cub
**/
INSERT INTO R04.[0419Conceptos] (Codigo, Concepto, Monto)
SELECT
	dat.Codigo,
	CASE WHEN (ra.SaldoReservaCubActual + ra.SaldoReservaExpActual) - (rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico) > 0 THEN '139301' ELSE '139211' END + conf.Concepto,
	ABS((ra.SaldoReservaCubActual + ra.SaldoReservaExpActual) - (rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico)) * dat.SaldoActual 
FROM R04.[0419Datos] dat
INNER JOIN dbo.RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo = dat.Codigo
WHERE ((dat.CalifCubHistorico IS NULL AND dat.CalifCubActual IS NOT NULL AND dat.CalifExpHistorico IS NOT NULL AND dat.CalifExpActual IS NULL)
	OR (dat.CalifExpHistorico IS NULL AND dat.CalifExpActual IS NOT NULL AND dat.CalifCubHistorico IS NOT NULL AND dat.CalifCubActual IS NULL))
	AND (ra.SaldoReservaCubActual + ra.SaldoReservaExpActual) - (rh.SaldoReservaExpHistorico + rh.SaldoReservaCubHistorico) <> 0
	AND dat.Moneda <> '0';
GO

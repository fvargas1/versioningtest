SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_023_Count]
 @IdInconsistencia BIGINT
AS
BEGIN
-- El monto del pago exigible deberá ser mayor a 0.
DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;
DECLARE @FechaPeriodo VARCHAR(6);

SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT @count = COUNT(rep.CodigoCredito)
FROM dbo.RW_R04C0443 rep
INNER JOIN dbo.SICCMX_Credito cre ON rep.NumeroDisposicion = cre.Codigo AND SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(cre.FechaProximaAmortizacion,0),102),'.',''),1,6) = @FechaPeriodo
WHERE CAST(ISNULL(NULLIF(rep.MontoExigible,''),'0') AS DECIMAL(23,2)) <= 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;
END
GO

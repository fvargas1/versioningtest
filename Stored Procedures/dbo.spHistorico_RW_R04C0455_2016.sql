SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0455_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0455_2016);

DELETE FROM Historico.RW_R04C0455_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0455_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoPersona, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC, HITSIC,
	FechaConsultaSIC, FechaInfoFinanc, MesesPI100, ID_PI100, GarantiaLeyFederal, CumpleCritContGral, P_DiasMoraInstBanc, P_PorcPagoInstBanc, P_PorcPagoInstNoBanc,
	P_NumInstCalif, P_DeudaTotalPartEleg, P_ServDeudaIngAjust, P_DeudaCortoPlazoTotal, P_IngTotGastoCorriente, P_InvIngTotales, P_IngPropIngTotales, SdoDeudaTotal,
	SdoPartEleg, SdoIngTotalesAjust, SdoDeudaCortoPlazo, SdoIngTotales, SdoGastosCorrientes, SdoInversion, SdoIngPropios, DiasMoraInstBanc, PorcPagoInstBanc,
	PorcPagoInstNoBanc, NumInstCalif, P_TasaDesempLocal, P_ServFinEntReg, P_ObligConting, P_BalanceOpPIB, P_NivEficRecauda, P_SolFlexEjecPres, P_SolFlexImpLoc,
	P_TranspFinPublicas, P_EmisionDeuda
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoPersona,
	[PI],
	PuntajeTotal,
	PuntajeCuantitativo,
	PuntajeCualitativo,
	CreditoReportadoSIC,
	HITSIC,
	FechaConsultaSIC,
	FechaInfoFinanc,
	MesesPI100,
	ID_PI100,
	GarantiaLeyFederal,
	CumpleCritContGral,
	P_DiasMoraInstBanc,
	P_PorcPagoInstBanc,
	P_PorcPagoInstNoBanc,
	P_NumInstCalif,
	P_DeudaTotalPartEleg,
	P_ServDeudaIngAjust,
	P_DeudaCortoPlazoTotal,
	P_IngTotGastoCorriente,
	P_InvIngTotales,
	P_IngPropIngTotales,
	SdoDeudaTotal,
	SdoPartEleg,
	SdoIngTotalesAjust,
	SdoDeudaCortoPlazo,
	SdoIngTotales,
	SdoGastosCorrientes,
	SdoInversion,
	SdoIngPropios,
	DiasMoraInstBanc,
	PorcPagoInstBanc,
	PorcPagoInstNoBanc,
	NumInstCalif,
	P_TasaDesempLocal,
	P_ServFinEntReg,
	P_ObligConting,
	P_BalanceOpPIB,
	P_NivEficRecauda,
	P_SolFlexEjecPres,
	P_SolFlexImpLoc,
	P_TranspFinPublicas,
	P_EmisionDeuda
FROM dbo.RW_R04C0455_2016
WHERE IdReporteLog = @IdReporteLog;
GO

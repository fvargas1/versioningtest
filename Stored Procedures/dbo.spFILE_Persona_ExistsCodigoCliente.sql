SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_ExistsCodigoCliente]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_ExistsCodigoCliente';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Persona
SET errorFormato = 1
WHERE LEN(ISNULL(CodigoCliente, '')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	CodigoCliente,
	'CodigoCliente',
	CodigoCliente,
	1,
	@Detalle
FROM dbo.FILE_Persona
WHERE LEN(ISNULL(CodigoCliente, '')) = 0;
GO

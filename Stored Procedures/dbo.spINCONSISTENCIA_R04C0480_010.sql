SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_010]
AS

BEGIN

-- Si el Número de Instituciones Reportadas en los últimos 12 meses (dat_num_instit_reportadas) es >= 4 y < 12,
-- entonces el Puntaje Asignado por Número de Instituciones Reportadas en los últimos 12 meses
-- (cve_ptaje_num_instit_report) debe ser = 57

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	NumInstRep AS NumeroInstReportadas,
	P_NumInstRep AS Puntos_NumeroInstReportadas
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(NumInstRep AS DECIMAL(10,6)) >= 4 AND CAST(NumInstRep AS DECIMAL(10,6)) < 12 AND ISNULL(P_NumInstRep,'') <> '57';

END


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_117]
AS

BEGIN

-- Si la Tasa de Referencia (cve_tasa) es igual a 100, 110, 120, 140, 410, 610, 620 ó 625, entonce la Moneda (cve_moneda) debe ser igual a pesos (0).

SELECT
	CodigoCredito,
	TipoAltaCredito,
	TasaInteres AS TasaInteres,
	Moneda
FROM dbo.RW_VW_R04C0473_INC
WHERE ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','136','137','138','139','700','701')
	AND ISNULL(TasaInteres,'') IN ('100','110','120','140','410','610','620','625') AND ISNULL(Moneda,'') <> '0'

END


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_InsumoRC15_Get]
	@IdReporteLog BIGINT
AS
SELECT
	CodigoCredito,
	CodigoPersona,
	RFC,
	MontoGarantiasReales,
	MontoReservas,
	MontoAvales,
	FechaDisposicion,
	SituacionCredito,
	Exposicion,
	ImporteRC3,
	ClienteRelacionado
FROM dbo.RW_InsumoRC15;
GO

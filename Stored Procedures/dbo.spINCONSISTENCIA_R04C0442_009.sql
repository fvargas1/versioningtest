SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_009]
AS

BEGIN

-- El Nombre del Grupo de Riesgo no se puede omitir.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, GrupoRiesgo
FROM dbo.RW_R04C0442
WHERE LEN(ISNULL(GrupoRiesgo,'')) = 0;

END
GO

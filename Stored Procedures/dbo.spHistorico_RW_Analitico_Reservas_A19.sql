SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Analitico_Reservas_A19]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Analitico_Reservas_A19);

DELETE FROM Historico.RW_Analitico_Reservas_A19 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Analitico_Reservas_A19 (
	IdPeriodoHistorico, Fecha, CodigoPersona, NombrePersona, CodigoProyecto, NombreProyecto, CodigoCredito,
	SaldoCredito, FechaVecimiento, Moneda, MontoGarantia, MontoReserva, PrctReserva, Calificacion
)
SELECT
	@IdPeriodoHistorico,
	Fecha,
	CodigoPersona,
	NombrePersona,
	CodigoProyecto,
	NombreProyecto,
	CodigoCredito,
	SaldoCredito,
	FechaVecimiento,
	Moneda,
	MontoGarantia,
	MontoReserva,
	PrctReserva,
	Calificacion
FROM dbo.RW_Analitico_Reservas_A19
WHERE IdReporteLog = @IdReporteLog;
GO

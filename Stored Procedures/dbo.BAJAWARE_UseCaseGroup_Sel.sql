SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCaseGroup_Sel]
	@IdUseCaseGroup BAJAWARE_utID
AS
SELECT
IdUseCaseGroup, Active, Name, IdApplication, SortOrder, StringKey, Description
FROM dbo.BAJAWARE_UseCaseGroup
WHERE IdUseCaseGroup = @IdUseCaseGroup;
GO

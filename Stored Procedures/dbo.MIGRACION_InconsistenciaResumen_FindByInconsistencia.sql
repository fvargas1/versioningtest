SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_InconsistenciaResumen_FindByInconsistencia]
	@IdInconsistencia BIGINT
AS
SELECT
	vw.IdInconsistenciaResumen,
	vw.IdInconsistencia,
	vw.InconsistenciaNombre,
	vw.Numero,
	vw.Fecha
FROM dbo.MIGRACION_VW_InconsistenciaResumen vw
WHERE vw.IdInconsistencia = @IdInconsistencia;
GO

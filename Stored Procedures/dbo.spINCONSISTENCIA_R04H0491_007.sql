SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_007]
AS

BEGIN

-- El "IDENTIFICADOR DEL CRÉDITO METODOLOGÍA CNBV" de la posición 8 a la posición 24 debe ser el igual al Numero de Avaluo (columna 23).

SELECT CodigoCredito, CodigoCreditoCNBV, NumeroAvaluo
FROM dbo.RW_R04H0491
WHERE SUBSTRING(CodigoCreditoCNBV,8,17) <> NumeroAvaluo;

END
GO

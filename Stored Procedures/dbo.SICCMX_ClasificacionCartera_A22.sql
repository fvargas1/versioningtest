SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ClasificacionCartera_A22]
AS
DECLARE @IdMetodologia INT;

-- Actualizamos en SICCMX_Credito para Metodologia 22
SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo=22;

UPDATE credito
SET IdMetodologia = @IdMetodologia
FROM dbo.SICCMX_Credito credito
INNER JOIN dbo.SICCMX_Persona persona ON credito.IdPersona = persona.IdPersona
INNER JOIN dbo.SICCMX_Anexo22 anx ON persona.IdPersona = anx.IdPersona
WHERE credito.IdMetodologia IS NULL;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_047]
AS

BEGIN

-- La "PÉRDIDA ESPERADA (Metodología Interna)" debe ser mayor o igual a cero y menor o igual a 100.

SELECT CodigoCredito, CodigoCreditoCNBV, PerdidaEsperadaInterna
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(PerdidaEsperadaInterna,''),'-1') AS DECIMAL) < 0;

END
GO

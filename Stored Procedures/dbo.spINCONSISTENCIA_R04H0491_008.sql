SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_008]
AS

BEGIN

-- Para créditos reestructurados se validará que el "IDENTIFICADOR DEL CRÉDITO METODOLOGÍA CNBV"
-- se haya dado de alta previamente a su registro como crédito reestructurado.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.TipoAlta
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICCMX_VW_Datos_Altas_SerieH vw ON r.CodigoCreditoCNBV = vw.CodigoCreditoCNBV AND vw.TipoRegistro = 'H'
WHERE r.TipoAlta='3' AND vw.CodigoCreditoCNBV IS NULL;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Califica_Persona_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Califica_Persona_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Califica f
LEFT OUTER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoDeudor) = per.Codigo
WHERE per.Codigo IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Califica_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoDeudor,
 'CodigoDeudor',
 f.CodigoDeudor,
 2,
 @Detalle
FROM dbo.FILE_Califica f
LEFT OUTER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoDeudor) = per.Codigo
WHERE per.Codigo IS NULL;
GO

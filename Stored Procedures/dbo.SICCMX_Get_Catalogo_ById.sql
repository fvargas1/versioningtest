SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Get_Catalogo_ById]
	@IdCatalogo INT,
	@filtro VARCHAR(50)
AS
DECLARE @SP VARCHAR(100);

SELECT @SP = SPGet FROM dbo.SICC_Catalogo WHERE IdCatalogo = @IdCatalogo;

SELECT Nombre FROM dbo.SICC_Catalogo WHERE IdCatalogo = @IdCatalogo;
EXEC ('EXEC ' + @SP + ' ''' + @filtro + '''');
GO

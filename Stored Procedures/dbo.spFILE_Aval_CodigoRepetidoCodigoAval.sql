SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Aval_CodigoRepetidoCodigoAval]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Aval_CodigoRepetidoCodigoAval';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Aval f
WHERE f.CodigoAval IN (
 SELECT f.CodigoAval
 FROM dbo.FILE_Aval f
 GROUP BY f.CodigoAval
 HAVING COUNT(f.CodigoAval)>1
) AND LEN(f.CodigoAval) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Aval_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoAval,
 'CodigoAval',
 f.CodigoAval,
 1,
 @Detalle
FROM dbo.FILE_Aval f
WHERE f.CodigoAval IN (
 SELECT f.CodigoAval
 FROM dbo.FILE_Aval f
 GROUP BY f.CodigoAval
 HAVING COUNT(f.CodigoAval)>1
) AND LEN(f.CodigoAval) > 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_006]
AS
BEGIN
-- Un mismo RFC (col. 4) no puede tener más de un Nombre de Garante (Col. 5) diferente.

SELECT
 r50.CodigoCredito,
 REPLACE(r50.NombreAcreditado, ',', '') AS NombreDeudor,
 r50.RFC_Garante,
 r50.NombreGarante
FROM dbo.RW_VW_R04C0450_INC r50
INNER JOIN (
 SELECT rep.NombreGarante
 FROM dbo.RW_VW_R04C0450_INC rep
 INNER JOIN dbo.RW_VW_R04C0450_INC rep2 ON rep.RFC_Garante = rep2.RFC_Garante AND rep.NombreGarante <> rep2.NombreGarante
 GROUP BY rep.NombreGarante, rep.RFC_Garante
) AS repetidos ON r50.NombreGarante = repetidos.NombreGarante AND LEN(r50.NombreGarante) > 0;

END
GO

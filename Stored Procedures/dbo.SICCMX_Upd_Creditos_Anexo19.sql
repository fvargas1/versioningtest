SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Upd_Creditos_Anexo19]
AS
DECLARE @IdCal INT;
SELECT @IdCal = IdCalificacion FROM dbo.SICC_CalificacionNvaMet WHERE Codigo='A1';

UPDATE crv
SET ReservaFinal = cre.MontoValorizado * pc.PorcentajeFinal,
 ReservaExpuesta = cre.MontoValorizado * pc.PorcentajeFinal,
 ReservaCubierta = 0,
 ReservaExpuestaOriginal = cre.MontoValorizado * pc.PorcentajeFinal,
 ReservaCubiertaOriginal = 0,
 ReservaExpuesta_GarPer = cre.MontoValorizado * pc.PorcentajeFinal,
 ReservaCubierta_GarPer = 0,
 PorReservaFinal = pc.PorcentajeFinal,
 PorExpuesta = pc.PorcentajeFinal,
 PorCubierto = 0,
 CalifFinal = pc.IdCalificacionFinal,
 CalifExpuesta = pc.IdCalificacionFinal,
 CalifCubierta = @IdCal,
 EI_Expuesta = crv.EI_Total,
 EI_Expuesta_GarPer = crv.EI_Total,
 EI_Cubierta = 0,
 EI_Cubierta_GarPer = 0
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON crv.IdCredito = cre.IdCredito
INNER JOIN (
 SELECT DISTINCT ln.IdProyecto, ln.IdLineaCredito
 FROM (
 SELECT pry.IdProyecto, pry.IdLineaCredito
 FROM dbo.SICCMX_Proyecto pry
 WHERE pry.IdLineaCredito IS NOT NULL
 UNION
 SELECT IdProyecto, IdLineaCredito
 FROM dbo.SICCMX_ProyectoLinea
 ) AS ln
) AS lnpr ON cre.IdLineaCredito = lnpr.IdLineaCredito
INNER JOIN dbo.SICCMX_ProyectoCalificacion pc ON lnpr.IdProyecto = pc.IdProyecto
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
WHERE met.Codigo='4';
GO

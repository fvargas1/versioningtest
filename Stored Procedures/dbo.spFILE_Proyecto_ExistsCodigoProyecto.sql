SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Proyecto_ExistsCodigoProyecto]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Proyecto_ExistsCodigoProyecto';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Proyecto
SET errorFormato = 1
WHERE LEN(ISNULL(CodigoProyecto, '')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Proyecto_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoProyecto,
 'CodigoProyecto',
 CodigoProyecto,
 1,
 @Detalle
FROM dbo.FILE_Proyecto
WHERE LEN(ISNULL(CodigoProyecto, '')) = 0;
GO

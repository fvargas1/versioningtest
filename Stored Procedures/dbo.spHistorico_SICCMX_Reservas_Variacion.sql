SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Reservas_Variacion]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Reservas_Variacion WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Reservas_Variacion (
	IdPeriodoHistorico,
	Credito,
	ReservaCalConsDesc,
	ReservaAdicConsDesc,
	MesesSP100,
	CreditoRes5Prct,
	CreditoSustPI,
	ReservaPeriodoAnt,
	ReservaAdicPeriodoAnt
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	cre.Codigo,
	crv.ReservaCalConsDesc,
	crv.ReservaAdicConsDesc,
	crv.MesesSP100,
	crv.CreditoRes5Prct,
	crv.CreditoSustPI,
	crv.ReservaPeriodoAnt,
	crv.ReservaAdicPeriodoAnt
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Reservas_Variacion crv ON cre.IdCredito = crv.IdCredito;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_055]
AS
BEGIN
-- Si el Tipo de Cartera (cve_tipo_cartera) es = 310, entonces el (dat_rfc) debe iniciar con guion bajo.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoCartera,
	RFC
FROM dbo.RW_VW_R04C0463_INC
WHERE ISNULL(TipoCartera,'') = '310' AND SUBSTRING(ISNULL(RFC,''),1,1) <> '_';

END

GO

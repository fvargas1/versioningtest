SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCaseGroup_Upd]
	@IdUseCaseGroup BAJAWARE_utID,
	@Active BAJAWARE_utActivo,
	@Name BAJAWARE_utDescripcion100,
	@IdApplication BAJAWARE_utID,
	@SortOrder BAJAWARE_utInt,
	@StringKey BAJAWARE_utDescripcion100,
	@Description BAJAWARE_utDescripcion100
AS
UPDATE dbo.BAJAWARE_UseCaseGroup
SET
	Active = @Active,
	Name = @Name,
	IdApplication = @IdApplication,
	SortOrder = @SortOrder,
	StringKey = @StringKey,
	Description = @Description
WHERE IdUseCaseGroup = @IdUseCaseGroup;
GO

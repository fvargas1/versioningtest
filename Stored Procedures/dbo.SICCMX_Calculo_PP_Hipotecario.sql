SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_PP_Hipotecario]
AS
-- SE CALCULAN LAS RESERVAS PARA GARANTIAS DE PRIMERAS PERDIDAS
UPDATE dbo.SICCMX_Garantia_Hipotecario_PP
SET ReservaCubierto = MontoCubierto * PIGarante * SPGarante,
 ReservaExpuesto = MontoExpuesto
FROM dbo.SICCMX_Garantia_Hipotecario_PP cg
INNER JOIN (
 SELECT cgPP.IdGarantia, SUM(resVar.Reserva) AS Reserva
 FROM dbo.SICCMX_HipotecarioGarantia_PP cgPP
 INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables resVar ON cgPP.IdHipotecario = resVar.IdHipotecario
 GROUP BY cgPP.IdGarantia
) AS tcg ON cg.IdGarantia = tcg.IdGarantia;


-- SE CALCULAN LAS RESERVA FINAL PARA GARANTIAS DE PRIMERAS PERDIDAS
UPDATE dbo.SICCMX_Garantia_Hipotecario_PP SET ReservaFinal = ReservaCubierto + ReservaExpuesto;


-- SE ACTUALIZAN LAS RESERVAS POR CREDITO
UPDATE cgPP
SET Reserva = CASE WHEN ISNULL(gPP.ReservaFinal,0)=0 THEN 0 ELSE cgPP.PrctReserva * gPP.ReservaFinal END
FROM dbo.SICCMX_HipotecarioGarantia_PP cgPP
INNER JOIN dbo.SICCMX_Garantia_Hipotecario_PP gPP ON cgPP.IdGarantia = gPP.IdGarantia;


-- SE ACTUALIZA EN RESERVAS CALCULADAS POR CREDITO
UPDATE rv
SET Reserva = cgPP.Reserva,
 PorReserva = CASE WHEN ISNULL(rv.E,0)=0 THEN 0 ELSE dbo.NoN(cgPP.Reserva, rv.E) END,
 ReservaCubPP = cgPP.Reserva,
 PorCubiertoPP = gPP.PrctCubierto
FROM dbo.SICCMX_Hipotecario_Reservas_Variables rv
INNER JOIN dbo.SICCMX_HipotecarioGarantia_PP cgPP ON rv.IdHipotecario = cgPP.IdHipotecario
INNER JOIN dbo.SICCMX_Garantia_Hipotecario_PP gPP ON cgPP.IdGarantia = gPP.IdGarantia;


-- SE ACTUALIZA LA CALIFICACION
UPDATE hrv
SET IdCalificacion = cal.IdCalificacion
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hrv.IdHipotecario = pre.IdHipotecario
INNER JOIN dbo.SICC_CalificacionHipotecario2011 cal ON pre.IdMetodologia = cal.IdMetodologia AND hrv.PorReserva BETWEEN cal.RangoMenor AND cal.RangoMayor;


-- MONTO CUBIERTO PRIMERAS PERDIDAS
UPDATE rv
SET ECubiertaPP = PorCubiertoPP * rv.ReservaOriginal
FROM dbo.SICCMX_Hipotecario_Reservas_Variables rv
INNER JOIN dbo.SICCMX_HipotecarioGarantia_PP cgPP ON rv.IdHipotecario = cgPP.IdHipotecario
INNER JOIN dbo.SICCMX_Garantia_Hipotecario_PP garPP ON cgPP.IdGarantia = garPP.IdGarantia;


-- ACTUALIZA EL PORCENTAJE TOTAL CUBIERTO
UPDATE g
SET PrctCubierto = dbo.NoN(g.MontoCubierto, g.MontoCubierto + g.MontoExpuesto)
FROM dbo.SICCMX_Garantia_Hipotecario_PP g;


-- ACTUALIZA EL PORCENTAJE CORRESPONDIENTE POR CREDITO
UPDATE cg
SET ReservaCubierta = g.ReservaCubierto * cg.PrctReserva,
 ReservaExpuesta = g.ReservaExpuesto * cg.PrctReserva
FROM dbo.SICCMX_HipotecarioGarantia_PP cg
INNER JOIN dbo.SICCMX_Garantia_Hipotecario_PP g ON cg.IdGarantia = g.IdGarantia;


UPDATE rv
SET ReservaExpuesto = cgPP.ReservaExpuesta,
 PorcentajeExpuesto = CASE WHEN ISNULL(hrv.E,0)=0 THEN 0 ELSE dbo.NoN(cgPP.ReservaExpuesta, hrv.E) END,
 ReservaCubierto = cgPP.ReservaCubierta,
 PorcentajeCubierto = CASE WHEN ISNULL(hrv.E,0)=0 THEN 0 ELSE dbo.NoN(cgPP.ReservaCubierta, hrv.E) END
FROM dbo.SICCMX_HipotecarioReservas rv
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON rv.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioGarantia_PP cgPP ON hrv.IdHipotecario = cgPP.IdHipotecario;

UPDATE res
SET IdCalificacionCubierto = calCub.IdCalificacion,
	IdCalificacionExpuesto = calExp.IdCalificacion
FROM dbo.SICCMX_HipotecarioReservas res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON res.IdHipotecario = pre.IdHipotecario
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calCub ON pre.IdMetodologia = calCub.IdMetodologia AND res.PorcentajeCubierto BETWEEN calCub.RangoMenor AND calCub.RangoMayor
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calExp ON pre.IdMetodologia = calExp.IdMetodologia AND res.PorcentajeCubierto BETWEEN calExp.RangoMenor AND calExp.RangoMayor;
GO

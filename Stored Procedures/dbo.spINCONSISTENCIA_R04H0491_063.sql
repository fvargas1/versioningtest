SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_063]
AS

BEGIN

-- La "ENTIDAD QUE OTORGA EL SEGURO DE CRÉDITO A LA VIVIENDA" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.EntidadSeguro
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_EntidadAseguradora cat ON ISNULL(r.EntidadSeguro,'') = cat.CodigoCNBV
WHERE cat.IdEntidadAseguradora IS NULL;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0455_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0455_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0455_2016;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0455_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC, HITSIC,
 FechaConsultaSIC, FechaInfoFinanc, MesesPI100, ID_PI100, GarantiaLeyFederal, CumpleCritContGral, P_DiasMoraInstBanc, P_PorcPagoInstBanc, P_PorcPagoInstNoBanc,
 P_NumInstCalif, P_DeudaTotalPartEleg, P_ServDeudaIngAjust, P_DeudaCortoPlazoTotal, P_IngTotGastoCorriente, P_InvIngTotales, P_IngPropIngTotales, SdoDeudaTotal,
 SdoPartEleg, SdoIngTotalesAjust, SdoDeudaCortoPlazo, SdoIngTotales, SdoGastosCorrientes, SdoInversion, SdoIngPropios, DiasMoraInstBanc, PorcPagoInstBanc,
 PorcPagoInstNoBanc, NumInstCalif, P_TasaDesempLocal, P_ServFinEntReg, P_ObligConting, P_BalanceOpPIB, P_NivEficRecauda, P_SolFlexEjecPres, P_SolFlexImpLoc,
 P_TranspFinPublicas, P_EmisionDeuda
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '455',
 per.Codigo AS CodigoPersona,
 ppi.[PI] * 100 AS [PI],
 ppi.FactorTotal AS PuntajeTotal,
 ppi.FactorCuantitativo AS PuntajeCuantitativo,
 ppi.FactorCualitativo AS PuntajeCualitativo,
 crs.CredRepSICCNBV AS CreditoReportadoSIC,
 htc.CodigoCNBV AS HITenSIC,
 CASE WHEN anx.FechaInfoBuro IS NULL THEN '0' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoBuro,102),'.',''),1,6) END AS FechaConsultaSIC,
 CASE WHEN anx.FechaInfoFinanc IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoFinanc,102),'.',''),1,6) END AS FechaInfoFinanciera,
 ppi.MesesPI100 AS MesesPI100,
 pi100.Codigo AS ID_PI100,
 CASE WHEN garLey.IdPersona IS NULL THEN 790 ELSE 770 END AS GarantiaLeyFederal,
 crs.CumpleCritCNBV AS CumpleCritContGral,
 ptNMC.[EM_DIAS_MORA] AS P_DiasMoraInstBanc,
 ptNMC.[EM_POR_PAGOS_BANCARIOS] AS P_PorcPagoInstBanc,
 ptNMC.[EM_POR_PAGOS_NOBANCARIOS] AS P_PorcPagoInstNoBanc,
 ptNMC.[EM_NUM_INSTITUCIONES] AS P_NumInstCalif,
 ptNMC.[EM_DEUDA_TOTAL] AS P_DeudaTotalPartEleg,
 ptNMC.[EM_SERVICIO_DEUDA] AS P_ServDeudaIngAjust,
 ptNMC.[EM_DEUDA_CORTOPLAZO] AS P_DeudaCortoPlazoTotal,
 ptNMC.[EM_INGRESOS_TOTALES] AS P_IngTotGastoCorriente,
 ptNMC.[EM_INVERSION] AS P_InvIngTotales,
 ptNMC.[EM_INGRESOS_AUTONOMOS] AS P_IngPropIngTotales,
 anx.SaldoDeudaTotal AS SdoDeudaTotal,
 anx.SaldoPartEle AS SdoPartEleg,
 anx.SaldoIngTotAju AS SdoIngTotalesAjust,
 anx.SaldoDeuCortPlzo AS SdoDeudaCortoPlazo,
 anx.SaldoIngTot AS SdoIngTotales,
 anx.SaldoGastoCorr AS SdoGastosCorrientes,
 anx.SaldoInversion AS SdoInversion,
 anx.SaldoIngPropios AS SdoIngPropios,
 vwVal.EM_DIAS_MORA AS DiasMoraInstBanc,
 vwVal.EM_POR_PAGOS_BANCARIOS AS PorcPagoInstBanc,
 vwVal.EM_POR_PAGOS_NOBANCARIOS AS PorcPagoInstNoBanc,
 vwVal.EM_NUM_INSTITUCIONES AS NumInstCalif,
 ptNMC.[EM_TASA_DESEMPLEO] AS P_TasaDesempLocal,
 ptNMC.[EM_PRESENCIA_SERVICIOS] AS P_ServFinEntReg,
 ptNMC.[EM_OBLIGACIONES_CON] AS P_ObligConting,
 ptNMC.[EM_BALANCE_OPERATIVO] AS P_BalanceOpPIB,
 ptNMC.[EM_NIVEL_RECAUDACION] AS P_NivEficRecauda,
 ptNMC.[EM_SOLIDEZ_FLEXIBILIDAD_EJE] AS P_SolFlexEjecPres,
 ptNMC.[EM_SOLIDEZ_FLEXIBILIDAD_IMP] AS P_SolFlexImpLoc,
 ptNMC.[EM_TRANSPARENCIA_FINANZAS] AS P_TranspFinPublicas,
 ptNMC.[EM_EMISION_DEUDA] AS P_EmisionDeuda
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Anexo18 anx ON pInfo.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON anx.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_VW_PersonasPuntaje_A18 ptNMC ON ppi.IdPersona = ptNMC.IdPersona
INNER JOIN dbo.SICCMX_VW_PersonasValor_A18 vwVal ON ppi.IdPersona = vwVal.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_Persona_CredRepSIC crs ON per.IdPersona = crs.IdPersona
LEFT OUTER JOIN dbo.SICC_HitSic htc ON pInfo.HITSIC = htc.IdHitSic
LEFT OUTER JOIN dbo.SICCMX_VW_Personas_GarantiaLey garLey ON per.IdPersona = garLey.IdPersona
LEFT OUTER JOIN dbo.SICC_PI100 pi100 ON per.PI100 = pi100.IdPI100;

EXEC dbo.RW_R04C0455_2016_Fact_Generate @IdReporteLog, @IdPeriodo, @Entidad; -- FACTORADO
EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0455_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO

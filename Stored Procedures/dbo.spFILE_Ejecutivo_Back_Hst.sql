SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Ejecutivo_Back_Hst]
AS

INSERT INTO dbo.FILE_Ejecutivo_Hst (
	CodigoEjecutivo,
	Nombre,
	Telefono,
	Correo
)
SELECT
	eje.CodigoEjecutivo,
	eje.Nombre,
	eje.Telefono,
	eje.Correo
FROM dbo.FILE_Ejecutivo eje
LEFT OUTER JOIN dbo.FILE_Ejecutivo_Hst hst ON LTRIM(eje.CodigoEjecutivo) = LTRIM(hst.CodigoEjecutivo)
WHERE hst.CodigoEjecutivo IS NULL;
GO

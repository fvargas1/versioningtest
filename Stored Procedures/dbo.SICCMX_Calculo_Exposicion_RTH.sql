SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_Exposicion_RTH]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaHipotecario FROM dbo.SICCMX_Hipotecario_Metodologia WHERE Codigo = '2';


UPDATE res
SET MontoExpuesto = CASE WHEN hrv.E - ISNULL(pre.SUBCVi,0) < 0 THEN 0 ELSE hrv.E - ISNULL(pre.SUBCVi,0) END,
 MontoCubierto = CASE WHEN ISNULL(pre.SUBCVi,0) > hrv.E THEN hrv.E ELSE ISNULL(pre.SUBCVi,0) END
FROM dbo.SICCMX_HipotecarioReservas res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON res.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hrv.IdHipotecario = pre.IdHipotecario
WHERE pre.IdMetodologia = @IdMetodologia;
GO

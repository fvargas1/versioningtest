SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_PM]
AS
-- CALCULAMOS SP PARA GARANTIAS DE PASO Y MEDIDA
UPDATE can
SET
	SeveridadCorresp = .45
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_CreditoGarantia cg ON cre.IdCredito = cg.IdCredito
INNER JOIN dbo.SICCMX_VW_Garantia gar ON cg.IdGarantia = gar.IdGarantia AND can.IdTipoGarantia = gar.IdTipoGarantia
WHERE gar.CodigoTipoGarantia = 'NMC-05';
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Consumo_Revolvente_IIA_Get]
 @IdPeriodoHistorico BIGINT
AS
SELECT
	Producto,
	FolioCredito,
	LimCredito,
	SaldoTot,
	PagoNGI,
	SaldoRev,
	InteresRev,
	SaldoPMSI,
	Meses,
	PagoMin,
	TasaRev,
	SaldoPCI,
	InteresPCI,
	SaldoPagar,
	PagoReal,
	LimCreditoA,
	PagoExige,
	ImpagosC,
	ImpagoSum,
	MesApert,
	SaldoCont,
	Situacion,
	ProbInc,
	SevPer,
	ExpInc,
	MReserv,
	Relacion,
	ClasCont,
	CveCons,
	limcreditocalif,
	montopagarinst,
	montopagarsic,
	mesantig,
	mesesic,
	segmento,
	gveces,
	garantia,
	Catcuenta,
	indicadorcat,
	FolioCliente,
	CP,
	comtotal,
	comtardio,
	pagonginicio,
	pagoexigepsi
FROM Historico.RW_Consumo_Revolvente_IIA
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Producto, FolioCredito;
GO

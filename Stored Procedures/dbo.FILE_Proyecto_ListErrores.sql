SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Proyecto_ListErrores]
AS
SELECT
 CodigoCliente,
 CodigoProyecto,
 NombreDelProyecto,
 DescripcionDelProyecto,
 NumeroLinea,
 Sobrecosto,
 MontoCubiertoPorTerceros,
 MesesContemplados,
 MesesAdicionales,
 VPTotal,
 UtilidadPerdidaAcumulada,
 Etapa,
 FechaInicioOper,
 TasaDescuentoVP,
 VentNetTotAnuales,
 Fuente
FROM dbo.FILE_Proyecto
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_CartasCredito]
AS
UPDATE crv
SET CalifFinal = cal.IdCalificacion,
	CalifCubierta = cal.IdCalificacion,
	CalifExpuesta = cal.IdCalificacion,
	IdCalificacionAdicional = cal.IdCalificacion
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Credito cre ON crv.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo inf ON cre.IdCredito = inf.IdCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON cal.Codigo = 'NA'
LEFT OUTER JOIN dbo.SICC_Posicion pos ON inf.Posicion = pos.IdPosicion
LEFT OUTER JOIN dbo.SICC_TipoLinea tl ON inf.TipoLinea = tl.IdTipoLinea
WHERE pos.Codigo = '181' OR tl.Codigo = '181';
GO

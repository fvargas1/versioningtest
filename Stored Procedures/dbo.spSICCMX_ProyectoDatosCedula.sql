SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_ProyectoDatosCedula]
	@IdProyecto BIGINT
AS
SELECT
	IdProyecto,
	Nombre,
	Descripcion
FROM dbo.SICCMX_Proyecto
WHERE IdProyecto = @IdProyecto;
GO

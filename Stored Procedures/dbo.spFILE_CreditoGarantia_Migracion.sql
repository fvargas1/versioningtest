SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_CreditoGarantia_Migracion]
AS

UPDATE dbo.SICCMX_CreditoGarantia
SET PorcCubiertoCredito = NULLIF(f.Porcentaje,'')
FROM dbo.FILE_CreditoGarantia f
INNER JOIN dbo.SICCMX_Credito c ON f.CodigoCredito = c.Codigo
INNER JOIN dbo.SICCMX_Garantia g ON f.CodigoGarantia = g.Codigo
INNER JOIN dbo.SICCMX_CreditoGarantia cg ON cg.IdCredito = c.IdCredito AND cg.IdGarantia = g.IdGarantia
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_CreditoGarantia (
	IdCredito,
	IdGarantia,
	PorcCubiertoCredito
)
SELECT
	(SELECT IdCredito FROM dbo.SICCMX_Credito WHERE Codigo = LTRIM(f.CodigoCredito)),
	(SELECT IdGarantia FROM dbo.SICCMX_Garantia WHERE Codigo = LTRIM(f.CodigoGarantia)),
	NULLIF(f.Porcentaje,'')
FROM dbo.FILE_CreditoGarantia f
WHERE f.CodigoGarantia+'-'+f.CodigoCredito
NOT IN (
	SELECT g.Codigo+'-'+cred.Codigo
	FROM dbo.SICCMX_CreditoGarantia cg
	INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia
	INNER JOIN dbo.SICCMX_Credito cred on cred.IdCredito=cg.IdCredito
) AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO

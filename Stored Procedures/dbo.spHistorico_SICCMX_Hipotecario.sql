SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Hipotecario]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Hipotecario WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Hipotecario (
 IdPeriodoHistorico,
 Hipotecario,
 Persona,
 SaldoCapitalVigente,
 InteresVigente,
 SaldoCapitalVencido,
 InteresVencido,
 MontoGarantia,
 InteresCarteraVencida,
 TipoCreditoR04A,
 EntidadCobertura,
 TipoCreditoR04A_2016
)
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 hip.Codigo,
 per.Codigo,
 hip.SaldoCapitalVigente,
 hip.InteresVigente,
 hip.SaldoCapitalVencido,
 hip.InteresVencido,
 hip.MontoGarantia,
 hip.InteresCarteraVencida,
 pro.Codigo,
 ent.Codigo,
 pro_16.Codigo
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_Persona per ON hip.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 pro ON hip.IdTipoCreditoR04A = pro.IdTipoProducto
LEFT OUTER JOIN dbo.SICC_EntidadCobertura ent ON hip.IdEntidadCobertura = ent.IdEntidadCobertura
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 pro_16 ON hip.IdTipoCreditoR04A_2016 = pro_16.IdTipoProducto;
GO

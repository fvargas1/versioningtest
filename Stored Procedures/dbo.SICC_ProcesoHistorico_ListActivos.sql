SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ProcesoHistorico_ListActivos]
AS
SELECT
	vw.IdProcesoHistorico,
	vw.Nombre,
	vw.Descripcion,
	vw.Codename,
	vw.Position,
	vw.Activo
FROM dbo.SICC_VW_ProcesoHistorico vw
WHERE vw.Activo = 1
ORDER BY vw.Position;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Detalle_Canasta_Deudor]
	@IdPersona BIGINT
AS
SELECT
	rw.NumeroLinea,
	rw.CodigoCredito,
	rw.TipoGarantia AS Tipo,
	rw.PrctSinAjust AS PorcCobSinAjust,
	rw.PrctAjust AS PorcCobAjust,
	rw.[PI],
	rw.SP,
	rw.MontoRecuperacion,
	rw.Reserva
FROM dbo.RW_CedulaNMC_Canasta rw
WHERE rw.IdPersona = @IdPersona
ORDER BY NumeroLinea, CodigoCredito, Posicion;
GO

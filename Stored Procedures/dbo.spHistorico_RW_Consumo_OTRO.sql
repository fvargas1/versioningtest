SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Consumo_OTRO]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Consumo_OTRO);

DELETE FROM Historico.RW_Consumo_OTRO WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Consumo_OTRO (
	IdPeriodoHistorico, IdProducto, FolioCredito, FolioCliente, Reestructura, FechaInicioCredito, FechaTeoricaVencimientoCredito, PeriodosFacturacionCredito,
	PlazoTotal, ImporteOriginalCredito, ValorOriginalBien, TasaInteresAnualCredito, MecanismoPago, CodigoPostal, FechaCorte, PlazoRemanente, SaldoCredito,
	MontoExigible, PagoRealizado, PromedioPorcentajePagosRealizados, DiasAtraso, NumeroAtrasos, MaximoNumeroAtrasos, IndicadorAtraso,
	SumaExigiblesTeoricosCredito, TipoGarantia, ImporteGarantia, RegistroUnicoGarantiasMobiliarias, ProbabilidadIncumplimiento,
	SeveridadPerdidaParteNoCubierta, SeveridadPerdidaParteCubierta, ExposicionIncumplimientoParteNoCubierta, ExposicionIncumplimientoParteCubierta,
	MontoReservasConstituirCredito, ClasificacionCredito, QuitasCondonacionesBonificacionesDescuentos, RelacionAcreditadoInstitucion,
	ClaveConsultaSociedadInformacionCrediticia, MetodologiaUtilizadaCalculoReservas, ProbabilidadIncumplimientoInterna, SeveridadPerdidaInterna,
	ExposicionIncumplimientoInterna, MontoReservasConstituirCreditoInterno, CAT
)
SELECT
	@IdPeriodoHistorico,
	IdProducto,
	FolioCredito,
	FolioCliente,
	Reestructura,
	FechaInicioCredito,
	FechaTeoricaVencimientoCredito,
	PeriodosFacturacionCredito,
	PlazoTotal,
	ImporteOriginalCredito,
	ValorOriginalBien,
	TasaInteresAnualCredito,
	MecanismoPago,
	CodigoPostal,
	FechaCorte,
	PlazoRemanente,
	SaldoCredito,
	MontoExigible,
	PagoRealizado,
	PromedioPorcentajePagosRealizados,
	DiasAtraso,
	NumeroAtrasos,
	MaximoNumeroAtrasos,
	IndicadorAtraso,
	SumaExigiblesTeoricosCredito,
	TipoGarantia,
	ImporteGarantia,
	RegistroUnicoGarantiasMobiliarias,
	ProbabilidadIncumplimiento,
	SeveridadPerdidaParteNoCubierta,
	SeveridadPerdidaParteCubierta,
	ExposicionIncumplimientoParteNoCubierta,
	ExposicionIncumplimientoParteCubierta,
	MontoReservasConstituirCredito,
	ClasificacionCredito,
	QuitasCondonacionesBonificacionesDescuentos,
	RelacionAcreditadoInstitucion,
	ClaveConsultaSociedadInformacionCrediticia,
	MetodologiaUtilizadaCalculoReservas,
	ProbabilidadIncumplimientoInterna,
	SeveridadPerdidaInterna,
	ExposicionIncumplimientoInterna,
	MontoReservasConstituirCreditoInterno,
	CAT
FROM dbo.RW_Consumo_OTRO
WHERE IdReporteLog = @IdReporteLog;
GO

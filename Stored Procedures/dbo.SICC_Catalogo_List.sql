SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Catalogo_List]
	@filtro VARCHAR(50)
AS
SELECT
	vw.IdCatalogo,
	vw.Nombre,
	vw.Descripcion,
	vw.SPGet,
	vw.Activo
FROM dbo.SICC_VW_Catalogo vw
WHERE vw.Activo = 1 AND ISNULL(vw.Nombre,'')+'|'+ISNULL(vw.Descripcion,'') LIKE '%' + @filtro + '%'
ORDER BY vw.Nombre;
GO

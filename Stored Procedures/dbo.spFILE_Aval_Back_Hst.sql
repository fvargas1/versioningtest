SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Aval_Back_Hst]
AS

INSERT INTO dbo.FILE_Aval_Hst (
 CodigoAval,
 NombreAval,
 RFC,
 Domicilio,
 PersonalidadJuridica,
 TipoAval,
 MunAval,
 PIAval,
 Moneda,
 Calif_Fitch,
 Calif_Moodys,
 Calif_SP,
 Calif_HRRATINGS,
 Calif_Otras,
 ActividadEconomica,
 LEI,
 TipoCobertura,
 FiguraGarantiza,
 CodigoCliente,
 Localidad,
 Estado,
 CodigoPostal
)
SELECT
 a.CodigoAval,
 a.NombreAval,
 a.RFC,
 a.Domicilio,
 a.PersonalidadJuridica,
 a.TipoAval,
 a.MunAval,
 a.PIAval,
 a.Moneda,
 a.Calif_Fitch,
 a.Calif_Moodys,
 a.Calif_SP,
 a.Calif_HRRATINGS,
 a.Calif_Otras,
 a.ActividadEconomica,
 a.LEI,
 a.TipoCobertura,
 a.FiguraGarantiza,
 a.CodigoCliente,
 a.Localidad,
 a.Estado,
 a.CodigoPostal
FROM dbo.FILE_Aval a
LEFT OUTER JOIN dbo.FILE_Aval_Hst hst ON LTRIM(a.CodigoAval) = LTRIM(hst.CodigoAval)
WHERE hst.CodigoAval IS NULL;
GO

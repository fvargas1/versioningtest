SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Get_Activate_Group_Config]
	@IdGroup INT
AS
UPDATE dbo.BAJAWARE_Config SET Value = @IdGroup WHERE CodeName = 'FRM_REP_CFG';
GO

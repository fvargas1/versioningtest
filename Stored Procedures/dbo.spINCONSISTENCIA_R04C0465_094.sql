SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_094]
AS

BEGIN

-- Validar que el Número de Meses Transcurridos desde que se asignó PI=100 (dat_meses_transcur_pi_100) sea MAYOR O IGUAL a cero.

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MesesPI100
FROM dbo.RW_VW_R04C0465_INC
WHERE CAST(ISNULL(NULLIF(MesesPI100,''),'-1') AS DECIMAL) < 0;

END

GO

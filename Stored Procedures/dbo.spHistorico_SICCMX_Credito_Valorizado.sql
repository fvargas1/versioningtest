SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Credito_Valorizado]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Credito_Valorizado WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Credito_Valorizado (
	IdPeriodoHistorico,
	Credito,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	InteresRefinanciado,
	MontoOriginal,
	MontoLineaCredito,
	InteresCarteraVencida
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	CodigoCredito,
	SaldoVigenteValorizado,
	InteresVigenteValorizado,
	SaldoVencidoValorizado,
	InteresVencidoValorizado,
	InteresRefinanciadoValorizado,
	MontoOriginalValorizado,
	MontoLineaValorizado,
	InteresCarteraVencidaValorizado
FROM dbo.SICCMX_VW_Credito_NMC;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Clasificacion_Anx20]
AS
DECLARE @IdMetodologia INT;
DECLARE @IdPeriodo BIGINT;
DECLARE @IdMoneda BIGINT;
DECLARE @udis_menor MONEY;
DECLARE @udis_mayor MONEY;
DECLARE @monto_udis_menor MONEY;
DECLARE @monto_udis_mayor MONEY;
DECLARE @TipoCambioUdis MONEY;

SET @udis_menor = NULL;
SET @udis_mayor = NULL;

SELECT @IdPeriodo = IdPeriodo FROM dbo.SICC_Periodo WHERE Activo=1;
SELECT @IdMoneda = IdMoneda FROM dbo.SICC_Moneda WHERE Codigo = (SELECT VALUE FROM dbo.BAJAWARE_Config WHERE Codename='MonedaUdis');

--SE TOMA EL RANGO DE UDIS PARA PEQUEÑAS ENTIDADES DE BAJAWARE_Config CodeName='A20_UDIS_MENOR'
SELECT @udis_menor = CONVERT(MONEY,Value) FROM dbo.BAJAWARE_Config WHERE CodeName='A20_UDIS_MENOR';
IF @udis_menor IS NULL SET @udis_menor = 600000000;

--SE TOMA EL RANGO DE UDIS PARA GRANDES ENTIDADES DE BAJAWARE_Config CodeName='A20_UDIS_MAYOR'
SELECT @udis_mayor = CONVERT(MONEY,Value) FROM dbo.BAJAWARE_Config WHERE CodeName='A20_UDIS_MAYOR';
IF @udis_mayor IS NULL SET @udis_mayor = 2500000000;


SELECT @TipoCambioUdis = Valor FROM dbo.SICC_TipoCambio WHERE IdPeriodo = @IdPeriodo AND IdMoneda = @IdMoneda;
SET @monto_udis_menor = @udis_menor * @TipoCambioUdis;
SET @monto_udis_mayor = @udis_mayor * @TipoCambioUdis;


SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo=20;

INSERT INTO dbo.SICCMX_Persona_PI (IdPersona, IdMetodologia, IdClasificacion)
SELECT DISTINCT
	cr.IdPersona,
	cr.IdMetodologia,
	CASE
	WHEN anx.OrgDescPartidoPolitico = 1 THEN 4
	WHEN anx.ActivoTotal < @monto_udis_menor THEN 1
	WHEN anx.ActivoTotal >= @monto_udis_mayor THEN 3
	ELSE 2 END
FROM dbo.SICCMX_Credito cr
INNER JOIN dbo.SICCMX_Anexo20 anx ON cr.IdPersona = anx.IdPersona
WHERE cr.IdMetodologia=@IdMetodologia;
GO

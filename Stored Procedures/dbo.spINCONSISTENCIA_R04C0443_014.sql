SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_014]
AS

BEGIN

-- La fecha de disposición del crédito debe ser menor o igual al periodo que se reporta.

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo = 1;

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, FechaDisposicion, @FechaPeriodo AS FechaPeriodo
FROM dbo.RW_R04C0443
WHERE FechaDisposicion > @FechaPeriodo;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_027]
AS

BEGIN

-- Se deberá validar la correspondencia para las siguientes claves en "Monto del Subsidio Federal al Frente" y "Tipo de Alta del Crédito":
-- - Cuando el "Monto del Subsidio Federal al Frente" sea mayor a 0, el "Tipo de Alta del Crédito" debe ser 2,5,6 o 10.

SELECT CodigoCredito, CodigoCreditoCNBV, MontoSubsidioFederal, TipoAlta
FROM dbo.RW_R04H0491
WHERE CAST(NULLIF(MontoSubsidioFederal,'') AS DECIMAL) > 0 AND TipoAlta NOT IN ('2','5','6','10');

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Persona]
 @IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_Persona WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Persona (
 IdPeriodoHistorico,
 Codigo,
 Nombre,
 RFC,
 Domicilio,
 Telefono,
 Correo,
 Ejecutivo,
 Institucion,
 ActividadEconomica,
 Localidad,
 DeudorRelacionadoMA,
 DeudorRelacionado,
 Sucursal,
 FechaCreacion,
 FechaActualizacion,
 Username,
 InstitucionNombre,
 EjecutivoNombre,
 ActividadEconomicaNombre,
 LocalidadNombre,
 EsFondo,
 PI100
) 
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 per.Codigo,
 per.Nombre,
 per.RFC,
 per.Domicilio,
 per.Telefono,
 per.Correo,
 ejec.Codigo AS Ejecutivo, --per.IdEjecutivo,
 inst.Codigo AS Institucion, --per.IdInstitucion,
 act.Codigo AS ActividadEconomica, --per.IdActividadEconomica,
 loc.CodigoCNBV AS Localidad, --per.IdLocalidad,
 deudMA.Codigo AS DeudorRelacionadoMA, --per.IdDeudorRelacionadoMA,
 deud.Codigo AS DeudorRelacionado, --per.IdDeudorRelacionado,
 suc.Codigo AS Sucursal, --per.IdSucursal,
 per.FechaCreacion,
 per.FechaActualizacion,
 per.Username,
 inst.Nombre,
 ejec.Nombre,
 act.Nombre,
 loc.NombreLocalidad,
 fon.Codigo,
 pi100.Codigo
FROM dbo.SICCMX_Persona per
LEFT OUTER JOIN dbo.SICCMX_Ejecutivo ejec ON per.IdEjecutivo = ejec.IdEjecutivo
LEFT OUTER JOIN dbo.SICC_Institucion inst ON per.IdInstitucion = inst.IdInstitucion
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON per.IdActividadEconomica = act.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_Localidad2015 loc ON per.IdLocalidad = loc.IdLocalidad
LEFT OUTER JOIN dbo.SICC_DeudorRelacionadoMA deudMA ON per.IdDeudorRelacionadoMA = deudMA.IdDeudorRelacionado
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado deud ON per.IdDeudorRelacionado = deud.IdDeudorRelacionado
LEFT OUTER JOIN dbo.SICC_Sucursal suc ON per.IdSucursal = suc.IdSucursal
LEFT OUTER JOIN dbo.SICC_TipoReserva5Prct fon ON per.EsFondo = fon.IdTipoReserva
LEFT OUTER JOIN dbo.SICC_PI100 pi100 ON per.PI100 = pi100.IdPI100;
GO

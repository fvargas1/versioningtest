SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Persona_PI]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_Persona_PI WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Persona_PI (
	IdPeriodoHistorico,
	Persona,
	FactorCuantitativo,
	PonderadoCuantitativo,
	FactorCualitativo,
	PonderadoCualitativo,
	FactorTotal,
	[PI],
	Metodologia,
	IdClasificacion,
	Comentarios,
	MesesPI100
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	per.Codigo, -- Persona
	ppi.FactorCuantitativo,
	ppi.PonderadoCuantitativo,
	ppi.FactorCualitativo,
	ppi.PonderadoCualitativo,
	ppi.FactorTotal,
	ppi.[PI],
	met.Codigo, -- Metodologia
	ppi.IdClasificacion,
	ppi.Comentarios,
	ppi.MesesPI100
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_Persona per ON ppi.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Metodologia met ON ppi.IdMetodologia = met.IdMetodologia;
GO

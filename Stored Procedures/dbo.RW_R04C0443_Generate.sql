SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0443_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(10);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0443';
SELECT @Entidad = [Value] FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_R04C0443;

INSERT INTO dbo.RW_R04C0443 (
 IdReporteLog, Entidad, Formulario, NumeroSecuencia, CodigoPersona, RFC, NombrePersona, CodigoCredito, CodigoCreditoCNBV, CodigoAgrupacion,
 NumeroDisposicion, FechaDisposicion, CalificacionPersona, CalificacionCreditoCubierta, CalificacionCreditoExpuesta,
 CalificacionPersonaCNBV, CalificacionCreditoCubiertaCNBV, CalificacionCreditoExpuestaCNBV, SituacionCredito, SaldoInicial,
 TasaInteres, MontoDispuesto, MontoExigible, MontoPagado, MontoInteresPagado, MontoComisionDevengada, DiasVencido, SaldoFinal,
 ResponsabilidadTotal, TipoBaja, MontoReconocido, ProductoComercial, PersonalidadJuridica
)
SELECT DISTINCT
 @IdReporteLog,
 @Entidad,
 '0443',
 DENSE_RANK() OVER ( ORDER BY cre.CodigoCredito ASC ),
 per.Codigo AS CodigoPersona,
 per.RFC AS RFC,
 REPLACE(pInfo.NombreCNBV,',','') AS NombrePersona,
 lin.Codigo AS CodigoCredito,
 cnbv.CNBV AS CodigoCreditoCNBV,
 NULLIF(cre.NumeroAgrupacion,'') AS CodigoAgrupacion,
 cre.CodigoCredito AS NumeroDisposicion,
 CASE WHEN cre.FechaDisposicion IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,cre.FechaDisposicion,102),'.',''),1,6) END AS FechaDisposicion,
 '' AS CalificacionPersona,
 '' AS CalificacionCreditoCubierta,
 '' AS CalificacionCreditoExpuesta,
 cal.Codigo AS CalificacionPersonaCNBV,
 calCub.Codigo AS CalificacionCreditoCubiertaCNBV,
 calExp.Codigo AS CalificacionCreditoExpuestaCNBV,
 sit.Codigo AS SituacionCredito,
 cInfo.SaldoInicial AS SaldoInicial,
 cre.TasaBruta AS TasaInteres,
 cinfo.MontoDispuesto AS MontoDispuesto,
 cInfo.MontoPagoExigible AS MontoExigible,
 cInfo.MontoPagosRealizados AS MontoPagado,
 cInfo.MontoInteresPagado AS MontoInteresPagado,
 cInfo.ComisionDelMes AS MontoComisionDevengada,
 cInfo.DiasMorosidad AS DiasVencido,
 cInfo.SaldoFinal AS SaldoFinal,
 cre.MontoValorizado AS ResponsabilidadTotal,
 tpoBaja.CodigoCNBV AS TipoBaja,
 cInfo.MontoBonificacionDesc AS MontoReconocido,
 tpoCre.CodigoCNBV AS ProductoComercial,
 tpoPer.CodigoCNBV AS PersonalidadJuridica
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON pInfo.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
INNER JOIN dbo.SICCMX_LineaCredito lin ON cre.IdLineaCredito = lin.IdLineaCredito
INNER JOIN dbo.SICCMX_PersonaCalificacion perCal ON per.IdPersona = perCal.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_CreditosCNBV cnbv ON cre.IdCredito = cnbv.IdCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON perCal.IdCalificacion = cal.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calCub ON crv.CalifCubierta = calCub.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calExp ON crv.CalifExpuesta = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_SituacionCredito sit ON cre.IdSituacionCredito = sit.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_TipoBajaMA tpoBaja ON cInfo.IdTipoBajaMA = tpoBaja.IdTipoBaja
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial tpoCre ON cInfo.ProductoComercial = tpoCre.IdTipoCredito
LEFT OUTER JOIN dbo.SICC_TipoPersonaMA tpoPer ON pInfo.IdTipoPersonaMA = tpoPer.IdTipoPersona;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0443 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_056]
AS
BEGIN
-- Validar que ID Metodología CNBV (dat_id_credito_met_cnbv) se haya reportado previamente en el reporte de altas R04-C 458

SELECT
	rep.CodigoCredito,
	rep.NumeroDisposicion,
	REPLACE(rep.NombrePersona, ',', '' ) AS NombreDeudor,
	rep.CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0459_INC rep
LEFT OUTER JOIN dbo.RW_R04C0458 alt ON ISNULL(rep.CodigoCreditoCNBV,'') = ISNULL(alt.CodigoCnbv,'')
LEFT OUTER JOIN Historico.RW_R04C0458 hst ON ISNULL(rep.CodigoCreditoCNBV,'') = hst.CodigoCnbv
WHERE hst.IdPeriodoHistorico IS NULL AND alt.CodigoCnbv IS NULL;

END


GO

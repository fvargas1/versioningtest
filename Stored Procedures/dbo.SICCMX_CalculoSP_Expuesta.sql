SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_Expuesta]
AS
UPDATE crv
SET SP_Expuesta = can.SeveridadCorresp
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Garantia_Canasta can ON crv.IdCredito = can.IdCredito
WHERE can.EsDescubierto = 1;

INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 crv.IdCredito,
 GETDATE(),
 '',
 CASE
 WHEN pos.Codigo IS NOT NULL THEN 'La SP Expuesta es de '
 + CONVERT(VARCHAR, CONVERT(DECIMAL(10,2), ISNULL(crv.SP_Expuesta*100, 0))) + '% dado que la posición es "'
 + UPPER(pos.Nombre)
 + '"'
 ELSE 'No se pudo determinar la SP dado que no cuenta con una posición PREFERENTE o SUBORDINADA'
 END
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON crv.IdCredito = cInfo.IdCredito
INNER JOIN dbo.SICC_Posicion pos ON cInfo.Posicion = pos.IdPosicion;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Entidad_Insert]
	@Nombre VARCHAR(50),
	@Descripcion VARCHAR(100) = NULL,
	@NombreView VARCHAR(50) = NULL
AS
INSERT INTO dbo.MIGRACION_Entidad (
	Nombre,
	Descripcion,
	NombreView
) VALUES (
	@Nombre,
	@Descripcion,
	@NombreView
);

SELECT SCOPE_IDENTITY();
GO

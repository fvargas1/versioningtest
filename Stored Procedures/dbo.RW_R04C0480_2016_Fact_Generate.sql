SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0480_2016_Fact_Generate]
	@IdReporteLog BIGINT,
	@IdPeriodo BIGINT,
	@Entidad VARCHAR(50)
AS
INSERT INTO dbo.RW_R04C0480_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, SinAtrasos, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC, Alfa,
 HITenSIC, FechaConsultaSIC, FechaInfoFinanciera, MesesPI100, ID_PI100, GarantiaLeyFederal, CumpleCritContGral, P_DiasMoraInstBanc, P_PorcPagoInstBanc, P_NumInstRep,
 P_PorcPagoInstNoBanc, P_PagosInfonavit, P_DiasAtrInfonavit, P_TasaRetLab, P_RotActTot, P_RotCapTrabajo, P_RendCapROE, DiasMoraInstBanc, PorcPagoInstBanc, NumInstRep,
 PorcPagoInstNoBanc, PagosInfonavit, DiasAtrInfonavit, NumeroEmpleados, TasaRetLab, PasivoCirculante, UtilidadNeta, CapitalContable, ActivoTotalAnual, VentasNetasTotales,
 RotActTot, ActivoCirculante, RotCapTrabajo, RendCapROE
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '480',
 'FACTORADO' + per.Codigo AS CodigoPersona,
 CASE WHEN anx.SinAtrasos = 1 THEN '1' ELSE '2' END AS SinAtrasos,
 ppi.[PI] * 100 AS [PI],
 ppi.FactorTotal AS PuntajeTotal,
 ppi.FactorCuantitativo AS PuntajeCuantitativo,
 ppi.FactorCualitativo AS PuntajeCualitativo,
 '750' AS CreditoReportadoSIC,
 dlt.FactorCuantitativo AS Alfa,
 '2' AS HITenSIC,
 CASE WHEN anx.FechaInfoBuro IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoBuro,102),'.',''),1,6) END AS FechaConsultaSIC,
 CASE WHEN anx.FechaInfoFinanc IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoFinanc,102),'.',''),1,6) END AS FechaInfoFinanciera,
 '0' AS MesesPI100,
 '0' AS ID_PI100,
 '790' AS GarantiaLeyFederal,
 '810' AS CumpleCritContGral,
 ptNMC.[22_DIAS_MORA_INST_BANC] AS P_DiasMoraInstBanc,
 ptNMC.[22_PORC_PAGO_INST_BANC] AS P_PorcPagoInstBanc,
 ptNMC.[22_NUM_INST_REP] AS P_NumInstRep,
 ptNMC.[22_PORC_PAGO_INST_NOBANC] AS P_PorcPagoInstNoBanc,
 ptNMC.[22_PAGOS_INFONAVIT] AS P_PagosInfonavit,
 ptNMC.[22_DIAS_ATR_INFONAVIT] AS P_DiasAtrInfonavit,
 ptNMC.[22_TASA_RET_LAB] AS P_TasaRetLab,
 ptNMC.[22_ROT_ACT_TOT] AS P_RotActTot,
 ptNMC.[22_ROT_CAP_TRABAJO] AS P_RotCapTrabajo,
 ptNMC.[22_REND_CAP_ROE] AS P_RendCapROE,
 vlNMC.[22_DIAS_MORA_INST_BANC] AS DiasMoraInstBanc,
 vlNMC.[22_PORC_PAGO_INST_BANC] AS PorcPagoInstBanc,
 vlNMC.[22_NUM_INST_REP] AS NumInstRep,
 vlNMC.[22_PORC_PAGO_INST_NOBANC] AS PorcPagoInstNoBanc,
 vlNMC.[22_PAGOS_INFONAVIT] AS PagosInfonavit,
 vlNMC.[22_DIAS_ATR_INFONAVIT] AS DiasAtrInfonavit,
 anx.NumeroEmpleados AS NumeroEmpleados,
 vlNMC.[22_TASA_RET_LAB] AS TasaRetLab,
 anx.PasivoCirculante AS PasivoCirculante,
 anx.UtilidadNeta AS UtilidadNeta,
 anx.CapitalContableProm AS CapitalContable,
 anx.ActTotalAnual AS ActivoTotalAnual,
 anx.VentNetTotAnuales AS VentasNetasTotales,
 vlNMC.[22_ROT_ACT_TOT] AS RotActTot,
 anx.ActivoCirculante AS ActivoCirculante,
 vlNMC.[22_ROT_CAP_TRABAJO] AS RotCapTrabajo,
 vlNMC.[22_REND_CAP_ROE] AS RendCapROE
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Credito cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoAval ca ON cre.IdCredito = ca.IdCredito
INNER JOIN dbo.SICCMX_Aval aval ON ca.IdAval = aval.IdAval
INNER JOIN dbo.SICC_FiguraGarantiza fig ON aval.FiguraGarantiza = fig.IdFigura AND fig.ReportePI = 1
INNER JOIN dbo.SICCMX_VW_Anexo22_GP anx ON aval.IdAval = anx.IdGP
INNER JOIN dbo.SICC_EsGarante esg ON anx.EsGarante = esg.IdEsGarante AND esg.Layout = 'AVAL'
INNER JOIN dbo.SICCMX_Persona_PI_GP ppi ON anx.IdGP = ppi.IdGP AND anx.EsGarante = ppi.EsGarante
INNER JOIN dbo.SICCMX_PI_ValoresDelta dlt ON ppi.IdMetodologia = dlt.IdMetodologia AND ISNULL(ppi.IdClasificacion,0) = ISNULL(dlt.IdClasificacion,0)
INNER JOIN dbo.SICCMX_VW_PersonasPuntaje_A22_GP ptNMC ON ppi.IdGP = ptNMC.IdGP AND ppi.EsGarante = ptNMC.EsGarante
INNER JOIN dbo.SICCMX_VW_PersonasValor_A22_GP vlNMC ON ppi.IdGP = vlNMC.IdGP AND ppi.EsGarante = vlNMC.EsGarante
WHERE ISNULL(anx.OrgDescPartidoPolitico,0) = 1;
GO

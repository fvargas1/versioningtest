SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0442]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0442);

DELETE FROM Historico.RW_R04C0442 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0442 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoPersona, RFC, NombrePersona, GrupoRiesgo, TipoAcreditadoRel, PersonalidadJuridica,
	SectorEconomico, ActividadEconomica, NumeroEmpleados, IngresosBrutos, Localidad, NumeroConsulta, TipoAltaCredito, SpecsDisposicionCredito, CodigoCredito,
	CodigoCreditoCNBV, CodigoAgrupacion, DestinoCredito, SaldoCredito, DenominacionCredito, FechaVencimiento, PeriodicidadPagosCapital, PeriodicidadPagosInteres,
	TasaRef, AjusteTasaRef, FrecuenciaRevisionTasa, MontoFondeo, CodigoBancoFondeador, Comisiones, PorcentajeCubierto, PorcentajeCubiertoFondeo,
	CodigoBancoGarantia, PorcentajeCubiertoAval, TipoGarantia, Municipio, Estado
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	NumeroSecuencia,
	CodigoPersona,
	RFC,
	NombrePersona,
	GrupoRiesgo,
	TipoAcreditadoRel,
	PersonalidadJuridica,
	SectorEconomico,
	ActividadEconomica,
	NumeroEmpleados,
	IngresosBrutos,
	Localidad,
	NumeroConsulta,
	TipoAltaCredito,
	SpecsDisposicionCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoAgrupacion,
	DestinoCredito,
	SaldoCredito,
	DenominacionCredito,
	FechaVencimiento,
	PeriodicidadPagosCapital,
	PeriodicidadPagosInteres,
	TasaRef,
	AjusteTasaRef,
	FrecuenciaRevisionTasa,
	MontoFondeo,
	CodigoBancoFondeador,
	Comisiones,
	PorcentajeCubierto,
	PorcentajeCubiertoFondeo,
	CodigoBancoGarantia,
	PorcentajeCubiertoAval,
	TipoGarantia,
	Municipio,
	Estado
FROM dbo.RW_R04C0442
WHERE IdReporteLog = @IdReporteLog;
GO

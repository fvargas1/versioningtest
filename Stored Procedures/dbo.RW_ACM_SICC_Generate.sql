SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_ACM_SICC_Generate]
AS
DECLARE @IdPeriodo BIGINT;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaMigracion DATETIME;

SELECT @IdPeriodo = IdPeriodo, @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'INSUMO_RR' AND Nombre = 'ACM_SICC';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_ACM_SICC;

INSERT INTO dbo.RW_ACM_SICC (
 IdReporteLog,
 Periodo,
 Periodo_Actual,
 Tipo_Cred_R04A,
 Fecha_Ven,
 Sector_Lab,
 Situacion_Cred,
 Interes_Vig,
 Interes_Ven,
 Saldo_Total,
 Moneda
)
SELECT
 @IdReporteLog,
 @IdPeriodo,
 CONVERT(CHAR(10), @FechaPeriodo, 126),
 TIPO_CRED_R04A,
 CONVERT(CHAR(10), FECHA_VEN, 126),
 SECTOR_LAB,
 SITUACION_CRED,
 SUM(ISNULL (INTERESES_VIG,0)) AS INTERESES_VIG,
 SUM(ISNULL (INTERESES_VEN,0)) AS INTERESES_VEN,
 SUM(ISNULL (SALDO_TOTAL,0)) AS SALDO_TOTAL,
 MONEDA
FROM (
 SELECT 
 cr04a.Codigo AS TIPO_CRED_R04A,
 cre.FechaVencimiento AS FECHA_VEN,
 secEco.CodigoRR AS SECTOR_LAB,
 sitcre.Codigo AS SITUACION_CRED,
 cre.InteresVigente AS INTERESES_VIG,
 cre.InteresVencido AS INTERESES_VEN,
 SaldoCapitalVigente + InteresVigente + SaldoCapitalVencido + InteresVencido AS SALDO_TOTAL,
 mon.CodigoISO AS MONEDA
 FROM dbo.SICCMX_Credito cre
 LEFT OUTER JOIN dbo.SICCMX_Persona per ON per.IdPersona = cre.IdPersona
 LEFT OUTER JOIN dbo.SICCMX_CreditoInfo cinf ON cinf.IdCredito = cre.IdCredito
 LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 cr04a ON cr04a.IdTipoProducto = cinf.IdTipoProductoSerie4
 LEFT OUTER JOIN dbo.SICCMX_PersonaInfo pinf ON pinf.IdPersona = per.IdPersona
 LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor secEco ON pinf.IdSectorEconomico = secEco.IdSectorEconomico
 LEFT OUTER JOIN dbo.SICC_SituacionCredito sitcre ON sitcre.IdSituacionCredito = cre.IdSituacionCredito
 LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = cre.IdMonedaOrigen

 UNION ALL

 SELECT 
 cr04a.Codigo AS TIPO_CRED_R04A,
 cinf.FechaVencimiento AS FECHA_VEN,
 secEco.CodigoRR AS SECTOR_LAB,
 sitcre.Codigo AS SITUACION_CRED,
 con.InteresVigente AS INTERESES_VIG,
 con.InteresVencido AS INTERESES_VEN,
 con.SaldoCapitalVigente + con.InteresVigente + con.SaldoCapitalVencido + con.InteresVencido AS SALDO_TOTAL,
 mon.CodigoISO AS MONEDA
 FROM dbo.SICCMX_Consumo con
 LEFT OUTER JOIN dbo.SICCMX_Persona per ON per.IdPersona = con.IdPersona
 LEFT OUTER JOIN dbo.SICCMX_ConsumoInfo cinf ON cinf.IdConsumo = con.IdConsumo
 LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 cr04a ON cr04a.IdTipoProducto = cinf.IdTipoProductoSerie4
 LEFT OUTER JOIN dbo.SICCMX_PersonaInfo pinf ON pinf.IdPersona = per.IdPersona
 LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor secEco ON pinf.IdSectorEconomico = secEco.IdSectorEconomico
 LEFT OUTER JOIN dbo.SICC_SituacionCredito sitcre ON sitcre.IdSituacionCredito = con.IdSituacionCredito
 LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = cinf.IdMoneda

 UNION ALL

 SELECT 
 cr04a.Codigo AS TIPO_CRED_R04A,
 hinf.FechaVencimiento AS FECHA_VEN,
 secEco.CodigoRR AS SECTOR_LAB,
 sitcre.Codigo AS SITUACION_CRED,
 hip.InteresVigente AS INTERESES_VIG,
 hip.InteresVencido AS INTERESES_VEN,
 hip.SaldoCapitalVigente + hip.InteresVigente + hip.SaldoCapitalVencido + hip.InteresVencido AS SALDO_TOTAL,
 mon.CodigoISO AS MONEDA
 FROM dbo.SICCMX_Hipotecario hip
 LEFT OUTER JOIN dbo.SICCMX_Persona per ON per.IdPersona = hip.IdPersona
 LEFT OUTER JOIN dbo.SICCMX_HipotecarioInfo hinf ON hinf.IdHipotecario = hip.IdHipotecario
 LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 cr04a ON cr04a.IdTipoProducto = hiP.IdTipoCreditoR04A
 LEFT OUTER JOIN dbo.SICCMX_PersonaInfo pinf ON pinf.IdPersona = per.IdPersona
 LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor secEco ON pinf.IdSectorEconomico = secEco.IdSectorEconomico
 LEFT OUTER JOIN dbo.SICC_SituacionCredito sitcre ON sitcre.IdSituacionCredito = hinf.IdSituacionCredito
 LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = hinf.IdMoneda
) AS ACM_SICC
GROUP BY TIPO_CRED_R04A, FECHA_VEN, SECTOR_LAB, SITUACION_CRED, MONEDA;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_ACM_SICC WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SELECT @TotalIntereses = 0;

SET @FechaMigracion = ( SELECT MAX( Fecha ) FROM MIGRACION_ProcesoLog );

UPDATE dbo.RW_ReporteLog
SET
 TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = @FechaMigracion,
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Consumo_Valorizado]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Consumo_Valorizado WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Consumo_Valorizado (
	IdPeriodoHistorico,
	Consumo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	InteresCarteraVencida
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	Codigo,
	SaldoCapitalVigenteValorizado,
	InteresVigenteValorizado,
	SaldoCapitalVencidoValorizado,
	InteresVencidoValorizado,
	InteresCarteraVencidaValorizado
FROM dbo.SICCMX_VW_Consumo;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0419Datos_Ajuste_2016]
AS
INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
	tb.Codigo,
	CASE WHEN tb.Diff > 0 THEN '139499000000' ELSE '139309000000' END,
	ABS(tb.Diff),
	CASE WHEN tb.Diff > 0 THEN 'A' ELSE 'C' END
FROM (
SELECT
	dat.Codigo,
	CAST(dat.ReservaActual AS DECIMAL) - CAST(
	dat.ReservaHistorico
	- SUM(CASE WHEN con.CargoAbono = 'C' THEN con.Monto ELSE 0 END)
	+ SUM(CASE WHEN con.CargoAbono = 'A' THEN con.Monto ELSE 0 END)
	AS DECIMAL) AS Diff
FROM R04.[0419Datos_2016] dat
LEFT OUTER JOIN R04.[0419Conceptos_2016] con ON dat.Codigo = con.Codigo AND con.CargoAbono IS NOT NULL
GROUP BY dat.Codigo, dat.CodigoProducto, dat.ReservaHistorico, dat.ReservaActual
HAVING ABS(CAST(dat.ReservaActual AS DECIMAL) - CAST(
	dat.ReservaHistorico
	- SUM(CASE WHEN con.CargoAbono = 'C' THEN con.Monto ELSE 0 END)
	+ SUM(CASE WHEN con.CargoAbono = 'A' THEN con.Monto ELSE 0 END)
	AS DECIMAL)) = 1
) AS tb
GO

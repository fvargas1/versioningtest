SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_039]
AS

BEGIN

-- El porcentaje total cubierto del crédito debe ser mayor o igual a 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, PorcentajeCubierto
FROM dbo.RW_R04C0442
WHERE CAST(ISNULL(NULLIF(PorcentajeCubierto,''),'-1') AS DECIMAL(10,2)) < 0;

END
GO

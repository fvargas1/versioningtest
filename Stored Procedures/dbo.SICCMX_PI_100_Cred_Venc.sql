SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_100_Cred_Venc]
AS
-- SE ACTUALIZA PI AL 100% PARA PERSONAS CON CREDITOS VENCIDOS Y QUE EL SALDO SEA MAYOR O IGUAL AL 5% DE LA DEUDA DEL CLIENTE CON LA INSTITUCION
UPDATE ppi
SET [PI] = 1
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_Personas_Creditos_Vencidos_PI100 PersVencidos ON ppi.IdPersona=PersVencidos.IdPersona;

-- SE INSERTA EN EL LOG DEL CALCULO DE LA PI
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT ppi.IdPersona, GETDATE(), '', 'Se actualizó la PI al 100% ya que cuenta con Creditos Vencidos mayor o igual al 5% del total de la deuda'
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN SICCMX_VW_Personas_Creditos_Vencidos_PI100 PersVencidos ON ppi.IdPersona=PersVencidos.IdPersona;

UPDATE per
SET PI100 = pi100.IdPI100
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_VW_Personas_Creditos_Vencidos_PI100 venc ON per.IdPersona = venc.IdPersona
INNER JOIN dbo.SICC_PI100 pi100 ON pi100.Codigo = '10';
GO

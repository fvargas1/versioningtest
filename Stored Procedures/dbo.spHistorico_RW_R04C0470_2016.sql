SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0470_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0470_2016);

DELETE FROM Historico.RW_R04C0470_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0470_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoPersona, Clasificacion, SinAtrasos, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC,
	Alfa, HITenSIC, FechaConsultaSIC, FechaInfoFinanciera, MesesPI100, ID_PI100, GarantiaLeyFederal, LugarRadica, P_DiasMoraInstBanc, P_PorcPagoInstBanc, P_NumInstRep,
	P_PorcPagoInstNoBanc, P_PagosInfonavit, P_DiasAtrInfonavit, P_TasaRetLab, P_RotActTot, P_RotCapTrabajo, P_RendCapROE, DiasMoraInstBanc, PorcPagoInstBanc, NumInstRep,
	PorcPagoInstNoBanc, PagosInfonavit, DiasAtrInfonavit, NumeroEmpleados, TasaRetLab, PasivoCirculante, UtilidadNeta, CapitalContable, ActivoTotalAnual, VentasNetasTotales,
	IngresosBrutosAnuales, AnioIngresosBrutosAnuales, RotActTot, ActivoCirculante, RotCapTrabajo, RendCapROE, P_EstEco, P_IntCarComp, P_Proveedores, P_Clientes, P_EstFinAudit,
	P_NumAgenCalif, P_IndepConsejoAdmon, P_EstOrg, P_CompAcc, P_LiqOper, P_UafirGastosFin
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoPersona,
	Clasificacion,
	SinAtrasos,
	[PI],
	PuntajeTotal,
	PuntajeCuantitativo,
	PuntajeCualitativo,
	CreditoReportadoSIC,
	Alfa,
	HITenSIC,
	FechaConsultaSIC,
	FechaInfoFinanciera,
	MesesPI100,
	ID_PI100,
	GarantiaLeyFederal,
	LugarRadica,
	P_DiasMoraInstBanc,
	P_PorcPagoInstBanc,
	P_NumInstRep,
	P_PorcPagoInstNoBanc,
	P_PagosInfonavit,
	P_DiasAtrInfonavit,
	P_TasaRetLab,
	P_RotActTot,
	P_RotCapTrabajo,
	P_RendCapROE,
	DiasMoraInstBanc,
	PorcPagoInstBanc,
	NumInstRep,
	PorcPagoInstNoBanc,
	PagosInfonavit,
	DiasAtrInfonavit,
	NumeroEmpleados,
	TasaRetLab,
	PasivoCirculante,
	UtilidadNeta,
	CapitalContable,
	ActivoTotalAnual,
	VentasNetasTotales,
	IngresosBrutosAnuales,
	AnioIngresosBrutosAnuales,
	RotActTot,
	ActivoCirculante,
	RotCapTrabajo,
	RendCapROE,
	P_EstEco,
	P_IntCarComp,
	P_Proveedores,
	P_Clientes,
	P_EstFinAudit,
	P_NumAgenCalif,
	P_IndepConsejoAdmon,
	P_EstOrg,
	P_CompAcc,
	P_LiqOper,
	P_UafirGastosFin
FROM dbo.RW_R04C0470_2016
WHERE IdReporteLog = @IdReporteLog;
GO

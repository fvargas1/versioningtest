SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ProyectoLinea_Back_Hst]
AS

INSERT INTO dbo.FILE_ProyectoLinea_Hst (
	NumeroLinea,
	CodigoProyecto
)
SELECT
	pl.NumeroLinea,
	pl.CodigoProyecto
FROM dbo.FILE_ProyectoLinea pl
LEFT OUTER JOIN dbo.FILE_ProyectoLinea_Hst hst ON LTRIM(pl.NumeroLinea) = LTRIM(hst.NumeroLinea) AND LTRIM(pl.CodigoProyecto) = LTRIM(hst.CodigoProyecto)
WHERE hst.CodigoProyecto IS NULL;
GO

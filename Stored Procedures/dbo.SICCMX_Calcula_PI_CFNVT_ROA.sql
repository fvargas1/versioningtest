SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_PI_CFNVT_ROA]
AS
UPDATE pre
SET ATR = 0,
	PorPago = 1
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaHipotecario AND met.Codigo = '3';

INSERT INTO dbo.SICCMX_Hipotecario_Reservas_Variables (IdHipotecario, [PI], E)
SELECT
 vp.IdHipotecario,
 1 /
 CASE WHEN vp.ATR >= const.ATRPI THEN 1
 ELSE
 (1 + EXP( -1 * ( const.Constante + (const.ATR * vp.ATR) + (const.PorVPAGO * vp.PorPago) + (const.PorCLTV * vp.PromedioDeRet))))
 END,
 hip.SaldoTotalValorizado
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON hip.IdHipotecario = vp.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON vp.IdMetodologia = met.IdMetodologiaHipotecario AND met.Codigo = '3';
GO

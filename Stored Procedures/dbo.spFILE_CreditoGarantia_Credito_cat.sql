SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_CreditoGarantia_Credito_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_CreditoGarantia_Credito_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_CreditoGarantia f
LEFT OUTER JOIN dbo.SICCMX_Credito cat ON LTRIM(f.CodigoCredito) = cat.Codigo
WHERE cat.IdCredito IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_CreditoGarantia_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoGarantia,
 'CodigoCredito',
 f.CodigoCredito,
 2,
 @Detalle
FROM dbo.FILE_CreditoGarantia f
LEFT OUTER JOIN dbo.SICCMX_Credito cat ON LTRIM(f.CodigoCredito) = cat.Codigo
WHERE cat.IdCredito IS NULL;
GO

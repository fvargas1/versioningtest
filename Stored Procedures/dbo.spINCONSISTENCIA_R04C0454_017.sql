SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_017]
AS
BEGIN
-- El Monto del Pago Exigible al acreditado debe ser mayor o igual a 0.

SELECT
	CodigoCredito,
	MontoPagoExigible
FROM dbo.RW_VW_R04C0454_INC
WHERE CAST(ISNULL(NULLIF(MontoPagoExigible,''),'-1') AS DECIMAL) < 0;

END
GO

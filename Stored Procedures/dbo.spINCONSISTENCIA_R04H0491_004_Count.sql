SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_004_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "IDENTIFICADOR DEL CRÉDITO METODOLOGÍA CNBV" deberá reportarse con letras mayúsculas y números, sin caracteres distintos a estos.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE CodigoCreditoCNBV LIKE '%[^A-Z0-9]%' OR BINARY_CHECKSUM(CodigoCreditoCNBV) <> BINARY_CHECKSUM(UPPER(CodigoCreditoCNBV));

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_AgenciaCalificadora_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_AgenciaCalificadora_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Garantia f
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora act ON LTRIM(f.AgenciaCalificadora) = act.Codigo
WHERE act.IdAgencia IS NULL AND LEN(f.AgenciaCalificadora) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoGarantia,
 'AgenciaCalificadora',
 f.AgenciaCalificadora,
 2,
 @Detalle
FROM dbo.FILE_Garantia f
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora act ON LTRIM(f.AgenciaCalificadora) = act.Codigo
WHERE act.IdAgencia IS NULL AND LEN(f.AgenciaCalificadora) > 0;
GO

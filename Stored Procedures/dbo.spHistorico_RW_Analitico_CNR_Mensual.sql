SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Analitico_CNR_Mensual]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Analitico_CNR_Mensual);

DELETE FROM Historico.RW_Analitico_CNR_Mensual WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Analitico_CNR_Mensual (
	IdPeriodoHistorico, CodigoCredito, CodigoDeudor, Nombre, Periodicidad_Facturacion, TipoCredito, Metodologia, Constante, FactorATR,
	ATR, FactorVECES, VECES, FactorProPago, ProPago, FactorTipoCredito, [PI], SP_Cubierta, SP_Expuesta, ECubierta, EExpuesta, ETotal,
	MontoGarantiaTotal, MontoGarantiaUsado, ReservaCubierta, ReservaExpuesta, ReservaTotal, PorcentajeReservaTotal, Calificacion
)
SELECT
	@IdPeriodoHistorico,
	CodigoCredito,
	CodigoDeudor,
	Nombre,
	Periodicidad_Facturacion,
	TipoCredito,
	Metodologia,
	Constante,
	FactorATR,
	ATR,
	FactorVECES,
	VECES,
	FactorProPago,
	ProPago,
	FactorTipoCredito,
	[PI],
	SP_Cubierta,
	SP_Expuesta,
	ECubierta,
	EExpuesta,
	ETotal,
	MontoGarantiaTotal,
	MontoGarantiaUsado,
	ReservaCubierta,
	ReservaExpuesta,
	ReservaTotal,
	PorcentajeReservaTotal,
	Calificacion
FROM dbo.RW_Analitico_CNR_Mensual
WHERE IdReporteLog = @IdReporteLog;
GO

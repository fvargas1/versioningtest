SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo18_MetodologiaDuplicada]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo18_MetodologiaDuplicada';

IF @Requerido = 1
BEGIN
UPDATE a18
SET errorCatalogo = 1
FROM dbo.FILE_Anexo18 a18
INNER JOIN (
 SELECT CodigoCliente FROM dbo.FILE_Anexo20
 UNION
 SELECT CodigoCliente FROM dbo.FILE_Anexo21
 UNION
 SELECT CodigoCliente FROM dbo.FILE_Anexo22
) anx ON LTRIM(a18.CodigoCliente) = LTRIM(anx.CodigoCliente);

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo18_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 a18.CodigoCliente,
 'CodigoCliente',
 a18.CodigoCliente,
 2,
 @Detalle + 'Anexo20'
FROM dbo.FILE_Anexo18 a18
INNER JOIN dbo.FILE_Anexo20 a20 ON LTRIM(a18.CodigoCliente) = LTRIM(a20.CodigoCliente);


INSERT INTO dbo.FILE_Anexo18_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 a18.CodigoCliente,
 'CodigoCliente',
 a18.CodigoCliente,
 2,
 @Detalle + 'Anexo21'
FROM dbo.FILE_Anexo18 a18
INNER JOIN dbo.FILE_Anexo21 a21 ON LTRIM(a18.CodigoCliente) = LTRIM(a21.CodigoCliente);


INSERT INTO dbo.FILE_Anexo18_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 a18.CodigoCliente,
 'CodigoCliente',
 a18.CodigoCliente,
 2,
 @Detalle + 'Anexo22'
FROM dbo.FILE_Anexo18 a18
INNER JOIN dbo.FILE_Anexo22 a22 ON LTRIM(a18.CodigoCliente) = LTRIM(a22.CodigoCliente);
GO

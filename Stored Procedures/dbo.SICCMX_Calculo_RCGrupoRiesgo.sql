SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_RCGrupoRiesgo]
AS
UPDATE inf
SET IdRCGrupoRiesgo = tp.IdRCGrupoRiesgo
FROM dbo.SICCMX_PersonaInfo inf
INNER JOIN dbo.SICC_TipoPersona tp ON inf.IdTipoPersona = tp.IdTipoPersona
WHERE inf.IdRCGrupoRiesgo IS NULL;
GO

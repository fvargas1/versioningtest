SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_ActualizacionMasiva_Select]
	@sessionID VARCHAR(100)
AS
SELECT
	vw.sessionID,
	vw.Id,
	vw.codigoBusqueda,
	vw.campo1Usuario,
	vw.campo1Original,
	vw.campo2Usuario,
	vw.campo2Original,
	vw.campo3Usuario,
	vw.campo3Original,
	vw.valorEncontrado,
	vw.seActualiza
FROM dbo.MIGRACION_ActualizacionMasiva_Temp vw
WHERE vw.sessionID = @sessionID AND vw.valorEncontrado = 1 AND vw.seActualiza = 0 AND vw.error IS NULL;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Cofinavit_Reservas]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Cofinavit_Reservas);

DELETE FROM Historico.RW_Cofinavit_Reservas WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Cofinavit_Reservas (
	IdPeriodoHistorico, Codigo, SaldoCapitalVigente, InteresVigente, SaldoCapitalVencido, InteresVencido, TipoRegimen, Constante, FactorATR, ATR,
	FactorPorpago, PorPago, FactorPromRete, PromRete, FactorMaxATR, MaxATR, [PI], SP, E, Reserva, PorReserva, Calificacion
)
SELECT
	@IdPeriodoHistorico,
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	TipoRegimen,
	Constante,
	FactorATR,
	ATR,
	FactorPorpago,
	PorPago,
	FactorPromRete,
	PromRete,
	FactorMaxATR,
	MaxATR,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion
FROM dbo.RW_Cofinavit_Reservas
WHERE IdReporteLog = @IdReporteLog;
GO

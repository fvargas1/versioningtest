SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_029]
AS

BEGIN

-- La Periodicidad de Pagos Capital debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.PeriodicidadPagosCapital
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalMA cat ON ISNULL(r.PeriodicidadPagosCapital,'') = cat.CodigoCNBV
WHERE cat.IdPeriodicidadCapital IS NULL;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculaExposicion_NMC]
AS
-- Calculamos la exposición del credito
UPDATE crvar
SET EI_Total = ISNULL(cr.MontoValorizado, 0)
FROM dbo.SICCMX_Credito_Reservas_Variables crvar
INNER JOIN dbo.SICCMX_VW_Credito_NMC cr ON crvar.IdCredito = cr.IdCredito;


--Insertamos en en log
INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 IdCredito,
 GETDATE(),
 '',
 'La Exposición del Crédito es de $ '
 + CONVERT(VARCHAR, CONVERT(MONEY, EI_Total), 1)
 + ' después de sumar los saldos de capital e intereses.'
FROM dbo.SICCMX_Credito_Reservas_Variables;


--Calculamos el para creditos pertenecientes a lineas irrevocables
UPDATE crv
SET EI_Total = crv.EI_Total * (
 CASE WHEN ISNULL(cre.MontoLineaCredito, 0) = 0 OR ISNULL(montoLinea.Monto,0) = 0 THEN 1
 ELSE dbo.MAX2VAR(1, POWER(montoLinea.Monto / cre.MontoLineaCredito, -0.5794)) END)
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON cre.IdCredito = crv.IdCredito
INNER JOIN SICCMX_LineaCredito_SaldoCreditos montoLinea ON montoLinea.IdLineaCredito = cre.IdLineaCredito
WHERE cre.TipoLinea = '2'; -- Linea Irrevocable


INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 crv.IdCredito,
 GETDATE(),
 '',
 'Se ajusta la exposición para créditos irrevocables según la formula '
 + '"SaldoCredito * MAX [ POWER( SumaSaldosPorLinea / MontoLineaAutorizado, -0.5794), 100% ] '
 + ' Considerando SumaSaldosPorLinea = '
 + CONVERT(VARCHAR, CONVERT(MONEY, montoLinea.Monto), 1)
 + ' MontoLineaAutorizado = '
 + CONVERT(VARCHAR, CONVERT(MONEY, cre.MontoLineaCredito), 1)
 + ' dando como resultado una exposición de $ '
 + CONVERT(VARCHAR, CONVERT(MONEY, crv.EI_Total), 1)
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON cre.IdCredito = crv.IdCredito
INNER JOIN SICCMX_LineaCredito_SaldoCreditos montoLinea ON montoLinea.IdLineaCredito = cre.IdLineaCredito
WHERE cre.TipoLinea = '2'; -- Linea Irrevocable
GO

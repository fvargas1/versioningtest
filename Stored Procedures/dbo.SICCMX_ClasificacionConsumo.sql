SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ClasificacionConsumo]
AS
DECLARE @IdMetodologia INT;

-- CONSUMO SEMANAL, QUINCENAL O MENSUAL
UPDATE pre
SET IdMetodologia = perCal.IdMetodologia
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_Consumo con ON pre.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICC_PeriodicidadCalificacionConsumo perCal ON con.IdPeriodicidadCalificacion = perCal.IdPeriodicidadCalificacionConsumo;

-- CONSUMO NO REVOLVENTE GRUPAL
SELECT @IdMetodologia = IdMetodologiaConsumo FROM dbo.SICCMX_Consumo_Metodologia WHERE Codigo='4';
UPDATE pre
SET IdMetodologia = @IdMetodologia
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_ConsumoInfo info ON pre.IdConsumo = info.IdConsumo
WHERE LEN(info.CodigoAgrupacion) > 0;

-- CONSUMO REVOLVENTE
SELECT @IdMetodologia = IdMetodologiaConsumo FROM dbo.SICCMX_Consumo_Metodologia WHERE Codigo='5';
UPDATE pre
SET IdMetodologia = @IdMetodologia
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_Consumo con ON pre.IdConsumo = con.IdConsumo
WHERE con.EsRevolvente = 1;
GO

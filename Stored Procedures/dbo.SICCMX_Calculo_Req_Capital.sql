SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_Req_Capital]
AS
UPDATE dbo.SICCMX_CreditoInfo
SET ReqCapital = ExposicionNeta * PonderadorRiesgo * 0.08;
GO

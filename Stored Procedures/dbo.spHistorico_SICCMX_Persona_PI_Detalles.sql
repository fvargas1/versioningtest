SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Persona_PI_Detalles]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_Persona_PI_Detalles WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Persona_PI_Detalles (
	IdPeriodoHistorico,
	Persona,
	Variable,
	ValorActual,
	PuntosActual,
	ValorH1,
	PuntosH1,
	ValorH2,
	PuntosH2,
	ValorH3,
	PuntosH3,
	ValorH4,
	PuntosH4,
	FechaValor,
	FechaMigracion,
	UsuarioMigracion,
	FechaCalculo,
	UsuarioCalculo
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	per.Codigo, -- Persona
	vbs.Codigo, -- Variable
	ppi.ValorActual,
	ppi.PuntosActual,
	ppi.ValorH1,
	ppi.PuntosH1,
	ppi.ValorH2,
	ppi.PuntosH2,
	ppi.ValorH3,
	ppi.PuntosH3,
	ppi.ValorH4,
	ppi.PuntosH4,
	ppi.FechaValor,
	ppi.FechaMigracion,
	ppi.UsuarioMigracion,
	ppi.FechaCalculo,
	ppi.UsuarioCalculo
FROM dbo.SICCMX_Persona_PI_Detalles ppi
INNER JOIN dbo.SICCMX_Persona per ON ppi.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PI_Variables vbs ON ppi.IdVariable = vbs.Id;
GO

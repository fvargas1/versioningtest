SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_015_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "DESTINO DEL CRÉDITO" debe ser un código valido del catálogo disponible en el SITI.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.IdReporteLog)
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_DestinoHipotecario cat ON ISNULL(r.DestinoCredito,'') = cat.CodigoCNBV
WHERE cat.IdDestinoHipotecario IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_HipotecarioGarantia_Back_Hst]
AS

INSERT INTO dbo.FILE_HipotecarioGarantia_Hst (
	CodigoGarantia,
	CodigoCredito
)
SELECT
	hg.CodigoGarantia,
	hg.CodigoCredito
FROM dbo.FILE_HipotecarioGarantia hg
LEFT OUTER JOIN dbo.FILE_HipotecarioGarantia_Hst hst ON LTRIM(hg.CodigoGarantia) = LTRIM(hst.CodigoGarantia) AND LTRIM(hg.CodigoCredito) = LTRIM(hst.CodigoCredito)
WHERE hst.CodigoGarantia IS NULL;
GO

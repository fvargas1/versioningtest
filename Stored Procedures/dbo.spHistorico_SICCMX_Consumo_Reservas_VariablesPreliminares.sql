SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Consumo_Reservas_VariablesPreliminares]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Consumo_Reservas_VariablesPreliminares WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Consumo_Reservas_VariablesPreliminares (
 IdPeriodoHistorico,
 Consumo,
 Metodologia,
 NumeroIntegrantes,
 Ciclos,
 LimiteCredito,
 SaldoFavor,
 PorcentajePagoRealizado,
 ATR,
 MaxATR,
 INDATR,
 VECES,
 Impago,
 NumeroImpagosConsecutivos,
 NumeroImpagosHistoricos,
 MesesTranscurridos,
 PorPago,
 PorSDOIMP,
 PorPR,
 PorcentajeUso,
 TarjetaActiva,
 Alto,
 Medio,
 Bajo
)
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 con.Codigo,
 met.Codigo,
 pre.NumeroIntegrantes,
 pre.Ciclos,
 pre.LimiteCredito,
 pre.SaldoFavor,
 pre.PorcentajePagoRealizado,
 pre.ATR,
 pre.MaxATR,
 pre.INDATR,
 pre.VECES,
 pre.Impago,
 pre.NumeroImpagosConsecutivos,
 pre.NumeroImpagosHistoricos,
 pre.MesesTranscurridos,
 pre.PorPago,
 pre.PorSDOIMP,
 pre.PorPR,
 pre.PorcentajeUso,
 pre.TarjetaActiva,
 pre.Alto,
 pre.Medio,
 pre.Bajo
FROM dbo.SICCMX_Consumo con
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON con.IdConsumo = pre.IdConsumo
LEFT OUTER JOIN dbo.SICCMX_Consumo_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaConsumo;
GO

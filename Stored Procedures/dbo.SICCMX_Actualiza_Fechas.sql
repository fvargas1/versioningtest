SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Actualiza_Fechas]
AS
UPDATE dbo.SICCMX_Anexo20
SET F_NumDiasMoraPromInstBanc = ISNULL(F_NumDiasMoraPromInstBanc,FechaInfoBuro),
F_PorPagoTiemInstBanc = ISNULL(F_PorPagoTiemInstBanc,FechaInfoBuro),
F_PorPagoTiemInstNoBanc = ISNULL(F_PorPagoTiemInstNoBanc,FechaInfoBuro),
F_EmiTitDeuPub = ISNULL(F_EmiTitDeuPub,FechaInfoFinanc),
F_PropPasLargoPlazo = ISNULL(F_PropPasLargoPlazo,FechaInfoFinanc),
F_RendCapitalROE = ISNULL(F_RendCapitalROE,FechaInfoFinanc),
F_IndCapitalizacion = ISNULL(F_IndCapitalizacion,FechaInfoFinanc),
F_GastoAdmonPromoIngTot = ISNULL(F_GastoAdmonPromoIngTot,FechaInfoFinanc),
F_CartVencReservCC = ISNULL(F_CartVencReservCC,FechaInfoFinanc),
F_MargenFinancieroAjustado = ISNULL(F_MargenFinancieroAjustado,FechaInfoFinanc),
F_MonTotPagInfonavitUltBimestre = ISNULL(F_MonTotPagInfonavitUltBimestre,FechaInfoFinanc),
F_NumDiasAtrInfonavitUltBimestre = ISNULL(F_NumDiasAtrInfonavitUltBimestre,FechaInfoFinanc),
F_Solvencia = ISNULL(F_Solvencia,FechaInfoFinanc),
F_Liquidez = ISNULL(F_Liquidez,FechaInfoFinanc),
F_Eficiencia = ISNULL(F_Eficiencia,FechaInfoFinanc),
F_DivLinNeg = ISNULL(F_DivLinNeg,FechaInfoFinanc),
F_DivTipFueFin = ISNULL(F_DivTipFueFin,FechaInfoFinanc),
F_ConcentracionActivos = ISNULL(F_ConcentracionActivos,FechaInfoFinanc),
F_IndepConsejoAdmon = ISNULL(F_IndepConsejoAdmon,FechaInfoFinanc),
F_CompAccionaria = ISNULL(F_CompAccionaria,FechaInfoFinanc),
F_CalGobCorp = ISNULL(F_CalGobCorp,FechaInfoFinanc),
F_ExpFunAdmon = ISNULL(F_ExpFunAdmon,FechaInfoFinanc),
F_ExpPolProc = ISNULL(F_ExpPolProc,FechaInfoFinanc),
F_EdoFinAudit = ISNULL(F_EdoFinAudit,FechaInfoFinanc);


UPDATE dbo.SICCMX_Anexo20_GP
SET F_NumDiasMoraPromInstBanc = ISNULL(F_NumDiasMoraPromInstBanc,FechaInfoBuro),
F_PorPagoTiemInstBanc = ISNULL(F_PorPagoTiemInstBanc,FechaInfoBuro),
F_PorPagoTiemInstNoBanc = ISNULL(F_PorPagoTiemInstNoBanc,FechaInfoBuro),
F_EmiTitDeuPub = ISNULL(F_EmiTitDeuPub,FechaInfoFinanc),
F_PropPasLargoPlazo = ISNULL(F_PropPasLargoPlazo,FechaInfoFinanc),
F_RendCapitalROE = ISNULL(F_RendCapitalROE,FechaInfoFinanc),
F_IndCapitalizacion = ISNULL(F_IndCapitalizacion,FechaInfoFinanc),
F_GastoAdmonPromoIngTot = ISNULL(F_GastoAdmonPromoIngTot,FechaInfoFinanc),
F_CartVencReservCC = ISNULL(F_CartVencReservCC,FechaInfoFinanc),
F_MargenFinancieroAjustado = ISNULL(F_MargenFinancieroAjustado,FechaInfoFinanc),
F_MonTotPagInfonavitUltBimestre = ISNULL(F_MonTotPagInfonavitUltBimestre,FechaInfoFinanc),
F_NumDiasAtrInfonavitUltBimestre = ISNULL(F_NumDiasAtrInfonavitUltBimestre,FechaInfoFinanc),
F_Solvencia = ISNULL(F_Solvencia,FechaInfoFinanc),
F_Liquidez = ISNULL(F_Liquidez,FechaInfoFinanc),
F_Eficiencia = ISNULL(F_Eficiencia,FechaInfoFinanc),
F_DivLinNeg = ISNULL(F_DivLinNeg,FechaInfoFinanc),
F_DivTipFueFin = ISNULL(F_DivTipFueFin,FechaInfoFinanc),
F_ConcentracionActivos = ISNULL(F_ConcentracionActivos,FechaInfoFinanc),
F_IndepConsejoAdmon = ISNULL(F_IndepConsejoAdmon,FechaInfoFinanc),
F_CompAccionaria = ISNULL(F_CompAccionaria,FechaInfoFinanc),
F_CalGobCorp = ISNULL(F_CalGobCorp,FechaInfoFinanc),
F_ExpFunAdmon = ISNULL(F_ExpFunAdmon,FechaInfoFinanc),
F_ExpPolProc = ISNULL(F_ExpPolProc,FechaInfoFinanc),
F_EdoFinAudit = ISNULL(F_EdoFinAudit,FechaInfoFinanc);


UPDATE dbo.SICCMX_Anexo21
SET F_AntSocInfCredCredito = ISNULL(F_AntSocInfCredCredito,FechaInfoBuro),
F_QuitasCastRest = ISNULL(F_QuitasCastRest,FechaInfoBuro),
F_PorPagoTiemInstNoBanc = ISNULL(F_PorPagoTiemInstNoBanc,FechaInfoBuro),
F_PorPagEntCom60oMasAtraso = ISNULL(F_PorPagEntCom60oMasAtraso,FechaInfoBuro),
F_CuenCredAbierInstBancUlt12mes = ISNULL(F_CuenCredAbierInstBancUlt12mes,FechaInfoBuro),
F_MonMaxCredOtorInstBanExpUDIS = ISNULL(F_MonMaxCredOtorInstBanExpUDIS,FechaInfoBuro),
F_NumMesUltCredAbiUlt12Meses = ISNULL(F_NumMesUltCredAbiUlt12Meses,FechaInfoBuro),
F_PorPagIntBan60oMasAtraso = ISNULL(F_PorPagIntBan60oMasAtraso,FechaInfoBuro),
F_PorPagIntBan1a29Atraso = ISNULL(F_PorPagIntBan1a29Atraso,FechaInfoBuro),
F_PorPagIntBan90oMasAtraso = ISNULL(F_PorPagIntBan90oMasAtraso,FechaInfoBuro),
F_NumDiasMoraPromInstBanc = ISNULL(F_NumDiasMoraPromInstBanc,FechaInfoBuro),
F_NumPagTiemIntBanUlt12Meses = ISNULL(F_NumPagTiemIntBanUlt12Meses,FechaInfoBuro),
F_MonTotPagInfonavitUltBimestre = ISNULL(F_MonTotPagInfonavitUltBimestre,FechaInfoFinanc),
F_NumDiasAtrInfonavitUltBimestre = ISNULL(F_NumDiasAtrInfonavitUltBimestre,FechaInfoFinanc),
F_TasaRetLab = ISNULL(F_TasaRetLab,FechaInfoFinanc);


UPDATE dbo.SICCMX_Anexo21_GP
SET F_AntSocInfCredCredito = ISNULL(F_AntSocInfCredCredito,FechaInfoBuro),
F_QuitasCastRest = ISNULL(F_QuitasCastRest,FechaInfoBuro),
F_PorPagoTiemInstNoBanc = ISNULL(F_PorPagoTiemInstNoBanc,FechaInfoBuro),
F_PorPagEntCom60oMasAtraso = ISNULL(F_PorPagEntCom60oMasAtraso,FechaInfoBuro),
F_CuenCredAbierInstBancUlt12mes = ISNULL(F_CuenCredAbierInstBancUlt12mes,FechaInfoBuro),
F_MonMaxCredOtorInstBanExpUDIS = ISNULL(F_MonMaxCredOtorInstBanExpUDIS,FechaInfoBuro),
F_NumMesUltCredAbiUlt12Meses = ISNULL(F_NumMesUltCredAbiUlt12Meses,FechaInfoBuro),
F_PorPagIntBan60oMasAtraso = ISNULL(F_PorPagIntBan60oMasAtraso,FechaInfoBuro),
F_PorPagIntBan1a29Atraso = ISNULL(F_PorPagIntBan1a29Atraso,FechaInfoBuro),
F_PorPagIntBan90oMasAtraso = ISNULL(F_PorPagIntBan90oMasAtraso,FechaInfoBuro),
F_NumDiasMoraPromInstBanc = ISNULL(F_NumDiasMoraPromInstBanc,FechaInfoBuro),
F_NumPagTiemIntBanUlt12Meses = ISNULL(F_NumPagTiemIntBanUlt12Meses,FechaInfoBuro),
F_MonTotPagInfonavitUltBimestre = ISNULL(F_MonTotPagInfonavitUltBimestre,FechaInfoFinanc),
F_NumDiasAtrInfonavitUltBimestre = ISNULL(F_NumDiasAtrInfonavitUltBimestre,FechaInfoFinanc),
F_TasaRetLab = ISNULL(F_TasaRetLab,FechaInfoFinanc);


UPDATE dbo.SICCMX_Anexo22
SET F_NumDiasMoraPromInstBanc = ISNULL(F_NumDiasMoraPromInstBanc,FechaInfoBuro),
F_PorPagoTiemInstBanc = ISNULL(F_PorPagoTiemInstBanc,FechaInfoBuro),
F_NumInstRepUlt12Meses = ISNULL(F_NumInstRepUlt12Meses,FechaInfoBuro),
F_PorPagoTiemInstNoBanc = ISNULL(F_PorPagoTiemInstNoBanc,FechaInfoBuro),
F_MonTotPagInfonavitUltBimestre = ISNULL(F_MonTotPagInfonavitUltBimestre,FechaInfoFinanc),
F_NumDiasAtrInfonavitUltBimestre = ISNULL(F_NumDiasAtrInfonavitUltBimestre,FechaInfoFinanc),
F_TasaRetLab = ISNULL(F_TasaRetLab,FechaInfoFinanc),
F_RotActTotales = ISNULL(F_RotActTotales,FechaInfoFinanc),
F_RotCapTrabajo = ISNULL(F_RotCapTrabajo,FechaInfoFinanc),
F_RendimeintosCapitalROE = ISNULL(F_RendimeintosCapitalROE,FechaInfoFinanc),
F_IndCalEstaEcono = ISNULL(F_IndCalEstaEcono,FechaInfoFinanc),
F_IntCarCompetencia = ISNULL(F_IntCarCompetencia,FechaInfoFinanc),
F_Provedores = ISNULL(F_Provedores,FechaInfoFinanc),
F_Clientes = ISNULL(F_Clientes,FechaInfoFinanc),
F_EdoFinAudit = ISNULL(F_EdoFinAudit,FechaInfoFinanc),
F_NumAgeCalf = ISNULL(F_NumAgeCalf,FechaInfoFinanc),
F_IndConsAdmon = ISNULL(F_IndConsAdmon,FechaInfoFinanc),
F_EstrucOrg = ISNULL(F_EstrucOrg,FechaInfoFinanc),
F_CompAccionaria = ISNULL(F_CompAccionaria,FechaInfoFinanc),
F_LiquidezOper = ISNULL(F_LiquidezOper,FechaInfoFinanc),
F_UAFIR_GastosFin = ISNULL(F_UAFIR_GastosFin,FechaInfoFinanc);


UPDATE dbo.SICCMX_Anexo22_GP
SET F_NumDiasMoraPromInstBanc = ISNULL(F_NumDiasMoraPromInstBanc,FechaInfoBuro),
F_PorPagoTiemInstBanc = ISNULL(F_PorPagoTiemInstBanc,FechaInfoBuro),
F_NumInstRepUlt12Meses = ISNULL(F_NumInstRepUlt12Meses,FechaInfoBuro),
F_PorPagoTiemInstNoBanc = ISNULL(F_PorPagoTiemInstNoBanc,FechaInfoBuro),
F_MonTotPagInfonavitUltBimestre = ISNULL(F_MonTotPagInfonavitUltBimestre,FechaInfoFinanc),
F_NumDiasAtrInfonavitUltBimestre = ISNULL(F_NumDiasAtrInfonavitUltBimestre,FechaInfoFinanc),
F_TasaRetLab = ISNULL(F_TasaRetLab,FechaInfoFinanc),
F_RotActTotales = ISNULL(F_RotActTotales,FechaInfoFinanc),
F_RotCapTrabajo = ISNULL(F_RotCapTrabajo,FechaInfoFinanc),
F_RendimeintosCapitalROE = ISNULL(F_RendimeintosCapitalROE,FechaInfoFinanc),
F_IndCalEstaEcono = ISNULL(F_IndCalEstaEcono,FechaInfoFinanc),
F_IntCarCompetencia = ISNULL(F_IntCarCompetencia,FechaInfoFinanc),
F_Provedores = ISNULL(F_Provedores,FechaInfoFinanc),
F_Clientes = ISNULL(F_Clientes,FechaInfoFinanc),
F_EdoFinAudit = ISNULL(F_EdoFinAudit,FechaInfoFinanc),
F_NumAgeCalf = ISNULL(F_NumAgeCalf,FechaInfoFinanc),
F_IndConsAdmon = ISNULL(F_IndConsAdmon,FechaInfoFinanc),
F_EstrucOrg = ISNULL(F_EstrucOrg,FechaInfoFinanc),
F_CompAccionaria = ISNULL(F_CompAccionaria,FechaInfoFinanc),
F_LiquidezOper = ISNULL(F_LiquidezOper,FechaInfoFinanc),
F_UAFIR_GastosFin = ISNULL(F_UAFIR_GastosFin,FechaInfoFinanc);
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_TipoProductoSerie4_2016]
	@filtro VARCHAR(50)
AS
SELECT DISTINCT
	cat.Codigo,
	cat.Nombre
FROM dbo.SICC_TipoProductoSerie4_2016 cat
WHERE ISNULL(cat.Codigo,'')+'|'+ISNULL(cat.Nombre,'') LIKE '%' + @filtro + '%'
ORDER BY cat.Codigo;
GO

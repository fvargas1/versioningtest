SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SI_Exec_Procesos_Migracion]
AS
SET NOCOUNT ON;
DECLARE @IdValidacion INT;
DECLARE @Codename VARCHAR(100);
DECLARE @AplicaUpd BIT;
DECLARE @CategoryName VARCHAR(50);
DECLARE @ClearLogCodename VARCHAR(50);
DECLARE @Tabla VARCHAR(50);
DECLARE @Descripcion VARCHAR(50);
DECLARE @Requerido BIT;

INSERT INTO dbo.MIGRACION_ProcesoLog (Categoria, Proceso, Descripcion, Fecha, Username) VALUES ('validacion', '', 'Se Inicio Proceso', GETDATE(), 'bajaware');

-- EJECUTAMOS PROCESOS DE INICILIZACION
DECLARE crsInit CURSOR FOR
SELECT Codename
FROM dbo.MIGRACION_Inicializador
WHERE Estatus=-1
ORDER BY Position;

OPEN crsInit;

FETCH NEXT FROM crsInit INTO @Codename;

WHILE @@FETCH_STATUS = 0
BEGIN

IF OBJECT_ID(@Codename, 'P') IS NOT NULL
BEGIN
 BEGIN TRY
 UPDATE dbo.MIGRACION_Inicializador SET FechaInicio=GETDATE(), Estatus=1 WHERE Codename = @Codename;
 EXEC @Codename;
 UPDATE dbo.MIGRACION_Inicializador SET FechaFin=GETDATE(), Estatus=0 WHERE Codename = @Codename;
 END TRY
 BEGIN CATCH
 END CATCH;
END
FETCH NEXT FROM crsInit INTO @Codename;

END;

CLOSE crsInit;
DEALLOCATE crsInit;


-- INICIALIZAMOS TABLA DE ERRORES
DECLARE crsErr CURSOR FOR
SELECT arc.ClearLogCodename
FROM dbo.MIGRACION_Archivo arc
WHERE arc.Activo=1 AND arc.ValidationStatus='-1'
ORDER BY arc.Position;

OPEN crsErr;

FETCH NEXT FROM crsErr INTO @ClearLogCodename;

WHILE @@FETCH_STATUS = 0
BEGIN

IF OBJECT_ID(@ClearLogCodename, 'P') IS NOT NULL
BEGIN
 BEGIN TRY
 EXEC @ClearLogCodename;
 END TRY
 BEGIN CATCH
 END CATCH;
END
FETCH NEXT FROM crsErr INTO @ClearLogCodename;

END;

CLOSE crsErr;
DEALLOCATE crsErr;


-- EJECUTAMOS PROCESOS DE MIGRACION
DECLARE crs CURSOR FOR
SELECT val.IdValidacion, val.Codename, val.Description, val.AplicaUpd, val.CategoryName, val.ReqCalificacion
FROM dbo.MIGRACION_Archivo arc
INNER JOIN dbo.MIGRACION_Validacion val ON arc.Codename = val.CategoryName
WHERE arc.Activo=1 AND val.Activo=1 AND val.Estatus=-1
ORDER BY arc.Position, val.Position;

OPEN crs;

FETCH NEXT FROM crs INTO @IdValidacion, @Codename, @Descripcion, @AplicaUpd, @CategoryName, @Requerido;

WHILE @@FETCH_STATUS = 0
BEGIN

 IF NOT EXISTS (SELECT * FROM dbo.MIGRACION_Validacion WHERE CategoryName=@CategoryName AND Activo=1 AND FechaInicio IS NOT NULL)
 BEGIN
 UPDATE dbo.MIGRACION_Archivo SET FechaInicio=GETDATE(), ValidationStatus=1 WHERE Codename=@CategoryName;
 END
 
 UPDATE dbo.MIGRACION_Validacion SET Estatus=1, FechaInicio=GETDATE() WHERE IdValidacion=@IdValidacion;
 
 BEGIN TRY
 INSERT INTO dbo.MIGRACION_ProcesoLog (Categoria, Proceso, Descripcion, Fecha, Username) VALUES ('validacion', @Codename, @Descripcion, GETDATE(), 'bajaware');
 EXEC @Codename;
 IF @@ROWCOUNT > 0 AND @AplicaUpd = 0
 BEGIN
 IF @Requerido = 1
 BEGIN
 UPDATE dbo.MIGRACION_Validacion SET Estatus=3, FechaFin=GETDATE() WHERE IdValidacion=@IdValidacion;
 END
 ELSE
 BEGIN
 UPDATE dbo.MIGRACION_Validacion SET Estatus=2, FechaFin=GETDATE() WHERE IdValidacion=@IdValidacion;
 END
 END
 ELSE
 BEGIN
 UPDATE dbo.MIGRACION_Validacion SET Estatus=0, FechaFin=GETDATE() WHERE IdValidacion=@IdValidacion;
 END
 END TRY
 BEGIN CATCH
 UPDATE dbo.MIGRACION_Validacion SET Estatus=4, FechaFin=GETDATE(), ErrorDetalle=ERROR_MESSAGE() WHERE IdValidacion=@IdValidacion;
 END CATCH;
 
 IF NOT EXISTS (SELECT * FROM dbo.MIGRACION_Validacion WHERE CategoryName=@CategoryName AND Activo=1 AND FechaFin IS NULL)
 BEGIN
 UPDATE dbo.MIGRACION_Archivo SET FechaFin=GETDATE() WHERE Codename=@CategoryName;
 UPDATE dbo.MIGRACION_Archivo SET ValidationStatus = (SELECT MAX(p.Estatus) FROM dbo.MIGRACION_Validacion p WHERE p.CategoryName=@CategoryName AND p.Activo=1) WHERE dbo.MIGRACION_Archivo.Codename = @CategoryName;
 END

 FETCH NEXT FROM crs INTO @IdValidacion, @Codename, @Descripcion, @AplicaUpd, @CategoryName, @Requerido;

END;

CLOSE crs;
DEALLOCATE crs;


-- ACTUALIZAMOS TOTAL DE ERRORES EN MIGRACION
DECLARE @sql VARCHAR(400);

DECLARE crsMig CURSOR FOR
SELECT arc.ViewName
FROM dbo.MIGRACION_Archivo arc
WHERE arc.Activo=1 AND arc.ValidationStatus IN ('2','3','4')
ORDER BY arc.Position;

OPEN crsMig;

FETCH NEXT FROM crsMig INTO @Tabla;

WHILE @@FETCH_STATUS = 0
BEGIN

IF OBJECT_ID(@Tabla, 'U') IS NOT NULL
BEGIN
 BEGIN TRY
 SET @sql = 'UPDATE dbo.MIGRACION_Archivo SET ErroresMigracion = (SELECT COUNT(ISNULL(errorCatalogo,0)) FROM dbo.' + @Tabla + ' WHERE ISNULL(errorCatalogo,0)=1 OR ISNULL(errorFormato,0)=1) WHERE ViewName=''' + @Tabla + '''';
 EXEC (@sql);
 END TRY
 BEGIN CATCH
 END CATCH;
END
FETCH NEXT FROM crsMig INTO @Tabla;

END;

INSERT INTO dbo.MIGRACION_ProcesoLog (Categoria, Proceso, Descripcion, Fecha, Username) VALUES ('validacion', '', 'Se Termino Proceso', GETDATE(), 'bajaware');

CLOSE crsMig;
DEALLOCATE crsMig;
GO

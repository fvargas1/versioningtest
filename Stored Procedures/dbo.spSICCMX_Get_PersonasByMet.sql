SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Get_PersonasByMet]
 @idMetodologia INT,
 @keywords VARCHAR(100)
AS
SELECT DISTINCT
 per.IdPersona AS Id,
 per.Codigo,
 per.Nombre AS DESCRIPTION,
 cal.Codigo AS Calificacion,
 cal.Color AS Color
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaCalificacion pc ON per.IdPersona = pc.IdPersona
INNER JOIN dbo.SICCMX_Credito cre ON pc.IdPersona = cre.IdPersona
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON pc.IdCalificacion = cal.IdCalificacion
WHERE cre.IdMetodologia = @idMetodologia AND per.Codigo + '|' + per.Nombre LIKE '%' + @keywords + '%'
ORDER BY per.Nombre;
GO

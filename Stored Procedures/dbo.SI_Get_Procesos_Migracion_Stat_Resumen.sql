SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SI_Get_Procesos_Migracion_Stat_Resumen]
AS
SELECT MAX(MaxNivel) AS MaxNivel, MIN(Estatus) AS Estatus FROM dbo.SICCMX_VW_ProcesosMigracion;

SELECT
 Codigo,
 Nombre,
 Estatus,
 CONVERT(VARCHAR(20), FechaInicio, 120) AS FechaInicio,
 CONVERT(VARCHAR(20), FechaFin, 120) AS FechaFin,
 Nivel,
 Progreso
FROM dbo.SICCMX_VW_ProcesosMigracion
ORDER BY Position

SELECT
 CONVERT(VARCHAR(20), MIN(FechaInicio), 120) AS FechaInicio,
 CONVERT(VARCHAR(20), MAX(FechaFin), 120) AS FechaFin,
 MAX(Estatus) AS Estatus,
 CASE WHEN (SELECT CAST(COUNT(pr.IdInicializador) AS DECIMAL(5,2)) FROM dbo.MIGRACION_Inicializador pr) = 0 THEN 0
 ELSE
 CAST(
 (SELECT CAST(COUNT(pr.IdInicializador) AS DECIMAL(5,2)) FROM dbo.MIGRACION_Inicializador pr WHERE pr.FechaFin IS NOT NULL) /
 (SELECT CAST(COUNT(pr.IdInicializador) AS DECIMAL(5,2)) FROM dbo.MIGRACION_Inicializador pr)
 * 100 AS INT) END AS Progreso
FROM dbo.MIGRACION_Inicializador
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_008_Count]
 @IdInconsistencia BIGINT
AS
BEGIN
-- Validar que la Localidad del Garante corresponda a Catalogo de CNBV.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(DISTINCT r50.CodigoCredito)
FROM dbo.RW_VW_R04C0450_INC r50
LEFT OUTER JOIN dbo.SICC_Municipio mun ON r50.Localidad = mun.CodigoCNBV
WHERE mun.IdMunicipio IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_Revolvente_Garantia_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'CONSUMO' AND Nombre = '-Revolvente_Garantia';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_Consumo_Revolvente_Garantia;

-- Informacion para reporte
INSERT INTO dbo.RW_Consumo_Revolvente_Garantia (
	IdReporteLog, FolioCredito, Garantia, GarantiaReal, ImporteGarantia, PI_Garante, SP, SP_SG, Hc, Hfx, PrctCobertura, Exposicion, EI_Ajustada,
	Reservas, ClaveGarante, RUGM, IdPortafolio
)
SELECT DISTINCT
	@IdReporteLog,
	inf.CreditoCodificado AS FolioCredito,
	ISNULL(tg.CodigoCR,'0') AS Garantia,
	ISNULL(tg.CodigoCRReal,'0') AS GarantiaReal,
	CAST(ISNULL(cg.MontoUsadoGarantia,0) AS DECIMAL(23,2)) AS ImporteGarantia,
	CAST(CASE WHEN tg.CodigoCR = '3' THEN ISNULL(can.[PI],0) * 100 ELSE 0 END AS DECIMAL(5,2)) AS PI_Garante,
	CAST(ISNULL(can.SeveridadCorresp,0) * 100 AS DECIMAL(5,2)) AS SP,
	CAST(ISNULL(canEXP.SeveridadCorresp,0) * 100 AS DECIMAL(5,2)) AS SP_SG,
	CAST(ISNULL(can.Hc,0) AS DECIMAL(5,2)) AS Hc,
	CAST(ISNULL(can.Hfx,0) AS DECIMAL(5,2)) AS Hfx,
	CAST(CASE WHEN tg.Codigo = 'NMC-05' THEN can.PrctCobSinAju * 100 ELSE 0 END AS DECIMAL(10,2)) AS PrctCobertura,
	CAST(ISNULL(crv.E,0) AS DECIMAL(23,2)) AS Exposicion,
	CAST(ISNULL(can.EI_Ajust,0) AS DECIMAL(23,2)) AS EI_Ajustada,
	CAST(ISNULL(can.Reserva,0) AS DECIMAL(23,2)) AS Reservas,
	ISNULL(gp.Codigo,'0') AS ClaveGarante,
	'0' AS RUGM,
	CASE WHEN gp.Tipo IN ('NMC-04','NMC-04-C') THEN ISNULL(gp.Codigo,'0') ELSE '0' END AS IdPortafolio
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_ConsumoInfo inf ON con.IdConsumo = inf.IdConsumo
INNER JOIN dbo.SICCMX_ConsumoGarantia cg ON inf.IdConsumo = cg.IdConsumo
INNER JOIN dbo.SICCMX_Garantia_Consumo gar ON cg.IdGarantia = gar.IdGarantia
INNER JOIN dbo.SICC_TipoGarantia tg ON gar.IdTipoGarantia = tg.IdTipoGarantia
INNER JOIN dbo.SICCMX_Garantia_Canasta_Consumo can ON con.IdConsumo = can.IdConsumo AND CASE WHEN tg.CodigoCR = '1' THEN 0 ELSE tg.IdTipoGarantia END = ISNULL(can.IdTipoGarantia,0) AND can.EsDescubierto IS NULL
INNER JOIN dbo.SICCMX_Garantia_Canasta_Consumo canEXP ON can.IdConsumo = canEXP.IdConsumo AND canEXP.EsDescubierto = 1
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON con.IdConsumo = crv.IdConsumo
LEFT OUTER JOIN dbo.SICCMX_VW_Consumo_GarPer gp ON con.IdConsumo = gp.IdConsumo AND gar.IdTipoGarantia = gp.IdTipoGarantia
WHERE con.Revolvente = 1

UNION ALL
SELECT
	@IdReporteLog,
	inf.CreditoCodificado AS FolioCredito,
	'0' AS Garantia,
	'0' AS GarantiaReal,
	0.00 AS ImporteGarantia,
	0.00 AS PI_Garante,
	CAST(ISNULL(can.SeveridadCorresp,0) * 100 AS DECIMAL(5,2)) AS SP,
	0.00 AS SP_SG,
	CAST(ISNULL(can.Hc,0) AS DECIMAL(5,2)) AS Hc,
	CAST(ISNULL(can.Hfx,0) AS DECIMAL(5,2)) AS Hfx,
	0.00 AS PrctCobertura,
	CAST(ISNULL(crv.E,0) AS DECIMAL(23,2)) AS Exposicion,
	CAST(ISNULL(can.EI_Ajust,0) AS DECIMAL(23,2)) AS EI_Ajustada,
	CAST(ISNULL(can.Reserva,0) AS DECIMAL(23,2)) AS Reservas,
	'0' AS ClaveGarante,
	'0' AS RUGM,
	'0' AS IdPortafolio
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_ConsumoInfo inf ON con.IdConsumo = inf.IdConsumo
INNER JOIN dbo.SICCMX_Garantia_Canasta_Consumo can ON inf.IdConsumo = can.IdConsumo AND can.EsDescubierto = 1
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON con.IdConsumo = crv.IdConsumo
WHERE con.Revolvente = 1;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Consumo_Revolvente_Garantia WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = GETDATE(),
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO

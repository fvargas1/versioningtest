SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_Hc]
AS
DECLARE @FechaPeriodo DATETIME;
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo=1;

-- Inicializamos el Factor Hc para todas las garantias
UPDATE dbo.SICCMX_Garantia 
SET Hc=NULL;

-- Calculamos el Factor Hc para las garantias en efectivo
UPDATE g
SET Hc=0
FROM dbo.SICCMX_Garantia g
INNER JOIN dbo.SICC_TipoGarantia tg ON g.IdTipoGarantia = tg.IdTipoGarantia
WHERE tg.EsEfectivo = 1;

-- Calculamos el Factor Hc para Garantias con Grado de Riesgo
UPDATE gar
SET Hc = graj.Factor
FROM dbo.SICCMX_Garantia gar
INNER JOIN dbo.SICCMX_GradoRiesgo_Ajuste graj ON gar.IdGradoRiesgo = graj.GradoRiesgo AND gar.IdEmisor = graj.Emisor
WHERE CONVERT(DECIMAL(18,6), DATEDIFF(DAY, @FechaPeriodo, gar.VencimientoRestante)) / CONVERT(DECIMAL(18,6), 365) BETWEEN graj.RangoMenor AND graj.RangoMayor
AND gar.Hc IS NULL;

-- Calculamos el Factor Hc para Garantias con IPC
UPDATE dbo.SICCMX_Garantia
SET Hc = .15
WHERE ISNULL(EsIPC, 0) = 1 AND Hc IS NULL;

-- Calculamos el Factor Hc para Garantias diferentes a los anteriores
UPDATE dbo.SICCMX_Garantia
SET Hc = .25
WHERE Hc IS NULL;
GO

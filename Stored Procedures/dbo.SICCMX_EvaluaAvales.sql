SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_EvaluaAvales]
AS

TRUNCATE TABLE dbo.SICCMX_Avales_Log;
UPDATE dbo.SICCMX_Aval SET Aplica=1;
UPDATE dbo.SICCMX_CreditoAval SET Aplica=0;
UPDATE dbo.SICCMX_Credito_Reservas_Variables SET PIAval=0;


-- NO APLICAN AVALES SIN CALIFICACION Y QUE NO SEAN OBLIGADO SOLIDARIO
UPDATE aval
SET Aplica = 0
FROM dbo.SICCMX_Aval aval
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=1 ) AS cFitch ON aval.Calif_Fitch = cFitch.Calificacion
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=2 ) AS cMoodys ON aval.Calif_Moodys = cMoodys.Calificacion
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=3 ) AS cSP ON aval.Calif_SP = cSP.Calificacion
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=4 ) AS cHR ON aval.Calif_HRRATINGS = cHR.Calificacion
LEFT OUTER JOIN dbo.SICC_FiguraGarantiza figGar ON ISNULL(aval.FiguraGarantiza,1) = figGar.IdFigura
WHERE cFitch.Calificacion IS NULL
 AND cMoodys.Calificacion IS NULL
 AND cSP.Calificacion IS NULL
 AND cHR.Calificacion IS NULL
 AND NULLIF(aval.Calif_Otras,'') IS NULL
 AND figGar.RequiereCalificacion=1;


INSERT INTO dbo.SICCMX_Avales_Log (IdAval, FechaCalculo, Usuario, Descripcion)
SELECT
 aval.IdAval,
 GETDATE(),
 '',
 'No aplica el Aval ya que no cuenta con calificación de alguna Agencia Calificadora'
FROM dbo.SICCMX_Aval aval
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=1 ) AS cFitch ON aval.Calif_Fitch = cFitch.Calificacion
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=2 ) AS cMoodys ON aval.Calif_Moodys = cMoodys.Calificacion
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=3 ) AS cSP ON aval.Calif_SP = cSP.Calificacion
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=4 ) AS cHR ON aval.Calif_HRRATINGS = cHR.Calificacion
LEFT OUTER JOIN dbo.SICC_FiguraGarantiza figGar ON ISNULL(aval.FiguraGarantiza,1) = figGar.IdFigura
WHERE cFitch.Calificacion IS NULL
 AND cMoodys.Calificacion IS NULL
 AND cSP.Calificacion IS NULL
 AND cHR.Calificacion IS NULL
 AND NULLIF(aval.Calif_Otras,'') IS NULL
 AND figGar.RequiereCalificacion=1;


INSERT INTO dbo.SICCMX_Avales_Log (IdAval, FechaCalculo, Usuario, Descripcion)
SELECT
 aval.IdAval,
 GETDATE(),
 '',
 'Calificación del Aval : '
 + ISNULL('[Fitch='+cFitch.Calificacion+']', '')
 + ISNULL('[Moodys='+cMoodys.Calificacion+']', '')
 + ISNULL('[SP='+cSP.Calificacion+']', '')
 + ISNULL('[HRRATINGS='+cHR.Calificacion+']', '')
 + ISNULL('[Otras='+NULLIF(aval.Calif_Otras,'')+']', '')
FROM dbo.SICCMX_Aval aval
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=1 ) AS cFitch ON aval.Calif_Fitch = cFitch.Calificacion
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=2 ) AS cMoodys ON aval.Calif_Moodys = cMoodys.Calificacion
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=3 ) AS cSP ON aval.Calif_SP = cSP.Calificacion
LEFT OUTER JOIN ( SELECT Calificacion FROM dbo.SICC_AgenciaCalificacionGarantias ac WHERE ac.IdAgencia=4 ) AS cHR ON aval.Calif_HRRATINGS = cHR.Calificacion
WHERE cFitch.Calificacion IS NOT NULL
 OR cMoodys.Calificacion IS NOT NULL
 OR cSP.Calificacion IS NOT NULL
 OR cHR.Calificacion IS NOT NULL
 OR NULLIF(aval.Calif_Otras,'') IS NOT NULL;


-- NO APLICAN AVALES CON PI MAYOR A LA DEL ACREDITADO
UPDATE ca
SET Aplica = 1
FROM dbo.SICCMX_CreditoAval ca
INNER JOIN dbo.SICCMX_Credito cre ON ca.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Aval av ON ca.IdAval = av.IdAval
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON per.IdPersona = ppi.IdPersona
WHERE av.PIAval < ppi.[PI];

INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT
 cre.IdCredito,
 GETDATE(),
 '',
 'No aplica el Aval [' + av.Nombre + '] ya que su PI es mayor ó igual a la del Acreditado'
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_CreditoAval ca ON cre.IdCredito = ca.IdCredito
INNER JOIN dbo.SICCMX_Aval av ON ca.IdAval = av.IdAval
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON per.IdPersona = ppi.IdPersona
WHERE av.PIAval >= ppi.[PI];


UPDATE ca
SET Aplica = 0
FROM dbo.SICCMX_CreditoAval ca
CROSS APPLY (
	SELECT TOP 1 caTmp.IdCredito, caTmp.IdAval
	FROM dbo.SICCMX_CreditoAval caTmp
	INNER JOIN dbo.SICCMX_Aval aTmp ON caTmp.IdAval = aTmp.IdAval
	WHERE caTmp.IdCredito = ca.IdCredito AND caTmp.Aplica = 1
	ORDER BY aTmp.PIAval
) AS val
WHERE ca.IdAval <> val.IdAval;


-- SE ACTUALIZA DATO SI CUENTA CON AL MENOS UN AVAL QUE APLIQUE
UPDATE crv
SET PIAval = 1
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_CreditoAval ca ON crv.IdCredito = ca.IdCredito
WHERE ca.Aplica = 1;
GO

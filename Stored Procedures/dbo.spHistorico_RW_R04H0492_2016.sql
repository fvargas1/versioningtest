SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04H0492_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04H0492_2016);

DELETE FROM Historico.RW_R04H0492_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04H0492_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, NumeroAvaluo, DenominacionCredito, SaldoPrincipalInicio,
	TasaInteres, ComisionesCobradasTasa, ComisionesCobradasMonto, MontoPagoExigible, MontoPagoRealizado, MontoQuitas, MontoCondonacion, MontoBonificacion,
	MontoDescuentos, SaldoPrincipalFinal, ResponsabilidadTotal, FechaUltimoPago, SituacionCredito, InteresesDevengados, SaldoCalculoInteres, DiasCalculoInteres,
	TipoRegimen, [PI], SP, DiasAtraso, ATR, PorCLTV, ValorViviendaAct, FactorActVivienda, TipoActualizacion, NumeroReAvaluo, MAXATR, PorVPAGO, PorRET, SUBCV,
	ConvenioJudicial, PorCobPaMed, PorCobPP, EntidadCobertura, ReservasSaldoFinal, ReservasConstDesc, PerdidaEsperada, ReservaPreventiva, PIInterna, SPInterna,
	EInterna, PerdidaEsperadaInterna, ReservasInterna, ReservasAdicionales, ReservasAdicConstDesc, GrupoRiesgoME, PonderadorRiesgoME, ExposicionNetaRes, ReqCapME,
	SP_ReqCap_MI, PI_ReqCap_MI, EI_ReqCap_MI, Ponderador_ReqCap_MI, ReqCap_MI
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	DenominacionCredito,
	SaldoPrincipalInicio,
	TasaInteres,
	ComisionesCobradasTasa,
	ComisionesCobradasMonto,
	MontoPagoExigible,
	MontoPagoRealizado,
	MontoQuitas,
	MontoCondonacion,
	MontoBonificacion,
	MontoDescuentos,
	SaldoPrincipalFinal,
	ResponsabilidadTotal,
	FechaUltimoPago,
	SituacionCredito,
	InteresesDevengados,
	SaldoCalculoInteres,
	DiasCalculoInteres,
	TipoRegimen,
	[PI],
	SP,
	DiasAtraso,
	ATR,
	PorCLTV,
	ValorViviendaAct,
	FactorActVivienda,
	TipoActualizacion,
	NumeroReAvaluo,
	MAXATR,
	PorVPAGO,
	PorRET,
	SUBCV,
	ConvenioJudicial,
	PorCobPaMed,
	PorCobPP,
	EntidadCobertura,
	ReservasSaldoFinal,
	ReservasConstDesc,
	PerdidaEsperada,
	ReservaPreventiva,
	PIInterna,
	SPInterna,
	EInterna,
	PerdidaEsperadaInterna,
	ReservasInterna,
	ReservasAdicionales,
	ReservasAdicConstDesc,
	GrupoRiesgoME,
	PonderadorRiesgoME,
	ExposicionNetaRes,
	ReqCapME,
	SP_ReqCap_MI,
	PI_ReqCap_MI,
	EI_ReqCap_MI,
	Ponderador_ReqCap_MI,
	ReqCap_MI
FROM dbo.RW_R04H0492_2016
WHERE IdReporteLog = @IdReporteLog;
GO

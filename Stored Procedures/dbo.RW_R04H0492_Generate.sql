SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04H0492_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE Nombre = 'H-0492';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_R04H0492;

INSERT INTO dbo.RW_R04H0492 (
	IdReporteLog, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, NumeroAvaluo, DenominacionCredito, SaldoPrincipalInicio,
	TasaInteres, ComisionesCobradasTasa, ComisionesCobradasMonto, MontoPagoExigible, MontoPagoRealizado, MontoBonificacion, SaldoPrincipalFinal,
	ResponsabilidadTotal, FechaUltimoPago, SituacionCredito, [PI], SP, DiasAtraso, MAXATR, PorVPAGO, SUBCV, ConvenioJudicial, PorCobPaMed, PorCobPP,
	EntidadCobertura, ReservasSaldoFinal, PerdidaEsperada, ReservaPreventiva, PIInterna, SPInterna, EInterna, PerdidaEsperadaInterna
)
SELECT DISTINCT
	@IdReporteLog,
	@IdPeriodo,
	@Entidad,
	'492',
	ROW_NUMBER() OVER ( ORDER BY hip.Codigo ASC ),
	hip.Codigo AS CodigoCredito,
	cnbv.CNBV AS CodigoCreditoCNBV,
	REPLACE(info.NumeroAvaluo, '-', '') AS NumeroAvaluo,
	ISNULL(mon.CodigoCNBV_Hipo, '0') AS DenominacionCredito,
	CAST(ISNULL(info.SaldoInicialCapital,0) AS DECIMAL) AS SaldoPrincipalInicio,
	CAST(ISNULL(info.TasaInteres,0) AS DECIMAL(10,2)) AS TasaInteres,
	CAST(ISNULL(info.TasaComisiones,0) AS DECIMAL(10,2)) AS ComisionesCobradasTasa,
	CAST(ISNULL(info.ComisionesCobradas,0) AS DECIMAL) AS ComisionesCobradasMonto,
	CAST(ISNULL(info.MontoExigible,0) AS DECIMAL) AS MontoPagoExigible,
	CAST(ISNULL(info.MontoPagadoCliente,0) AS DECIMAL) AS MontoPagoRealizado,
	CAST(ISNULL(info.MontoBonificacion,0) AS DECIMAL) AS MontoBonificacion,
	CAST(ISNULL(info.SaldoFinalCapital,0) AS DECIMAL) AS SaldoPrincipalFinal,
	CAST(ISNULL(info.ResponsabilidadTotal,0) AS DECIMAL) AS ResponsabilidadTotal,
	CASE WHEN info.FechaUltimoPagoCliente IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaUltimoPagoCliente,102),'.',''),1,6) END AS FechaUltimoPago,
	sit.CodigoCNBV AS SituacionCredito,
	CAST(ISNULL(hrv.[PI],0) * 100 AS DECIMAL(10,6)) AS [PI],
	CAST(ISNULL(hrv.SP,0) * 100 AS DECIMAL(10,6)) AS SP,
	ISNULL(pre.DiasAtraso,0) AS DiasAtraso,
	ISNULL(pre.MAXATR,0) AS MAXATR,
	CAST(ISNULL(pre.PorPago,0) AS DECIMAL(10,6)) AS PorVPAGO,
	CAST(ISNULL(pre.SUBCVi,0) AS DECIMAL) AS SUBCV,
	conv.CodigoCNBV AS ConvenioJudicial,
	CAST(ISNULL(pre.PorCOBPAMED,0) * 100 AS DECIMAL(12,6)) AS PorCobPaMed,
	CAST(ISNULL(pre.PorCOBPP,0) * 100 AS DECIMAL(12,6)) AS PorCobPP,
	ISNULL(ent.CodigoCNBV, '0') AS EntidadCobertura,
	CAST(ISNULL(info.ReservasSaldoFinal,0) AS DECIMAL) AS ReservasSaldoFinal,
	CAST(ISNULL(hrv.PerdidaEsperada,0) * 100 AS DECIMAL (10,6)) AS PerdidaEsperada,
	CAST(ISNULL(hrv.Reserva,0) AS DECIMAL) AS ReservaPreventiva,
	0.000000 AS PIInterna,
	0.000000 AS SPInterna,
	0 AS EInterna,
	0.000000 AS PerdidaEsperadaInterna
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON info.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hrv.IdHipotecario = pre.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_VW_HipotecarioCNBV cnbv ON hip.IdHipotecario = cnbv.IdHipotecario
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_SituacionHipotecario sit ON info.IdSituacionCredito = sit.IdSituacionHipotecario
LEFT OUTER JOIN dbo.SICC_ConvenioJudicial conv ON pre.IdConvenio = conv.IdConvenioJudicial
LEFT OUTER JOIN dbo.SICC_EntidadCobertura ent ON hip.IdEntidadCobertura = ent.IdEntidadCobertura;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04H0492 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = GETDATE(),
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO

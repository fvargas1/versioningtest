SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SI_Get_Detalle_File_Carga]
	@codename VARCHAR(50)
AS
SELECT
	arc.Nombre,
	car.Fuente,
	car.RutaCarga,
	car.FileCarga
FROM dbo.MIGRACION_Archivo arc
LEFT OUTER JOIN dbo.MIGRACION_Archivos_Carga car ON arc.IdArchivo = car.IdArchivo
WHERE arc.JobCodename = @codename;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Garantia_Hipotecario]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Garantia_Hipotecario WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Garantia_Hipotecario (
	IdPeriodoHistorico,
	Codigo,
	ValorGarantia,
	TipoGarantia,
	Moneda,
	Descripcion,
	PIGarante
)
SELECT
	@IdPeriodoHistorico,
	gar.Codigo,
	gar.ValorGarantia,
	tg.Codigo,
	mon.Codigo,
	gar.Descripcion,
	gar.PIGarante
FROM dbo.SICCMX_Garantia_Hipotecario gar
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON gar.IdTipoGarantia = tg.IdTipoGarantia
LEFT OUTER JOIN dbo.SICC_Moneda mon ON gar.IdMoneda = mon.IdMoneda;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_069]
AS

BEGIN

-- El Municipio debe ser un código valido del catalogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.Municipio
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_Municipio cat ON ISNULL(r.Municipio,'') = cat.CodigoMunicipio
WHERE cat.IdMunicipio IS NULL;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_031]
AS

BEGIN

-- Se anotará el saldo del principal al final del periodo deberá ser mayor o igual a 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, SaldoFinal
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(SaldoFinal,''),'-1') AS DECIMAL(23,2)) < 0;

END
GO

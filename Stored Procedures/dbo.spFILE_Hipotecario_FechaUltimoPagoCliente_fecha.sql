SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_FechaUltimoPagoCliente_fecha]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Hipotecario_FechaUltimoPagoCliente_fecha';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Hipotecario
SET errorFormato = 1
WHERE LEN(FechaUltimoPagoCliente)>0 AND ISDATE(FechaUltimoPagoCliente) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCredito,
 'FechaUltimoPagoCliente',
 FechaUltimoPagoCliente,
 1,
 @Detalle
FROM dbo.FILE_Hipotecario
WHERE LEN(FechaUltimoPagoCliente)>0 AND ISDATE(FechaUltimoPagoCliente) = 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Analitico_Reservas_A18_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Fecha,
	CodigoPersona,
	Nombre,
	CodigoCredito,
	MontoCredito,
	FechaVencimiento,
	[PI],
	Moneda,
	ActividadEconomica,
	MontoGarantia,
	Prct_Reserva,
	SP,
	MontoReserva,
	PI_Aval,
	SP_Aval,
	Calificacion,
	MontoGarantiaAjustado
FROM Historico.RW_Analitico_Reservas_A18
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Nombre;
GO

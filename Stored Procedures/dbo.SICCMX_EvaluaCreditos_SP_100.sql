SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_EvaluaCreditos_SP_100]
AS
-- ACTUALIZAMOS LA SP al 100% A CREDITOS CON MAS DE 18 MESES DE INCUMPLIMIENTO
UPDATE can
SET SeveridadCorresp = 1
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
WHERE can.EsDescubierto = 1 AND cre.MesesIncumplimiento >= 18;


INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT cre.IdPersona, GETDATE(), '', 'Se actualizó la SP al 100% ya que cuenta con créditos con 18 o más meses de incumplimiento'
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
WHERE can.EsDescubierto = 1 AND cre.MesesIncumplimiento >= 18;
GO

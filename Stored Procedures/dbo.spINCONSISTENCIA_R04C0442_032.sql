SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_032]
AS

BEGIN

-- Si el crédito es a tasa fija, entonces la frecuencia de revisión de tasa deberá ser igual a 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, TasaRef, FrecuenciaRevisionTasa
FROM dbo.RW_R04C0442
WHERE TasaRef = '600' AND FrecuenciaRevisionTasa <> '0';

END
GO

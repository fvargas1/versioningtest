SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ProcesoLog_Busqueda]
	@txtObjecto varchar(500),
	@txtUsuario VARCHAR(500),
	@txtFechaCondicion VARCHAR (100)
AS
DECLARE @Fecha DATETIME;

SELECT @Fecha = CASE WHEN @txtFechaCondicion = 'Hoy' THEN CONVERT(VARCHAR(10),GETDATE(),112)
WHEN @txtFechaCondicion = 'Ayer' THEN CONVERT(VARCHAR(10),DATEADD( DAY, -1, GETDATE()),112)
WHEN @txtFechaCondicion = '7_Dias' THEN CONVERT(VARCHAR(10),DATEADD( DAY, -7, GETDATE()),112) 
WHEN @txtFechaCondicion = '30_Dias' THEN CONVERT(VARCHAR(10),DATEADD( DAY, -30, GETDATE()),112)
ELSE NULL END

SELECT
	Categoria AS Categoria,
	Proceso AS Proceso,
	Descripcion AS Descripcion,
	Fecha AS Fecha,
	Username AS Usuario
FROM dbo.SICCMX_ProcesoLog
WHERE ( Categoria = @txtObjecto OR @txtObjecto = 'Todos' )
AND ( Username = @txtUsuario OR @txtUsuario = 'Todos' )
AND ( CONVERT(VARCHAR(10),Fecha,112) >= @Fecha OR @Fecha IS NULL );
GO

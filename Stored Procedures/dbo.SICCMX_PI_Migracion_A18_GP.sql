SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_Migracion_A18_GP]
AS
DECLARE @IdVar INT;
DECLARE @Campo VARCHAR(50);
DECLARE @SENT VARCHAR(500);
DECLARE @IdMetodologia INT;

SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo = '18';

DELETE piDet
FROM dbo.SICCMX_Persona_PI_Detalles_GP piDet
INNER JOIN dbo.SICCMX_Anexo18_GP anx ON piDet.IdGP = anx.IdGP AND piDet.EsGarante = anx.EsGarante;

DECLARE curAnexo CURSOR FOR
SELECT Id, Campo
FROM dbo.SICCMX_PI_Variables
WHERE IdMetodologia=@IdMetodologia AND Campo IS NOT NULL;

OPEN curAnexo;

FETCH NEXT FROM curAnexo INTO @IdVar, @Campo;

WHILE @@FETCH_STATUS = 0
BEGIN

SET @SENT =
'INSERT INTO dbo.SICCMX_Persona_PI_Detalles_GP (IdGP, EsGarante, IdVariable, ValorActual) ' +
'SELECT anx.IdGP, anx.EsGarante, ' + CONVERT(VARCHAR(3), @IdVar) + ', ' + @Campo + ' ' +
'FROM dbo.SICCMX_VW_Anexo18_GP anx ';
EXEC(@SENT);

FETCH NEXT FROM curAnexo INTO @IdVar, @Campo;

END

CLOSE curAnexo;
DEALLOCATE curAnexo;
GO

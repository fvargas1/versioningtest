SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Persona_PI_GP]
 @IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_Persona_PI_GP WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Persona_PI_GP (
 IdPeriodoHistorico,
 GP,
 EsGarante,
 FactorCuantitativo,
 PonderadoCuantitativo,
 FactorCualitativo,
 PonderadoCualitativo,
 FactorTotal,
 [PI],
 Metodologia,
 IdClasificacion,
 Comentarios
) 
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 av.Codigo, -- Persona
 esg.Codigo,
 ppi.FactorCuantitativo,
 ppi.PonderadoCuantitativo,
 ppi.FactorCualitativo,
 ppi.PonderadoCualitativo,
 ppi.FactorTotal,
 ppi.[PI],
 met.Codigo, -- Metodologia
 ppi.IdClasificacion,
 ppi.Comentarios
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_Aval av ON ppi.IdGP = av.IdAval
INNER JOIN dbo.SICC_EsGarante esg ON ppi.EsGarante = esg.IdEsGarante
LEFT OUTER JOIN dbo.SICCMX_Metodologia met ON ppi.IdMetodologia = met.IdMetodologia
WHERE esg.Layout = 'AVAL'
UNION
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 gar.Codigo, -- Persona
 esg.Codigo,
 ppi.FactorCuantitativo,
 ppi.PonderadoCuantitativo,
 ppi.FactorCualitativo,
 ppi.PonderadoCualitativo,
 ppi.FactorTotal,
 ppi.[PI],
 met.Codigo, -- Metodologia
 ppi.IdClasificacion,
 ppi.Comentarios
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_Garantia gar ON ppi.IdGP = gar.IdGarantia
INNER JOIN dbo.SICC_EsGarante esg ON ppi.EsGarante = esg.IdEsGarante
LEFT OUTER JOIN dbo.SICCMX_Metodologia met ON ppi.IdMetodologia = met.IdMetodologia
WHERE esg.Layout = 'GARANTIA';
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_015]
AS

BEGIN

-- El "DESTINO DEL CRÉDITO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.DestinoCredito
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_DestinoHipotecario cat ON ISNULL(r.DestinoCredito,'') = cat.CodigoCNBV
WHERE cat.IdDestinoHipotecario IS NULL;

END
GO

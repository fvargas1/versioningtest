SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_PersonaComentariosBuscar]
	@IdPersona BIGINT
AS
SELECT IdPersona, Comentarios
FROM dbo.SICCMX_PersonaComentarios
WHERE IdPersona=@IdPersona;
GO

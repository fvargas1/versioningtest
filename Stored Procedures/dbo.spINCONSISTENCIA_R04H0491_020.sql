SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_020]
AS

BEGIN

-- La "FECHA DE VENCIMIENTO DEL CRÉDITO" deberá ser cero (0) si y solo si "Tipo de Alta del Crédito" es igual 3 (columna 9).

SELECT CodigoCredito, CodigoCreditoCNBV, FechaVencimiento, TipoAlta
FROM dbo.RW_R04H0491
WHERE FechaVencimiento = '0' AND TipoAlta <> '3';

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_004]
AS

BEGIN

-- El Número de Avalúo debe corresponder con el dato reportado en la columna 23 del formulario H-0491 Altas y reestructuras de créditos a la vivienda.

SELECT DISTINCT r.CodigoCredito, r.CodigoCreditoCNBV, r.NumeroAvaluo
FROM dbo.RW_R04H0493 r
INNER JOIN dbo.SICCMX_VW_Datos_Altas_SerieH vw ON r.CodigoCreditoCNBV = vw.CodigoCreditoCNBV
WHERE r.NumeroAvaluo <> vw.NumeroAvaluo;

END
GO

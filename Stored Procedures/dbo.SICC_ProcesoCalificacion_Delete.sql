SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ProcesoCalificacion_Delete]
	@IdProcesoCalificacion int
AS
DELETE FROM dbo.SICC_ProcesoCalificacion WHERE IdProcesoCalificacion = @IdProcesoCalificacion;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0458]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0458);

DELETE FROM Historico.RW_R04C0458 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0458 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombreCNBV, TipoCartera, IdActividadEconomica, GrupoRiesgo,
	EntFinAcreOtorgantesCre, IdLocalidad, Municipio, Estado, Nacionalidad, NumInfoCrediticia, LEI, IdTipoAlta, TipoOperacion,
	IdDestino, CodigoCredito, CodigoCnbv, GrupalCnbv, MontoLineaCredito, FecMaxDis, FecVenLinea, IdMoneda, IdDisposicion, TipoLinea,
	Posicion, RegGarantiaMob, IdDeudorRelacionado, IdInstitucionOrigen, IdTasaReferencia, AjusteTasaReferencia, OperacionTasaReferencia,
	FrecuenciaRevisionTasa, IdPeriodicidadCapital, IdPeriodicidadInteres, MesesGraciaCap, MesesGraciaIntereses, GastosOrigTasa,
	GastosOriginacion, ComisionTasaDis, ComisionMontoDis, LocalidadDestinoCredito, MunicipioDestinoCredito, EstadoDestinoCredito,
	ActividadDestinoCredito
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoPersona,
	RFC,
	NombreCNBV,
	TipoCartera,
	IdActividadEconomica,
	GrupoRiesgo,
	EntFinAcreOtorgantesCre,
	IdLocalidad,
	Municipio,
	Estado,
	Nacionalidad,
	NumInfoCrediticia,
	LEI,
	IdTipoAlta,
	TipoOperacion,
	IdDestino,
	CodigoCredito,
	CodigoCnbv,
	GrupalCnbv,
	MontoLineaCredito,
	FecMaxDis,
	FecVenLinea,
	IdMoneda,
	IdDisposicion,
	TipoLinea,
	Posicion,
	RegGarantiaMob,
	IdDeudorRelacionado,
	IdInstitucionOrigen,
	IdTasaReferencia,
	AjusteTasaReferencia,
	OperacionTasaReferencia,
	FrecuenciaRevisionTasa,
	IdPeriodicidadCapital,
	IdPeriodicidadInteres,
	MesesGraciaCap,
	MesesGraciaIntereses,
	GastosOrigTasa,
	GastosOriginacion,
	ComisionTasaDis,
	ComisionMontoDis,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM dbo.RW_R04C0458
WHERE IdReporteLog = @IdReporteLog;
GO

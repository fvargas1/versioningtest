SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Exp_Incumplimiento_EI]
	@IdPersona BIGINT
AS
SELECT
	rw.NumeroLinea,
	rw.CodigoCredito,
	rw.SaldoTotal,
	rw.MontoCubierto,
	rw.MontoExpuesto,
	rw.EI,
	rw.TipoLinea,
	rw.PI_Ponderada,
	rw.SP_Ponderada,
	rw.Reserva,
	rw.PrctReserva,
	rw.Calificacion
FROM dbo.RW_CedulaNMC_Creditos rw
WHERE rw.IdPersona = @IdPersona
ORDER BY rw.NumeroLinea, rw.CodigoCredito;
GO

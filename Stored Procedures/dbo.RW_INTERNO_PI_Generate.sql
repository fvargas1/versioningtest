SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_INTERNO_PI_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @IdPeriodo BIGINT;

SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte='INTERNO' AND Nombre = '_PI';

INSERT INTO dbo.RW_ReporteLog ( IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_INTERNO_PI;

-- Informacion para reporte
INSERT INTO dbo.RW_INTERNO_PI (
 IdReporteLog, Periodo, Codigo, Nombre, Metodologia, Clasificacion, FactorCuantitativo, FactorCualitativo,
 Factor_Alpha, Factor_1mAlpha, PonderadoCuantitativo, PonderadoCualitativo, FactorTotal, [PI]
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 per.Codigo,
 REPLACE(per.Nombre,',','') AS Nombre,
 REPLACE(met.Nombre,',','') AS Metodologia,
 REPLACE(ISNULL(del.NombreClasificacion,''),',','') AS Clasificacion,
 CAST(ppi.FactorCuantitativo AS DECIMAL) AS FactorCuantitativo,
 CAST(ppi.FactorCualitativo AS DECIMAL) AS FactorCualitativo,
 CAST(del.FactorCuantitativo AS DECIMAL) AS Factor_Alpha,
 CAST(del.FactorCualitativo AS DECIMAL) AS Factor_1mAlpha,
 CAST(ppi.PonderadoCuantitativo AS DECIMAL) AS PonderadoCuantitativo,
 CAST(ppi.PonderadoCualitativo AS DECIMAL) AS PonderadoCualitativo,
 CAST(ppi.FactorTotal AS DECIMAL) AS FactorTotal,
 CAST(ppi.[PI] * 100 AS DECIMAL(10,6)) AS [PI]
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Persona_PI ppi ON per.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON ppi.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICCMX_PI_ValoresDelta del ON ppi.IdMetodologia = del.IdMetodologia AND ISNULL(ppi.IdClasificacion,0) = ISNULL(del.IdClasificacion,0);


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_INTERNO_pi WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO

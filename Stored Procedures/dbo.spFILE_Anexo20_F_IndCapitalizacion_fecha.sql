SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo20_F_IndCapitalizacion_fecha]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
DECLARE @FechaPeriodo DATETIME;
DECLARE @DiasGracia INT;

SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo20_F_IndCapitalizacion_fecha';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @DiasGracia = CASE WHEN ISNUMERIC([Value]) = 1 THEN [Value] ELSE 0 END FROM dbo.BAJAWARE_Config WHERE CodeName = 'DIAS_INFO_FIN_BURO';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo20
SET errorFormato = 1
WHERE (LEN(F_IndCapitalizacion)>0 AND ISDATE(F_IndCapitalizacion) = 0) OR F_IndCapitalizacion > DATEADD(DAY,@DiasGracia,@FechaPeriodo);

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo20_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'F_IndCapitalizacion',
 F_IndCapitalizacion,
 1,
 @Detalle
FROM dbo.FILE_Anexo20
WHERE (LEN(F_IndCapitalizacion)>0 AND ISDATE(F_IndCapitalizacion) = 0) OR F_IndCapitalizacion > DATEADD(DAY,@DiasGracia,@FechaPeriodo);
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo22_F_MonTotPagInfonavitUltBimestre_fecha]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
DECLARE @FechaPeriodo DATETIME;
DECLARE @DiasGracia INT;

SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo22_F_MonTotPagInfonavitUltBimestre_fecha';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @DiasGracia = CASE WHEN ISNUMERIC([Value]) = 1 THEN [Value] ELSE 0 END FROM dbo.BAJAWARE_Config WHERE CodeName = 'DIAS_INFO_FIN_BURO';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo22
SET errorFormato = 1
WHERE (LEN(F_MonTotPagInfonavitUltBimestre)>0 AND ISDATE(F_MonTotPagInfonavitUltBimestre) = 0) OR F_MonTotPagInfonavitUltBimestre > DATEADD(DAY,@DiasGracia,@FechaPeriodo);

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo22_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'F_MonTotPagInfonavitUltBimestre',
 F_MonTotPagInfonavitUltBimestre,
 1,
 @Detalle
FROM dbo.FILE_Anexo22
WHERE (LEN(F_MonTotPagInfonavitUltBimestre)>0 AND ISDATE(F_MonTotPagInfonavitUltBimestre) = 0) OR F_MonTotPagInfonavitUltBimestre > DATEADD(DAY,@DiasGracia,@FechaPeriodo);
GO

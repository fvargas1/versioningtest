SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_Migracion]
AS
DECLARE @IdInst BIGINT;

SELECT @IdInst=IdInstitucion
FROM dbo.SICC_Institucion
WHERE Codigo=LTRIM((SELECT [Value] FROM dbo.BAJAWARE_Config WHERE Codename='CodigoInstitucion'));

UPDATE per
SET
 Nombre = REPLACE(REPLACE(f.NombreCliente, '"', ''), '.', ''),
 Codigo = LTRIM(RTRIM(f.CodigoCliente)),
 RFC = LTRIM(RTRIM(f.RFC)),
 IdActividadEconomica = actEc.IdActividadEconomica,
 IdDeudorRelacionadoMA = deudRelMA.IdDeudorRelacionado,
 IdDeudorRelacionado = deudRel.IdDeudorRelacionado,
 Domicilio = LTRIM(RTRIM(f.Domicilio)),
 IdEjecutivo = ejec.IdEjecutivo,
 Telefono = LTRIM(RTRIM(f.Telefono)),
 Correo = LTRIM(RTRIM(f.Correo)),
 IdSucursal = suc.IdSucursal,
 IdLocalidad = (SELECT TOP 1 IdLocalidad FROM dbo.SICC_Localidad2015 WHERE CodigoCNBV = LTRIM(f.Localidad)),
 FechaActualizacion = GETDATE(),
 Username = 'Dataware',
 IdInstitucion = @IdInst,
 EsFondo = fon.IdTipoReserva,
 PI100 = pi100.IdPI100
FROM dbo.FILE_Persona f
INNER JOIN dbo.SICCMX_Persona per ON per.Codigo = LTRIM(f.CodigoCliente)
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEc ON actEc.Codigo = LTRIM(f.ActividadEconomica)
LEFT OUTER JOIN dbo.SICC_DeudorRelacionadoMA deudRelMA ON deudRelMA.Codigo = LTRIM(f.TipoAcreditadoRelacionadoMA)
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado deudRel ON deudRel.Codigo = LTRIM(f.TipoAcreditadoRelacionado)
LEFT OUTER JOIN dbo.SICCMX_Ejecutivo ejec ON ejec.Codigo = LTRIM(f.Ejecutivo)
LEFT OUTER JOIN dbo.SICC_Sucursal suc ON suc.Codigo = LTRIM(f.Sucursal)
LEFT OUTER JOIN dbo.SICC_TipoReserva5Prct fon ON LTRIM(f.EsFondo) = fon.Codigo
LEFT OUTER JOIN dbo.SICC_PI100 pi100 ON LTRIM(f.PI100) = pi100.Codigo
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_Persona (
 Nombre,
 Codigo,
 RFC,
 IdActividadEconomica,
 IdDeudorRelacionadoMA,
 IdDeudorRelacionado,
 Domicilio,
 IdEjecutivo,
 Telefono,
 Correo,
 IdSucursal,
 FechaActualizacion,
 Username,
 IdInstitucion,
 IdLocalidad,
 EsFondo,
 PI100
)
SELECT
 REPLACE( REPLACE( f.NombreCliente, '"', '' ), '.', '' ),
 LTRIM(RTRIM(f.CodigoCliente)),
 LTRIM(RTRIM(f.RFC)),
 actEc.IdActividadEconomica,
 deudRelMA.IdDeudorRelacionado,
 deudRel.IdDeudorRelacionado,
 LTRIM(RTRIM(f.Domicilio)),
 ejec.IdEjecutivo,
 LTRIM(RTRIM(f.Telefono)),
 LTRIM(RTRIM(f.Correo)),
 suc.IdSucursal,
 GETDATE(),
 'Dataware',
 @IdInst,
 (SELECT TOP 1 IdLocalidad FROM dbo.SICC_Localidad2015 WHERE CodigoCNBV = LTRIM(f.Localidad)),
 fon.IdTipoReserva,
 pi100.IdPI100
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEc ON actEc.Codigo = LTRIM(f.ActividadEconomica)
LEFT OUTER JOIN dbo.SICC_DeudorRelacionadoMA deudRelMA ON deudRelMA.Codigo = LTRIM(f.TipoAcreditadoRelacionadoMA)
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado deudRel ON deudRel.Codigo = LTRIM(f.TipoAcreditadoRelacionado)
LEFT OUTER JOIN dbo.SICCMX_Ejecutivo ejec ON ejec.Codigo = LTRIM(f.Ejecutivo)
LEFT OUTER JOIN dbo.SICC_Sucursal suc ON suc.Codigo = LTRIM(f.Sucursal)
LEFT OUTER JOIN dbo.SICCMX_Persona per ON per.Codigo = LTRIM(f.CodigoCliente)
LEFT OUTER JOIN dbo.SICC_TipoReserva5Prct fon ON LTRIM(f.EsFondo) = fon.Codigo
LEFT OUTER JOIN dbo.SICC_PI100 pi100 ON LTRIM(f.PI100) = pi100.Codigo
WHERE per.Codigo IS NULL AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


SET NOCOUNT ON;
INSERT INTO dbo.SICCMX_PersonaInfo (IdPersona)
SELECT per.IdPersona
FROM dbo.SICCMX_Persona per
LEFT OUTER JOIN dbo.SICCMX_PersonaInfo perInfo ON perInfo.IdPersona=per.IdPersona
WHERE perInfo.IdPersona IS NULL;


--PERSONA INFO
UPDATE pInfo
SET
 IdTipoPersonaMA = tipoPerMA.IdTipoPersona,
 IdTipoPersona = tipoPer.IdTipoPersona,
 NumeroEmpleados = NULLIF(f.NumeroEmpleados,''),
 IngresosBrutos = NULLIF(f.IngresosBrutos,''),
 IdSectorLaboral = secLab.IdSectorLaboral,
 GrupoRiesgo = f.NombreGrupoEconomico,
 IdTipoIngreso = tipoIng.IdTipoIngreso,
 NombreCNBV = dbo.fnFormat_Name(LTRIM(RTRIM(f.NombreCNBV))),
 Estado = LTRIM(RTRIM(f.Estado)),
 Municipio = LTRIM(RTRIM(f.Municipio)),
 IdNacionalidad = pais.IdPais,
 CURP = f.CURP,
 LEI = f.LEI,
 HITSIC = htc.IdHitSic,
 IdSectorEconomico = sect.IdSectorEconomico,
 IdRCGrupoRiesgo = gpo.IdRCGrupoRiesgo,
 IdRCCalificacion = cal.IdRCCalificacion,
 CalificacionLargoPlazo = LTRIM(RTRIM(f.CalificacionLargoPlazo)),
 IdAgenciaLargoPlazo = agLP.IdAgencia,
 CalificacionCortoPlazo = LTRIM(RTRIM(f.CalificacionCortoPlazo)),
 IdAgenciaCortoPlazo = agCP.IdAgencia,
 CodigoPostal = LTRIM(RTRIM(f.CodigoPostal))
FROM dbo.FILE_Persona f
INNER JOIN dbo.SICCMX_Persona per ON per.Codigo = f.CodigoCliente
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
LEFT OUTER JOIN dbo.SICC_TipoPersonaMA tipoPerMA ON tipoPerMA.Codigo = LTRIM(f.PersonalidadJuridicaMA)
LEFT OUTER JOIN dbo.SICC_TipoPersona tipoPer ON tipoPer.Codigo = LTRIM(f.PersonalidadJuridica)
LEFT OUTER JOIN dbo.SICC_TipoIngreso tipoIng ON tipoIng.Codigo = LTRIM(f.ComprobanteIngresos)
LEFT OUTER JOIN dbo.SICC_SectorLaboral secLab ON secLab.Codigo = LTRIM(f.SectorLaboral)
LEFT OUTER JOIN dbo.SICC_Pais pais ON LTRIM(f.Nacionalidad) = pais.Codigo
LEFT OUTER JOIN dbo.SICC_HitSic htc ON LTRIM(f.HITSIC) = htc.Codigo
LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor sect ON LTRIM(f.SectorEconomico) = sect.Codigo
LEFT OUTER JOIN dbo.SICC_RCGrupoRiesgo gpo ON LTRIM(f.RCGrupoRiesgo) = gpo.Codigo
LEFT OUTER JOIN dbo.SICC_RCCalificacion cal ON LTRIM(f.RCCalificacion) = cal.Codigo
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora agLP ON LTRIM(f.AgenciaLargoPlazo) = agLP.Codigo
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora agCP ON LTRIM(f.AgenciaCortoPlazo) = agCP.Codigo
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO

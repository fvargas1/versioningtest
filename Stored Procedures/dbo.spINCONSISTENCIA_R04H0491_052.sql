SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_052]
AS

BEGIN

-- Se deberá validar la correspondencia para las siguientes claves en "Sector Laboral del Acreditado" y "Tipo de Comprobación" de acuerdo a la siguiente tabla:
-- - Si el "Sector Laboral del Acreditado" es 1,2,3 o 4, el "Tipo de Comprobación de Ingresos" debe ser 2.
-- - Si el "Sector Laboral del Acreditado" es 5, el "Tipo de Comprobación de Ingresos" debe ser 1.

SELECT CodigoCredito, CodigoCreditoCNBV, SectorLaboral, TipoComprobacionIngresos
FROM dbo.RW_R04H0491
WHERE (SectorLaboral IN ('1','2','3','4') AND TipoComprobacionIngresos <> '2') OR (SectorLaboral = '5' AND TipoComprobacionIngresos <> '1');

END
GO

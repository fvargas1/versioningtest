SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Validacion_Insert]
 @Description varchar(250),
 @Codename varchar(150),
 @Position int,
 @CategoryName varchar(50) = NULL,
 @FieldName varchar(50) = NULL,
 @Activo bit
AS
INSERT INTO dbo.MIGRACION_Validacion (
	Description,
	Codename,
	Position,
	CategoryName,
	FieldName,
	Activo
) VALUES (
	@Description,
	@Codename,
	@Position,
	@CategoryName,
	@FieldName,
	@Activo
)

SELECT SCOPE_IDENTITY();
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_028]
AS

BEGIN

-- La "ENTIDAD QUE OTORGÓ EL COFINANCIAMIENTO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.EntidadCoFin
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_Cofinanciamiento cat ON ISNULL(r.EntidadCoFin,'') = cat.CodigoCNBV
WHERE cat.IdCofinanciamiento IS NULL;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_PersonaDatosCedula_Historico]
	@CodigoPersona VARCHAR(50),
	@CodigoProyecto VARCHAR(50),
	@IdPeriodoHistorico BIGINT
AS
DECLARE @Periodo DATETIME;
SELECT @Periodo=Fecha FROM dbo.SICC_PeriodoHistorico WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

SELECT
	p.InstitucionNombre AS Institucion,
	@Periodo AS Fecha_Evaluacion,
	p.Nombre AS Nombre_Deudor,
	p.Codigo AS Expediente,
	REPLACE(p.RFC,'_','') AS RFC,
	ISNULL(p.Domicilio,'') AS Dir_Deudor,
	ISNULL(p.Telefono,'NA') AS Telefono,
	ISNULL(p.Correo,'NA') AS Email,
	'' AS Region_Economica,
	ISNULL(sec.Nombre,'') AS Sector_Economico,
	ISNULL(p.ActividadEconomicaNombre,'NA') AS Actividad_Especifica,
	'' AS Socio_Principal,
	CASE WHEN dr.Codigo='8' OR dr.Codigo IS NULL THEN 'NO' ELSE 'SI' END AS Acreditado_Relacionado,
	'' AS Grupo_Economico,
	ISNULL(dr.Descripcion,'') AS Fraccion_Art_73,
	pc.SaldoInsoluto AS Total_Responsabilidades
FROM Historico.SICCMX_Persona p
INNER JOIN Historico.SICCMX_PersonaInfo pInfo ON p.Codigo = pInfo.Codigo AND p.IdPeriodoHistorico = pInfo.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_Proyecto pry ON pinfo.Codigo = pry.Persona AND pInfo.IdPeriodoHistorico = pry.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_ProyectoCalificacion pc ON pry.Proyecto = pc.Proyecto AND pry.IdPeriodoHistorico = pc.IdPeriodoHistorico
LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor sec ON pInfo.SectorEconomico = sec.Codigo
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado dr ON p.DeudorRelacionado = dr.Codigo
WHERE p.Codigo = @CodigoPersona AND pry.Proyecto = @CodigoProyecto AND p.IdPeriodoHistorico = @IdPeriodoHistorico;
GO

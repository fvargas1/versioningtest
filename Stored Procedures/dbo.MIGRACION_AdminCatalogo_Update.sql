SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_AdminCatalogo_Update]
	@IdAdminCatalog INT,
	@Nombre VARCHAR(50),
	@CodeName VARCHAR(50),
	@NombreTabla VARCHAR(100),
	@NombreIdentidad VARCHAR(50),
	@NombreBusqueda VARCHAR(50)
AS
UPDATE dbo.MIGRACION_AdminCatalog
SET
	Nombre = @Nombre,
	CodeName = @CodeName,
	NombreTabla = @NombreTabla,
	NombreIdentidad = @NombreIdentidad,
	NombreBusqueda = @NombreBusqueda
WHERE IdAdminCatalog = @IdAdminCatalog;
GO

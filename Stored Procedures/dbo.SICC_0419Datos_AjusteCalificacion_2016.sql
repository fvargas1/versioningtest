SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0419Datos_AjusteCalificacion_2016]
AS
UPDATE R04.[0419Datos_2016]
SET AjusteCalificacion = CAST(CASE WHEN Moneda = '0' THEN SaldoActualOrigen ELSE SaldoActual END * PrctReservasHistorico AS DECIMAL(23,2)) - ReservaActual;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0419Datos_Insert]
AS
DECLARE @IdPeriodoHistorico BIGINT;
DECLARE @FechaPeriodo DATETIME;

SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdPeriodoHistorico = IdPeriodoHistorico
FROM dbo.SICC_PeriodoHistorico
WHERE YEAR(Fecha) = YEAR(DATEADD(MONTH, -1, @FechaPeriodo)) AND MONTH(Fecha) = MONTH(DATEADD(MONTH, -1, @FechaPeriodo)) AND Reporte = 1;


TRUNCATE TABLE R04.[0419Datos];

-- Insertamos la Informacion Historica Comercial
INSERT INTO R04.[0419Datos] (
	Codigo,
	Moneda,
	CodigoProducto,
	CalifCubHistorico,
	CalifExpHistorico,
	SaldoReservaExpHistorico,
	SaldoReservaCubHistorico,
	SaldoHistorico,
	ReservasAdicOrdenadasCNBVHistorico,
	InteresCarteraVencidaHistorico
)
SELECT
	crv.Credito,
	mon.Codigo,
	info.TipoProductoSerie4,
	crv.CalifCubierta,
	crv.CalifExpuesta,
	CAST(ISNULL(crv.ReservaExpuesta,0) AS DECIMAL),
	CAST(ISNULL(crv.ReservaCubierta,0) AS DECIMAL),
	CAST(val.SaldoCapitalVigente AS DECIMAL)+CAST(val.InteresVigente AS DECIMAL)+CAST(val.SaldoCapitalVencido AS DECIMAL)+CAST(val.InteresVencido AS DECIMAL),
	CAST(ISNULL(crv.ReservaAdicional,0) AS DECIMAL),
	CAST(ISNULL(cre.InteresCarteraVencida,0) AS DECIMAL)
FROM Historico.SICCMX_Credito_Reservas_Variables crv
INNER JOIN Historico.SICCMX_Credito cre ON crv.IdPeriodoHistorico = cre.IdPeriodoHistorico AND crv.Credito = cre.Codigo
INNER JOIN Historico.SICCMX_CreditoInfo info ON cre.IdPeriodoHistorico = info.IdPeriodoHistorico AND cre.Codigo = info.Credito
INNER JOIN Historico.SICCMX_Credito_Valorizado val ON crv.IdPeriodoHistorico = val.IdPeriodoHistorico AND info.Credito = val.Credito
INNER JOIN dbo.SICC_Moneda mon ON mon.Codigo = cre.Moneda
WHERE crv.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Historial Consumo
INSERT INTO R04.[0419Datos] (
	Codigo,
	Moneda,
	CodigoProducto,
	CalifCubHistorico,
	CalifExpHistorico,
	SaldoReservaExpHistorico,
	SaldoReservaCubHistorico,
	SaldoHistorico,
	ReservasAdicOrdenadasCNBVHistorico,
	InteresCarteraVencidaHistorico
)
SELECT
	crv.Consumo,
	mon.Codigo,
	info.TipoProductoSerie4,
	crv.CalificacionCubierta,
	crv.CalificacionExpuesta,
	CAST(ISNULL(crv.ReservaExpuesta,0) AS DECIMAL),
	CAST(ISNULL(crv.ReservaCubierta,0) AS DECIMAL),
	CAST(val.SaldoCapitalVigente AS DECIMAL)+CAST(val.InteresVigente AS DECIMAL)+CAST(val.SaldoCapitalVencido AS DECIMAL)+CAST(val.InteresVencido AS DECIMAL),
	CAST(ISNULL(crv.ReservaAdicional,0) AS DECIMAL),
	CAST(ISNULL(val.InteresCarteraVencida,0) AS DECIMAL)
FROM Historico.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN Historico.SICCMX_Consumo con ON crv.IdPeriodoHistorico = con.IdPeriodoHistorico AND crv.Consumo = con.Codigo
INNER JOIN Historico.SICCMX_ConsumoInfo info ON con.IdPeriodoHistorico = info.IdPeriodoHistorico AND con.Codigo = info.Consumo
INNER JOIN Historico.SICCMX_Consumo_Valorizado val ON info.IdPeriodoHistorico = val.IdPeriodoHistorico AND info.Consumo = val.Consumo
INNER JOIN dbo.SICC_Moneda mon ON info.Moneda = mon.Codigo
WHERE crv.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Historial Hipotecario
INSERT INTO R04.[0419Datos] (
	Codigo,
	Moneda,
	CodigoProducto,
	CalifCubHistorico,
	CalifExpHistorico,
	SaldoReservaExpHistorico,
	SaldoReservaCubHistorico,
	SaldoHistorico,
	ReservasAdicOrdenadasCNBVHistorico,
	InteresCarteraVencidaHistorico
)
SELECT
	res.Hipotecario,
	mon.Codigo,
	hip.TipoCreditoR04A,
	res.CalificacionCubierto,
	res.CalificacionExpuesto,
	CAST(ISNULL(res.ReservaExpuesto,0) AS DECIMAL),
	CAST(ISNULL(res.ReservaCubierto,0) AS DECIMAL),
	CAST(val.SaldoCapitalVigente AS DECIMAL)+CAST(val.InteresVigente AS DECIMAL)+CAST(val.SaldoCapitalVencido AS DECIMAL)+CAST(val.InteresVencido AS DECIMAL),
	CAST(ISNULL(res.ReservaAdicional,0) AS DECIMAL),
	CAST(ISNULL(val.InteresCarteraVencida,0) AS DECIMAL)
FROM Historico.SICCMX_HipotecarioReservas res
INNER JOIN Historico.SICCMX_Hipotecario hip ON res.IdPeriodoHistorico = hip.IdPeriodoHistorico AND res.Hipotecario = hip.Hipotecario
INNER JOIN Historico.SICCMX_HipotecarioInfo info ON hip.IdPeriodoHistorico = info.IdPeriodoHistorico AND hip.Hipotecario = info.Hipotecario
INNER JOIN Historico.SICCMX_Hipotecario_Valorizado val ON info.IdPeriodoHistorico = val.IdPeriodoHistorico AND info.Hipotecario = val.Hipotecario
INNER JOIN dbo.SICC_Moneda mon ON info.Moneda = mon.Codigo
WHERE res.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Actualizar Reservas Creditos Actual(comercial)
UPDATE dat
SET
	CodigoProducto = CASE WHEN dat.CodigoProducto IS NULL THEN tp.Codigo ELSE dat.CodigoProducto END,
	SaldoReservaExpActual = CAST(ISNULL(crv.ReservaExpuesta,0) AS DECIMAL),
	SaldoReservaCubActual = CAST(ISNULL(crv.ReservaCubierta,0) AS DECIMAL),
	SaldoActual = cre.MontoValorizadoRound,
	CalifExpActual = calExp.Codigo,
	CalifCubActual = calCub.Codigo,
	InteresCarteraVencida = CAST(ISNULL(cre.InteresCarteraVencidaValorizado,0) AS DECIMAL),
	ReservasAdicOrdenadasCNBV = CAST(ISNULL(crv.ReservaAdicional,0) AS DECIMAL),
	IdTipoBaja = CASE WHEN dat.IdTipoBaja IS NOT NULL THEN dat.IdTipoBaja ELSE info.IdTipoBaja END
FROM R04.[0419Datos] dat
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON dat.Codigo = cre.CodigoCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cre.IdCredito = info.IdCredito
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON info.IdCredito = crv.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON info.IdTipoProductoSerie4 = tp.IdTipoProducto
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calExp ON crv.CalifExpuesta = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calCub ON crv.CalifCubierta = calCub.IdCalificacion;


-- Insert de nuevos creditos
INSERT INTO R04.[0419Datos] (
	Codigo,
	Moneda,
	CodigoProducto,
	SaldoReservaExpActual,
	SaldoReservaCubActual,
	SaldoActual,
	CalifExpActual,
	CalifCubActual,
	InteresCarteraVencida,
	ReservasAdicOrdenadasCNBV,
	IdTipoBaja
)
SELECT
	cre.CodigoCredito,
	mon.Codigo,
	tp.Codigo,
	CAST(ISNULL(crv.ReservaExpuesta,0) AS DECIMAL),
	CAST(ISNULL(crv.ReservaCubierta,0) AS DECIMAL),
	cre.MontoValorizadoRound,
	calExp.Codigo,
	calCub.Codigo,
	CAST(ISNULL(cre.InteresCarteraVencidaValorizado,0) AS DECIMAL),
	CAST(ISNULL(crv.ReservaAdicional,0) AS DECIMAL),
	info.IdTipoBaja
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_CreditoInfo info ON cre.IdCredito = info.IdCredito
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON info.IdCredito = crv.IdCredito 
INNER JOIN dbo.SICC_Moneda mon ON cre.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calExp ON crv.CalifExpuesta = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calCub ON crv.CalifCubierta = calCub.IdCalificacion
LEFT OUTER JOIN R04.[0419Datos] dat ON cre.CodigoCredito = dat.Codigo
WHERE dat.Id IS NULL;


-- Actualizar Consumo que ya tengan historico
UPDATE dat
SET
	CodigoProducto = CASE WHEN dat.CodigoProducto IS NULL THEN tp.Codigo ELSE dat.CodigoProducto END,
	SaldoReservaExpActual = CAST(ISNULL(crv.ReservaExpuesta,0) AS DECIMAL),
	SaldoReservaCubActual = CAST(ISNULL(crv.ReservaCubierta,0) AS DECIMAL),
	SaldoActual = CAST(ISNULL(con.SaldoTotalValorizadoRound,0) AS DECIMAL),
	CalifExpActual = calExp.Codigo,
	CalifCubActual = calCub.Codigo,
	InteresCarteraVencida = CAST(ISNULL(con.InteresCarteraVencidaValorizado,0) AS DECIMAL),
	ReservasAdicOrdenadasCNBV = CAST(ISNULL(crv.ReservaAdicional,0) AS DECIMAL),
	IdTipoBaja = CASE WHEN dat.IdTipoBaja IS NOT NULL THEN dat.IdTipoBaja ELSE info.IdTipoBaja END
FROM R04.[0419Datos] dat
INNER JOIN dbo.SICCMX_VW_Consumo con ON dat.Codigo = con.Codigo
INNER JOIN dbo.SICCMX_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON info.IdConsumo = crv.IdConsumo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON info.IdTipoProductoSerie4 = tp.IdTipoProducto
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calExp ON crv.IdCalificacionExpuesta = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calCub ON crv.IdCalificacionCubierta = calCub.IdCalificacion;


-- Insert nuevos Registros Reserva Consumo
INSERT INTO R04.[0419Datos] (
	Codigo,
	Moneda,
	CodigoProducto,
	SaldoReservaExpActual,
	SaldoReservaCubActual,
	SaldoActual,
	CalifExpActual,
	CalifCubActual,
	InteresCarteraVencida,
	ReservasAdicOrdenadasCNBV,
	IdTipoBaja
)
SELECT
	con.Codigo,
	mon.Codigo,
	tp.Codigo AS CodigoProducto,
	CAST(ISNULL(crv.ReservaExpuesta,0) AS DECIMAL),
	CAST(ISNULL(crv.ReservaCubierta,0) AS DECIMAL),
	CAST(ISNULL(con.SaldoTotalValorizadoRound,0) AS DECIMAL),
	calExp.Codigo,
	calCub.Codigo,
	CAST(ISNULL(con.InteresCarteraVencidaValorizado,0) AS DECIMAL),
	CAST(ISNULL(crv.ReservaAdicional,0) AS DECIMAL),
	info.IdTipoBaja
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON info.IdConsumo = crv.IdConsumo
INNER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calExp ON crv.IdCalificacionExpuesta = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calCub ON crv.IdCalificacionCubierta = calCub.IdCalificacion
LEFT OUTER JOIN R04.[0419Datos] dat ON con.Codigo = dat.Codigo
WHERE dat.Id IS NULL;


-- Actualizar registros Hipotecario
UPDATE dat
SET
	CodigoProducto = CASE WHEN dat.CodigoProducto IS NULL THEN tp.Codigo ELSE dat.CodigoProducto END,
	SaldoReservaExpActual = CAST(ISNULL(res.ReservaExpuesto,0) AS DECIMAL),
	SaldoReservaCubActual = CAST(ISNULL(res.ReservaCubierto,0) AS DECIMAL),
	SaldoActual = CAST(ISNULL(hip.SaldoTotalValorizadoRound,0) AS DECIMAL),
	CalifExpActual = calExp.Codigo,
	CalifCubActual = calCub.Codigo,
	InteresCarteraVencida = CAST(ISNULL(hip.InteresCarteraVencidaValorizado,0) AS DECIMAL),
	ReservasAdicOrdenadasCNBV = CAST(ISNULL(res.ReservaAdicional,0) AS DECIMAL),
	dat.IdTipoBaja = CASE WHEN dat.IdTipoBaja IS NOT NULL THEN dat.IdTipoBaja ELSE info.IdTipoBaja END
FROM R04.[0419Datos] dat
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON dat.Codigo = hip.Codigo
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioReservas res ON info.IdHipotecario = res.IdHipotecario
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = hip.IdTipoCreditoR04A
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calExp ON res.IdCalificacionExpuesto = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calCub ON res.IdCalificacionCubierto = calCub.IdCalificacion;


-- Insert Registros Reserva Hipotecario
INSERT INTO R04.[0419Datos] (
	Codigo,
	Moneda,
	CodigoProducto,
	SaldoReservaExpActual,
	SaldoReservaCubActual,
	SaldoActual,
	CalifExpActual,
	CalifCubActual,
	InteresCarteraVencida,
	ReservasAdicOrdenadasCNBV,
	IdTipoBaja
)
SELECT
	hip.Codigo,
	mon.Codigo,
	tp.Codigo AS CodigoProducto,
	CAST(ISNULL(res.ReservaExpuesto,0) AS DECIMAL),
	CAST(ISNULL(res.ReservaCubierto,0) AS DECIMAL),
	CAST(ISNULL(hip.SaldoTotalValorizadoRound,0) AS DECIMAL),
	calExp.Codigo,
	calCub.Codigo,
	CAST(ISNULL(hip.InteresCarteraVencidaValorizado,0) AS DECIMAL),
	CAST(ISNULL(res.ReservaAdicional,0) AS DECIMAL),
	info.IdTipoBaja
FROM dbo.SICCMX_VW_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioReservas res ON info.IdHipotecario = res.IdHipotecario
INNER JOIN sicc_moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = hip.IdTipoCreditoR04A
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calExp ON res.IdCalificacionExpuesto = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calCub ON res.IdCalificacionCubierto = calCub.IdCalificacion
LEFT OUTER JOIN R04.[0419Datos] dat ON hip.Codigo = dat.Codigo
WHERE dat.Id IS NULL;
GO

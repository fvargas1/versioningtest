SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_045]
AS

BEGIN

-- El Estado debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.Estado
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_Municipio cat ON ISNULL(r.Estado,'') = cat.CodigoEstado
WHERE cat.IdMunicipio IS NULL;

END
GO

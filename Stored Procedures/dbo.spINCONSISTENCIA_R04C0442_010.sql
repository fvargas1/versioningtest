SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_010]
AS

BEGIN

-- El tipo de acreditado relacionado debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.TipoAcreditadoRel
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_DeudorRelacionadoMA cat ON ISNULL(r.TipoAcreditadoRel,'') = cat.CodigoCNBV
WHERE cat.IdDeudorRelacionado IS NULL;

END
GO

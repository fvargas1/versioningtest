SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Upd_CC_Carga]
 @Codename VARCHAR(50),
 @Fuente VARCHAR(50),
 @Linea VARCHAR(50)
AS
DECLARE @NumRegistros INT;
DECLARE @MontoCC DECIMAL(23,2);
SET @Linea = REPLACE(@Linea, 'TR|', '');
SET @NumRegistros = CASE WHEN ISNUMERIC(SUBSTRING(@Linea, 1, CHARINDEX('|', @Linea) - 1)) = 1 THEN SUBSTRING(@Linea, 1, CHARINDEX('|', @Linea) - 1) ELSE 0 END;
SET @Linea = SUBSTRING(@Linea, CHARINDEX('|', @Linea) + 1, LEN(@Linea));
SET @Linea = SUBSTRING(@Linea, 1, LEN(@Linea) - 1);
SET @MontoCC = CASE WHEN ISNUMERIC(@Linea) = 1 THEN CAST(@Linea AS DECIMAL(23,2)) ELSE 0 END;

UPDATE car
SET NumRegistros = @NumRegistros,
 MontoCC = @MontoCC
FROM dbo.MIGRACION_Archivo arc
INNER JOIN dbo.MIGRACION_Archivos_Carga car ON arc.IdArchivo = car.IdArchivo
WHERE arc.Codename = @Codename AND car.Fuente = @Fuente;
GO

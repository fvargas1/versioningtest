SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Categoria_List]
AS
SELECT tb.IdCategoria, tb.Nombre, tb.NombreTabla
FROM (
	SELECT DISTINCT
	vw.IdCategoria,
	vw.Nombre + ISNULL(' -' + rep.Descripcion,'') AS Nombre,
	vw.NombreTabla
	FROM dbo.MIGRACION_VW_Categoria vw
	INNER JOIN dbo.RW_Reporte rep ON vw.Nombre = ISNULL(rep.GrupoReporte,'') + ISNULL(rep.Nombre,'')
	INNER JOIN dbo.MIGRACION_Inconsistencia inc ON vw.IdCategoria = inc.IdCategoria
	WHERE vw.Nombre NOT LIKE 'RETORNO%' AND vw.Nombre NOT LIKE 'INTERNO%' AND inc.Activo = 1
) AS tb
ORDER BY tb.Nombre;
GO

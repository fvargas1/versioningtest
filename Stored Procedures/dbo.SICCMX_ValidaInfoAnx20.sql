SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ValidaInfoAnx20]
AS

UPDATE piDet
SET PuntosActual = 24
FROM SICCMX_Anexo20 anx
INNER JOIN SICCMX_Persona_PI ppi ON anx.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI_Detalles piDet ON ppi.IdPersona = piDet.IdPersona
INNER JOIN dbo.SICCMX_PI_Variables variab ON piDet.IdVariable = variab.Id AND variab.Codigo='20IA_REND_CAP_ROE'
WHERE ISNULL(anx.UtilNetaTrimestre,0) < 0 AND ISNULL(anx.CapitalContableProm,0) < 0;


INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT ppi.IdPersona, GETDATE(), '', 'Se actualiza puntaje de ''' + variab.Nombre + ''' a ''24'', ya que la "Utilidad Neta Trimestral" y el "Capital Contable Promedio" son menores a 0'
FROM SICCMX_Anexo20 anx
INNER JOIN SICCMX_Persona_PI ppi ON anx.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI_Detalles piDet ON ppi.IdPersona = piDet.IdPersona
INNER JOIN dbo.SICCMX_PI_Variables variab ON piDet.IdVariable = variab.Id AND variab.Codigo='20IA_REND_CAP_ROE'
WHERE ISNULL(anx.UtilNetaTrimestre,0) < 0 AND ISNULL(anx.CapitalContableProm,0) < 0;
GO

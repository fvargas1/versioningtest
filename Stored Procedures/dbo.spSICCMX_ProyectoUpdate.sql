SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_ProyectoUpdate]
	@IdPersona BIGINT,
	@Nombre VARCHAR(250),
	@Descripcion VARCHAR(1500),
	@Etapa INT
AS
IF EXISTS(SELECT IdProyecto FROM dbo.SICCMX_Proyecto WHERE IdPersona=@IdPersona)
BEGIN
	UPDATE dbo.SICCMX_Proyecto
	SET Nombre = @Nombre,
	Descripcion = @Descripcion
	WHERE IdPersona=@IdPersona;
END
GO

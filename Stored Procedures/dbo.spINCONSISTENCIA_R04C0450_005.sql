SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_005]
AS
BEGIN
-- Se debe reportar el Monto de la Garantía o el Porcentaje Cubierto del Crédito

SELECT
 CodigoCredito,
 REPLACE(NombreAcreditado, ',', '') AS NombreDeudor,
 CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0450_INC
WHERE CAST(ISNULL(MontoGarantia,'0') AS MONEY) = 0 AND CAST(ISNULL(PrctGarantia,'0') AS MONEY) = 0;

END
GO

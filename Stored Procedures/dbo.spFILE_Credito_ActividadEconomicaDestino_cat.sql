SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_ActividadEconomicaDestino_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_ActividadEconomicaDestino_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_ActividadEconomica cat ON LTRIM(f.ActividadEconomicaDestino) = cat.Codigo
WHERE LEN(f.ActividadEconomicaDestino)>0 AND cat.IdActividadEconomica IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	f.CodigoCredito,
	'ActividadEconomicaDestino',
	f.ActividadEconomicaDestino,
	2,
	@Detalle
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_ActividadEconomica cat ON LTRIM(f.ActividadEconomicaDestino) = cat.Codigo
WHERE LEN(f.ActividadEconomicaDestino)>0 AND cat.IdActividadEconomica IS NULL;
GO

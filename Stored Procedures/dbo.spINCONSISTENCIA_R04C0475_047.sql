SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_047]
AS

BEGIN

-- Si el Puntaje Asignado por Número de pagos en tiempo que la empresa realizó a inst financ bcarias en los últimos 12 meses (cve_ptaje_num_pagos_bcos) es = 47,
-- entonces el Número de pagos en tiempo que la empresa realizó a inst financ bcarias en los últimos 12 meses (dat_num_pgos_tiempo_bcos) debe ser >= 5 y < 10

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PagosInstBanc,
	P_PagosInstBanc AS Puntos_PagosInstBanc
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_PagosInstBanc,'') = '47' AND (CAST(PagosInstBanc AS DECIMAL) < 5 OR CAST(PagosInstBanc AS DECIMAL) >= 10);

END


GO

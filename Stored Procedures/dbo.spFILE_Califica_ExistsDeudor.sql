SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Califica_ExistsDeudor]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Califica_ExistsDeudor';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Califica
SET errorFormato = 1
WHERE LEN(ISNULL(CodigoDeudor, '')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Califica_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoDeudor,
 'CodigoDeudor',
 CodigoDeudor,
 1,
 @Detalle
FROM dbo.FILE_Califica 
WHERE LEN(ISNULL(CodigoDeudor, '')) = 0;
GO

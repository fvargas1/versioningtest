SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_027_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La denominación del crédito debe existir en catálogo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.CodigoCredito)
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_Moneda cat ON ISNULL(r.DenominacionCredito,'') = cat.CodigoCNBV
WHERE cat.IdMoneda IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

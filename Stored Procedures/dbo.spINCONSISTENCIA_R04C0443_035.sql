SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_035]
AS

BEGIN

-- El Producto Comercial de la Entidad deberá existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.NumeroDisposicion, r.ProductoComercial
FROM dbo.RW_R04C0443 r
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial cat ON ISNULL(r.ProductoComercial,'') = cat.CodigoCNBV
WHERE cat.IdTipoCredito IS NULL;

END
GO

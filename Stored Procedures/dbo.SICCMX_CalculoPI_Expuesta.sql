SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoPI_Expuesta]
AS
UPDATE crv
SET PI_Expuesta = ppi.[PI]
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON crv.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Persona_PI ppi ON cre.IdPersona = ppi.IdPersona;
GO

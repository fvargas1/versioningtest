SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_Total_Proy]
AS
UPDATE crv
SET SP_Total = (CAST(SP_Expuesta * EI_Expuesta_GarPer AS DECIMAL(22,10)) / inf.ExposicionIncumplimiento) + (CAST(SP_Cubierta * EI_Cubierta_GarPer AS DECIMAL(22,10)) / inf.ExposicionIncumplimiento)
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_CreditoInfo inf ON inf.IdCredito = crv.IdCredito
WHERE inf.ExposicionIncumplimiento > 0;


INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 IdCredito,
 GETDATE(),
 '',
 'Se calculó una SP Total de ' + CONVERT(VARCHAR, CONVERT(DECIMAL(10,2), ISNULL(SP_Total*100, 0))) + '%'
FROM dbo.SICCMX_Credito_Reservas_Variables;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_PersonaComentariosModifica]
	@IdPersona BIGINT,
	@Comments VARCHAR(3000)
AS
IF NOT EXISTS (SELECT * FROM dbo.SICCMX_PersonaComentarios WHERE IdPersona=@IdPersona)
BEGIN
INSERT INTO dbo.SICCMX_PersonaComentarios (IdPersona, Comentarios) VALUES (@IdPersona, @Comments);
END
ELSE
BEGIN
UPDATE dbo.SICCMX_PersonaComentarios
SET Comentarios=@Comments
WHERE IdPersona=@IdPersona;	
END
GO

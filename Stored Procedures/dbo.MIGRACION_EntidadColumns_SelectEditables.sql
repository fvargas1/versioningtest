SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_EntidadColumns_SelectEditables]
	@IdEntidad INT
AS
SELECT
	IdEntidadColumns,
	Nombre,
	Descripcion,
	IdEntidad,
	IdEntidadNombre,
	NombreCampo,
	TipoDato,
	IsRequerido,
	IsKey,
	IsIdentidadBusqueda,
	IdCatalogo,
	IdCatalogoNombre,
	NombreIdCatalogo
FROM dbo.MIGRACION_VW_EntidadColumna
WHERE IdEntidad = @IdEntidad AND IsKey = 0 AND IsIdentidadBusqueda = 0
ORDER BY Nombre ASC;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CodigosCNBV_Banco]
AS
DECLARE @IdPeriodo BIGINT;
SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;

INSERT INTO dbo.SICCMX_CreditoCNBV (
 NumeroLinea,
 CodigoCredito,
 CodigoPersona,
 CNBV,
 FechaCreacion,
 IdPeriodo,
 MontoLinea,
 IdMoneda,
 IdTipoProducto,
 IdTipoOperacion,
 IdTasaReferencia,
 FechaVencimiento
)
SELECT
 lin.Codigo,
 cre.Codigo,
 per.Codigo,
 lin.CodigoCNBV,
 GETDATE(),
 @IdPeriodo,
 lin.MontoLinea,
 lin.IdMoneda,
 lin.ProductoComercial,
 lin.TipoOperacion,
 lin.IdTasaReferencia,
 lin.FecVenLinea
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Credito cre ON lin.IdLineaCredito = cre.IdLineaCredito
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICCMX_CreditoCNBV cnbv ON lin.Codigo = cnbv.NumeroLinea
WHERE LEN(lin.CodigoCNBV) > 0 AND cnbv.IdCreditoCNBV IS NULL;


-- INSERTAMOS CODIGOS CNBV DE LINEAS QUE NO SE HAN DISPUESTO
INSERT INTO dbo.SICCMX_CreditoCNBV (
 NumeroLinea,
 CodigoCredito,
 CodigoPersona,
 CNBV,
 FechaCreacion,
 IdPeriodo,
 MontoLinea,
 IdMoneda,
 IdTipoProducto,
 IdTipoOperacion,
 IdTasaReferencia,
 FechaVencimiento
)
SELECT
 lin.Codigo,
 NULL,
 per.Codigo,
 lin.CodigoCNBV,
 GETDATE(),
 @IdPeriodo,
 lin.MontoLinea,
 lin.IdMoneda,
 lin.ProductoComercial,
 lin.TipoOperacion,
 lin.IdTasaReferencia,
 lin.FecVenLinea
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICCMX_CreditoCNBV cnbv ON lin.Codigo = cnbv.NumeroLinea
WHERE LEN(lin.CodigoCNBV) > 0 AND cnbv.IdCreditoCNBV IS NULL;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0456_003]
AS
BEGIN
-- El factor ajuste hfx debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	Hfx
FROM dbo.RW_VW_R04C0456_INC
WHERE ISNULL(Hfx,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END
GO

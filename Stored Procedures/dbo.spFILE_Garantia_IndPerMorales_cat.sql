SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_IndPerMorales_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_IndPerMorales_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Garantia
SET errorCatalogo = 1
WHERE LEN(IndPerMorales)>0 AND LTRIM(IndPerMorales) NOT IN ('0','1');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoGarantia,
 'IndPerMorales',
 IndPerMorales,
 2,
 @Detalle
FROM dbo.FILE_Garantia
WHERE LEN(IndPerMorales)>0 AND LTRIM(IndPerMorales) NOT IN ('0','1');
GO

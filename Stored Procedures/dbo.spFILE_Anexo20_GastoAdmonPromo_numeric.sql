SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo20_GastoAdmonPromo_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo20_GastoAdmonPromo_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo20
SET errorFormato = 1
WHERE LEN(GastoAdmonPromo)>0 AND (ISNUMERIC(ISNULL(GastoAdmonPromo,'0')) = 0 OR GastoAdmonPromo LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo20_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'GastoAdmonPromo',
 GastoAdmonPromo,
 1,
 @Detalle
FROM dbo.FILE_Anexo20
WHERE LEN(GastoAdmonPromo)>0 AND (ISNUMERIC(ISNULL(GastoAdmonPromo,'0')) = 0 OR GastoAdmonPromo LIKE '%[^0-9 .]%');
GO

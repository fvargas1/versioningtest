SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_002]
AS

BEGIN

-- El "ID ACREDITADO ASIGNADO INST" solo debe contener letras en mayúsculas y números, sin caracteres distintos a estos.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion
FROM dbo.RW_R04C0443
WHERE CodigoPersona LIKE '%[^A-Z0-9]%' OR BINARY_CHECKSUM(CodigoPersona) <> BINARY_CHECKSUM(UPPER(CodigoPersona));

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_030]
AS

BEGIN

-- El monto de la subcuenta de vivienda es igual a cero (0) si "Entidad que otorgó el Cofinanciamiento"
-- es igual a cero (0) (columna 17), o si el "Tipo de Alta del Crédito" es igual 3 (columna 9).

SELECT CodigoCredito, CodigoCreditoCNBV, MontoSubCuenta, EntidadCoFin, TipoAlta
FROM dbo.RW_R04H0491
WHERE MontoSubCuenta = '0' AND (EntidadCoFin <> '0' AND TipoAlta <> '3');

END
GO

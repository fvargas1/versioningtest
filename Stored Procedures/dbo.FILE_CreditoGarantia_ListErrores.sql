SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_CreditoGarantia_ListErrores]
AS
SELECT
 CodigoGarantia,
 CodigoCredito,
 Porcentaje,
 Fuente
FROM dbo.FILE_CreditoGarantia
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO

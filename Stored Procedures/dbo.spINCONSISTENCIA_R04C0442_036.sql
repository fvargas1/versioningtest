SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_036]
AS

BEGIN

-- Si no hay banca de desarrollo que otorgó recursos (Col.32), entonces el monto apoyo banca de desarrollo (Col.31) deberá ser 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, CodigoBancoFondeador, MontoFondeo
FROM dbo.RW_R04C0442
WHERE LEN(ISNULL(CodigoBancoFondeador,'')) = 0 AND CAST(ISNULL(NULLIF(MontoFondeo,''),'0') AS DECIMAL(21)) > 0;

END
GO

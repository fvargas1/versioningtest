SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_035_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Producto Comercial de la Entidad deberá existir en catálogo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.CodigoCredito)
FROM dbo.RW_R04C0443 r
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial cat ON ISNULL(r.ProductoComercial,'') = cat.CodigoCNBV
WHERE cat.IdTipoCredito IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

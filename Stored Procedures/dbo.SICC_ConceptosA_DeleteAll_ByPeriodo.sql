SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ConceptosA_DeleteAll_ByPeriodo]
 @IdPeriodo INT
AS
DELETE FROM dbo.SICC_Conceptos_Periodo WHERE IdPeriodo = @IdPeriodo;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Credito_GarantiaList]
	@IdCredito BIGINT
AS
SELECT
	cg.IdGarantia,
	g.Codigo,
	g.Descripcion,
	tgr.Nombre AS TipoGarantia,
	g.ValorGarantia,
	ISNULL(cg.MontoUsadoGarantia,0) AS MontoAsignado
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_Garantia g ON g.IdGarantia = cg.IdGarantia
INNER JOIN dbo.SICC_TipoGarantia tgr ON g.IdTipoGarantia = tgr.IdTipoGarantia
WHERE cg.IdCredito = @IdCredito
ORDER BY tgr.Codigo;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Cofinavit_Reservas_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	TipoRegimen,
	Constante,
	FactorATR,
	ATR,
	FactorPorpago,
	PorPago,
	FactorPromRete,
	PromRete,
	FactorMaxATR,
	MaxATR,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion
FROM Historico.RW_Cofinavit_Reservas
WHERE IdPeriodoHistorico=@IdPeriodoHistorico;
GO

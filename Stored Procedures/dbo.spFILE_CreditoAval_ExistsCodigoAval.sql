SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_CreditoAval_ExistsCodigoAval]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_CreditoAval_ExistsCodigoAval';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_CreditoAval
SET errorFormato = 1
WHERE LEN(ISNULL(CodigoAval, '')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_CreditoAval_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoAval,
 'CodigoAval',
 CodigoAval,
 1,
 @Detalle
FROM dbo.FILE_CreditoAval
WHERE LEN(ISNULL(CodigoAval, '')) = 0;
GO

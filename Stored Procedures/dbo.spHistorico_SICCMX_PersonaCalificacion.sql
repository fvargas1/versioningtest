SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_PersonaCalificacion]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_PersonaCalificacion WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_PersonaCalificacion (
	IdPeriodoHistorico,
	Persona,
	EI,
	Reserva,
	PrctReserva,
	Calificacion,
	EI_Consumo,
	Reserva_Consumo,
	PrctReserva_Consumo,
	Calificacion_Consumo,
	EI_Hipotecario,
	Reserva_Hipotecario,
	PrctReserva_Hipotecario,
	Calificacion_Hipotecario
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	p.Codigo,
	pc.EI,
	pc.Reserva,
	pc.PrctReserva,
	cal.Codigo,
	pc.EI_Consumo,
	pc.Reserva_Consumo,
	pc.PrctReserva_Consumo,
	calCon.Codigo,
	pc.EI_Hipotecario,
	pc.Reserva_Hipotecario,
	pc.PrctReserva_Hipotecario,
	calHip.Codigo
FROM dbo.SICCMX_PersonaCalificacion pc
INNER JOIN dbo.SICCMX_Persona p ON pc.IdPersona = p.IdPersona
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON pc.IdCalificacion = cal.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calCon ON pc.IdCalificacion_Consumo = calCon.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calHip ON pc.IdCalificacion_Hipotecario = calHip.IdCalificacion;
GO

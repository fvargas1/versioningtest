SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_LineaCredito_EstadoDestino_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_LineaCredito_EstadoDestino_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_LineaCredito f
LEFT OUTER JOIN (
	SELECT CodigoEstado
	FROM dbo.SICC_Localidad2015
) AS cat ON LTRIM(f.EstadoDestino) = cat.CodigoEstado
WHERE cat.CodigoEstado IS NULL AND LEN(f.EstadoDestino) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_LineaCredito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.NumeroLinea,
 'EstadoDestino',
 f.EstadoDestino,
 2,
 @Detalle
FROM dbo.FILE_LineaCredito f
LEFT OUTER JOIN (
	SELECT CodigoEstado
	FROM dbo.SICC_Localidad2015
) AS cat ON LTRIM(f.EstadoDestino) = cat.CodigoEstado
WHERE cat.CodigoEstado IS NULL AND LEN(f.EstadoDestino) > 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Califica_BK12_PCT_SAT_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Califica_BK12_PCT_SAT_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Califica
SET errorFormato = 1
WHERE LEN(BK12_PCT_SAT)>0 AND (ISNUMERIC(ISNULL(BK12_PCT_SAT,'0')) = 0 OR BK12_PCT_SAT LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Califica_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoDeudor,
 'BK12_PCT_SAT',
 BK12_PCT_SAT,
 1,
 @Detalle
FROM dbo.FILE_Califica
WHERE LEN(BK12_PCT_SAT)>0 AND (ISNUMERIC(ISNULL(BK12_PCT_SAT,'0')) = 0 OR BK12_PCT_SAT LIKE '%[^0-9 .]%');
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_ActualizacionMasiva_Insert]
	@sessionID VARCHAR(100),
	@codigoBusqueda VARCHAR(100),
	@campo1Usuario VARCHAR(200),
	@campo2Usuario VARCHAR(200) = NULL,
	@campo3Usuario VARCHAR(200) = NULL
AS
INSERT INTO dbo.MIGRACION_ActualizacionMasiva_Temp (
	sessionID,
	codigoBusqueda,
	campo1Usuario,
	campo2Usuario,
	campo3Usuario
) VALUES (
	@sessionID,
	@codigoBusqueda,
	@campo1Usuario,
	@campo2Usuario,
	@campo3Usuario
)
GO

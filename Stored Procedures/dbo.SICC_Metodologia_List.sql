SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Metodologia_List]
AS
SELECT
 vw.IdMetodologia,
 vw.Codigo,
 vw.Nombre
FROM dbo.SICC_VW_Metodologia vw;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_UpdAlta_Lineas]
AS
DECLARE @FechaPeriodo DATETIME;
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo=1;

UPDATE lin
SET IdTipoAlta = NULL
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_CreditoCNBV cnbv ON lin.Codigo = cnbv.NumeroLinea
LEFT OUTER JOIN dbo.SICC_Periodo prd ON cnbv.IdPeriodo = prd.IdPeriodo
WHERE (prd.Fecha < @FechaPeriodo OR cnbv.IdPeriodo = 0) AND lin.Fuente = 'CR';

UPDATE lin
SET IdTipoBaja = NULL
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Credito cre ON lin.IdLineaCredito = cre.IdLineaCredito
INNER JOIN dbo.SICCMX_CreditoInfo inf ON cre.IdCredito = inf.IdCredito
LEFT OUTER JOIN dbo.SICC_DisposicionCredito disp ON inf.IdDisposicion = disp.IdDisposicion
WHERE ISNULL(disp.AplicaBaja,0) = 0 AND lin.Fuente = 'CR';

UPDATE lin
SET IdTipoBaja = NULL
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_VW_Lineas_Bajas vw ON lin.Codigo = vw.CodigoCredito
INNER JOIN dbo.SICC_PeriodoHistorico prd ON vw.IdPeriodoHistorico = prd.IdPeriodoHistorico AND prd.Fecha < @FechaPeriodo;
GO

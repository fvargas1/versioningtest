SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_029]
AS

BEGIN

-- Se anotará el número de días vencidos. Si no hay días vencidos se anotará 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, DiasVencido
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(DiasVencido,''),'-1') AS DECIMAL) < 0;

END
GO

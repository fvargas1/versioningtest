SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_DestinoCredito]
	@filtro VARCHAR(50)
AS
SELECT DISTINCT
	hm.CodigoBanco,
	CAST(cat.Codigo AS INT) AS Codigo,
	cat.Nombre,
	cat.CodigoCNBVEYM AS CNBV_A18,
	cat.CodigoCNBV_A20 AS CNBV_A20,
	cat.CodigoCNBV AS CNBV_A21,
	cat.CodigoCNBV_A22 AS CNBV_A22,
	cat.CodigoCNBVOD AS CNBV_OD,
	cat.CNBV_2016,
	cat.CNBV_2016_EYM AS CNBV_2016_EYM_OD,
	CAST(cat.Activo AS INT) AS Activo,
	SUM(CASE WHEN hm.CodigoBanco IS NULL THEN 0 ELSE 1 END) OVER() AS vwCB
FROM dbo.SICC_Destino cat
LEFT OUTER JOIN dbo.FILE_Credito_Homologacion hm ON cat.Codigo = hm.CodigoBW AND hm.Campo = 'DestinoCredito' AND hm.Activo = 1
WHERE ISNULL(hm.CodigoBanco,'')+'|'+ISNULL(cat.Codigo,'')+'|'+ISNULL(cat.Nombre,'')+'|'+ISNULL(cat.CodigoCNBVEYM,'')
	+'|'+ISNULL(cat.CodigoCNBV_A20,'')+'|'+ISNULL(cat.CodigoCNBV,'')+'|'+ISNULL(cat.CodigoCNBV_A22,'')+'|'+ISNULL(cat.CodigoCNBVOD,'')
	+'|'+ISNULL(cat.CNBV_2016,'')+'|'+ISNULL(cat.CNBV_2016_EYM,'') LIKE '%' + @filtro + '%'
ORDER BY CAST(cat.Codigo AS INT);
GO

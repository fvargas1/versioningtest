SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_Estado_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_Estado_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Persona f
LEFT OUTER JOIN (
	SELECT CodigoEstado
	FROM dbo.SICC_Localidad2015
) AS mun ON LTRIM(f.Estado) = mun.CodigoEstado
WHERE mun.CodigoEstado IS NULL AND LEN(f.Estado) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCliente,
 'Estado',
 f.Estado,
 2,
 @Detalle
FROM dbo.FILE_Persona f
LEFT OUTER JOIN (
	SELECT CodigoEstado
	FROM dbo.SICC_Localidad2015
) AS mun ON LTRIM(f.Estado) = mun.CodigoEstado
WHERE mun.CodigoEstado IS NULL AND LEN(f.Estado) > 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Periodo_List]
AS
SELECT
 vw.IdPeriodo,
 vw.Fecha,
 vw.Trimestral,
 CASE WHEN vw.Trimestral = 1 THEN 'on' ELSE 'blank' END AS TrimestralImg,
 vw.Activo,
 CASE WHEN vw.Activo = 1 THEN 'on' ELSE 'off' END AS ActivoImg
FROM dbo.SICC_VW_Periodo vw
ORDER BY Fecha DESC;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_109_Count]
 @IdInconsistencia BIGINT
AS
BEGIN

-- Si la clave de Revocable (cve_revocable=2) en el formulario de ALTAS es igual a 2,
-- entonces en el formulario de SEGUIMIENTO la Exposición al Incumplimiento (dat_exposicion_incumplimiento)
-- debe ser igual a la Responsabilidad Total (dat_responsabilidad_total*(max(100%,(100*dat_responsabilidad_total/dat_monto_credito_linea_aut)^ -0.5794))).
-- El monto de línea de crédito autorizada debe tomarse del formulario de altas.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(DISTINCT rep.NumeroDisposicion)
FROM dbo.RW_VW_R04C0464_INC rep
INNER JOIN dbo.SICCMX_VW_Datos_Reportes_Altas vw ON rep.CodigoCreditoCNBV = vw.CNBV
WHERE ISNULL(vw.TipoLinea,'') = '2'
 AND CAST(ISNULL(NULLIF(rep.EITotal,''),0) AS DECIMAL) <> CAST(CAST(ISNULL(NULLIF(rep.SaldoInsoluto,''),0) AS DECIMAL) * (
 CASE WHEN ISNULL(CAST(NULLIF(vw.MontoLineaAut,'') AS DECIMAL(23,2)),0) = 0 THEN 1
 ELSE dbo.MAX2VAR(1, NULLIF(POWER(dbo.NoN(NULLIF(rep.SaldoInsoluto,''), NULLIF(vw.MontoLineaAut,'')),0), -0.5794)) END) AS DECIMAL);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UserModificationLog_Ins]
	@IdUser AS INT,
	@IdUserModified AS INT,
	@FechaModificacion AS DATETIME,
	@ModificationType AS INT
AS
INSERT INTO dbo.BAJAWARE_UserLog (IdUser, IdUserModified, FechaModificacion, ModificationType)
VALUES (@IdUser, @IdUserModified, @FechaModificacion, @ModificationType);
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCase_Sel]
	@IdUseCase BAJAWARE_utID
AS
SELECT IdUseCase, Active, Description, Name, CodeName, Visible, PageLink, IdUseCaseGroup, SortOrder, StringKey
FROM dbo.BAJAWARE_UseCase
WHERE IdUseCase = @IdUseCase;
GO

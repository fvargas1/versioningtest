SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_003]
AS

BEGIN

-- Si el Número de Días de Mora Promedio con Instituciones Bancarias en los Últimos meses (dat_dias_mora_prom_bcos)
-- es > 0 y < 2.12, entonces el Puntaje Asignado por los Días de Mora Promedio con Instituciones Bancarias en los
-- Últimos 12 meses (cve_ptaje_dias_mora_bcos) debe ser = 72

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	DiasMoraInstBanc,
	P_DiasMoraInstBanc AS Puntos_DiasMoraInstBanc
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(DiasMoraInstBanc AS DECIMAL(10,6)) > 0 AND CAST(DiasMoraInstBanc AS DECIMAL(10,6)) < 2.12 AND ISNULL(P_DiasMoraInstBanc,'') <> '72';

END


GO

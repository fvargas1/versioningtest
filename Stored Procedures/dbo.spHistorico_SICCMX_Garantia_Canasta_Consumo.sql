SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Garantia_Canasta_Consumo]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Garantia_Canasta_Consumo WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Garantia_Canasta_Consumo (
	IdPeriodoHistorico,
	Consumo,
	TipoGarantia,
	He,
	Hc,
	Hfx,
	C,
	PrctCobSinAju,
	PrctCobAjust,
	MontoCobAjust,
	[PI],
	EI_Ajust,
	SeveridadCorresp,
	MontoRecuperacion,
	Reserva,
	EsDescubierto
)
SELECT
	@IdPeriodoHistorico,
	con.Codigo,
	tg.Codigo,
	can.He,
	can.Hc,
	can.Hfx,
	can.C,
	can.PrctCobSinAju,
	can.PrctCobAjust,
	can.MontoCobAjust,
	can.[PI],
	can.EI_Ajust,
	can.SeveridadCorresp,
	can.MontoRecuperacion,
	can.Reserva,
	can.EsDescubierto
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICCMX_Consumo con ON can.IdConsumo = con.IdConsumo
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia;
GO

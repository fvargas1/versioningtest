SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Calificacion_RTH]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaHipotecario FROM dbo.SICCMX_Hipotecario_Metodologia WHERE Codigo = '2';

UPDATE res
SET IdCalificacionExpuesto = calExp.IdCalificacion,
	IdCalificacionCubierto = calCub.IdCalificacion
FROM dbo.SICCMX_HipotecarioReservas res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp ON vp.IdHipotecario = res.IdHipotecario
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calExp ON vp.IdMetodologia = calExp.IdMetodologia AND res.PorcentajeExpuesto BETWEEN calExp.RangoMenor AND calExp.RangoMayor
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calCub ON vp.IdMetodologia = calCub.IdMetodologia AND res.PorcentajeCubierto BETWEEN calCub.RangoMenor AND calCub.RangoMayor
WHERE vp.IdMetodologia = @IdMetodologia;
GO

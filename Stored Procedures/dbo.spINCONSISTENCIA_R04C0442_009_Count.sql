SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_009_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Nombre del Grupo de Riesgo no se puede omitir.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(CodigoCredito)
FROM dbo.RW_R04C0442
WHERE LEN(ISNULL(GrupoRiesgo,'')) = 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

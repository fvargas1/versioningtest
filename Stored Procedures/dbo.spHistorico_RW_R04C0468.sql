SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0468]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0468);
 
DELETE FROM Historico.RW_R04C0468 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0468 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, TipoCartera, ActividadEconomica, GrupoRiesgo, LocalidadDeudor,
	DomicilioMunicipio, DomicilioEstado, Nacionalidad, NumInfoCrediticia, CURP, LEI, TipoAlta, TipoProducto, TipoOperacion, DestinoCredito,
	CodigoCredito, CodigoCreditoCNBV, GrupalCNBV, MontoLineaAutorizado, FechaMaxDisponer, FechaVencLinea, Moneda, FormaDisposicion, TipoLinea,
	PrelacionPago, NoRUGM, AcredRelacionado, InstitucionOrigen, TasaReferencia, AjusteTasaReferencia, OperacionTasaReferencia, FrecRevTasa,
	PeriodicidadCapital, PeriodicidadInteres, MesesGraciaCapital, MesesGraciaInteres, ComAperturaTasa, ComAperturaMonto, ComDispTasa, ComDispMonto,
	LocalidadDestinoCredito, MunicipioDestinoCredito, EstadoDestinoCredito, ActividadDestinoCredito
)
SELECT 
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	TipoCartera,
	ActividadEconomica,
	GrupoRiesgo,
	LocalidadDeudor,
	DomicilioMunicipio,
	DomicilioEstado,
	Nacionalidad,
	NumInfoCrediticia,
	CURP,
	LEI,
	TipoAlta,
	TipoProducto,
	TipoOperacion,
	DestinoCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	GrupalCNBV,
	MontoLineaAutorizado,
	FechaMaxDisponer,
	FechaVencLinea,
	Moneda,
	FormaDisposicion,
	TipoLinea,
	PrelacionPago,
	NoRUGM,
	AcredRelacionado,
	InstitucionOrigen,
	TasaReferencia,
	AjusteTasaReferencia,
	OperacionTasaReferencia,
	FrecRevTasa,
	PeriodicidadCapital,
	PeriodicidadInteres,
	MesesGraciaCapital,
	MesesGraciaInteres,
	ComAperturaTasa,
	ComAperturaMonto,
	ComDispTasa,
	ComDispMonto,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM dbo.RW_R04C0468
WHERE IdReporteLog = @IdReporteLog;
GO

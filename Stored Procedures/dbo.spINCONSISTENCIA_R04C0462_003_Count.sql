SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0462_003_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Tipo de Baja (cve_tipo_baja_credito) es = 133, validar que el mismo ID Metodología CNBV (dat_id_credito_met_cnbv)
-- se encuentre en el subreporte 458 con Tipo de Alta (cve_tipo_alta_credito) = 133.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0462_INC rep
LEFT OUTER JOIN Historico.RW_R04C0458 hist ON rep.CodigoCreditoCNBV = hist.CodigoCnbv
WHERE rep.TipoBaja = '133' AND hist.IdTipoAlta <> '133';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO

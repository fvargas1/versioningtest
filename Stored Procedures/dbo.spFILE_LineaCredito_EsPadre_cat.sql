SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_LineaCredito_EsPadre_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_LineaCredito_EsPadre_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_LineaCredito
SET errorCatalogo = 1
WHERE LEN(EsPadre)>0 AND LTRIM(EsPadre) NOT IN ('0','1');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_LineaCredito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 NumeroLinea,
 'EsPadre',
 EsPadre,
 2,
 @Detalle
FROM dbo.FILE_LineaCredito
WHERE LEN(EsPadre)>0 AND LTRIM(EsPadre) NOT IN ('0','1');
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_ConsumoGarantia_ListErrores]
AS
SELECT
 CodigoGarantia,
 CodigoCredito,
 Porcentaje,
 Fuente
FROM dbo.FILE_ConsumoGarantia
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO

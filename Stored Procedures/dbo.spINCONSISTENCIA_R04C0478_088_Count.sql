SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_088_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Las claves de Actividad Económica (cve_actividad_economica) válidas para créditos otorgados al Gobierno Federal (tipo de cartera = 510) son:
-- 93111, 93121, 93131, 93141, 93151, 93161 ó 93181.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC
WHERE TipoCartera='510' AND ActEconomica NOT IN ('93111','93121','93131','93141','93151','93161','93181')

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO

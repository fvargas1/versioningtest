SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_039]
AS

BEGIN

-- En el "NUMERO DEL AVALUO DEL INMUEBLE" el valor de los dígitos 6 y 7 en conjunto no pueden ser mayor al año reportado en la fecha de otorgamiento del crédito (col. 11).

SELECT CodigoCredito, CodigoCreditoCNBV, NumeroAvaluo, FechaOtorgamiento
FROM dbo.RW_R04H0491
WHERE SUBSTRING(NumeroAvaluo,6,2) <> SUBSTRING(FechaOtorgamiento,3,2);

END
GO

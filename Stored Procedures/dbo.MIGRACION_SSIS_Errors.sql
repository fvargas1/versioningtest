SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_SSIS_Errors]
	@jobname VARCHAR(50),
	@enddate VARCHAR(50)
AS
SELECT Codename, TipoError, Descripcion
FROM dbo.MIGRACION_Carga_Errores
WHERE Codename=@jobname
UNION
SELECT @jobname, event, message
FROM dbo.MIGRACION_VW_DtsLog vw
WHERE vw.source=@jobname AND event='OnError' AND endtime=@enddate;
GO

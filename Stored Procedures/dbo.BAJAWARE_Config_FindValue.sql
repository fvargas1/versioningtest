SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Config_FindValue]
	@CodeName VARCHAR(50)
AS
SELECT Value FROM dbo.BAJAWARE_Config WHERE CodeName = @CodeName;
GO

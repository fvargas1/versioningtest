SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0480_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0480';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0480;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0480 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo,
 CreditoReportadoSIC, HITenSIC, FechaConsultaSIC, FechaInfoFinanciera, MesesPI100, GarantiaLeyFederal, CumpleCritContGub, P_DiasMoraInstBanc,
 P_PorcPagoInstBanc, P_NumInstRep, P_PorcPagoInstNoBanc, P_PagosInfonavit, P_DiasAtrInfonavit, P_TasaRetLab, P_RotActTot, P_RotCapTrabajo,
 P_RendCapROE, DiasMoraInstBanc, PorcPagoInstBanc, NumInstRep, PorcPagoInstNoBanc, PagosInfonavit, DiasAtrInfonavit, NumeroEmpleados,
 TasaRetLab, PasivoCirculante, UtilidadNeta, CapitalContable, ActivoTotalAnual, VentasNetasTotales, RotActTot, ActivoCirculante,
 RotCapTrabajo, RendCapROE
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '0480',
 per.Codigo AS CodigoPersona,
 per.RFC AS RFC,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombrePersona,
 ppi.[PI] * 100 AS [PI],
 ppi.FactorTotal AS PuntajeTotal,
 ppi.PonderadoCuantitativo AS PuntajeCuantitativo,
 ppi.PonderadoCualitativo AS PuntajeCualitativo,
 crs.CodigoCNBV AS CreditoReportadoSIC,
 htc.CodigoCNBV AS HITenSIC,
 CASE WHEN anx.FechaInfoBuro IS NULL THEN '0' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoBuro,102),'.',''),1,6) END AS FechaConsultaSIC,
 CASE WHEN anx.FechaInfoFinanc IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoFinanc,102),'.',''),1,6) END AS FechaInfoFinanciera,
 CASE WHEN ISNULL(DATEDIFF(MONTH,castigados.FechaIngreso,@FechaPeriodo),0) > 12 THEN 0 ELSE ISNULL(DATEDIFF(MONTH,castigados.FechaIngreso,@FechaPeriodo),0) END AS MesesPI100,
 CASE WHEN garLey.IdPersona IS NULL THEN 790 ELSE 770 END AS GarantiaLeyFederal,
 ccc.CodigoCNBV AS CumpleCritContGub,
 ptNMC.[22_DIAS_MORA_INST_BANC] AS P_DiasMoraInstBanc,
 ptNMC.[22_PORC_PAGO_INST_BANC] AS P_PorcPagoInstBanc,
 ptNMC.[22_NUM_INST_REP] AS P_NumInstRep,
 ptNMC.[22_PORC_PAGO_INST_NOBANC] AS P_PorcPagoInstNoBanc,
 ptNMC.[22_PAGOS_INFONAVIT] AS P_PagosInfonavit,
 ptNMC.[22_DIAS_ATR_INFONAVIT] AS P_DiasAtrInfonavit,
 ptNMC.[22_TASA_RET_LAB] AS P_TasaRetLab,
 ptNMC.[22_ROT_ACT_TOT] AS P_RotActTot,
 ptNMC.[22_ROT_CAP_TRABAJO] AS P_RotCapTrabajo,
 ptNMC.[22_REND_CAP_ROE] AS P_RendCapROE,
 vlNMC.[22_DIAS_MORA_INST_BANC] AS DiasMoraInstBanc,
 vlNMC.[22_PORC_PAGO_INST_BANC] AS PorcPagoInstBanc,
 vlNMC.[22_NUM_INST_REP] AS NumInstRep,
 vlNMC.[22_PORC_PAGO_INST_NOBANC] AS PorcPagoInstNoBanc,
 vlNMC.[22_PAGOS_INFONAVIT] AS PagosInfonavit,
 vlNMC.[22_DIAS_ATR_INFONAVIT] AS DiasAtrInfonavit,
 anx.NumeroEmpleados AS NumeroEmpleados,
 vlNMC.[22_TASA_RET_LAB] AS TasaRetLab,
 anx.PasivoCirculante AS PasivoCirculante,
 anx.UtilidadNeta AS UtilidadNeta,
 anx.CapitalContableProm AS CapitalContable,
 anx.ActTotalAnual AS ActivoTotalAnual,
 anx.VentNetTotAnuales AS VentasNetasTotales,
 vlNMC.[22_ROT_ACT_TOT] AS RotActTot,
 anx.ActivoCirculante AS ActivoCirculante,
 vlNMC.[22_ROT_CAP_TRABAJO] AS RotCapTrabajo, vlNMC.[22_REND_CAP_ROE] AS RendCapROE
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Anexo22 anx ON pInfo.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON anx.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_VW_PersonasValor_A22 vlNMC ON ppi.IdPersona = vlNMC.IdPersona
INNER JOIN dbo.SICCMX_VW_PersonasPuntaje_A22 ptNMC ON ppi.IdPersona = ptNMC.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
LEFT OUTER JOIN dbo.SICC_CredRepSIC crs ON cInfo.CredRepSIC = crs.IdCredRepSIC
LEFT OUTER JOIN dbo.SICC_HitSic htc ON pInfo.HITSIC = htc.IdHitSic
LEFT OUTER JOIN dbo.SICC_CumpleCritContGral ccc ON cInfo.CumpleCritContGral = ccc.IdCumpleCritContGral
LEFT OUTER JOIN (
 SELECT IdPersona, MAX(FechaIngreso) AS FechaIngreso
 FROM dbo.SICCMX_Creditos_Castigados_SIC
 GROUP BY IdPersona
) AS castigados ON per.IdPersona = castigados.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_Personas_GarantiaLey garLey ON per.IdPersona = garLey.IdPersona
WHERE ISNULL(anx.OrgDescPartidoPolitico,0)=1 AND cre.MontoValorizado > 0;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0480 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Monto_Aval]
AS
UPDATE dbo.SICCMX_CreditoAval SET Porcentaje = dbo.MIN2VAR(1, PorcentajeOriginal);

UPDATE ca
SET Monto = CASE WHEN cob.Codigo = '1' THEN cre.EI_Total ELSE CAST(cre.SaldoVigenteValorizado + cre.SaldoVencidoValorizado AS DECIMAL(23,2)) END * ca.Porcentaje
FROM dbo.SICCMX_CreditoAval ca
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON ca.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Aval av ON ca.IdAval = av.IdAval
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito AND crv.PIAval = 1
INNER JOIN dbo.SICC_TipoCobertura cob ON av.TipoCobertura = cob.IdTipoCobertura
WHERE ca.Aplica = 1;

-- ACTUALIZAMOS EL PORCENTAJE REAL PARA AVALES QUE SOLO CUBREN EL CAPITAL
UPDATE ca
SET Porcentaje = Monto / cre.EI_Total
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_CreditoAval ca ON cre.IdCredito = ca.IdCredito AND ca.Aplica = 1
INNER JOIN dbo.SICCMX_Aval av ON ca.IdAval = av.IdAval AND av.Aplica = 1
INNER JOIN dbo.SICC_TipoCobertura tc ON av.TipoCobertura = tc.IdTipoCobertura AND tc.Codigo <> '1'
WHERE cre.EI_Total > 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04H0491_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	ProductoHipotecario,
	CategoriaCredito,
	TipoAlta,
	DestinoCredito,
	FechaOtorgamiento,
	FechaVencimiento,
	DenominacionCredito,
	MontoOriginal,
	Comisiones,
	MontoSubsidioFederal,
	EntidadCoFin,
	MontoSubCuenta,
	MontoOtorgadoCoFin,
	Apoyo,
	ValorOriginalVivienda,
	ValorAvaluo,
	NumeroAvaluo,
	Localidad,
	FechaFirmaReestructura,
	FechaVencimientoReestructura,
	MontoReestructura,
	DenominacionCreditoReestructura,
	IngresosMensuales,
	TipoComprobacionIngresos,
	SectorLaboral,
	NumeroConsultaSIC,
	PeriodicidadAmortizaciones,
	TipoTasaInteres,
	TasaRef,
	AjusteTasaRef,
	SeguroAcreditado,
	TipoSeguro,
	EntidadSeguro,
	PorcentajeCubiertoSeguro,
	MontoSubCuentaGarantia,
	INTEXP,
	SDES,
	Municipio,
	Estado
FROM dbo.RW_VW_R04H0491
ORDER BY NumeroSecuencia;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_RAi]
AS
-- INICIALIZAMOS LA RAi CON LOS VALORES RECIBIDOS EN LA MIGRACION
UPDATE pre
SET RAi = info.RAi
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON pre.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaHipotecario AND met.Codigo = '1';

UPDATE vp
SET RAi = CASE WHEN ISNULL(hip.SaldoTotalValorizado,0) = 0 THEN 0 ELSE (ISNULL(vp.SUBCVi,0) + ISNULL(vp.SDESi,0) + ISNULL(hip.MontoGarantiaValorizado,0)) / ISNULL(hip.SaldoTotalValorizado,0) END
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON vp.IdHipotecario = hip.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON vp.IdMetodologia = met.IdMetodologiaHipotecario AND met.Codigo = '1'
WHERE vp.RAi IS NULL;
GO

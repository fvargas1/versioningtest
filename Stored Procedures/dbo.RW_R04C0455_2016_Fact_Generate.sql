SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0455_2016_Fact_Generate]
	@IdReporteLog BIGINT,
	@IdPeriodo BIGINT,
	@Entidad VARCHAR(50)
AS
INSERT INTO dbo.RW_R04C0455_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC, HITSIC,
 FechaConsultaSIC, FechaInfoFinanc, MesesPI100, ID_PI100, GarantiaLeyFederal, CumpleCritContGral, P_DiasMoraInstBanc, P_PorcPagoInstBanc, P_PorcPagoInstNoBanc,
 P_NumInstCalif, P_DeudaTotalPartEleg, P_ServDeudaIngAjust, P_DeudaCortoPlazoTotal, P_IngTotGastoCorriente, P_InvIngTotales, P_IngPropIngTotales, SdoDeudaTotal,
 SdoPartEleg, SdoIngTotalesAjust, SdoDeudaCortoPlazo, SdoIngTotales, SdoGastosCorrientes, SdoInversion, SdoIngPropios, DiasMoraInstBanc, PorcPagoInstBanc,
 PorcPagoInstNoBanc, NumInstCalif, P_TasaDesempLocal, P_ServFinEntReg, P_ObligConting, P_BalanceOpPIB, P_NivEficRecauda, P_SolFlexEjecPres, P_SolFlexImpLoc,
 P_TranspFinPublicas, P_EmisionDeuda
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '455',
 'FACTORADO' + per.Codigo AS CodigoPersona,
 ppi.[PI] * 100 AS [PI],
 ppi.FactorTotal AS PuntajeTotal,
 ppi.FactorCuantitativo AS PuntajeCuantitativo,
 ppi.FactorCualitativo AS PuntajeCualitativo,
 '750' AS CreditoReportadoSIC,
 '2' AS HITenSIC,
 CASE WHEN anx.FechaInfoBuro IS NULL THEN '0' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoBuro,102),'.',''),1,6) END AS FechaConsultaSIC,
 CASE WHEN anx.FechaInfoFinanc IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoFinanc,102),'.',''),1,6) END AS FechaInfoFinanciera,
 '0' AS MesesPI100,
 '0' AS ID_PI100,
 '790' AS GarantiaLeyFederal,
 '810' AS CumpleCritContGral,
 ptNMC.[EM_DIAS_MORA] AS P_DiasMoraInstBanc,
 ptNMC.[EM_POR_PAGOS_BANCARIOS] AS P_PorcPagoInstBanc,
 ptNMC.[EM_POR_PAGOS_NOBANCARIOS] AS P_PorcPagoInstNoBanc,
 ptNMC.[EM_NUM_INSTITUCIONES] AS P_NumInstCalif,
 ptNMC.[EM_DEUDA_TOTAL] AS P_DeudaTotalPartEleg,
 ptNMC.[EM_SERVICIO_DEUDA] AS P_ServDeudaIngAjust,
 ptNMC.[EM_DEUDA_CORTOPLAZO] AS P_DeudaCortoPlazoTotal,
 ptNMC.[EM_INGRESOS_TOTALES] AS P_IngTotGastoCorriente,
 ptNMC.[EM_INVERSION] AS P_InvIngTotales,
 ptNMC.[EM_INGRESOS_AUTONOMOS] AS P_IngPropIngTotales,
 anx.SaldoDeudaTotal AS SdoDeudaTotal,
 anx.SaldoPartEle AS SdoPartEleg,
 anx.SaldoIngTotAju AS SdoIngTotalesAjust,
 anx.SaldoDeuCortPlzo AS SdoDeudaCortoPlazo,
 anx.SaldoIngTot AS SdoIngTotales,
 anx.SaldoGastoCorr AS SdoGastosCorrientes,
 anx.SaldoInversion AS SdoInversion,
 anx.SaldoIngPropios AS SdoIngPropios,
 vwVal.EM_DIAS_MORA AS DiasMoraInstBanc,
 vwVal.EM_POR_PAGOS_BANCARIOS AS PorcPagoInstBanc,
 vwVal.EM_POR_PAGOS_NOBANCARIOS AS PorcPagoInstNoBanc,
 vwVal.EM_NUM_INSTITUCIONES AS NumInstCalif,
 ptNMC.[EM_TASA_DESEMPLEO] AS P_TasaDesempLocal,
 ptNMC.[EM_PRESENCIA_SERVICIOS] AS P_ServFinEntReg,
 ptNMC.[EM_OBLIGACIONES_CON] AS P_ObligConting,
 ptNMC.[EM_BALANCE_OPERATIVO] AS P_BalanceOpPIB,
 ptNMC.[EM_NIVEL_RECAUDACION] AS P_NivEficRecauda,
 ptNMC.[EM_SOLIDEZ_FLEXIBILIDAD_EJE] AS P_SolFlexEjecPres,
 ptNMC.[EM_SOLIDEZ_FLEXIBILIDAD_IMP] AS P_SolFlexImpLoc,
 ptNMC.[EM_TRANSPARENCIA_FINANZAS] AS P_TranspFinPublicas,
 ptNMC.[EM_EMISION_DEUDA] AS P_EmisionDeuda
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Credito cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoAval ca ON cre.IdCredito = ca.IdCredito
INNER JOIN dbo.SICCMX_Aval aval ON ca.IdAval = aval.IdAval
INNER JOIN dbo.SICC_FiguraGarantiza fig ON aval.FiguraGarantiza = fig.IdFigura AND fig.ReportePI = 1
INNER JOIN dbo.SICCMX_VW_Anexo18_GP anx ON aval.IdAval = anx.IdGP
INNER JOIN dbo.SICC_EsGarante esg ON anx.EsGarante = esg.IdEsGarante AND esg.Layout = 'AVAL'
INNER JOIN dbo.SICCMX_Persona_PI_GP ppi ON anx.IdGP = ppi.IdGP AND anx.EsGarante = ppi.EsGarante
INNER JOIN dbo.SICCMX_VW_PersonasPuntaje_A18_GP ptNMC ON ppi.IdGP = ptNMC.IdGP AND ppi.EsGarante = ptNMC.EsGarante
INNER JOIN dbo.SICCMX_VW_PersonasValor_A18_GP vwVal ON ppi.IdGP = vwVal.IdGP AND ppi.EsGarante = vwVal.EsGarante;
GO

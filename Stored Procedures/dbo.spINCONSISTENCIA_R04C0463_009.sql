SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_009]
AS
BEGIN
-- El monto de la línea de crédito simple debe ser mayor o igual a cero

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	MontoSinAccesorios
FROM dbo.RW_VW_R04C0463_INC
WHERE CAST(ISNULL(NULLIF(MontoSinAccesorios,''),'-1') AS DECIMAL) < 0;

END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_User_Upd]
	@IdUser BAJAWARE_utID,
	@Active BAJAWARE_utActivo,
	@Description BAJAWARE_utDescripcion100,
	@Blocked BAJAWARE_utActivo,
	@Name BAJAWARE_utDescripcion100,
	@Email BAJAWARE_utDescripcion100,
	@Telefono BAJAWARE_utTelefono,
	@Mobil BAJAWARE_utTelefono,
	@NumeroControl BAJAWARE_utNombreCodigo,
	@Username BAJAWARE_utNombreCodigo,
	@UsuarioWindows BAJAWARE_utDescripcion100,
	@FechaNacimiento DATETIME,
	@NeverLogedIn BIT,
	@UltimaFechaLogin DATETIME
AS

UPDATE dbo.BAJAWARE_User
SET
	Active = @Active,
	Description = @Description,
	Blocked = @Blocked,
	Name = @Name,
	Email = @Email,
	Telefono = @Telefono,
	Mobil = @Mobil,
	NumeroControl = @NumeroControl,
	Username = @Username,
	UsuarioWindows = @UsuarioWindows,
	FechaNacimiento = @FechaNacimiento,
	NeverLogedIn = @NeverLogedIn
WHERE IdUser = @IdUser;


IF (MONTH(@UltimaFechaLogin)=MONTH(GETDATE()) AND DAY(@UltimaFechaLogin)=DAY(GETDATE()) AND YEAR(@UltimaFechaLogin)=YEAR(GETDATE()))
BEGIN
UPDATE dbo.BAJAWARE_User
SET
	UltimaFechaLogin= GETDATE(),
	LastLoginAccess= GETDATE()
WHERE IdUser=@IdUser;
END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0420Datos_Insert]
AS
DECLARE @IdPeriodoHistorico BIGINT;
DECLARE @FechaPeriodo DATETIME;

SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

SELECT @IdPeriodoHistorico = IdPeriodoHistorico
FROM dbo.SICC_PeriodoHistorico
WHERE YEAR(Fecha) = YEAR(DATEADD(MONTH, -1, @FechaPeriodo)) AND MONTH(Fecha) = MONTH(DATEADD(MONTH, -1, @FechaPeriodo)) AND Reporte = 1;


TRUNCATE TABLE R04.[0420Datos];

/* COMERCIAL */
-- Insertamos la Informacion Historica
INSERT INTO R04.[0420Datos] (
 Codigo,
 IdMoneda,
 SituacionHistorica,
 CodigoProducto,
 SaldoHistorico,
 InteresVigenteHistorico,
 InteresCarteraVencidaHistorico,
 InteresVencidoHistorico
)
SELECT
 cr.Codigo,
 mon.IdMoneda,
 cr.SituacionCredito,
 info.TipoProductoSerie4,
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(cr.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(cr.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(cr.SaldoCapitalVigente,0) + ISNULL(cr.SaldoCapitalVencido,0) END,
 ISNULL(cr.InteresVigente,0),
 ISNULL(cr.InteresCarteraVencida,0),
 ISNULL(cr.InteresVencido,0)
FROM Historico.SICCMX_Credito cr
INNER JOIN Historico.SICCMX_CreditoInfo info ON info.IdPeriodoHistorico = cr.IdPeriodoHistorico AND info.Credito = cr.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.Codigo = cr.Moneda
WHERE cr.IdPeriodoHistorico = @IdPeriodoHistorico
 AND (info.Posicion <> '181' AND info.TipoLinea <> '181') -- no se consideran cartas de credito
 AND cr.SituacionCredito = '2' -- creditos vencidos


-- Actualizar info actual de los creditos historicos
UPDATE datos
SET
 CodigoProducto = tp.Codigo,
 SituacionActual = sc.Codigo,
 SaldoActual = CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(cr.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(cr.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(cr.SaldoCapitalVigente,0) + ISNULL(cr.SaldoCapitalVencido,0) END,
 InteresVigente = ISNULL(cr.InteresVigente,0),
 InteresCarteraVencida = ISNULL(cr.InteresCarteraVencida,0),
 InteresVencido = ISNULL(cr.InteresVencido,0),
 IdMoneda = cr.IdMoneda,
 Reestructurado = CASE WHEN reest.Codigo = '2' THEN 1 ELSE 0 END,
 CodigoCreditoReestructurado = crInfo.CodigoCreditoReestructurado
FROM R04.[0420Datos] datos 
INNER JOIN dbo.SICCMX_VW_Credito_NMC cr ON cr.CodigoCredito = datos.Codigo
LEFT OUTER JOIN dbo.SICCMX_CreditoInfo crInfo ON crInfo.IdCredito = cr.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = crInfo.IdTipoProductoSerie4
LEFT OUTER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Reestructura reest ON cr.IdReestructura = reest.IdReestructura
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cr.IdMoneda = mon.IdMoneda
WHERE cr.Posicion <> '181' AND cr.TipoLinea <> '181'; -- no se consideran cartas de credito


-- Insertar creditos que estan en la tabla SICC_Credito pero que no estab en SICCMX_Credito
INSERT INTO R04.[0420Datos] (
 Codigo,
 IdMoneda,
 CodigoProducto,
 SituacionActual,
 SaldoActual,
 InteresVigente,
 InteresCarteraVencida,
 InteresVencido,
 Reestructurado,
 CodigoCreditoReestructurado
)
SELECT
 cr.CodigoCredito,
 cr.IdMoneda,
 tp.Codigo,
 sc.Codigo,
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(cr.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(cr.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(cr.SaldoCapitalVigente,0) + ISNULL(cr.SaldoCapitalVencido,0) END,
 ISNULL(cr.InteresVigente,0),
 ISNULL(cr.InteresCarteraVencida,0),
 ISNULL(cr.InteresVencido,0),
 CASE WHEN reest.Codigo = '2' THEN 1 ELSE 0 END,
 crInfo.CodigoCreditoReestructurado
FROM dbo.SICCMX_VW_Credito_NMC cr
LEFT OUTER JOIN dbo.SICCMX_CreditoInfo crInfo ON cr.IdCredito = crInfo.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = crInfo.IdTipoProductoSerie4
LEFT OUTER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Reestructura reest ON cr.IdReestructura = reest.IdReestructura
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cr.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN R04.[0420Datos] dat ON cr.CodigoCredito = dat.Codigo
WHERE sc.Codigo IN ('2') -- creditos vencidos
 AND (cr.Posicion <> '181' AND cr.TipoLinea <> '181') -- no se consideran cartas de credito
 AND dat.Id IS NULL;


-- CONSUMOS
INSERT INTO R04.[0420Datos] (
 Codigo,
 IdMoneda,
 SituacionHistorica,
 CodigoProducto,
 SaldoHistorico,
 InteresVigenteHistorico,
 InteresCarteraVencidaHistorico,
 InteresVencidoHistorico
)
SELECT
 con.Codigo,
 mon.IdMoneda,
 con.SituacionCredito,
 info.TipoProductoSerie4,
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(con.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(con.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(con.SaldoCapitalVigente,0) + ISNULL(con.SaldoCapitalVencido,0) END,
 ISNULL(con.InteresVigente,0),
 ISNULL(con.InteresCarteraVencida,0),
 ISNULL(con.InteresVencido,0)
FROM Historico.SICCMX_Consumo con
INNER JOIN Historico.SICCMX_ConsumoInfo info ON info.IdPeriodoHistorico = con.IdPeriodoHistorico AND info.Consumo = con.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.Moneda = mon.Codigo
WHERE con.IdPeriodoHistorico = @IdPeriodoHistorico AND con.SituacionCredito = '2'; --vencidos


-- Actualizar info actual de consumo historicos
UPDATE datos
SET
 CodigoProducto = tp.Codigo,
 SituacionActual = sc.Codigo,
 SaldoActual = CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(con.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(con.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(con.SaldoCapitalVigente,0) + ISNULL(con.SaldoCapitalVencido,0) END,
 InteresVigente = ISNULL(con.InteresVigente,0),
 InteresCarteraVencida = ISNULL(con.InteresCarteraVencida,0),
 InteresVencido = ISNULL(con.InteresVencido,0),
 IdMoneda = info.IdMoneda
FROM R04.[0420Datos] datos
INNER JOIN dbo.SICCMX_Consumo con ON con.Codigo = datos.Codigo
INNER JOIN dbo.SICCMX_ConsumoInfo info ON info.IdConsumo = con.IdConsumo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sc ON sc.IdSituacionConsumo = con.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda;


-- Insertar consumos que estan en la tabla SICC_Consumo pero que no estan en Historico.SICCMX_Consumo
INSERT INTO R04.[0420Datos] (
 Codigo,
 IdMoneda,
 CodigoProducto,
 SituacionActual,
 SaldoActual,
 InteresVigente,
 InteresCarteraVencida,
 InteresVencido
)
SELECT
 con.Codigo,
 info.IdMoneda,
 tp.Codigo,
 sc.Codigo,
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(con.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(con.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(con.SaldoCapitalVigente,0) + ISNULL(con.SaldoCapitalVencido,0) END,
 ISNULL(con.InteresVigente,0),
 ISNULL(con.InteresCarteraVencida,0),
 ISNULL(con.InteresVencido,0)
FROM dbo.SICCMX_Consumo con
INNER JOIN dbo.SICCMX_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sc ON sc.IdSituacionConsumo = con.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN R04.[0420Datos] dat ON con.Codigo = dat.Codigo
WHERE sc.Codigo IN ('2') --vencidos
 AND dat.Id IS NULL;


-- HIPOTECARIO
INSERT INTO R04.[0420Datos] (
 Codigo,
 IdMoneda,
 SituacionHistorica,
 CodigoProducto,
 SaldoHistorico,
 InteresVigenteHistorico,
 InteresCarteraVencidaHistorico,
 InteresVencidoHistorico
)
SELECT
 hip.Hipotecario,
 mon.IdMoneda,
 info.SituacionCredito, --Situacion vencida
 hip.TipoCreditoR04A,
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(hip.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(hip.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(hip.SaldoCapitalVigente,0) + ISNULL(hip.SaldoCapitalVencido,0) END,
 ISNULL(hip.InteresVigente,0),
 ISNULL(hip.InteresCarteraVencida,0),
 ISNULL(hip.InteresVencido,0)
FROM Historico.SICCMX_Hipotecario hip
INNER JOIN Historico.SICCMX_HipotecarioInfo info ON info.IdPeriodoHistorico = hip.IdPeriodoHistorico AND info.Hipotecario = hip.Hipotecario
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.Moneda = mon.Codigo
WHERE hip.IdPeriodoHistorico = @IdPeriodoHistorico
 AND info.SituacionCredito = '2'; -- vencidos


-- Actualizar info actual de hipotecario historicos
UPDATE datos
SET
 CodigoProducto = tp.Codigo,
 SituacionActual = sc.Codigo,--Situacion vencida
 SaldoActual = CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(hip.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(hip.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(hip.SaldoCapitalVigente,0) + ISNULL(hip.SaldoCapitalVencido,0) END,
 InteresVigente = ISNULL(hip.InteresVigente,0),
 InteresCarteraVencida = ISNULL(hip.InteresCarteraVencida,0),
 InteresVencido = ISNULL(hip.InteresVencido,0)
FROM R04.[0420Datos] datos
INNER JOIN dbo.SICCMX_Hipotecario hip ON hip.Codigo = datos.Codigo
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON info.IdHipotecario = hip.IdHipotecario
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = hip.IdTipoCreditoR04A
LEFT OUTER JOIN dbo.SICC_SituacionHipotecario sc ON info.IdSituacionCredito = sc.IdSituacionHipotecario
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda;


-- Insertar hipotecarios que estan en la tabla SICC_Hipotecario pero que no estab en Historico.SICCMX_Hipotecario
INSERT INTO R04.[0420Datos] (
 Codigo,
 IdMoneda,
 CodigoProducto,
 SituacionActual,
 SaldoActual,
 InteresVencido,
 InteresCarteraVencida,
 InteresVigente
)
SELECT
 hip.Codigo,
 info.IdMoneda,
 tp.Codigo,
 sic.Codigo,
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(hip.SaldoCapitalVencido,0) AS DECIMAL) + CAST(hip.SaldoCapitalVigente AS DECIMAL) ELSE ISNULL(hip.SaldoCapitalVencido,0) + hip.SaldoCapitalVigente END,
 ISNULL(hip.InteresVencido,0),
 ISNULL(hip.InteresCarteraVencida,0),
 ISNULL(hip.InteresVigente,0)
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = hip.IdTipoCreditoR04A
LEFT OUTER JOIN dbo.SICC_SituacionHipotecario sic ON sic.IdSituacionHipotecario = info.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN R04.[0420Datos] dat ON hip.Codigo = dat.Codigo
WHERE sic.Codigo = '2' AND dat.Id IS NULL;


-- Actualizar info historica comercial ultimos agregados
UPDATE datos
SET
 SituacionHistorica = cr.SituacionCredito,
 SaldoHistorico = CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(cr.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(cr.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(cr.SaldoCapitalVigente,0) + ISNULL(cr.SaldoCapitalVencido,0) END,
 InteresVigenteHistorico = ISNULL(cr.InteresVigente,0),
 InteresCarteraVencidaHistorico = ISNULL(cr.InteresCarteraVencida,0),
 InteresVencidoHistorico = ISNULL(cr.InteresVencido,0)
FROM R04.[0420Datos] datos 
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = datos.Codigo
INNER JOIN Historico.SICCMX_CreditoInfo info ON info.IdPeriodoHistorico = cr.IdPeriodoHistorico AND info.Credito = cr.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cr.Moneda = mon.Codigo
WHERE cr.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Actualizar info historica consumo ultimos agregados
UPDATE datos
SET
 SituacionHistorica = cr.SituacionCredito,
 SaldoHistorico = CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(cr.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(cr.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(cr.SaldoCapitalVigente,0) + ISNULL(cr.SaldoCapitalVencido,0) END,
 InteresVigenteHistorico = ISNULL(cr.InteresVigente,0),
 InteresCarteraVencidaHistorico = ISNULL(cr.InteresCarteraVencida,0),
 InteresVencidoHistorico = ISNULL(cr.InteresVencido,0)
FROM R04.[0420Datos] datos
INNER JOIN Historico.SICCMX_Consumo cr ON cr.Codigo = datos.Codigo
INNER JOIN Historico.SICCMX_ConsumoInfo info ON info.IdPeriodoHistorico = cr.IdPeriodoHistorico AND info.Consumo = cr.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.Moneda = mon.Codigo
WHERE cr.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Actualizar info historica HIPOTECARIO ultimos agregados
UPDATE datos
SET
 SituacionHistorica = info.SituacionCredito,
 SaldoHistorico = CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(cr.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(cr.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(cr.SaldoCapitalVigente,0) + ISNULL(cr.SaldoCapitalVencido,0) END,
 InteresVigenteHistorico = ISNULL(cr.InteresVigente,0),
 InteresCarteraVencidaHistorico = ISNULL(cr.InteresCarteraVencida,0),
 InteresVencidoHistorico = ISNULL(cr.InteresVencido,0)
FROM R04.[0420Datos] datos
INNER JOIN Historico.SICCMX_Hipotecario cr ON cr.Hipotecario = datos.Codigo
INNER JOIN Historico.SICCMX_HipotecarioInfo info ON info.IdPeriodoHistorico = cr.IdPeriodoHistorico AND info.Hipotecario = cr.Hipotecario
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.Moneda = mon.Codigo
WHERE cr.IdPeriodoHistorico = @IdPeriodoHistorico;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_HipotecarioReservasVariables_Select]
	@IdHipotecario BIGINT
AS
SELECT
	vw.IdHipotecario,
	vw.[PI],
	vw.SP,
	vw.E,
	vw.Reserva,
	vw.PorReserva,
	vw.IdCalificacion,
	vw.IdCalificacionNombre
FROM SICCMX_VW_HipotecarioReservasVariables vw
WHERE vw.IdHipotecario = @IdHipotecario;
GO

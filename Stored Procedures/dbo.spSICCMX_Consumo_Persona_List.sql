SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Consumo_Persona_List]
	@IdPersona BIGINT
AS
SELECT
	c.IdConsumo,
	c.Codigo,
	ISNULL(c.SaldoCapitalVigente,0)AS 'Capital Vigente',
	ISNULL(c.InteresVigente,0)AS 'Interes Vigente',
	ISNULL(c.SaldoCapitalVencido,0) AS 'Capital Vencido',
	ISNULL(c.InteresVencido,0) AS 'Interes Vencido',
	c.SaldoTotalValorizado AS 'Cartera Total',
	ISNULL(met.Nombre,'') AS 'Periodicidad de Pago', 
	--c.PeriodosIncumplimiento AS 'No. Periodos Vencidos',
	0 AS 'No. Periodos Vencidos',
	cc.PorReservaCubierta AS 'Porcentaje Cubierto',
	cc.PorReservaExpuesta AS 'Porcentaje Expuesto',
	ISNULL(cc.ECubierta,0) AS 'Monto Cubierto',
	ISNULL(cc.EExpuesta,0) AS 'Monto Expuesto',
	ISNULL(cc.ReservaCubierta,0) AS 'Reserva Cubierto',
	ISNULL(cc.ReservaExpuesta,0) AS 'Reserva Expuesto',
	ISNULL(cc.ReservaTotal,0) AS 'Total Reservas',
	calCub.Codigo AS 'Grado Riesgo Cubierto',
	calExp.Codigo AS 'Grado Riesgo Expuesto'
FROM dbo.SICCMX_VW_Consumo c
LEFT OUTER JOIN dbo.SICCMX_VW_ConsumoInfo ci ON ci.IdConsumo = c.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables cc ON c.IdConsumo = cc.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON cc.IdConsumo = pre.IdConsumo
INNER JOIN dbo.SICCMX_Persona p ON c.IdPersona = p.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Consumo_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaConsumo
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calCub ON cc.IdCalificacionCubierta = calCub.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calExp ON cc.IdCalificacionExpuesta = calExp.IdCalificacion
WHERE c.SaldoTotalValorizado > 0 AND p.IdPersona = @IdPersona;
GO

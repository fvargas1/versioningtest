SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Get_DescripcionMet]
	@IdMetodologia INT
AS

SELECT Nombre AS Descripcion FROM dbo.SICCMX_Metodologia WHERE IdMetodologia=@IdMetodologia
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_015]
AS

BEGIN

-- El "MONTO DEL PAGO EXIGIBLE AL ACREDITADO" no deberá ser mayor a lo registrado en el saldo del principal al inicio del periodo (col. 9).

SELECT CodigoCredito, CodigoCreditoCNBV, MontoPagoExigible, SaldoPrincipalInicio
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(MontoPagoExigible,''),'0') AS DECIMAL) > CAST(ISNULL(NULLIF(SaldoPrincipalInicio,''),'0') AS DECIMAL);

END
GO

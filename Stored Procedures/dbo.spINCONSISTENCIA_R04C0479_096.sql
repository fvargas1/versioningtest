SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_096]
AS

BEGIN

-- Validar que si la Responsabilidad Total (dat_responsabilidad_total) es menor o igual a 0, entonces las Reservas Totales (dat_reservas_totales) son igual a 0.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	ResponsabilidadFinal,
	ReservaTotal
FROM dbo.RW_R04C0479
WHERE CAST(ISNULL(NULLIF(ResponsabilidadFinal,''),'0') AS DECIMAL) <= 0 AND CAST(ISNULL(NULLIF(ReservaTotal,''),'0') AS DECIMAL) > 0

END
GO

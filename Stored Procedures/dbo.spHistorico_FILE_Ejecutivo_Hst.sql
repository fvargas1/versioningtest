SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_Ejecutivo_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_Ejecutivo_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_Ejecutivo_Hst (
 IdPeriodoHistorico,
 CodigoEjecutivo,
 Nombre,
 Telefono,
 Correo,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoEjecutivo,
 Nombre,
 Telefono,
 Correo,
 Fuente
FROM dbo.FILE_Ejecutivo_Hst;
GO

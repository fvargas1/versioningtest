SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0419_Display]
	@IdReporteLog BIGINT
AS

WITH Reporte (
	IdReporteLog, Codigo, Concepto, Padre, ConsolidadoCargos, ConsolidadoAbonos, ConsolidadoSaldo, CargosMN,
	AbonosMN, SaldoMN, CargosUSD, AbonosUSD, SaldoUSD, CargosUDIS, AbonosUDIS, SaldoUDIS, LEVEL
) AS (
	SELECT
	@IdReporteLog,
	con.Codigo,
	con.Concepto,
	con.Padre,
	con.ConsolidadoCargos,
	con.ConsolidadoAbonos,
	con.ConsolidadoSaldo,
	con.MNCargos,
	con.MNAbonos,
	con.MNSaldo,
	con.MECargos,
	con.MEAbonos,
	con.MESaldo,
	con.UDICargos,
	con.UDIAbonos,
	con.UDISaldo,
	0 AS LEVEL
	FROM dbo.RW_R04A0419_VW_Consolidado con
	WHERE Padre = ''
	UNION ALL
	SELECT
	@IdReporteLog,
	con.Codigo,
	con.Concepto,
	con.Padre,
	con.ConsolidadoCargos,
	con.ConsolidadoAbonos,
	con.ConsolidadoSaldo,
	con.MNCargos,
	con.MNAbonos,
	con.MNSaldo,
	con.MECargos,
	con.MEAbonos,
	con.MESaldo,
	con.UDICargos,
	con.UDIAbonos,
	con.UDISaldo,
	LEVEL + 1
	FROM dbo.RW_R04A0419_VW_Consolidado con
	INNER JOIN Reporte rep ON con.Padre = rep.Codigo
)
SELECT
	rep.Codigo,
	REPLICATE(' ',LEVEL*5)+rep.Concepto AS Concepto,
	ConsolidadoCargos,
	ConsolidadoAbonos,
	ConsolidadoSaldo,
	CargosMN,
	AbonosMN,
	SaldoMN,
	CargosUSD,
	AbonosUSD,
	SaldoUSD,
	CargosUDIS,
	AbonosUDIS,
	SaldoUDIS,
	LEVEL
FROM Reporte rep
INNER JOIN ReportWare_VW_0419Conceptos vw ON vw.Codigo = rep.Codigo
WHERE IdReporteLog = @IdReporteLog
ORDER BY vw.Orden;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_018]
AS

BEGIN

-- La situación del crédito deberá existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.NumeroDisposicion, r.SituacionCredito
FROM dbo.RW_R04C0443 r
LEFT OUTER JOIN dbo.SICC_SituacionCredito cat ON ISNULL(r.SituacionCredito,'') = cat.Codigo
WHERE cat.IdSituacionCredito IS NULL;

END
GO

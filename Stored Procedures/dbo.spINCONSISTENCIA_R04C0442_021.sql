SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_021]
AS

BEGIN

-- El ID met CNBV deberá tener la estructura definida de acuerdo a la metodología CNBV (29 posiciones)

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito
FROM dbo.RW_R04C0442
WHERE LEN(ISNULL(CodigoCreditoCNBV,'')) <> 29;

END
GO

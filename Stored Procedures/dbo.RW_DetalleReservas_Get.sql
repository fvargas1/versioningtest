SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_DetalleReservas_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Fecha,
	CodigoCredito,
	TipoProducto,
	ReservaInicial,
	ReservaFinal,
	Cargo,
	Abono,
	GradoRiesgo
FROM dbo.RW_VW_DetalleReservas
ORDER BY CodigoCredito;
GO

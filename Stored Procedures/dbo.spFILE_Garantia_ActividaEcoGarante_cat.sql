SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_ActividaEcoGarante_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_ActividaEcoGarante_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Garantia f
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON LTRIM(f.ActividaEcoGarante) = act.Codigo
WHERE act.IdActividadEconomica IS NULL AND LEN(f.ActividaEcoGarante) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoGarantia,
 'ActividaEcoGarante',
 f.ActividaEcoGarante,
 2,
 @Detalle
FROM dbo.FILE_Garantia f
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON LTRIM(f.ActividaEcoGarante) = act.Codigo
WHERE act.IdActividadEconomica IS NULL AND LEN(f.ActividaEcoGarante) > 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_048]
AS

BEGIN

-- La "DENOMINACION DEL CREDITO REESTRUCTURADO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.DenominacionCreditoReestructura
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_Moneda cat ON ISNULL(r.DenominacionCreditoReestructura,'') = cat.CodigoCNBV_Hipo
WHERE cat.IdMoneda IS NULL;

END
GO

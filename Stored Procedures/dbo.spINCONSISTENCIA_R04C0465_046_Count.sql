SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_046_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por Número de pagos en tiempo que la empresa realizó a inst financ bcarias en los últimos 12 meses (cve_ptaje_num_pagos_bcos) es = 44,
-- entonces el Número de pagos en tiempo que la empresa realizó a inst financ bcarias en los últimos 12 meses (dat_num_pgos_tiempo_bcos) debe ser >= 1 y < 5

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_PagosInstBanc,'') = '44' AND (CAST(PagosInstBanc AS DECIMAL) < 1 OR CAST(PagosInstBanc AS DECIMAL) >= 5);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_Expuesta_Proy]
AS
-- CALCULANDO PARTE DESCUBIERTA (CREDITO) CON VALORES PROYECTADOS
UPDATE can
SET MontoCobAjust = dbo.MAX2VAR(0, ISNULL(inf.ExposicionIncumplimiento, 0) - (ISNULL(cub.Cobertura, 0)))
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_CreditoInfo inf ON inf.IdCredito = can.IdCredito
INNER JOIN (
	SELECT c.IdCredito
	FROM dbo.SICCMX_Garantia_Canasta c
	INNER JOIN dbo.SICC_TipoGarantia t ON t.IdTipoGarantia = c.IdTipoGarantia
	WHERE t.Codigo IN ('EYM-01','EYM-02','EYM-03')
) AS eym ON eym.IdCredito = can.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_Cub cub ON inf.IdCredito = cub.IdCredito
WHERE can.EsDescubierto = 1;
GO

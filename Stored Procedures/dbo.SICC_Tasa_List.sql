SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Tasa_List]
AS
SELECT
	vw.IdTasa,
	vw.Codigo,
	vw.Descripcion,
	vw.IdMoneda,
	vw.MonedaNombre
FROM dbo.SICC_VW_Tasa vw;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Ejecutivo_errores_ClearLog]
AS
UPDATE dbo.FILE_Ejecutivo
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_Ejecutivo_errores;
GO

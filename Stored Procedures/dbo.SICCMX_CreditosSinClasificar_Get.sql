SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CreditosSinClasificar_Get]
AS
SELECT
	CodigoPersona,
	NombrePersona,
	NumeroLinea,
	CodigoCredito
FROM dbo.SICCMX_CreditosSinClasificar;
GO

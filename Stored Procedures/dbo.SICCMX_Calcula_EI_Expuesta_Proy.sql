SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_EI_Expuesta_Proy]
AS
UPDATE crv
SET EI_Expuesta = dbo.MAX2VAR(0, inf.ExposicionIncumplimiento - ISNULL(cub.Cobertura,0)),
	 EI_Expuesta_GarPer = dbo.MAX2VAR(0, inf.ExposicionIncumplimiento - ISNULL(per.MontoCubierto,0))
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_CreditoInfo inf ON inf.IdCredito = crv.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_Cub cub ON crv.IdCredito = cub.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Cobertura_Personales per ON crv.IdCredito = per.IdCredito
WHERE inf.ExposicionIncumplimiento > 0;
GO

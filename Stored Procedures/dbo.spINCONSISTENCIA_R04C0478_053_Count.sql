SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_053_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Número de Meses de Gracia para Amortizar el Capital (dat_num_meses_gracia_capital) sea MAYOR O IGUAL a cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC
WHERE CAST(ISNULL(NULLIF(NumMesesAmortCap,''),'-1') AS DECIMAL) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO

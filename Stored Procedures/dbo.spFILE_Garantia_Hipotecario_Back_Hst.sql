SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Hipotecario_Back_Hst]
AS

INSERT INTO dbo.FILE_Garantia_Hipotecario_Hst (
	CodigoGarantia,
	TipoGarantia,
	ValorGarantia,
	Moneda,
	Descripcion,
	PIGarante
)
SELECT
	gh.CodigoGarantia,
	gh.TipoGarantia,
	gh.ValorGarantia,
	gh.Moneda,
	gh.Descripcion,
	gh.PIGarante
FROM dbo.FILE_Garantia_Hipotecario gh
LEFT OUTER JOIN dbo.FILE_Garantia_Hipotecario_Hst hst ON LTRIM(gh.CodigoGarantia) = LTRIM(hst.CodigoGarantia)
WHERE hst.CodigoGarantia IS NULL;
GO

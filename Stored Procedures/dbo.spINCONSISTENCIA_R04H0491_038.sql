SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_038]
AS

BEGIN

-- El número de avalúo debe tener 17 dígitos.

SELECT CodigoCredito, CodigoCreditoCNBV, NumeroAvaluo
FROM dbo.RW_R04H0491
WHERE LEN(ISNULL(NumeroAvaluo,'')) <> 17;

END
GO

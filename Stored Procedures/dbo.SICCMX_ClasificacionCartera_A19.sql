SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ClasificacionCartera_A19]
AS
DECLARE @IdMetodologia INT;

-- Actualizamos en SICCMX_Credito para Metodologia 19
SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo='4';

UPDATE cre
SET IdMetodologia = @IdMetodologia
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Proyecto pry ON cre.IdLineaCredito = pry.IdLineaCredito;

UPDATE cre
SET IdMetodologia = @IdMetodologia
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_ProyectoLinea pl ON cre.IdLineaCredito = pl.IdLineaCredito;
GO

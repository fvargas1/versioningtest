SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Analitico_CR]
 @IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Analitico_CR);

DELETE FROM Historico.RW_Analitico_CR WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Analitico_CR (
 IdPeriodoHistorico, CodigoPersona, CodigoCredito, SaldoTotal, [PI], SP, EI, Reserva, PrctReserva, GradoRiesgo, B0, B1, ACT, B2, HIST, B3,
 PrctUSO, B4, PrctPago, B5, Alto, B6, Medio, B7, Bajo, B8, GVeces1, B9, GVeces2, B10, GVeces3, B11, BKATR, LimiteCredito, Tipo_EX, Zi,
 MontoGarantia
)
SELECT
 @IdPeriodoHistorico,
 CodigoPersona,
 CodigoCredito,
 SaldoTotal,
 [PI],
 SP,
 EI,
 Reserva,
 PrctReserva,
 GradoRiesgo,
 B0,
 B1,
 ACT,
 B2,
 HIST,
 B3,
 PrctUSO,
 B4,
 PrctPago,
 B5,
 Alto,
 B6,
 Medio,
 B7,
 Bajo,
 B8,
 GVeces1,
 B9,
 GVeces2,
 B10,
 GVeces3,
 B11,
 BKATR,
 LimiteCredito,
 Tipo_EX,
 Zi,
 MontoGarantia
FROM dbo.RW_Analitico_CR
WHERE IdReporteLog = @IdReporteLog;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_051]
AS

BEGIN

-- Validar que el Monto de la Comisión por la Disposición del Crédito (dat_com_disposicion_monto) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	ComisionDisposicionMonto
FROM dbo.RW_VW_R04C0478_INC
WHERE CAST(ISNULL(NULLIF(ComisionDisposicionMonto,''),'-1') AS DECIMAL) < 0;

END


GO

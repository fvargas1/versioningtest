SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CodigosCNBV_Bajas]
AS
DECLARE @IdPeriodo BIGINT;
SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;

UPDATE dbo.SICCMX_CreditoCNBV SET IdPeriodoBaja=NULL WHERE IdPeriodoBaja=@IdPeriodo;


-- SE DAN DE BAJA CODIGOS CNBV CON TIPO DE BAJA EN CREDITO
UPDATE cnbv
SET IdPeriodoBaja = @IdPeriodo
FROM dbo.SICCMX_CreditoCNBV cnbv
INNER JOIN dbo.SICCMX_Credito cre ON cnbv.CodigoCredito = cre.Codigo
INNER JOIN dbo.SICCMX_CreditoInfo ci ON cre.IdCredito = ci.IdCredito
WHERE ci.IdTipoBaja IS NOT NULL;

UPDATE cnbv
SET IdPeriodoBaja = @IdPeriodo
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_CreditoCNBV cnbv ON lin.Codigo = cnbv.NumeroLinea
WHERE lin.IdTipoBaja IS NOT NULL;
GO

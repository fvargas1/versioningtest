SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_006]
AS

BEGIN

-- El "IDENTIFICADOR DEL CRÉDITO METODOLOGÍA CNBV" de la posición 2 a la posición 7 deberá ser igual a la Fecha de Otorgamiento (columna 11).

SELECT CodigoCredito, CodigoCreditoCNBV, FechaOtorgamiento
FROM dbo.RW_R04H0491
WHERE SUBSTRING(CodigoCreditoCNBV,2,6) <> FechaOtorgamiento;

END
GO

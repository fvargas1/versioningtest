SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_020_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La "FECHA DEL ULTIMO PAGO REALIZADO POR EL ACREDITADO" no se podrá omitir ni se podrá anotar cero (0).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0492
WHERE ISNULL(NULLIF(ResponsabilidadTotal,''),'0') = '0';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_CreditoAval]
 @IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_CreditoAval WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_CreditoAval (
 IdPeriodoHistorico,
 Credito,
 Aval,
 Porcentaje,
 Monto,
 Aplica,
 PorcentajeOriginal
)
SELECT
 @IdPeriodoHistorico,
 c.Codigo,
 a.Codigo,
 ca.Porcentaje,
 ca.Monto,
 ca.Aplica,
 ca.PorcentajeOriginal
FROM dbo.SICCMX_CreditoAval ca
INNER JOIN dbo.SICCMX_Credito c ON ca.IdCredito = c.IdCredito
INNER JOIN dbo.SICCMX_Aval a ON ca.IdAval = a.IdAval;
GO

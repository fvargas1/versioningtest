SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_PersonaComentarios]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_PersonaComentarios WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_PersonaComentarios (
 IdPeriodoHistorico,
 Persona,
 Comentarios
) 
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 p.Codigo, -- Persona
 pc.Comentarios
FROM dbo.SICCMX_PersonaComentarios pc
INNER JOIN dbo.SICCMX_Persona p ON pc.IdPersona = p.IdPersona;
GO

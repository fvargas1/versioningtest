SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Consumo]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Consumo WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Consumo (
	IdPeriodoHistorico,
	Codigo,
	Persona,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	InteresCarteraVencida,
	Producto,
	Reestructura,
	TasaInteres,
	SituacionCredito,
	PeriodicidadCapital,
	TipoCredito,
	EsRevolvente,
	PeriodicidadCalificacion
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	con.Codigo,
	per.Codigo,
	con.SaldoCapitalVigente,
	con.InteresVigente,
	con.SaldoCapitalVencido,
	con.InteresVencido,
	con.InteresCarteraVencida,
	con.Producto,
	reest.Codigo,
	con.TasaInteres,
	sit.Codigo,
	perCon.Codigo,
	tcc.Codigo,
	con.EsRevolvente,
	perCal.Codigo
FROM dbo.SICCMX_Consumo con
INNER JOIN dbo.SICCMX_Persona per ON con.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo reest ON con.IdReestructura = reest.IdReestructuraConsumo
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sit ON con.IdSituacionCredito = sit.IdSituacionConsumo
LEFT OUTER JOIN dbo.SICC_PeriodicidadPagoConsumo perCon ON con.IdPeriodicidadCapital = perCon.IdPeriodicidadPagoConsumo
LEFT OUTER JOIN dbo.SICC_TipoCreditoConsumo tcc ON con.IdTipoCredito = tcc.IdTipoCreditoConsumo
LEFT OUTER JOIN dbo.SICC_PeriodicidadCalificacionConsumo perCal ON con.IdPeriodicidadCalificacion = perCal.IdPeriodicidadCalificacionConsumo;
GO

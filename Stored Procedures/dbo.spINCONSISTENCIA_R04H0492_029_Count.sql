SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_029_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El dato "PROMEDIO DEL PAGO REALIZADO RESPECTO AL MONTO EXIGIBLE (%VPAGO)" debe estar expresado en base 100 y a seis decimales.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0492
WHERE LEN(SUBSTRING(PorVPAGO,CHARINDEX('.',PorVPAGO)+1,LEN(PorVPAGO))) <> 6;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

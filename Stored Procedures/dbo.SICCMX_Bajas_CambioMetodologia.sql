SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Bajas_CambioMetodologia]
AS
TRUNCATE TABLE dbo.SICCMX_Persona_CambioMetodologia;

INSERT INTO dbo.SICCMX_Persona_CambioMetodologia (Persona, NumeroDeLineaActual, MetodologiaAnterior, MetodologiaActual, IdPeriodoHistorico)
SELECT
 per.Codigo AS Persona,
 cre.NumeroLinea,
 hcre.Metodologia AS MetHist,
 met.Codigo AS MetActual,
 hcre.IdPeriodoHistorico
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_CreditoInfo info ON cre.IdCredito = info.IdCredito
INNER JOIN dbo.SICCMX_Persona per ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_VW_Persona_Ultima_Metodologia hcre ON cre.IdPersona = hcre.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
WHERE met.Codigo <> hcre.Metodologia AND cre.MontoValorizado > 0 AND info.IdTipoBaja IS NULL;


-- ACTUALIZAMOS INFORMACION PARA SABER SI EL CLIENTE ERA UN ORGRANISMO DESCENTRALIZADO EN LA METODOLOGIA ANTERIOR
UPDATE cm
SET OrgDescAnterior = anx.OrgDescPartidoPolitico
FROM dbo.SICCMX_Persona_CambioMetodologia cm
INNER JOIN Historico.SICCMX_Anexo21 anx ON cm.IdPeriodoHistorico = anx.IdPeriodoHistorico AND cm.Persona = anx.Persona
WHERE cm.MetodologiaAnterior = '21'

UPDATE cm
SET OrgDescAnterior = anx.OrgDescPartidoPolitico
FROM dbo.SICCMX_Persona_CambioMetodologia cm
INNER JOIN Historico.SICCMX_Anexo22 anx ON cm.IdPeriodoHistorico = anx.IdPeriodoHistorico AND cm.Persona = anx.Persona
WHERE cm.MetodologiaAnterior = '22'


-- ACTUALIZAMOS INFORMACION PARA SABER SI EL CLIENTE ERA UN ORGRANISMO DESCENTRALIZADO EN LA METODOLOGIA ANTERIOR
UPDATE cm
SET OrgDescActual = anx.OrgDescPartidoPolitico
FROM dbo.SICCMX_Persona_CambioMetodologia cm
INNER JOIN dbo.SICCMX_Persona per ON cm.Persona = per.Codigo
INNER JOIN dbo.SICCMX_Anexo21 anx ON per.IdPersona = anx.IdPersona
WHERE cm.MetodologiaActual='21';

UPDATE cm
SET OrgDescActual = anx.OrgDescPartidoPolitico
FROM dbo.SICCMX_Persona_CambioMetodologia cm
INNER JOIN dbo.SICCMX_Persona per ON cm.Persona = per.Codigo
INNER JOIN dbo.SICCMX_Anexo22 anx ON per.IdPersona = anx.IdPersona
WHERE cm.MetodologiaActual='22';
GO

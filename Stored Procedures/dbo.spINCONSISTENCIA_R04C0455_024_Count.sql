SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_024_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Saldo de Inversión (dat_saldo_inversion) es = 0, entonces el Puntaje Inversión a Ingresos Totales (cve_puntaje_inver_a_ingreso) debe ser = 29 ó = 42

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoInversion,''),'0') AS DECIMAL(18,6)) = 0 AND ISNULL(P_InvIngTotales,'') NOT IN ('29','42');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

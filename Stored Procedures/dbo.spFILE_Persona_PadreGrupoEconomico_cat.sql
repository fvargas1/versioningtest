SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_PadreGrupoEconomico_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_PadreGrupoEconomico_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Persona
SET errorCatalogo = 1
WHERE LEN(PadreGrupoEconomico) > 0 AND LTRIM(PadreGrupoEconomico) NOT IN ('0','1');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoCliente,
	'PadreGrupoEconomico',
	PadreGrupoEconomico,
	1,
	@Detalle
FROM dbo.FILE_Persona 
WHERE LEN(PadreGrupoEconomico) > 0 AND LTRIM(PadreGrupoEconomico) NOT IN ('0','1');
GO

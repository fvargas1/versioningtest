SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Periodo_GetActivo]
AS
SELECT TOP 1
	vw.IdPeriodo,
	vw.Fecha,
	vw.Trimestral,
	vw.Activo
FROM dbo.SICC_VW_Periodo vw
WHERE vw.Activo = 1
ORDER BY Fecha DESC;
GO

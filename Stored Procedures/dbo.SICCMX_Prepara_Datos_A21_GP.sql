SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Prepara_Datos_A21_GP]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo=21;

INSERT INTO dbo.SICCMX_Persona_PI_GP (IdGP, EsGarante, IdMetodologia, IdClasificacion)
SELECT DISTINCT IdGP, EsGarante, @IdMetodologia, NULL
FROM dbo.SICCMX_Anexo21_GP;
GO

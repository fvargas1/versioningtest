SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_HipotecarioCNBV_Init]
AS
DECLARE @IdPeriodo BIGINT;
SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;

DELETE FROM dbo.SICCMX_HipotecarioCNBV WHERE IdPeriodo=@IdPeriodo;
UPDATE dbo.SICCMX_HipotecarioCNBV SET IdPeriodoBaja=NULL WHERE IdPeriodoBaja=@IdPeriodo;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_PM_Hipotecario]
AS
UPDATE hg
SET PorUsadoGarantia = CASE WHEN cgTotal.Total > 0 THEN hip.E / cgTotal.Total ELSE 0 END,
 MontoUsadoGarantia = CASE WHEN cgTotal.Total > 0 THEN gar.ValorGtiaValorizado * (hip.E / cgTotal.Total) ELSE 0 END
FROM dbo.SICCMX_HipotecarioGarantia hg
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hip ON hg.IdHipotecario = hip.IdHipotecario
INNER JOIN dbo.SICCMX_VW_Garantia_Hipotecario gar ON hg.IdGarantia = gar.IdGarantia
INNER JOIN (
 SELECT hg2.IdGarantia, CAST(SUM(ISNULL(hrv.E, 0)) AS DECIMAL(23,2)) AS Total
 FROM dbo.SICCMX_HipotecarioGarantia hg2
 INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON hg2.IdHipotecario = hrv.IdHipotecario
 INNER JOIN dbo.SICCMX_VW_Garantia_Hipotecario g2 ON hg2.IdGarantia = g2.IdGarantia
 WHERE g2.IdTipoGarantiaCodigo = 'NMC-05'
 GROUP BY hg2.IdGarantia
) AS cgTotal ON gar.IdGarantia = cgTotal.IdGarantia;


UPDATE pre
SET PorCOBPAMED = CASE WHEN hrv.E > 0 THEN dbo.MIN2VAR(1, ISNULL(gar.Monto,0) / hrv.E) ELSE 0 END
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON pre.IdHipotecario = hrv.IdHipotecario
LEFT OUTER JOIN (
 SELECT hg.IdHipotecario, CAST(SUM(hg.MontoUsadoGarantia) AS DECIMAL(23,2)) AS Monto
 FROM dbo.SICCMX_HipotecarioGarantia hg
 INNER JOIN dbo.SICCMX_VW_Garantia_Hipotecario g ON hg.IdGarantia = g.IdGarantia
 WHERE g.IdTipoGarantiaCodigo = 'NMC-05'
 GROUP BY hg.IdHipotecario
) AS gar ON hrv.IdHipotecario = gar.IdHipotecario


UPDATE res
SET MontoExpuesto = hrv.E * (1 - pre.PorCOBPAMED),
 MontoCubierto = hrv.E * pre.PorCOBPAMED,
 SPCubierto = 0.45,
 ReservaExpuesto = hrv.Reserva * (1 - pre.PorCOBPAMED),
 ReservaCubierto = (hrv.E * pre.PorCOBPAMED * 0.45 * 0.01111111)
FROM dbo.SICCMX_HipotecarioReservas res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON res.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hrv.IdHipotecario = pre.IdHipotecario
WHERE pre.PorCOBPAMED > 0;

UPDATE hrv
SET Reserva = res.ReservaCubierto + res.ReservaExpuesto,
	PorReserva = (res.ReservaCubierto + res.ReservaExpuesto) / hrv.E
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_HipotecarioReservas res ON hrv.IdHipotecario = res.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON res.IdHipotecario = pre.IdHipotecario
WHERE pre.PorCOBPAMED > 0;


UPDATE res
SET PorcentajeExpuesto = res.ReservaExpuesto / res.MontoExpuesto,
 PorcentajeCubierto = res.ReservaCubierto / res.MontoCubierto
FROM dbo.SICCMX_HipotecarioReservas res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON res.IdHipotecario = pre.IdHipotecario
WHERE pre.PorCOBPAMED > 0;


UPDATE res
SET IdCalificacionExpuesto = calExp.IdCalificacion,
 IdCalificacionCubierto = calCub.IdCalificacion
FROM dbo.SICCMX_HipotecarioReservas res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON pre.IdHipotecario = res.IdHipotecario
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calExp ON pre.IdMetodologia = calExp.IdMetodologia AND res.PorcentajeExpuesto BETWEEN calExp.RangoMenor AND calExp.RangoMayor
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calCub ON pre.IdMetodologia = calCub.IdMetodologia AND res.PorcentajeCubierto BETWEEN calCub.RangoMenor AND calCub.RangoMayor
WHERE pre.PorCOBPAMED > 0;

UPDATE hrv
SET IdCalificacion = cal.IdCalificacion
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hrv.IdHipotecario = pre.IdHipotecario
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 cal ON pre.IdMetodologia = cal.IdMetodologia AND hrv.PorReserva BETWEEN cal.RangoMenor AND cal.RangoMayor
WHERE pre.PorCOBPAMED > 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04H0493_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04H0493_2016);

DELETE FROM Historico.RW_R04H0493_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04H0493_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, NumeroAvaluo, TipoBaja, SaldoPrincipalInicial,
	ResponsabilidadTotal, MontoPagoAcreditado, MontoQuitas, MontoCondonaciones, MontoBonificaciones, MontoDescuentos, ValorBienAdjudicado, MontoDacion,
	ReservasCalifCanceladas, ReservasAdicCanceladas
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	TipoBaja,
	SaldoPrincipalInicial,
	ResponsabilidadTotal,
	MontoPagoAcreditado,
	MontoQuitas,
	MontoCondonaciones,
	MontoBonificaciones,
	MontoDescuentos,
	ValorBienAdjudicado,
	MontoDacion,
	ReservasCalifCanceladas,
	ReservasAdicCanceladas
FROM dbo.RW_R04H0493_2016
WHERE IdReporteLog = @IdReporteLog;
GO

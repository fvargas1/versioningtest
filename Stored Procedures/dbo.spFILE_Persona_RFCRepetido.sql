SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_RFCRepetido]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_RFCRepetido';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Persona f
WHERE f.RFC IN (
 SELECT f.RFC
 FROM dbo.FILE_Persona f
 GROUP BY f.RFC
 HAVING COUNT(f.RFC)>1
) AND LEN(f.RFC) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	f.CodigoCliente,
	'RFC',
	f.RFC,
	1,
	@Detalle
FROM dbo.FILE_Persona f
WHERE f.RFC IN (
 SELECT f.RFC
 FROM dbo.FILE_Persona f
 GROUP BY f.RFC
 HAVING COUNT(f.RFC)>1
) AND LEN(f.RFC) > 0;
GO

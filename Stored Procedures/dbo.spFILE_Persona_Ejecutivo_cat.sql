SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_Ejecutivo_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_Ejecutivo_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICCMX_Ejecutivo cat ON LTRIM(f.Ejecutivo) = cat.Codigo
WHERE cat.Codigo IS NULL AND LEN(f.Ejecutivo) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	f.CodigoCliente,
	'Ejecutivo',
	f.Ejecutivo,
	2,
	@Detalle
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICCMX_Ejecutivo cat ON LTRIM(f.Ejecutivo) = cat.Codigo
WHERE cat.Codigo IS NULL AND LEN(f.Ejecutivo) > 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_ConsDesc_Reservas]
AS
DECLARE @FechaPeriodo DATETIME;
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
TRUNCATE TABLE dbo.SICCMX_Reservas_Variacion;

INSERT INTO dbo.SICCMX_Reservas_Variacion (IdCredito, MesesSP100, CreditoRes5Prct)
SELECT DISTINCT IdCredito, 0, 2
FROM dbo.SICCMX_Credito;

-- CALCULAMOS LA VARIACION EN LAS RESERVAS EN COMPARACION CON EL MES ANTERIOR
UPDATE res
SET ReservaCalConsDesc = crv.ReservaFinal - ISNULL(hst.ReservaFinal,0),
 ReservaAdicConsDesc = ISNULL(inf.ReservasAdicTotal,0) - ISNULL(hst.ReservasAdicTotal,0),
 ReservaPeriodoAnt = ISNULL(hst.ReservaFinal,0),
 ReservaAdicPeriodoAnt = ISNULL(hst.ReservasAdicTotal,0)
FROM dbo.SICCMX_Reservas_Variacion res
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON res.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Credito cre ON crv.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_VW_Credito_ReservasAdicionales inf ON cre.IdCredito = inf.IdCredito
OUTER APPLY (
 SELECT TOP 1 hcrv.ReservaFinal, hinf.ReservasAdicTotal
 FROM Historico.SICCMX_Credito_Reservas_Variables hcrv
 INNER JOIN Historico.SICCMX_VW_Credito_ReservasAdicionales hinf ON hcrv.IdPeriodoHistorico = hinf.IdPeriodoHistorico AND hcrv.Credito = hinf.Credito
 INNER JOIN dbo.SICC_PeriodoHistorico ph ON hcrv.IdPeriodoHistorico = ph.IdPeriodoHistorico
 WHERE cre.Codigo = hcrv.Credito AND ph.Fecha < @FechaPeriodo AND ph.Activo = 1
 ORDER BY ph.Fecha DESC
) AS hst;

-- CALCULAMOS LOS MESES CON SP AL 100%
UPDATE res
SET MesesSP100 = DATEDIFF(MONTH, ISNULL(hst.Fecha,@FechaPeriodo), @FechaPeriodo) + 1
FROM dbo.SICCMX_Reservas_Variacion res
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON res.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Credito cre ON crv.IdCredito = cre.IdCredito
LEFT OUTER JOIN (
 SELECT hcrv.Credito, MAX(ph.Fecha) AS Fecha
 FROM Historico.SICCMX_Credito_Reservas_Variables hcrv
 INNER JOIN dbo.SICC_PeriodoHistorico ph ON hcrv.IdPeriodoHistorico = ph.IdPeriodoHistorico AND ph.Activo = 1 AND ph.Fecha < @FechaPeriodo
 WHERE hcrv.SP_Expuesta < 1
 GROUP BY hcrv.Credito
) AS hst ON cre.Codigo = hst.Credito
WHERE crv.SP_Expuesta = 1;

-- CREDITOS CON RESERVAS AL 0.5%
UPDATE res
SET CreditoRes5Prct = 1
FROM dbo.SICCMX_Reservas_Variacion res
INNER JOIN dbo.SICCMX_Credito cre ON res.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
LEFT OUTER JOIN (
 SELECT tcre.IdCredito
 FROM dbo.SICCMX_Credito tcre
 INNER JOIN dbo.SICCMX_Garantia_Canasta tcan ON tcre.IdCredito = tcan.IdCredito
 INNER JOIN dbo.SICC_TipoGarantia ttg ON tcan.IdTipoGarantia = ttg.IdTipoGarantia
 WHERE ttg.Codigo = 'NMC-05' AND tcan.PrctCobAjust >= 1
) can ON res.IdCredito = can.IdCredito
WHERE per.EsFondo IS NOT NULL OR can.IdCredito IS NOT NULL;

-- CREDITOS CON SUSTITUCION DE PI
UPDATE res
SET CreditoSustPI =
 CASE
 WHEN crv.PI_Total = crv.PI_Cubierta THEN 1
 WHEN crv.PI_Total = crv.PI_Expuesta THEN 2
 ELSE 3
 END
FROM dbo.SICCMX_Reservas_Variacion res
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON res.IdCredito = crv.IdCredito;
GO

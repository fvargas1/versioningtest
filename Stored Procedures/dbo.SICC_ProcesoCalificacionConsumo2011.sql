SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ProcesoCalificacionConsumo2011]
AS
DECLARE @FechaCalificacion DATETIME;
SELECT @FechaCalificacion = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

--INICIALIZAMOS LA CALIFICACION DE CONSUMO
TRUNCATE TABLE dbo.SICCMX_Consumo_Reservas_Variables;

INSERT INTO dbo.SICCMX_Consumo_Reservas_Variables (IdConsumo, FechaCalificacion)
SELECT IdConsumo, @FechaCalificacion
FROM dbo.SICCMX_Consumo;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_Expuesta]
AS
-- CALCULANDO PARTE DESCUBIERTA (CREDITO)
INSERT INTO dbo.SICCMX_Garantia_Canasta(
 IdCredito,
 PrctCobSinAju,
 PrctCobAjust,
 MontoCobAjust,
 [PI],
 SeveridadCorresp,
 EsDescubierto
)
SELECT DISTINCT
 vw.IdCredito,
 dbo.MAX2VAR(0, 1 - ISNULL(cub.PrctCobSinAju, 0)),
 dbo.MAX2VAR(0, 1 - ISNULL(cub.PrctCobAjust, 0)),
 dbo.MAX2VAR(0, ISNULL(vw.EI_Total, 0) - (ISNULL(cub.Cobertura, 0))),
 ppi.[PI],
 pos.SP,
 1
FROM dbo.SICCMX_VW_Credito_NMC vw
INNER JOIN dbo.SICCMX_Persona_PI ppi ON vw.IdPersona = ppi.IdPersona
LEFT OUTER JOIN dbo.SICC_Posicion pos ON vw.Posicion = pos.IdPosicion
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_Cub cub ON vw.IdCredito = cub.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Garantia_Canasta canasta ON vw.IdCredito = canasta.IdCredito AND canasta.EsDescubierto = 1
WHERE canasta.IdCredito IS NULL;
GO

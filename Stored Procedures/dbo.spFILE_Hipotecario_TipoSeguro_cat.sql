SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_TipoSeguro_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Hipotecario_TipoSeguro_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Hipotecario f
LEFT OUTER JOIN dbo.SICC_TipoSeguro cat ON LTRIM(f.TipoSeguro) = cat.Codigo
WHERE cat.IdTipoSeguro IS NULL AND LEN(ISNULL(f.TipoSeguro,'')) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCredito,
 'TipoSeguro',
 f.TipoSeguro,
 2,
 @Detalle
FROM dbo.FILE_Hipotecario f
LEFT OUTER JOIN dbo.SICC_TipoSeguro cat ON LTRIM(f.TipoSeguro) = cat.Codigo
WHERE cat.IdTipoSeguro IS NULL AND LEN(ISNULL(f.TipoSeguro,'')) > 0;
GO

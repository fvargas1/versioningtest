SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_AjusteSP_Cubierta_Consumo]
AS
-- CALCULAMOS SEVERIDAD DE LA PERDIDA
UPDATE crv
SET SPCubierta = sp.SP
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_VW_Consumo_SP sp ON crv.IdConsumo = sp.IdConsumo;

UPDATE can
SET SeveridadCorresp = crv.SPCubierta
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON can.IdConsumo = crv.IdConsumo
INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE tg.Codigo = 'GP';

/*
INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 IdCredito,
 GETDATE(),
 '',
 'Se ajusta SP Expuesta a ' + CONVERT(VARCHAR, CONVERT(DECIMAL(10,2), ISNULL(SP_Expuesta*100, 0)))
FROM dbo.SICCMX_Credito_Reservas_Variables;
*/
GO

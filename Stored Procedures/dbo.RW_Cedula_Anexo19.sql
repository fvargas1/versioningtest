SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Cedula_Anexo19]
AS
SELECT DISTINCT
	per.IdPersona,
	per.Nombre AS 'Description',
	dbo.fnFormat_Name(per.Nombre) AS NombrePersona,
	pry.IdProyecto,
	pry.CodigoProyecto
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Proyecto pry ON per.IdPersona = pry.IdPersona
ORDER BY NombrePersona, CodigoProyecto;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_010]
AS
BEGIN
-- El monto de la línea de crédito simple debe ser menor o igual al monto línea de crédito autorizada

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	MontoSinAccesorios,
	MonLineaCred
FROM dbo.RW_VW_R04C0463_INC
WHERE CAST(ISNULL(NULLIF(MontoSinAccesorios,''),'0') AS DECIMAL) > CAST(ISNULL(NULLIF(MonLineaCred,''),'0') AS DECIMAL);

END

GO

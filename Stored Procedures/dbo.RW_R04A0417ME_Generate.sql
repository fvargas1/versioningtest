SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0417ME_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @SaldoFinalTotalConsumo DECIMAL;
DECLARE @FechaMigracion DATETIME;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE Nombre = 'A-0417ME' AND GrupoReporte = 'R04';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion)
VALUES (@IdReporte, 'Calificación de la Cartera de Crédito y Estimación Preventiva para Riesgos Crediticios Moneda Extranjera', GETDATE(), 'Bajaware');

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_R04A0417ME;

-- Calculamos saldo base en dlls
INSERT INTO dbo.RW_R04A0417ME (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 dat.Concepto,
 '0417',
 '2',
 '9', -- Saldo base
 '1', -- Saldo base
 SUM(CAST(dat.Saldo AS DECIMAL))
FROM R04.[0417Datos] dat
WHERE dat.Concepto IS NOT NULL AND dat.Moneda = 'ME'
GROUP BY dat.Concepto;


-- Calculamos saldo base en dlls valorizado
INSERT INTO dbo.RW_R04A0417ME (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 dat.Concepto,
 '0417',
 '4', -- Dlls valorizados
 '9', -- Saldo base
 '1', -- Saldo base
 SUM(CAST(dat.Saldo * tc.Valor AS DECIMAL)) 
FROM R04.[0417Datos] dat
INNER JOIN dbo.SICC_Moneda mon ON mon.Codigo = '1' -- DLLS
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo per ON per.IdPeriodo = tc.IdPeriodo AND per.Activo = 1
WHERE dat.Concepto IS NOT NULL AND dat.Moneda = 'ME'
GROUP BY dat.Concepto;


-- Calculamos estimacion
INSERT INTO dbo.RW_R04A0417ME (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 dat.Concepto,
 '0417',
 '4',
 '10', -- Saldo de estimacion
 '1', -- Saldo base
 SUM(CAST(dat.Estimacion AS DECIMAL))
FROM R04.[0417Datos] dat
WHERE dat.Concepto IS NOT NULL AND dat.Moneda = 'ME'
GROUP BY dat.Concepto;


-- CALCULAMOS LAS CUENTAS PADRES
DECLARE @maxLevel INT;
CREATE TABLE #tree (
 Codigo VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
 nivel INT
);

WITH Conceptos (Codigo, LEVEL)
AS (
 SELECT Codigo, 0 AS LEVEL
 FROM dbo.ReportWare_VW_0417Concepto
 WHERE Padre = '' OR Padre IS NULL
 UNION ALL
 SELECT vw.Codigo, LEVEL + 1
 FROM dbo.ReportWare_VW_0417Concepto vw
 INNER JOIN Conceptos con ON vw.Padre = con.Codigo
)

INSERT INTO #tree (Codigo, nivel)
SELECT Codigo, [LEVEL] FROM Conceptos;

SELECT @maxLevel = MAX (nivel) FROM #tree;

WHILE @maxLevel >= 0
BEGIN

 INSERT INTO dbo.RW_R04A0417ME (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
 SELECT rep.IdReporteLog, conceptos.Padre, rep.SubReporte, rep.Moneda, rep.TipoDeCartera, rep.TipoDeSaldo,
 SUM(CAST(ISNULL(rep.Dato,0) AS DECIMAL))
 FROM dbo.ReportWare_VW_0417Concepto conceptos
 INNER JOIN dbo.RW_R04A0417ME rep ON rep.Concepto = conceptos.Codigo
 INNER JOIN #tree con ON rep.Concepto = con.Codigo
 WHERE con.nivel = @maxLevel AND
 conceptos.Padre NOT IN (SELECT Concepto FROM dbo.RW_R04A0417ME)
 AND conceptos.Padre <> ''
 GROUP BY rep.IdReporteLog, conceptos.Padre, rep.SubReporte, rep.Moneda, rep.TipoDeSaldo, rep.TipoDeCartera;

 SET @maxLevel = @maxLevel - 1;
END

DROP TABLE #tree;

UPDATE dbo.RW_R04A0417ME SET Dato = '-' + Dato WHERE TipoDeCartera='10' AND Dato <> '0'; --Estimaciones se reportan con signo negativo


INSERT INTO dbo.RW_R04A0417ME (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
	@IdReporteLog,
	sal.Concepto,
	sal.SubReporte,
	sal.Moneda,
	sal.TipoCartera,
	sal.TipoSaldo,
	'0'
FROM dbo.BAJAWARE_R04A_Salida sal
LEFT OUTER JOIN dbo.RW_R04A0417ME r04 ON sal.Concepto = r04.Concepto AND sal.Moneda = r04.Moneda AND sal.TipoCartera = r04.TipoDeCartera AND sal.TipoSaldo = r04.TipoDeSaldo
WHERE sal.Subreporte = '0417' AND sal.Moneda <> '14' AND r04.Concepto IS NULL;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04A0417ME WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SELECT @TotalIntereses = 0;
SELECT @FechaMigracion = MAX( Fecha ) FROM dbo.MIGRACION_ProcesoLog;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos= @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = @FechaMigracion,
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculaAforo_BANCREA]
AS
UPDATE dbo.SICCMX_CreditoGarantia SET PorcBanco = PorcCubiertoCredito;

UPDATE cg
SET MontoBanco = c.MontoValorizado * cg.PorcBanco
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_VW_Credito_NMC c ON cg.IdCredito = c.IdCredito;
GO

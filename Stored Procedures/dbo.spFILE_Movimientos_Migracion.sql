SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Movimientos_Migracion]
AS
TRUNCATE TABLE dbo.SICCMX_Credito_Movimientos;
TRUNCATE TABLE dbo.SICCMX_Consumo_Movimientos;
TRUNCATE TABLE dbo.SICCMX_Hipotecario_Movimientos;

-- Insertamos Comercial
INSERT INTO dbo.SICCMX_Credito_Movimientos (IdCredito, IdTipoMovimiento, Monto, Fecha)
SELECT cre.IdCredito, tm.IdTipoMovimiento, f.Monto, f.Fecha
FROM dbo.FILE_Movimientos f
INNER JOIN dbo.SICCMX_Credito cre ON LTRIM(f.CodigoCredito) = cre.Codigo
INNER JOIN dbo.SICC_TipoMovimiento tm ON LTRIM(f.TipoMovimiento) = tm.Codigo
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;

-- Insertamos Consumo
INSERT INTO dbo.SICCMX_Consumo_Movimientos (IdConsumo, IdTipoMovimiento, Monto, Fecha)
SELECT con.IdConsumo, tm.IdTipoMovimiento, f.Monto, f.Fecha
FROM dbo.FILE_Movimientos f
INNER JOIN dbo.SICCMX_Consumo con ON LTRIM(f.CodigoCredito) = con.Codigo
INNER JOIN dbo.SICC_TipoMovimiento tm ON LTRIM(f.TipoMovimiento) = tm.Codigo
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;

-- Insertamos Hipotecario
INSERT INTO dbo.SICCMX_Hipotecario_Movimientos (IdHipotecario, IdTipoMovimiento, Monto, Fecha)
SELECT hip.IdHipotecario, tm.IdTipoMovimiento, f.Monto, f.Fecha
FROM dbo.FILE_Movimientos f
INNER JOIN dbo.SICCMX_Hipotecario hip ON LTRIM(f.CodigoCredito) = hip.Codigo
INNER JOIN dbo.SICC_TipoMovimiento tm ON LTRIM(f.TipoMovimiento) = tm.Codigo
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Inicializa_Datos_Credito]
AS
TRUNCATE TABLE dbo.SICCMX_Credito_Reservas_Variables;

INSERT INTO dbo.SICCMX_Credito_Reservas_Variables (IdCredito, ExpProyectada)
SELECT cre.IdCredito, inf.ExposicionIncumplimiento
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_CreditoInfo inf ON inf.IdCredito = cre.IdCredito;
GO

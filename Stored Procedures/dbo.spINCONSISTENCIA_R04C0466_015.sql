SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_015]
AS

BEGIN

-- Se identifican créditos reportados en el formulario de severidad que no fueron reportados en el formulario de seguimiento.

SELECT
	rSP.CodigoCredito,
	rSP.NumeroDisposicion,
	rSP.CodigoPersona,
	rSP.CodigoCreditoCNBV
FROM dbo.RW_R04C0466 rSP
LEFT OUTER JOIN dbo.RW_R04C0464 rSE ON rSP.NumeroDisposicion = rSE.NumeroDisposicion
WHERE rSE.NumeroDisposicion IS NULL

END
GO

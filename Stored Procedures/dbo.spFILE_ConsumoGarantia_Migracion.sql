SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ConsumoGarantia_Migracion]
AS
UPDATE cg
SET PorUsadoGarantia = 0,
	MontoUsadoGarantia = 0
FROM dbo.SICCMX_ConsumoGarantia cg
INNER JOIN dbo.SICCMX_Consumo c ON cg.IdConsumo = c.IdConsumo
INNER JOIN dbo.SICCMX_Garantia_Consumo g ON cg.IdGarantia = g.IdGarantia
INNER JOIN dbo.FILE_ConsumoGarantia f ON c.Codigo = LTRIM (f.CodigoCredito) AND g.Codigo = LTRIM(f.CodigoGarantia)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_ConsumoGarantia (
	IdConsumo,
	IdGarantia
)
SELECT
	(SELECT IdConsumo FROM dbo.SICCMX_Consumo WHERE Codigo = LTRIM(f.CodigoCredito)),
	(SELECT IdGarantia FROM dbo.SICCMX_Garantia_Consumo WHERE Codigo = LTRIM(f.CodigoGarantia))
FROM dbo.FILE_ConsumoGarantia f
WHERE f.CodigoGarantia+'-'+f.CodigoCredito
NOT IN (
	SELECT g.Codigo+'-'+c.Codigo
	FROM dbo.SICCMX_ConsumoGarantia cg
	INNER JOIN dbo.SICCMX_Garantia_Consumo g ON cg.IdGarantia = g.IdGarantia
	INNER JOIN dbo.SICCMX_Consumo c ON c.IdConsumo = cg.IdConsumo
) AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO

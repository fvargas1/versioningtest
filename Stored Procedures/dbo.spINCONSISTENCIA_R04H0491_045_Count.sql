SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_045_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La fecha de vencimiento del crédito es cero (0) si y solo si el crédito no es reestructurado ("Tipo de Alta del Crédito" distinto de 3 en columna 9).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE FechaVencimientoReestructura = '0' AND TipoAlta = '3';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_ReporteInterno_Select]
	@IdReporteInterno BIGINT
AS
SELECT
	vw.IdReporteInterno,
	vw.GrupoReporte,
	vw.Nombre,
	vw.Descripcion,
	vw.SProc
FROM dbo.RW_VW_ReporteInterno vw
WHERE vw.IdReporteInterno = @IdReporteInterno;
GO

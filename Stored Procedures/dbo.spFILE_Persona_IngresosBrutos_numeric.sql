SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_IngresosBrutos_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_IngresosBrutos_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Persona
SET errorFormato = 1
WHERE LEN(IngresosBrutos) > 0 AND (ISNUMERIC(ISNULL(IngresosBrutos,'0')) = 0 OR IngresosBrutos LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	CodigoCliente,
	'IngresosBrutos',
	IngresosBrutos,
	1,
	@Detalle
FROM dbo.FILE_Persona
WHERE LEN(IngresosBrutos) > 0 AND (ISNUMERIC(ISNULL(IngresosBrutos,'0')) = 0 OR IngresosBrutos LIKE '%[^0-9 .]%');
GO

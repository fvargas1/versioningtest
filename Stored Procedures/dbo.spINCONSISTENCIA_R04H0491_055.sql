SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_055]
AS

BEGIN

-- El "TIPO DE TASA DE INTERÉS DEL CRÉDITO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.TipoTasaInteres
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_TasaInteresHipotecario cat ON ISNULL(r.TipoTasaInteres,'') = cat.CodigoCNBV
WHERE cat.IdTasaInteresHipotecario IS NULL;

END
GO

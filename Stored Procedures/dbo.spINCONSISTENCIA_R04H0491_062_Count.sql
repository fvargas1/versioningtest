SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_062_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se deberá validar la correspondencia entre las claves "Tipo de Seguro de Crédito a la Vivienda" y "Entidad que Otorga el Seguro de Crédito a la Vivienda":
-- - Si el "Tipo de Seguro de Crédito a la Vivienda" es 1, 2 o 3, la "Entidad que Otorga el Seguro" debe ser 022601, 022602, 022603, 031001 ó 022023.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE TipoSeguro IN ('1','2','3') AND EntidadSeguro NOT IN ('022601','022602','022603','031001','022023');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

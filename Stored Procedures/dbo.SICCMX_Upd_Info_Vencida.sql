SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Upd_Info_Vencida]
AS
DECLARE @FechaPeriodo DATETIME;
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

-- ACTUALIZAMOS LAS 'VENTAS NETAS' DE ANEXO 21 A 'SIN INFORMACION' CUANDO LA INFORMACION FINANCIERA SEA MAYOR A 18 MESES
UPDATE dbo.SICCMX_Anexo21
SET VentNetTotAnuales = NULL
WHERE FechaInfoFinanc < DATEADD(MONTH, -18, @FechaPeriodo);

UPDATE dbo.SICCMX_Anexo21_GP
SET VentNetTotAnuales = NULL
WHERE FechaInfoFinanc < DATEADD(MONTH, -18, @FechaPeriodo);


INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT IdPersona, GETDATE(), '', 'Se actualizan las "Ventas Netas" a "Sin Información" ya que la Fecha de Información Financiera es mayor a 18 meses.'
FROM dbo.SICCMX_Anexo21
WHERE FechaInfoFinanc < DATEADD(MONTH, -18, @FechaPeriodo);

INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT IdGP, EsGarante, GETDATE(), '', 'Se actualizan las "Ventas Netas" a "Sin Información" ya que la Fecha de Información Financiera es mayor a 18 meses.'
FROM dbo.SICCMX_Anexo21_GP
WHERE FechaInfoFinanc < DATEADD(MONTH, -18, @FechaPeriodo);
GO

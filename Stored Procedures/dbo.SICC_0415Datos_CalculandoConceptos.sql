SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0415Datos_CalculandoConceptos]
AS
UPDATE dat
SET dat.Concepto = conf.Concepto
FROM R04.[0415Datos] dat
INNER JOIN R04.[0415Configuracion] conf ON conf.TipoProducto = dat.TipoProducto AND conf.SituacionCredito = dat.SituacionCredito;
GO

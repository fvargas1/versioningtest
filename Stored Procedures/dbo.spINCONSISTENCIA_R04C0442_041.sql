SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_041]
AS

BEGIN

-- El Fondo de Fomento o banco de desarrollo que otorgó la garantía debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.CodigoBancoGarantia
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_Institucion cat ON ISNULL(r.CodigoBancoGarantia,'') = cat.Codigo
WHERE LEN(CodigoBancoGarantia) > 0 AND cat.IdInstitucion IS NULL;

END
GO

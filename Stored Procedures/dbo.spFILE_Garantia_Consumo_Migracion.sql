SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Consumo_Migracion]
AS
UPDATE gar
SET IdTipoGarantia = tg.IdTipoGarantia,
	ValorGarantia = NULLIF(f.ValorGarantia,''),
	IdMoneda = mon.IdMoneda,
	FechaValuacion = NULLIF(f.FechaValuacion,''),
	Descripcion = LTRIM(RTRIM(f.Descripcion)),
	IdBancoGarantia = ins.IdInstitucion,
	ValorGarantiaProyectado = NULLIF(f.ValorGarantiaProyectado,''),
	Hc = NULLIF(f.Hc,''),
	VencimientoRestante = NULLIF(f.VencimientoRestante,''),
	IdGradoRiesgo = NULLIF(f.GradoRiesgo,''),
	IdAgenciaCalificadora = agen.IdAgencia,
	Calificacion = LTRIM(RTRIM(f.Calificacion)),
	IdEmisor = emi.IdEmisor,
	IdEscala = esc.IdEscala,
	EsIPC = NULLIF(f.EsIPC,''),
	PIGarante = NULLIF(f.PIGarante,''),
	IdPersona = per.IdPersona,
	IndPerMorales = NULLIF(f.IndPerMorales,'')
FROM dbo.SICCMX_Garantia_Consumo gar
INNER JOIN dbo.FILE_Garantia_Consumo f ON gar.Codigo = LTRIM(f.CodigoGarantia)
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON LTRIM(f.TipoGarantia) = tg.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
LEFT OUTER JOIN dbo.SICC_Institucion ins ON LTRIM(f.BancoGarantia) = ins.Codigo
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora agen ON LTRIM(f.AgenciaCalificadora) = agen.Codigo
LEFT OUTER JOIN dbo.SICC_Emisor emi ON LTRIM(f.Emisor) = emi.Codigo
LEFT OUTER JOIN dbo.SICC_Escala esc ON LTRIM(f.Escala) = esc.Codigo
LEFT OUTER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_Garantia_Consumo (
	Codigo,
	IdTipoGarantia,
	ValorGarantia,
	IdMoneda,
	FechaValuacion,
	Descripcion,
	IdBancoGarantia,
	ValorGarantiaProyectado,
	Hc,
	VencimientoRestante,
	IdGradoRiesgo,
	IdAgenciaCalificadora,
	Calificacion,
	IdEmisor,
	IdEscala,
	EsIPC,
	PIGarante,
	IdPersona,
	IndPerMorales
)
SELECT
	LTRIM(RTRIM(f.CodigoGarantia)),
	tg.IdTipoGarantia,
	NULLIF(f.ValorGarantia,''),
	mon.IdMoneda,
	NULLIF(f.FechaValuacion,''),
	LTRIM(RTRIM(f.Descripcion)),
	ins.IdInstitucion,
	NULLIF(f.ValorGarantiaProyectado,''),
	NULLIF(f.Hc,''),
	NULLIF(f.VencimientoRestante,''),
	NULLIF(f.GradoRiesgo,''),
	agen.IdAgencia,
	LTRIM(RTRIM(f.Calificacion)),
	emi.IdEmisor,
	esc.IdEscala,
	NULLIF(f.EsIPC,''),
	NULLIF(f.PIGarante,''),
	per.IdPersona,
	NULLIF(f.IndPerMorales,'')
FROM dbo.FILE_Garantia_Consumo f
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON LTRIM(f.TipoGarantia) = tg.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
LEFT OUTER JOIN dbo.SICC_Institucion ins ON LTRIM(f.BancoGarantia) = ins.Codigo
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora agen ON LTRIM(f.AgenciaCalificadora) = agen.Codigo
LEFT OUTER JOIN dbo.SICC_Emisor emi ON LTRIM(f.Emisor) = emi.Codigo
LEFT OUTER JOIN dbo.SICC_Escala esc ON LTRIM(f.Escala) = esc.Codigo
LEFT OUTER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
LEFT OUTER JOIN dbo.SICCMX_Garantia_Consumo gar ON LTRIM(f.CodigoGarantia) = gar.Codigo
WHERE gar.IdGarantia IS NULL AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO

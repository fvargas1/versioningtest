SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_HipotecarioGarantia]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_HipotecarioGarantia WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_HipotecarioGarantia (
	IdPeriodoHistorico,
	Codigo,
	Garantia,
	PorUsadoGarantia,
	MontoUsadoGarantia
)
SELECT
	@IdPeriodoHistorico,
	h.Codigo,
	g.Codigo,
	hg.PorUsadoGarantia,
	hg.MontoUsadoGarantia
FROM dbo.SICCMX_HipotecarioGarantia hg
INNER JOIN dbo.SICCMX_Hipotecario h ON hg.IdHipotecario = h.IdHipotecario
INNER JOIN dbo.SICCMX_Garantia_Hipotecario g ON hg.IdGarantia = g.IdGarantia;
GO

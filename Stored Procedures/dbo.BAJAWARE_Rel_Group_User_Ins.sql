SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Rel_Group_User_Ins]
	@IdGroup BAJAWARE_utID,
	@IdUser BAJAWARE_utID
AS
INSERT INTO dbo.BAJAWARE_Rel_Group_User (IdGroup, IdUser)
VALUES (@IdGroup, @IdUser);
GO

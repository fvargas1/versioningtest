SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Anexo18_Preliminares]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Anexo18_Preliminares WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Anexo18_Preliminares (
	IdPeriodoHistorico,
	Persona,
	DeudaTotalPartEleg,
	ServicioDeuda,
	ServicioDeudaIngresosTotales,
	DeudaCortoPlazoDeudaTotal,
	IngresosTotalesGastoCorr,
	InvIngresosTotales,
	IngPropiosIngTotales,
	ObligContDerivadasBenef,
	BalanceOperativoPIB,
	NivelEficienciaRec
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	per.Codigo,
	anx.DeudaTotalPartEleg,
	anx.ServicioDeuda,
	anx.ServicioDeudaIngresosTotales,
	anx.DeudaCortoPlazoDeudaTotal,
	anx.IngresosTotalesGastoCorr,
	anx.InvIngresosTotales,
	anx.IngPropiosIngTotales,
	anx.ObligContDerivadasBenef,
	anx.BalanceOperativoPIB,
	anx.NivelEficienciaRec
FROM dbo.SICCMX_Anexo18_Preliminares anx
INNER JOIN dbo.SICCMX_Persona per ON anx.IdPersona = per.IdPersona;
GO

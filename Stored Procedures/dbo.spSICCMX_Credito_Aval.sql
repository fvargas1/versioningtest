SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Credito_Aval]
 @IdPersona BIGINT
AS
SELECT
 av.IdAval,
 cre.Codigo,
 av.Codigo AS CodigoAval,
 av.nombre AS Nombre,
 ISNULL(av.PIAval,0) AS [PI],
 ISNULL(ca.Monto,0) AS Monto,
 ISNULL(ca.Porcentaje,0) AS Porcentaje,
 ISNULL(res.Reserva,0) AS Reserva
FROM dbo.SICCMX_CreditoAval ca
INNER JOIN dbo.SICCMX_Credito cre ON ca.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Aval av ON ca.IdAval = av.IdAval
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN (
 SELECT can.IdCredito, can.Reserva
 FROM dbo.SICCMX_Garantia_Canasta can
 INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia AND tg.Codigo='GP'
) res ON cre.IdCredito = res.IdCredito
WHERE cre.IdPersona = @IdPersona AND met.Codigo IN ('7','20','21','22') AND ca.Aplica = 1
ORDER BY cre.Codigo;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Consumo_Revolvente_CAT]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Consumo_Revolvente_CAT);

DELETE FROM Historico.RW_Consumo_Revolvente_CAT WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Consumo_Revolvente_CAT (
	IdPeriodoHistorico, Fecha, FolioCredito, CatContrato, LimCredito, Producto
)
SELECT
	@IdPeriodoHistorico,
	Fecha,
	FolioCredito,
	CatContrato,
	LimCredito,
	Producto
FROM dbo.RW_Consumo_Revolvente_CAT
WHERE IdReporteLog = @IdReporteLog;
GO

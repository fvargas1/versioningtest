SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SI_Get_Procesos_Migracion_ByCodename]
 @codename VARCHAR(50)
AS
SELECT
 val.Description,
 CASE WHEN val.Estatus IN (2,3) THEN val.Detalle WHEN val.Estatus = 4 THEN val.ErrorDetalle ELSE '' END AS Detalle,
 val.Estatus
FROM dbo.MIGRACION_Archivo arc
INNER JOIN dbo.MIGRACION_Validacion val ON arc.Codename = val.CategoryName
WHERE arc.Activo=1 AND val.Activo=1 AND val.Estatus NOT IN (0,-1) AND arc.Codename=@codename
ORDER BY arc.Position, val.Position
GO

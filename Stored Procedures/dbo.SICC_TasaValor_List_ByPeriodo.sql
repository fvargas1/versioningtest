SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_TasaValor_List_ByPeriodo]
	@IdPeriodo INT
AS
SELECT
	vw.IdTasaValor,
	vw.IdTasa,
	vw.DescripcionTasa,
	vw.IdPeriodo,
	vw.PeriodoFecha,
	vw.Valor
FROM dbo.SICC_VW_TasaValor vw
WHERE vw.IdPeriodo = @IdPeriodo;
GO

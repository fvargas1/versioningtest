SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_SaldoCapitalVigente_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_SaldoCapitalVigente_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Credito
SET errorFormato = 1
WHERE ISNUMERIC(ISNULL(NULLIF(SaldoCapitalVigente,''),'0')) = 0 OR SaldoCapitalVigente LIKE '%[^0-9 -.]%' OR CAST(NULLIF(SaldoCapitalVigente,'') AS DECIMAL(23,2)) < 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCredito,
 'SaldoCapitalVigente',
 SaldoCapitalVigente,
 1,
 @Detalle
FROM dbo.FILE_Credito
WHERE ISNUMERIC(ISNULL(NULLIF(SaldoCapitalVigente,''),'0')) = 0 OR SaldoCapitalVigente LIKE '%[^0-9 -.]%' OR CAST(NULLIF(SaldoCapitalVigente,'') AS DECIMAL(23,2)) < 0;
GO

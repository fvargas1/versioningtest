SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_023]
AS
BEGIN
-- El monto del pago exigible deberá ser mayor a 0.
DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	rep.CodigoPersona,
	rep.CodigoCreditoCNBV,
	rep.CodigoCredito,
	rep.NumeroDisposicion,
	rep.MontoExigible,
	SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(cre.FechaProximaAmortizacion,0),102),'.',''),1,6) AS FechaProximaAmortizacion,
	@FechaPeriodo AS FechaPeriodo
FROM dbo.RW_R04C0443 rep
INNER JOIN dbo.SICCMX_Credito cre ON rep.NumeroDisposicion = cre.Codigo AND SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(cre.FechaProximaAmortizacion,0),102),'.',''),1,6) = @FechaPeriodo
WHERE CAST(ISNULL(NULLIF(rep.MontoExigible,''),'0') AS DECIMAL(23,2)) <= 0;
END
GO

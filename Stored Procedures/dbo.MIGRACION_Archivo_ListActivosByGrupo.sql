SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Archivo_ListActivosByGrupo]
	@fltr VARCHAR(100)
AS

SET @fltr = NULLIF(@fltr,'');

SELECT
	vw.IdArchivo,
	vw.Nombre,
	vw.Codename,
	vw.JobCodename,
	vw.ErrorCodename,
	vw.LogCodename,
	vw.ClearLogCodename,
	vw.Position,
	vw.Activo,
	vw.ViewName,
	vw.ValidationStatus,
	vw.LastValidationExecuted,
	vw.InitCodename
FROM dbo.MIGRACION_VW_Archivo vw
WHERE (@fltr IS NULL OR vw.GrupoArchivo = @fltr) AND vw.Activo = 1
ORDER BY vw.Position;
GO

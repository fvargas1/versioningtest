SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_IntCartVencida]
AS
UPDATE cre
SET InteresCarteraVencida = cre.InteresVencidoValorizado * crv.PI_Total * (1 - crv.SP_Total)
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON	cre.IdCredito = crv.IdCredito;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_039]
AS
BEGIN
-- Si el crédito es vigente, entonces el número de días vencido (Col.27) debe ser menor a 91.

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	SituacionCredito,
	DiasAtraso
FROM dbo.RW_VW_R04C0484_INC
WHERE ISNULL(SituacionCredito,'') ='1' AND CAST(ISNULL(NULLIF(DiasAtraso,''),'0') AS INT) >= 91;

END
GO

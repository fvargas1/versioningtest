SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Totales_Persona]
AS
INSERT INTO dbo.SICCMX_PersonaCalificacion (IdPersona)
SELECT DISTINCT p.IdPersona
FROM dbo.SICCMX_Persona p
INNER JOIN dbo.SICCMX_Credito c ON p.IdPersona = c.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON c.IdCredito = crv.IdCredito
LEFT OUTER JOIN dbo.SICCMX_PersonaCalificacion pc ON p.IdPersona = pc.IdPersona
WHERE pc.IdPersona IS NULL;

UPDATE pc
SET EI = tot.EI_Total,
 Reserva = tot.ReservaFinal,
 PrctReserva = CASE WHEN tot.EI_Total > 0 THEN tot.ReservaFinal / tot.EI_Total ELSE 0 END
FROM dbo.SICCMX_PersonaCalificacion pc
INNER JOIN (
 SELECT pc.IdPersona, CAST(SUM(ISNULL(crv.EI_Total,0)) AS DECIMAL(23,2)) AS EI_Total, CAST(SUM(ISNULL(crv.ReservaFinal,0)) AS DECIMAL(23,2)) AS ReservaFinal
 FROM dbo.SICCMX_PersonaCalificacion pc
 INNER JOIN dbo.SICCMX_Credito c ON pc.IdPersona = c.IdPersona
 INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON c.IdCredito = crv.IdCredito
 GROUP BY pc.IdPersona
) tot ON pc.IdPersona = tot.IdPersona;

UPDATE pc
SET IdCalificacion = c.IdCalificacion
FROM dbo.SICCMX_PersonaCalificacion pc
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet c ON pc.PrctReserva BETWEEN c.RangoMenor AND c.RangoMayor;

UPDATE pc
SET IdCalificacion = c.IdCalificacion
FROM dbo.SICCMX_PersonaCalificacion pc
INNER JOIN dbo.SICC_CalificacionNvaMet c ON c.Codigo = 'NA'
WHERE pc.EI = 0 AND pc.Reserva = 0;
GO

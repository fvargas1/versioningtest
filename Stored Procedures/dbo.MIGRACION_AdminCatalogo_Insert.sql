SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_AdminCatalogo_Insert]
	@Nombre VARCHAR(50),
	@CodeName VARCHAR(50),
	@NombreTabla VARCHAR(100),
	@NombreIdentidad VARCHAR(50),
	@NombreBusqueda VARCHAR(50)
AS
INSERT INTO dbo.MIGRACION_AdminCatalog (
	Nombre,
	CodeName,
	NombreTabla,
	NombreIdentidad,
	NombreBusqueda
) VALUES (
	@Nombre,
	@CodeName,
	@NombreTabla,
	@NombreIdentidad,
	@NombreBusqueda
);

SELECT SCOPE_IDENTITY();
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Hipotecario_Reservas_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	Constante,
	FactorATR,
	ATR,
	FactorMAXATR,
	MAXATR,
	FactorProPago,
	PorPago,
	FactorPorCLTV,
	PorCLTVi,
	FactorINTEXP,
	INTEXP,
	FactorMON,
	MON,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion
FROM dbo.RW_VW_Hipotecario_Reservas
ORDER BY Codigo;
GO

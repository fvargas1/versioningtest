SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_037_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se validará que la clave de banca de desarrollo que otorgó los recursos exista en el catálogo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.CodigoCredito)
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_Institucion cat ON ISNULL(r.CodigoBancoFondeador,'') = cat.Codigo
WHERE LEN(R.CodigoBancoFondeador) > 0 AND cat.IdInstitucion IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

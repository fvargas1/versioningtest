SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0461_015]
AS

BEGIN

-- Se identifican créditos reportados en el formulario de severidad que no fueron reportados en el formulario de seguimiento.

SELECT
	rSP.CodigoPersona,
	rSP.NumeroDisposicion,
	rSP.CodigoCreditoCNBV
FROM dbo.RW_R04C0461 rSP
LEFT OUTER JOIN dbo.RW_R04C0459 rSE ON rSP.NumeroDisposicion = rSE.NumeroDisposicion
WHERE rSE.NumeroDisposicion IS NULL

END
GO

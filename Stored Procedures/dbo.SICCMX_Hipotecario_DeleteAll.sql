SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Hipotecario_DeleteAll]
AS
TRUNCATE TABLE dbo.SICCMX_HipotecarioInfo;
TRUNCATE TABLE dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares;
TRUNCATE TABLE dbo.SICCMX_HipotecarioAdicional;
TRUNCATE TABLE dbo.SICCMX_Hipotecario;
TRUNCATE TABLE dbo.SICCMX_Hipotecario_Reservas_Variables;
TRUNCATE TABLE dbo.SICCMX_HipotecarioReservas;
TRUNCATE TABLE dbo.SICCMX_Garantia_Hipotecario;
TRUNCATE TABLE dbo.SICCMX_HipotecarioGarantia;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_006_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "IDENTIFICADOR DEL CRÉDITO METODOLOGÍA CNBV" de la posición 2 a la posición 7 deberá ser igual a la Fecha de Otorgamiento (columna 11).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE SUBSTRING(CodigoCreditoCNBV,2,6) <> FechaOtorgamiento;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

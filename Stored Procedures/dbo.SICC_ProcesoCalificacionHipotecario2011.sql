SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ProcesoCalificacionHipotecario2011]
AS
DECLARE @FechaCalificacion DATETIME;
SELECT @FechaCalificacion = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

 --INICIALIZAMOS LA CALIFICACION DE HIPOTECARIO
TRUNCATE TABLE dbo.SICCMX_Hipotecario_Reservas_Variables;
TRUNCATE TABLE dbo.SICCMX_HipotecarioReservas;

INSERT INTO dbo.SICCMX_HipotecarioReservas (IdHipotecario, FechaCalificacion)
SELECT IdHipotecario, @FechaCalificacion
FROM dbo.SICCMX_Hipotecario;
GO

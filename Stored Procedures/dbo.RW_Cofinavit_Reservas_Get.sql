SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Cofinavit_Reservas_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	TipoRegimen,
	Constante,
	FactorATR,
	ATR,
	FactorPorpago,
	PorPago,
	FactorPromRete,
	PromRete,
	FactorMaxATR,
	MaxATR,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion
FROM dbo.RW_VW_Cofinavit_Reservas;
GO

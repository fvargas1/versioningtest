SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04H0493_2016_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	TipoBaja,
	SaldoPrincipalInicial,
	ResponsabilidadTotal,
	MontoPagoAcreditado,
	MontoQuitas,
	MontoCondonaciones,
	MontoBonificaciones,
	MontoDescuentos,
	ValorBienAdjudicado,
	MontoDacion,
	ReservasCalifCanceladas,
	ReservasAdicCanceladas
FROM Historico.RW_R04H0493_2016
WHERE IdPeriodoHistorico=@IdPeriodoHistorico;
GO

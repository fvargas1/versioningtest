SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_PeriodoHistorico_Update]
	@IdPeriodoHistorico BIGINT,
	@Fecha DATETIME = NULL,
	@Version VARCHAR(50) = NULL,
	@FechaCreacion DATETIME = NULL,
	@Username VARCHAR(50) = NULL,
	@CarteraComercial MONEY = NULL,
	@CastigosNetos MONEY = NULL,
	@Activo BIT = NULL,
	@Reporte BIT = NULL
AS

IF @Activo=1
BEGIN
UPDATE dbo.SICC_PeriodoHistorico
SET Activo = NULL
WHERE Fecha = (SELECT Fecha FROM dbo.SICC_PeriodoHistorico WHERE IdPeriodoHistorico=@IdPeriodoHistorico)
AND IdPeriodoHistorico <> @IdPeriodoHistorico;
END

IF @Reporte=1
BEGIN
UPDATE dbo.SICC_PeriodoHistorico
SET Reporte = NULL
WHERE Fecha = (SELECT Fecha FROM dbo.SICC_PeriodoHistorico WHERE IdPeriodoHistorico=@IdPeriodoHistorico)
AND IdPeriodoHistorico <> @IdPeriodoHistorico;
END

UPDATE dbo.SICC_PeriodoHistorico
SET CarteraComercial = @CarteraComercial,
	CastigosNetos = @CastigosNetos,
	Activo = CASE WHEN @Activo = 0 THEN NULL ELSE @Activo END,
	Reporte = CASE WHEN @Reporte = 0 THEN NULL ELSE @Reporte END
WHERE IdPeriodoHistorico= @IdPeriodoHistorico;
GO

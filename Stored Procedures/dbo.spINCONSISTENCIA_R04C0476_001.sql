SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0476_001]
AS

BEGIN

-- El factor ajuste hc debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	Hc
FROM dbo.RW_VW_R04C0476_INC
WHERE ISNULL(Hc,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END


GO

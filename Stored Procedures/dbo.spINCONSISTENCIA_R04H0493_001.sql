SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_001]
AS

BEGIN

-- El "IDENTIFICADOR DEL CRÉDITO ASIGNADO POR LA ENTIDAD" deberá ser único e irrepetible dentro del archivo que se reporta.

SELECT CodigoCredito, COUNT(CodigoCredito) AS Repetidos
FROM dbo.RW_R04H0493
GROUP BY CodigoCredito
HAVING COUNT(CodigoCredito) > 1;

END
GO

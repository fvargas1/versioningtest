SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_Totales_Gen]
AS
DECLARE @IdPeriodo INT;
DECLARE @FechaPeriodo DATETIME;
SELECT @IdPeriodo = IdPeriodo, @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

DELETE FROM dbo.RW_Totales_Por_Periodo WHERE IdPeriodo = @IdPeriodo;

INSERT INTO dbo.RW_Totales_Por_Periodo (IdPeriodo, FechaPeriodo, IdPersona, [PI], EI, EI_Cubierta, EI_Expuesta, Reserva, Reserva_Cubierta, Reserva_Expuesta)
SELECT
	@IdPeriodo,
	@FechaPeriodo,
	per.IdPersona,
	ppi.[PI],
	SUM(crv.EI_Total),
	SUM(crv.EI_Cubierta),
	SUM(crv.EI_Expuesta),
	SUM(crv.ReservaFinal),
	SUM(crv.ReservaCubierta),
	SUM(crv.ReservaExpuesta)
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Persona_PI ppi ON per.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_Credito cre ON ppi.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
WHERE met.Codigo IN ('18','20','21','22')
GROUP BY per.IdPersona, ppi.[PI];
GO

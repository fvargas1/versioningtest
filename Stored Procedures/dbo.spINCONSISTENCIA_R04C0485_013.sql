SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0485_013]
AS
BEGIN
-- Validar que la Responsabilidad Total al Inicio del Periodo (dat_responsabilidad_total) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCreditoCNBV,
	SaldoInsoluto
FROM dbo.RW_VW_R04C0485_INC
WHERE CAST(ISNULL(NULLIF(SaldoInsoluto,''),'-1') AS DECIMAL) < 0;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0457_2016_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	CodigoCreditoCNBV,
	TipoBaja,
	SaldoPrincipalInicio,
	SaldoInsoluto,
	MontoPagado,
	MontoCastigos,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificaciones,
	MontoDescuentos,
	MontoBienDacion,
	ReservasCalifCanceladas,
	ReservasAdicCanceladas
FROM dbo.RW_VW_R04C0457_2016;
GO

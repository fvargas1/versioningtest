SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04C0458_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	CodigoPersona,
	RFC,
	NombreCNBV,
	TipoCartera,
	IdActividadEconomica,
	GrupoRiesgo,
	EntFinAcreOtorgantesCre,
	IdLocalidad,
	Municipio,
	Estado,
	Nacionalidad,
	NumInfoCrediticia,
	LEI,
	IdTipoAlta,
	TipoOperacion,
	IdDestino,
	CodigoCredito,
	CodigoCnbv,
	GrupalCnbv,
	MontoLineaCredito,
	FecMaxDis,
	FecVenLinea,
	IdMoneda,
	IdDisposicion,
	TipoLinea,
	Posicion,
	RegGarantiaMob,
	IdDeudorRelacionado,
	IdInstitucionOrigen,
	IdTasaReferencia,
	AjusteTasaReferencia,
	OperacionTasaReferencia,
	FrecuenciaRevisionTasa,
	IdPeriodicidadCapital,
	IdPeriodicidadInteres,
	MesesGraciaCap,
	MesesGraciaIntereses,
	GastosOrigTasa,
	GastosOriginacion,
	ComisionTasaDis,
	ComisionMontoDis,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM Historico.RW_R04C0458
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY CodigoCredito
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_004]
AS
BEGIN
-- Validar que el Puntaje Crediticio Cuantitativo (dat_puntaje_cred_cuantitativo) sea >= 221 pero <=849.

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PuntajeCuantitativo
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(PuntajeCuantitativo,''),'-1') AS DECIMAL) NOT BETWEEN 221 AND 849;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Group_ListGuiAll]
AS
SELECT
	g.IdGroup,
	g.Active,
	g.Description,
	'Internal Security' AS IdGroupType
FROM dbo.BAJAWARE_Group g;
GO

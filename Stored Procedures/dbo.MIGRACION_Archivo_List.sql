SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Archivo_List]
AS
SELECT
 vw.IdArchivo,
 vw.Nombre,
 vw.Codename,
 vw.JobCodename,
 vw.ErrorCodename,
 vw.LogCodename,
 vw.ClearLogCodename,
 vw.Position,
 vw.Activo,
 vw.ViewName,
 vw.ValidationStatus,
 vw.LastValidationExecuted,
 vw.InitCodename,
 CASE WHEN vw.Activo = 1 THEN 'on' ELSE 'blank' END AS ActivoImg
FROM dbo.MIGRACION_VW_Archivo vw
ORDER BY vw.Position;
GO

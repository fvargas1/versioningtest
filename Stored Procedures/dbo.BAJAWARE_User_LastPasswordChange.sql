SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_User_LastPasswordChange]
	@idUser AS INTEGER
AS
SELECT MAX(iter.FechaCambio)
FROM dbo.BAJAWARE_User u
INNER JOIN dbo.BAJAWARE_UserPasswordIteration iter ON u.IdUser = iter.IdUser
WHERE iter.IdUser = @idUser;
GO

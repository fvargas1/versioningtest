SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_084]
AS
BEGIN
-- Validar que la Institucion de Fondeo corresponda a Catalogo CNBV

SELECT
	rep.CodigoCreditoCNBV,
	rep.NumeroDisposicion,
	rep.InstitucionFondeo
FROM dbo.RW_VW_R04C0484_INC rep
LEFT OUTER JOIN dbo.SICC_Institucion inst ON ISNULL(rep.InstitucionFondeo,'') = inst.Codigo
WHERE inst.IdInstitucion IS NULL;

END
GO

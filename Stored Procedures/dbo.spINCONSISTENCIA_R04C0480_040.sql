SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_040]
AS

BEGIN

-- Si la Tasa de Retención Laboral (dat_tasa_retencion_laboral) es >=35 y < 66.6,
-- entonces el Puntaje Asignado por Tasa de Retención Laboral (cve_ptaje_tasa_retenc_laboral) debe ser = 51

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	TasaRetLab,
	P_TasaRetLab AS Puntos_TasaRetLab
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(TasaRetLab AS DECIMAL(10,6)) >= 35 AND CAST(TasaRetLab AS DECIMAL(10,6)) < 66.6 AND ISNULL(P_TasaRetLab,'') <> '51';

END


GO

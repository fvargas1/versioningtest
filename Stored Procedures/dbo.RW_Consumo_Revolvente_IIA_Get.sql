SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_Revolvente_IIA_Get]
 @IdReporteLog BIGINT
AS
SELECT
	Producto,
	FolioCredito,
	LimCredito,
	SaldoTot,
	PagoNGI,
	SaldoRev,
	InteresRev,
	SaldoPMSI,
	Meses,
	PagoMin,
	TasaRev,
	SaldoPCI,
	InteresPCI,
	SaldoPagar,
	PagoReal,
	LimCreditoA,
	PagoExige,
	ImpagosC,
	ImpagoSum,
	MesApert,
	SaldoCont,
	Situacion,
	ProbInc,
	SevPer,
	ExpInc,
	MReserv,
	Relacion,
	ClasCont,
	CveCons,
	limcreditocalif,
	montopagarinst,
	montopagarsic,
	mesantig,
	mesesic,
	segmento,
	gveces,
	garantia,
	Catcuenta,
	indicadorcat,
	FolioCliente,
	CP,
	comtotal,
	comtardio,
	pagonginicio,
	pagoexigepsi
FROM dbo.RW_VW_Consumo_Revolvente_IIA
ORDER BY FolioCredito;
GO

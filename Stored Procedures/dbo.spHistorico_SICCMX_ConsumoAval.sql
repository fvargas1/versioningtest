SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_ConsumoAval]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_ConsumoAval WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_ConsumoAval (
	IdPeriodoHistorico,
	Codigo,
	Aval,
	Porcentaje,
	Monto,
	Aplica
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	c.Codigo,
	a.Codigo,
	cg.Porcentaje,
	cg.Monto,
	cg.Aplica
FROM dbo.SICCMX_ConsumoAval cg
INNER JOIN dbo.SICCMX_Consumo c ON cg.IdConsumo = c.IdConsumo
INNER JOIN dbo.SICCMX_Aval a ON cg.IdAval = a.IdAval;
GO

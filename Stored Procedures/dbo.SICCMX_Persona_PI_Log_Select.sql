SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_PI_Log_Select]
 @IdPersona BIGINT
AS
SELECT
 IdPersona, 
 FechaCalculo,
 Usuario,
 Descripcion
FROM dbo.SICCMX_Persona_PI_Log
WHERE IdPersona = @IdPersona
ORDER BY FechaCalculo ASC
GO

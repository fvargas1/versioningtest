SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_CredRepSIC_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_CredRepSIC_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_CredRepSIC cat ON LTRIM(f.CredRepSIC) = cat.Codigo
WHERE cat.IdCredRepSIC IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	f.CodigoCredito,
	'CredRepSIC',
	f.CredRepSIC,
	2,
	@Detalle
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_CredRepSIC cat ON LTRIM(f.CredRepSIC) = cat.Codigo
WHERE cat.IdCredRepSIC IS NULL;
GO

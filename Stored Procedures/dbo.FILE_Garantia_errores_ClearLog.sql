SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Garantia_errores_ClearLog]
AS
UPDATE dbo.FILE_Garantia
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_Garantia_errores;
GO

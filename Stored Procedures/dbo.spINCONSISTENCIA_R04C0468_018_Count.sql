SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_018_Count]
 @IdInconsistencia BIGINT
AS
BEGIN

-- Las instituciones NO deben presentar las siguientes claves de Actividad Económica (cve_actividad_economica):
-- 93111, 93121, 93122, 93123, 93131, 93132, 93133, 93141, 93142, 93143, 93151, 93152, 93153, 93161,
-- 93162, 93163, 93171, 93181, 93211, 93212, 52111, 52211, 52221, 52222, 52231, 52232, 52241, 52242,
-- 52243, 52311, 52411, 52412

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(ActEconomica,'') IN ('',
 '93111', '93121', '93122', '93123', '93131', '93132', '93133', '93141', '93142', '93143', '93151', '93152', '93153', '93161',
 '93162', '93163', '93171', '93181', '93211', '93212', '52111', '52211', '52221', '52222', '52231', '52232', '52241', '52242',
 '52243', '52311', '52411', '52412'
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO

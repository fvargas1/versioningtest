SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_026]
AS

BEGIN

-- El "MONTO DE LA LÍNEA DEL CRÉDITO AUTORIZADA" deberá ser mayor a 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, SaldoCredito
FROM dbo.RW_R04C0442
WHERE CAST(ISNULL(NULLIF(SaldoCredito,''),'-1') AS DECIMAL(23,2)) < 0;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Ponderados]
	@IdPersona BIGINT
AS
SELECT
	delta.FactorCuantitativo,
	delta.FactorCualitativo
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_PI_ValoresDelta delta ON ppi.IdMetodologia = delta.IdMetodologia AND ISNULL(ppi.IdClasificacion, '') = ISNULL(delta.IdClasificacion, '')
WHERE ppi.IdPersona=@IdPersona;
GO

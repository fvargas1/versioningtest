SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Formatos_Reporte_Save]
 @IdGroup INT,
 @xml VARCHAR(MAX)
AS
DECLARE @DocHandle AS INT;

-- Create an internal representation
EXEC sys.sp_xml_preparedocument @DocHandle OUTPUT, @xml;

DELETE rel
FROM dbo.RW_Rel_Grupo_ReporteCampos rel
INNER JOIN OPENXML (@DocHandle, 'Campos/Campo',11) WITH (idRC INT) AS tmp ON rel.IdReporteCampos = tmp.idRC
WHERE rel.IdGroup = @IdGroup;

INSERT INTO dbo.RW_Rel_Grupo_ReporteCampos (
 IdGroup,
 IdReporteCampos,
 TipoDato,
 Longitud,
 Decimales,
 Predeterminado,
 AplicaFormatoVP
)
SELECT
 @IdGroup,
 tmp.idRC AS Id,
 tmp.tpo AS TipoDato,
 NULLIF(tmp.lng,'') AS Longitud,
 NULLIF(tmp.dec,'') AS Decimales,
 NULLIF(tmp.pre,'') AS Predeterminado,
 NULLIF(tmp.vp,'') AS AplicaFormatoVP
FROM OPENXML (@DocHandle, 'Campos/Campo',11) WITH (idRC INT, tpo INT, lng VARCHAR(3), dec VARCHAR(3), pre VARCHAR(250), vp BIT) AS tmp;

-- Remove the DOM
EXEC sys.sp_xml_removedocument @DocHandle;
GO

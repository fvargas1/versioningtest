SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_CreditosPorPersona]
	@IdPersona BIGINT,
	@IdMetodologia INT
AS
SELECT
	cre.IdCredito,
	cre.CodigoCredito AS Codigo,
	ISNULL(crv.EI_Total,0) AS SaldoInsoluto,
	cre.Moneda,
	'' AS ClasificacionLegal,
	cgr.Cantidad AS Garantias,
	0 AS Avales,
	0 AS Obligados,
	ISNULL(crv.ReservaFinal,0) AS Reserva,
	ISNULL(crv.EI_Cubierta,0) AS MontoCubierto,
	ISNULL(crv.ReservaCubierta,0) AS ReservaCubierto,
	ISNULL(calCub.Codigo,'SC') AS CalificacionCubierto,
	ISNULL(crv.PorCubierto,0) AS PorcentajeCubierto,
	ISNULL(crv.EI_Expuesta,0) AS MontoExpuesto,
	ISNULL(crv.ReservaExpuesta,0) AS ReservaExpuesto,
	ISNULL(calExp.Codigo,'SC') AS CalificacionExpuesto,
	ISNULL(crv.PorExpuesta,0) AS PorcentajeExpuesto,
	ISNULL(crv.EI_Total,0) AS TotalResponsabilidad
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
LEFT OUTER JOIN (
	SELECT cg.IdCredito, COUNT(cg.IdGarantia) AS Cantidad
	FROM dbo.SICCMX_CreditoGarantia cg
	GROUP BY cg.IdCredito
) AS cgr ON crv.IdCredito = cgr.IdCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calCub ON crv.CalifCubierta = calCub.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calExp ON crv.CalifExpuesta = calExp.IdCalificacion
WHERE cre.IdMetodologia = @IdMetodologia AND cre.IdPersona = @IdPersona AND crv.EI_Total > 0
ORDER BY cre.CodigoCredito ASC;
GO

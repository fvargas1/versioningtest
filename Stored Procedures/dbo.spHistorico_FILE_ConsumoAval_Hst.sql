SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_ConsumoAval_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_ConsumoAval_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_ConsumoAval_Hst (
 IdPeriodoHistorico,
 CodigoAval,
 CodigoCredito,
 Porcentaje,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoAval,
 CodigoCredito,
 Porcentaje,
 Fuente
FROM dbo.FILE_ConsumoAval_Hst;
GO

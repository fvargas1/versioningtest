SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04A_Consolidado]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04A_Consolidado);

DELETE FROM Historico.RW_R04A_Consolidado WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04A_Consolidado (
	IdPeriodoHistorico, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato
)
SELECT
	@IdPeriodoHistorico,
	Concepto,
	SubReporte,
	Moneda,
	TipoDeCartera,
	TipoDeSaldo,
	Dato
FROM dbo.RW_R04A_Consolidado
WHERE IdReporteLog = @IdReporteLog;
GO

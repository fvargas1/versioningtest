SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_019]
AS

BEGIN

-- La "FECHA DE VENCIMIENTO DEL CRÉDITO" debe ser mayor al periodo que se reporta.

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT CodigoCredito, CodigoCreditoCNBV, FechaVencimiento, @FechaPeriodo AS FechaPeriodo
FROM dbo.RW_R04H0491
WHERE FechaVencimiento <= @FechaPeriodo AND TipoAlta <> '3';

END
GO

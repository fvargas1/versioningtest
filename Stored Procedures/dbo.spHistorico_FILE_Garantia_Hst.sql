SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_Garantia_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_Garantia_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_Garantia_Hst (
 IdPeriodoHistorico,
 CodigoGarantia,
 TipoGarantiaMA,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 FechaValuacion,
 Descripcion,
 BancoGarantia,
 ValorGarantiaProyectado,
 RegGarantiaMob,
 Hc,
 VencimientoRestante,
 GradoRiesgo,
 AgenciaCalificadora,
 Calificacion,
 Emisor,
 Escala,
 EsIPC,
 PIGarante,
 NumRPPC,
 RFCGarante,
 NombreGarante,
 IdGarante,
 ActividaEcoGarante,
 LocalidadGarante,
 MunicipioGarante,
 EstadoGarante,
 TipoGarante,
 LEI,
 IndPerMorales,
 CodigoCliente,
 CodigoPostalGarante,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoGarantia,
 TipoGarantiaMA,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 FechaValuacion,
 Descripcion,
 BancoGarantia,
 ValorGarantiaProyectado,
 RegGarantiaMob,
 Hc,
 VencimientoRestante,
 GradoRiesgo,
 AgenciaCalificadora,
 Calificacion,
 Emisor,
 Escala,
 EsIPC,
 PIGarante,
 NumRPPC,
 RFCGarante,
 NombreGarante,
 IdGarante,
 ActividaEcoGarante,
 LocalidadGarante,
 MunicipioGarante,
 EstadoGarante,
 TipoGarante,
 LEI,
 IndPerMorales,
 CodigoCliente,
 CodigoPostalGarante,
 Fuente
FROM dbo.FILE_Garantia_Hst;
GO

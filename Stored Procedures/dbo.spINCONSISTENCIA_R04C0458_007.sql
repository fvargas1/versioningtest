SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_007]
AS
BEGIN
-- Si el Tipo de Cartera (cve_tipo_cartera) es igual 299, 410 ó 420, validar que el RFC (dat_rfc) inicie con las letras CNBX, CNBF, _CNB o CNBV

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoCartera,
	RFC
FROM dbo.RW_VW_R04C0458_INC
WHERE ISNULL(TipoCartera,'') IN ('299','410','420') AND SUBSTRING(ISNULL(RFC,''),1,4) NOT IN ('CNBX','CNBF','_CNB','CNBV')
	AND TipoAltaCredito IN ('132','700','701','702');

END
GO

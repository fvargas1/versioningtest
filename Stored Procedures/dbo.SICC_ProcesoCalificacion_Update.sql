SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ProcesoCalificacion_Update](
 @IdProcesoCalificacion int,
 @Nombre varchar(100),
 @Descripcion varchar(200),
 @Codename varchar(150),
 @IdMetodologia int,
 @Position int,
 @Activo BIT
)
AS
UPDATE SICC_ProcesoCalificacion
SET
 Nombre = @Nombre,
 Descripcion = @Descripcion,
 Codename = @Codename,
 IdMetodologia = @IdMetodologia,
 Position = @Position,
 Activo = @Activo
WHERE IdProcesoCalificacion = @IdProcesoCalificacion
GO

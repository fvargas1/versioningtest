SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04C0473_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	TipoCartera,
	ActividadEconomica,
	GrupoRiesgo,
	LocalidadDeudor,
	DomicilioMunicipio,
	DomicilioEstado,
	NumInfoCrediticia,
	LEI,
	TipoAlta,
	TipoOperacion,
	DestinoCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	GrupalCNBV,
	MontoLineaAutorizado,
	FechaMaxDisponer,
	FechaVencLinea,
	Moneda,
	FormaDisposicion,
	TipoLinea,
	PrelacionPago,
	NoRUGM,
	PrctPartFed,
	InstAgenciaExt,
	TasaReferencia,
	AjusteTasaReferencia,
	OperacionTasaReferencia,
	FrecRevTasa,
	PeriodicidadCapital,
	PeriodicidadInteres,
	MesesGraciaCapital,
	MesesGraciaInteres,
	ComAperturaTasa,
	ComAperturaMonto,
	ComDispTasa,
	ComDispMonto,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM Historico.RW_R04C0473
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY CodigoCredito;
GO

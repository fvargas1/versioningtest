SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Consumo_Emisor_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_Consumo_Emisor_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Garantia_Consumo f
LEFT OUTER JOIN dbo.SICC_Emisor act ON LTRIM(f.Emisor) = act.Codigo
WHERE act.IdEmisor IS NULL AND LEN(f.Emisor) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	f.CodigoGarantia,
	'Emisor',
	f.Emisor,
	2,
	@Detalle
FROM dbo.FILE_Garantia_Consumo f
LEFT OUTER JOIN dbo.SICC_Emisor act ON LTRIM(f.Emisor) = act.Codigo
WHERE act.IdEmisor IS NULL AND LEN(f.Emisor) > 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Totales_Persona_Consumo]
AS
INSERT INTO dbo.SICCMX_PersonaCalificacion (IdPersona)
SELECT DISTINCT p.IdPersona
FROM dbo.SICCMX_Persona p
INNER JOIN dbo.SICCMX_Consumo c ON p.IdPersona = c.IdPersona
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON c.IdConsumo = crv.IdConsumo
LEFT OUTER JOIN dbo.SICCMX_PersonaCalificacion pc ON p.IdPersona = pc.IdPersona
WHERE pc.IdPersona IS NULL;

UPDATE pc
SET EI_Consumo = tot.EI_Total,
 Reserva_Consumo = tot.ReservaFinal,
 PrctReserva_Consumo = CASE WHEN tot.EI_Total > 0 THEN tot.ReservaFinal / tot.EI_Total ELSE NULL END
FROM dbo.SICCMX_PersonaCalificacion pc
INNER JOIN (
 SELECT pc.IdPersona, CAST(SUM(ISNULL(crv.E,0)) AS DECIMAL(23,2)) AS EI_Total, CAST(SUM(ISNULL(crv.ReservaTotal,0)) AS DECIMAL(23,2)) AS ReservaFinal
 FROM dbo.SICCMX_PersonaCalificacion pc
 INNER JOIN dbo.SICCMX_Consumo c ON pc.IdPersona = c.IdPersona
 INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON c.IdConsumo = crv.IdConsumo
 GROUP BY pc.IdPersona
) tot ON pc.IdPersona = tot.IdPersona;

UPDATE pc
SET IdCalificacion_Consumo = cal.IdCalificacion
FROM dbo.SICCMX_PersonaCalificacion pc
CROSS APPLY (
	SELECT TOP 1 pc2.IdPersona, cal.IdCalificacion
	FROM dbo.SICCMX_PersonaCalificacion pc2
	INNER JOIN dbo.SICCMX_Consumo con ON pc2.IdPersona = con.IdPersona
	INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON con.IdConsumo = pre.IdConsumo
	LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 cal ON pre.IdMetodologia = cal.IdMetodologia AND pc2.PrctReserva_Consumo BETWEEN cal.RangoMenor AND cal.RangoMayor
	WHERE pc.IdPersona = pc2.IdPersona
	ORDER BY cal.Codigo DESC
) AS cal;
GO

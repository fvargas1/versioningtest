SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_013]
AS

BEGIN

-- Si el Porcentaje de Pagos en Tiempo con Instituciones Financiera Bancarias en los Últimos 12 meses
-- (dat_porcent_pgo_bcos) es >= 0 y <75, entonces el Puntaje Asignado por Porcentaje de Pagos en Tiempo
-- con Instituciones Financiera Bancarias en los Últimos 12 meses (cve_ptaje_porcent_pago_bcos) debe ser = 17

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PorcPagoInstBanc,
	P_PorcPagoInstBanc AS Puntos_PorcPagoInstBanc
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(PorcPagoInstBanc AS DECIMAL(10,6)) >= 0 AND CAST(PorcPagoInstBanc AS DECIMAL(10,6)) < 75 AND ISNULL(P_PorcPagoInstBanc,'') <> '17';

END


GO

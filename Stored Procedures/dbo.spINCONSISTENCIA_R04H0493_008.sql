SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_008]
AS

BEGIN

-- La "RESPONSABILIDAD TOTAL AL MOMENTO DE LA BAJA" debe ser mayor o igual a cero.

SELECT CodigoCredito, CodigoCreditoCNBV, MontoTotalLiquidacion
FROM dbo.RW_R04H0493
WHERE CAST(ISNULL(NULLIF(MontoTotalLiquidacion,''),'-1') AS DECIMAL) < 0;

END
GO

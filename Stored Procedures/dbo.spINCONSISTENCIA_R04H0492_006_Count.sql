SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_006_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La "DENOMINACION DEL CREDITO" debe corresponder con el reportado en el formulario H-0491 Altas y reestructuras de
-- créditos a la vivienda con relación a la columna 13 para créditos originales o columna 28 para créditos reestructurados.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.IdReporteLog)
FROM dbo.RW_R04H0492 r
INNER JOIN dbo.SICCMX_VW_Datos_Altas_SerieH vw ON r.CodigoCreditoCNBV = vw.CodigoCreditoCNBV
WHERE r.DenominacionCredito <> vw.DenominacionCredito AND r.DenominacionCredito <> vw.DenominacionCreditoReestructura;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

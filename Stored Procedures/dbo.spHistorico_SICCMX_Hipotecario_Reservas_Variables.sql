SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Hipotecario_Reservas_Variables]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Hipotecario_Reservas_Variables WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Hipotecario_Reservas_Variables (
 IdPeriodoHistorico,
 Hipotecario,
 [PI],
 SP,
 E,
 Reserva,
 PorReserva,
 Calificacion,
 PerdidaEsperada,
 ReservaOriginal,
 PorCubiertoPP,
 ECubiertaPP,
 ReservaCubPP,
 ReservaPP_Credito
)
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 hip.Codigo,
 hrv.[PI],
 hrv.SP,
 hrv.E,
 hrv.Reserva,
 hrv.PorReserva,
 cal.Codigo,
 hrv.PerdidaEsperada,
 hrv.ReservaOriginal,
 hrv.PorCubiertoPP,
 hrv.ECubiertaPP,
 hrv.ReservaCubPP,
 hrv.ReservaPP_Credito
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON hip.IdHipotecario = hrv.IdHipotecario
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 cal ON hrv.IdCalificacion = cal.IdCalificacion;
GO

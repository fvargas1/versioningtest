SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_045]
AS

BEGIN

-- La "SEVERIDAD DE LA PÉRDIDA (Metodología Interna)" debe estar expresado a seis decimales.

SELECT CodigoCredito, CodigoCreditoCNBV, SPInterna
FROM dbo.RW_R04H0492
WHERE LEN(SUBSTRING(SPInterna,CHARINDEX('.',SPInterna)+1,LEN(SPInterna))) <> 6;

END
GO

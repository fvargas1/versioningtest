SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_002_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "IDENTIFICADOR DEL CRÉDITO METODOLOGÍA CNBV" deberá tener la estructura definida de acuerdo a la metodología CNBV (24 posiciones).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE LEN(ISNULL(CodigoCreditoCNBV,'')) <> 24;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

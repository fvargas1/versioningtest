SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_002]
AS

BEGIN

-- El "IDENTIFICADOR DEL CRÉDITO METODOLOGÍA CNBV" deberá tener la estructura definida de acuerdo a la metodología CNBV (24 posiciones).

SELECT CodigoCredito, CodigoCreditoCNBV
FROM dbo.RW_R04H0493
WHERE LEN(ISNULL(CodigoCreditoCNBV,'')) <> 24;

END
GO

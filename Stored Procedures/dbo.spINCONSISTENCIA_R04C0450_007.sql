SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_007]
AS
BEGIN
-- Validar que el Nombre del Garante (Columna 4) venga con letras mayúsculas.

SELECT DISTINCT
 CodigoCredito,
 REPLACE(NombreAcreditado, ',', '') AS NombreDeudor,
 NombreGarante
FROM dbo.RW_VW_R04C0450_INC
WHERE BINARY_CHECKSUM(NombreGarante) <> BINARY_CHECKSUM(UPPER(NombreGarante));

END
GO

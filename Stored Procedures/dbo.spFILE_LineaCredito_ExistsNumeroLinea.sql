SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_LineaCredito_ExistsNumeroLinea]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_LineaCredito_ExistsNumeroLinea';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_LineaCredito
SET errorFormato = 1
WHERE LEN(ISNULL(NumeroLinea, '')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_LineaCredito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 NumeroLinea,
 'NumeroLinea',
 NumeroLinea,
 1,
 @Detalle
FROM dbo.FILE_LineaCredito
WHERE LEN(ISNULL(NumeroLinea, '')) = 0;
GO

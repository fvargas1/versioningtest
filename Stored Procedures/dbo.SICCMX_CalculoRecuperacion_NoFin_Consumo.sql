SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoRecuperacion_NoFin_Consumo]
AS
-- CALCULAMOS EL MONTO DE RECUPERACION DE LAS GARANTIAS QUE NO SON FINANCIERAS
UPDATE can
SET MontoRecuperacion = can.MontoCobAjust * (1 - ISNULL(can.SeveridadCorresp, 0))
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE tg.IdClasificacionNvaMet = 2 OR EsDescubierto = 1;
GO

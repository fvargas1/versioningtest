SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_CedulaNMC_Info]
 @IdPeriodoHistorico INT
AS
DELETE FROM Historico.RW_CedulaNMC_Info WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_CedulaNMC_Info (
 IdPeriodoHistorico, Codigo, Nombre, RFC, MetodologiaNombre, MetodologiaDesc, Promotor, Domicilio, Telefono, Correo, ActividadEconomica,
 SocioPrincipal, GrupoEconomico, EI, Reserva, Calificacion, FactorCuantitativo, FactorCualitativo, Alpha, MAlpha, PonderadoCuantitativo,
 PonderadoCualitativo, FactorTotal, [PI], CodigoAval, NombreAval, MetodologiaAval, FactorCuantitativoAval, FactorCualitativoAval,
 AlphaAval, MAlphaAval, PonderadoCuantitativoAval, PonderadoCualitativoAval, FactorTotalAval, PIAval
)
SELECT
 @IdPeriodoHistorico,
 rw.Codigo,
 rw.Nombre,
 rw.RFC,
 rw.MetodologiaNombre,
 rw.MetodologiaDesc,
 rw.Promotor,
 rw.Domicilio,
 rw.Telefono,
 rw.Correo,
 rw.ActividadEconomica,
 rw.SocioPrincipal,
 rw.GrupoEconomico,
 rw.EI,
 rw.Reserva,
 rw.Calificacion,
 rw.FactorCuantitativo,
 rw.FactorCualitativo,
 rw.Alpha,
 rw.MAlpha,
 rw.PonderadoCuantitativo,
 rw.PonderadoCualitativo,
 rw.FactorTotal,
 rw.[PI],
 rw.CodigoAval,
 rw.NombreAval,
 rw.MetodologiaAval,
 rw.FactorCuantitativoAval,
 rw.FactorCualitativoAval,
 rw.AlphaAval,
 rw.MAlphaAval,
 rw.PonderadoCuantitativoAval,
 rw.PonderadoCualitativoAval,
 rw.FactorTotalAval,
 rw.PIAval
FROM dbo.RW_CedulaNMC_Info rw;
GO

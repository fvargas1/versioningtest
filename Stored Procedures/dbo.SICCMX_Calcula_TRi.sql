SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_TRi]
AS
-- INICIALIZAMOS LA TRi CON LOS VALORES RECIBIDOS EN LA MIGRACION
UPDATE pre
SET TRi = info.TRi
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON pre.IdHipotecario = info.IdHipotecario;

UPDATE vp
SET TRi = ((1 / vp.PorCLTVi) * val.VariableA) + (vp.RAi * val.VariableB) + (ISNULL(vp.PorCOBPP,0) * val.VariableC)
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp
LEFT OUTER JOIN dbo.SICC_EstadoRegion_Anx16 reg ON vp.Estado = reg.Estado
LEFT OUTER JOIN dbo.SICCMX_Hipotecario_Metodologia_ValoresCalculoTR val ON reg.Region = val.Region AND vp.IdConvenio = val.Convenio AND vp.IdMetodologia = val.IdMetodologia
WHERE vp.TRi IS NULL;
GO

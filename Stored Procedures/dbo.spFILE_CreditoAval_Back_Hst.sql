SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_CreditoAval_Back_Hst]
AS

INSERT INTO dbo.FILE_CreditoAval_Hst (
	CodigoAval,
	CodigoCredito,
	Porcentaje
)
SELECT
	ca.CodigoAval,
	ca.CodigoCredito,
	ca.Porcentaje
FROM dbo.FILE_CreditoAval ca
LEFT OUTER JOIN dbo.FILE_CreditoAval_Hst hst ON LTRIM(ca.CodigoCredito) = LTRIM(hst.CodigoCredito) AND LTRIM(ca.CodigoAval) = LTRIM(hst.CodigoAval)
WHERE hst.CodigoAval IS NULL;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_RTH_Reservas_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	Constante,
	FactorATR,
	ATR,
	FactorVECES,
	Veces,
	FactorPorPago,
	PorPago,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion
FROM Historico.RW_RTH_Reservas
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Codigo;
GO

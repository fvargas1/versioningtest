SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PersonaInfo_Select]
	@IdPersona bigint
AS
SELECT
 vw.IdPersona,
 vw.IdTipoPersona,
 vw.IdTipoPersonaNombre,
 vw.IdSectorEconomico,
 vw.IdSectorEconomicoNombre,
 vw.NumeroEmpleados,
 vw.IngresosBrutos,
 vw.IdSectorLaboral,
 vw.IdSectorLaboralNombre,
 vw.GrupoRiesgo,
 vw.IdTipoIngreso,
 vw.IdTipoIngresoNombre
FROM dbo.SICCMX_VW_PersonaInfo vw
WHERE vw.IdPersona = @IdPersona;
GO

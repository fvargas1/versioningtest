SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_030]
AS

BEGIN

-- Se verificará que el número de días vencidos, no sea mayor a 90 días para seguirlo considerando como vigente.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, SituacionCredito, DiasVencido
FROM dbo.RW_R04C0443
WHERE SituacionCredito ='1' AND CAST(ISNULL(NULLIF(DiasVencido,''),'0') AS DECIMAL) > 90;

END
GO

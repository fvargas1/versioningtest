SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_011]
AS
BEGIN
-- El monto de la prima debe ser mayor o igual a 0.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	MontoPrimasAnuales
FROM dbo.RW_VW_R04C0463_INC
WHERE CAST(ISNULL(NULLIF(MontoPrimasAnuales,''),'-1') AS DECIMAL) < 0;

END

GO

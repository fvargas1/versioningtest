SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Proyecto_CodigoRepetidoCodigoProyecto]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Proyecto_CodigoRepetidoCodigoProyecto';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Proyecto f
WHERE f.CodigoProyecto IN (
 SELECT f.CodigoProyecto
 FROM dbo.FILE_Proyecto f
 GROUP BY f.CodigoProyecto
 HAVING COUNT(f.CodigoProyecto )>1
) AND LEN(f.CodigoProyecto) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Proyecto_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoProyecto,
 'CodigoProyecto',
 f.CodigoProyecto,
 1,
 @Detalle
FROM dbo.FILE_Proyecto f
WHERE f.CodigoProyecto IN (
 SELECT f.CodigoProyecto
 FROM dbo.FILE_Proyecto f
 GROUP BY f.CodigoProyecto
 HAVING COUNT(f.CodigoProyecto )>1
) AND LEN(f.CodigoProyecto) > 0;
GO

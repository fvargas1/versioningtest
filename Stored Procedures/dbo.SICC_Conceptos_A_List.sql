SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Conceptos_A_List]
 @SubReporte VARCHAR(3)
AS
SELECT
 IdConcepto AS IdConcepto,
 Concepto + ' - ' + Descripcion AS Concepto
FROM dbo.SICC_Conceptos_A
WHERE SubReporte = @SubReporte;
GO

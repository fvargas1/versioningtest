SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Validacion_Update]
 @IdValidacion int,
 @Description varchar(250),
 @Codename varchar(150),
 @Position int,
 @CategoryName varchar(50) = NULL,
 @FieldName varchar(50) = NULL,
 @Activo BIT,
 @AplicaUpd BIT,
 @ReqCalificacion BIT,
 @ReqReportes BIT
AS
UPDATE dbo.MIGRACION_Validacion
SET
 Description = @Description,
 Codename = @Codename,
 Position = @Position,
 CategoryName = @CategoryName,
 FieldName = @FieldName,
 Activo = @Activo,
 AplicaUpd = @AplicaUpd,
 ReqCalificacion = @ReqCalificacion,
 ReqReportes = @ReqReportes
WHERE IdValidacion= @IdValidacion;
GO

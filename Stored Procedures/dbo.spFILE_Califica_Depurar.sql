SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Califica_Depurar]
AS
DELETE
FROM dbo.FILE_Califica
WHERE '[' + ISNULL(CodigoDeudor,'') + '][' + ISNULL(FECHA_BURO,'') + ']' NOT IN (
	SELECT '[' + ISNULL(f.CodigoDeudor,'') + '][' + ISNULL(MAX(f.FECHA_BURO),'') + ']'
	FROM dbo.FILE_Califica f
	GROUP BY f.CodigoDeudor
);
GO

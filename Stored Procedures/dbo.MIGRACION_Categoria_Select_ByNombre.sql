SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Categoria_Select_ByNombre]
	@Nombre varchar(150)
AS
SELECT
	vw.IdCategoria,
	vw.Nombre,
	vw.NombreTabla
FROM dbo.MIGRACION_VW_Categoria vw
WHERE vw.Nombre = @Nombre;
GO

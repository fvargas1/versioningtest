SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_Financieras_Consumo]
AS
-- CALCULAMOS SP PARA GARANTIAS REALES FINANCIERAS
UPDATE can
SET SeveridadCorresp = CASE WHEN crv.E = 0 THEN 0 ELSE crv.SPExpuesta * CAST(can.EI_Ajust / crv.E AS DECIMAL(18,12)) END
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON can.IdConsumo = crv.IdConsumo
WHERE can.IdTipoGarantia IS NULL AND can.EsDescubierto IS NULL;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_060]
AS

BEGIN

-- El campo "SEGUROS A CARGO DEL ACREDITADO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.SeguroAcreditado
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_SeguroAcreditado cat ON ISNULL(r.SeguroAcreditado,'') = cat.CodigoCNBV
WHERE cat.IdSeguroAcreditado IS NULL;

END
GO

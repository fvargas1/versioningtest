SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_TotalResponsabilidad]
	@IdPersona BIGINT,
	@IdMetodologia INT
AS
SELECT ISNULL(SUM(ISNULL(crv.EI_Total,0)),0) AS TotalResponsabilidad 
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
WHERE cre.IdPersona = @IdPersona AND cre.IdMetodologia = @IdMetodologia;
GO

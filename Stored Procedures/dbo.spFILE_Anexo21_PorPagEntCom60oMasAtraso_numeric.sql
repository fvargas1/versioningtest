SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo21_PorPagEntCom60oMasAtraso_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo21_PorPagEntCom60oMasAtraso_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo21
SET errorFormato = 1
WHERE LEN(PorPagEntCom60oMasAtraso)>0 AND (ISNUMERIC(ISNULL(PorPagEntCom60oMasAtraso,'0')) = 0 OR PorPagEntCom60oMasAtraso LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo21_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoCliente,
	'PorPagEntCom60oMasAtraso',
	PorPagEntCom60oMasAtraso,
	1,
	@Detalle
FROM dbo.FILE_Anexo21
WHERE LEN(PorPagEntCom60oMasAtraso)>0 AND (ISNUMERIC(ISNULL(PorPagEntCom60oMasAtraso,'0')) = 0 OR PorPagEntCom60oMasAtraso LIKE '%[^0-9 .]%');
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_Localidad2015]
	@filtro VARCHAR(50)
AS
SELECT DISTINCT TOP 300
	cat.CodigoPais,
	pais.Nombre AS NombrePais,
	cat.CodigoEstado,
	cat.NombreEstado,
	cat.CodigoMunicipio,
	cat.NombreMunicipio,
	cat.CodigoLocalidad,
	cat.NombreLocalidad,
	cat.CodigoCNBV AS CodigoCNBV
FROM dbo.SICC_Localidad2015 cat
LEFT OUTER JOIN dbo.SICC_Pais pais ON cat.CodigoPais = pais.Codigo
WHERE ISNULL(cat.CodigoPais,'')+'|'+ISNULL(pais.Nombre,'')+'|'+ISNULL(cat.CodigoEstado,'')+'|'+ISNULL(cat.NombreEstado,'')
	+'|'+ISNULL(cat.CodigoMunicipio,'')+'|'+ISNULL(cat.NombreMunicipio,'')+'|'+ISNULL(cat.CodigoLocalidad,'')+'|'+ISNULL(cat.NombreLocalidad,'')
	+'|'+ISNULL(cat.CodigoCNBV,'') LIKE '%' + @filtro + '%';
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_Revolvente_CAT_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Fecha,
	FolioCredito,
	CatContrato,
	LimCredito,
	Producto
FROM dbo.RW_VW_Consumo_Revolvente_CAT;
GO

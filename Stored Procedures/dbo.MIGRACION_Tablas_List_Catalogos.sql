SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Tablas_List_Catalogos]
AS

CREATE TABLE #Tablas (
 Qualifier VARCHAR(50),
 Owner VARCHAR(25),
 Name VARCHAR(100),
 Type VARCHAR(50),
 Remarks VARCHAR(50) NULL
);

INSERT INTO #Tablas (Qualifier, Owner, Name, Type, Remarks)
EXEC sp_tables;

SELECT Name
FROM #Tablas
WHERE [Name] LIKE 'SICC[_]%'
 AND [Name] NOT LIKE 'SICC[_]VW%'
 AND [Name] NOT LIKE '%MAPPING'
 AND Owner = 'dbo'
ORDER BY [Name];

DROP TABLE #Tablas;
GO

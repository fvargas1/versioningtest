SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_023_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Saldo de Gastos Corrientes (dat_saldo_gasto_corriente) es > 120.5,
-- entonces el Puntaje Ingresos Totales a Gasto Corriente (cve_puntaje_ingreso_gasto) debe ser = 112

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoIngTotales,''),'0') AS DECIMAL(18,6)) > 120.5 AND ISNULL(P_IngTotGastoCorriente,'') <> '112';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

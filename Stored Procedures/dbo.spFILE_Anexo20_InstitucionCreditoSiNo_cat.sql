SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo20_InstitucionCreditoSiNo_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo20_InstitucionCreditoSiNo_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo20
SET errorCatalogo = 1
WHERE LTRIM(ISNULL(InstitucionCreditoSiNo,'')) NOT IN ('0','1') AND LTRIM(EntFinAcreOtorgantesCre)='1';

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo20_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoCliente,
 'InstitucionCreditoSiNo',
 InstitucionCreditoSiNo,
 2,
 @Detalle
FROM dbo.FILE_Anexo20
WHERE LTRIM(ISNULL(InstitucionCreditoSiNo,'')) NOT IN ('0','1') AND LTRIM(EntFinAcreOtorgantesCre)='1';
GO

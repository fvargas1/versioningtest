SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_005]
AS

BEGIN

-- El "TIPO DE BAJA DEL CRÉDITO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.TipoBaja
FROM dbo.RW_R04H0493 r
LEFT OUTER JOIN dbo.SICC_TipoBajaHipotecario cat ON ISNULL(r.TipoBaja,'') = cat.CodigoCNBV
WHERE cat.IdTipoBajaHipotecario IS NULL;

END
GO

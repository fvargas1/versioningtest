SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Upd_SaldoInsoluto]
AS
UPDATE inf
SET SaldoInsoluto = cre.MontoValorizado
FROM dbo.SICCMX_CreditoInfo inf
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON inf.IdCredito = cre.IdCredito;
GO

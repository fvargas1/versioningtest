SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_070]
AS

BEGIN

-- La clave de Municipio es igual a cero (0) si y solo si “Tipo de Alta del Crédito” igual 3 (columna 9).

SELECT CodigoCredito, CodigoCreditoCNBV, Municipio, TipoAlta
FROM dbo.RW_R04H0491
WHERE Municipio = '0' AND TipoAlta <> '3';

END
GO

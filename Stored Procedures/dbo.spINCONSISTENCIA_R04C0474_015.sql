SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_015]
AS

BEGIN

-- El campo SEVERIDAD PERDIDA debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SPTotal AS SP
FROM dbo.RW_R04C0474
WHERE CAST(ISNULL(NULLIF(SPTotal,''),'-1') AS DECIMAL(10,6)) < 0;

END
GO

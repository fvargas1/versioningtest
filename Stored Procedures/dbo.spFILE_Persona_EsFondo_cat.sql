SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_EsFondo_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_EsFondo_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_TipoReserva5Prct fon ON LTRIM(f.EsFondo) = fon.Codigo
WHERE LEN(f.EsFondo) > 0 AND fon.IdTipoReserva IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoCliente,
 'EsFondo',
 f.EsFondo,
 1,
 @Detalle
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_TipoReserva5Prct fon ON LTRIM(f.EsFondo) = fon.Codigo
WHERE LEN(f.EsFondo) > 0 AND fon.IdTipoReserva IS NULL;
GO

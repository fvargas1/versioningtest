SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_Expuesta_Consumo]
AS
-- CALCULANDO PARTE DESCUBIERTA (CREDITO)
INSERT INTO dbo.SICCMX_Garantia_Canasta_Consumo (
 IdConsumo,
 PrctCobSinAju,
 PrctCobAjust,
 MontoCobAjust,
 [PI],
 SeveridadCorresp,
 EsDescubierto
)
SELECT DISTINCT
 vw.IdConsumo,
 dbo.MAX2VAR(0, 1 - ISNULL(cub.PrctCobSinAju, 0)),
 dbo.MAX2VAR(0, 1 - ISNULL(cub.PrctCobAjust, 0)),
 dbo.MAX2VAR(0, ISNULL(crv.E, 0) - (ISNULL(cub.Cobertura, 0))),
 crv.[PI],
 crv.SPExpuesta,
 1
FROM dbo.SICCMX_VW_Consumo vw
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON vw.IdConsumo = crv.IdConsumo
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_Cub_Consumo cub ON vw.IdConsumo = cub.IdConsumo
LEFT OUTER JOIN dbo.SICCMX_Garantia_Canasta_Consumo canasta ON vw.IdConsumo = canasta.IdConsumo AND canasta.EsDescubierto=1
WHERE canasta.IdConsumo IS NULL;
GO

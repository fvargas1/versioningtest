SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_007_Count]
 @IdInconsistencia BIGINT
AS
BEGIN
-- Validar que el Nombre del Garante (Columna 4) venga con letras mayúsculas.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(DISTINCT CodigoCredito)
FROM dbo.RW_VW_R04C0450_INC
WHERE BINARY_CHECKSUM(NombreGarante) <> BINARY_CHECKSUM(UPPER(NombreGarante));

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

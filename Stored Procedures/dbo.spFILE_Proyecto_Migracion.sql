SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Proyecto_Migracion]
AS
DECLARE @FechaPeriodo DATETIME;
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;


UPDATE pry
SET
 IdPersona = per.IdPersona,
 Nombre = LTRIM(RTRIM(f.NombredelProyecto)),
 Descripcion = LTRIM(RTRIM(f.DescripciondelProyecto)),
 IdLineaCredito = lin.IdLineaCredito,
 FechaCreacion = GETDATE(),
 Periodo = @FechaPeriodo
FROM dbo.SICCMX_Proyecto pry
INNER JOIN dbo.FILE_Proyecto f ON pry.CodigoProyecto = LTRIM(f.CodigoProyecto)
INNER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
LEFT OUTER JOIN dbo.SICCMX_LineaCredito lin ON LTRIM(f.NumeroLinea) = lin.Codigo
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;

INSERT INTO dbo.SICCMX_Proyecto (
 CodigoProyecto,
 IdPersona,
 Nombre,
 Descripcion,
 IdLineaCredito,
 FechaCreacion,
 Periodo
)
SELECT
 LTRIM(RTRIM(f.CodigoProyecto)),
 per.IdPersona,
 LTRIM(RTRIM(f.NombredelProyecto)),
 LTRIM(RTRIM(f.DescripciondelProyecto)),
 lin.IdLineaCredito,
 GETDATE(),
 @FechaPeriodo
FROM dbo.FILE_Proyecto f
INNER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
LEFT OUTER JOIN dbo.SICCMX_LineaCredito lin ON LTRIM(f.NumeroLinea) = lin.Codigo
LEFT OUTER JOIN dbo.SICCMX_Proyecto pry ON LTRIM(f.CodigoProyecto) = pry.CodigoProyecto
WHERE pry.IdProyecto IS NULL AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


SET NOCOUNT ON;

INSERT INTO dbo.SICCMX_ProyectoCalificacion (IdProyecto, Fecha)
SELECT pry.IdProyecto, GETDATE()
FROM dbo.SICCMX_Proyecto pry
LEFT OUTER JOIN dbo.SICCMX_ProyectoCalificacion pc ON pry.IdProyecto = pc.IdProyecto
WHERE pc.IdProyecto IS NULL;

UPDATE pc
SET
 SobreCosto = NULLIF(f.Sobrecosto,''),
 MontoCubierto = NULLIF(f.MontoCubiertoPorTerceros,''),
 Meses = NULLIF(f.MesesContemplados,''),
 MesesAdicionales = NULLIF(f.MesesAdicionales,''),
 VPTotal = NULLIF(f.VPTotal,''),
 UpAcumulada = NULLIF(f.UtilidadPerdidaAcumulada,''),
 IdEtapa = etp.IdEtapa
FROM dbo.SICCMX_ProyectoCalificacion pc
INNER JOIN dbo.SICCMX_Proyecto pry ON pc.IdProyecto = pry.IdProyecto
INNER JOIN dbo.FILE_Proyecto f ON pry.CodigoProyecto = LTRIM(f.CodigoProyecto)
INNER JOIN dbo.SICC_Etapa etp ON LTRIM(f.Etapa) = etp.Codigo;

-- CAMPOS CAMBIO REGULATORIO 2016
UPDATE pry
SET FechaInicioOper = NULLIF(f.FechaInicioOper,''),
	TasaDescuentoVP = NULLIF(f.TasaDescuentoVP,''),
	VentNetTotAnuales = NULLIF(f.VentNetTotAnuales,'')
FROM dbo.SICCMX_Proyecto pry
INNER JOIN dbo.FILE_Proyecto f ON pry.CodigoProyecto = LTRIM(f.CodigoProyecto)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Consumo_Select]
	@IdConsumo BIGINT
AS
SELECT
	vw.IdConsumo,
	vw.Codigo,
	vw.IdPersona,
	vw.IdPersonaNombre,
	vw.SaldoCapitalVigente,
	vw.SaldoCapitalVigenteValorizado,
	vw.InteresVigente,
	vw.InteresVigenteValorizado,
	vw.SaldoCapitalVencido,
	vw.SaldoCapitalVencidoValorizado,
	vw.InteresVencido,
	vw.InteresVencidoValorizado,
	vw.PeriodosIncumplimiento,
	vw.Periodicidad,
	CASE WHEN vw.SituacionCreditoCodigo='1' THEN 0 ELSE 1 END AS CarteraVencida,
	vw.Revolvente,
	0 AS Liquidado,
	0 AS PagosParciales,
	vw.IdTipoCredito,
	vw.IdTipoCreditoNombre,
	0 AS Garantia,
	0 AS MontoGarantia,
	vw.IdSituacionCredito,
	vw.IdSituacionCreditoNombre,
	NULL AS IdClasificacionContable,
	NULL AS IdClasificacionContableNombre,
	NULL AS FechaVencimiento,
	vw.IdReestructura,
	vw.IdReestructuraNombre,
	NULL AS IdTasaReferencia,
	NULL AS IdTasaReferenciaNombre,
	0 AS InteresRefinanciado,
	vw.TasaInteres,
	NULL AS IdSucursal,
	NULL AS IdSucursalNombre,
	NULL AS IdProducto,
	vw.IdProductoNombre ,
	info.IdMoneda,
	info.IdMonedaNombre,
	NULL AS FechaCreacion,
	NULL AS FechaActualizacion,
	NULL AS Username
FROM dbo.SICCMX_VW_Consumo vw
INNER JOIN dbo.SICCMX_VW_ConsumoInfo info ON vw.IdConsumo = info.IdConsumo
WHERE vw.IdConsumo = @IdConsumo;
GO

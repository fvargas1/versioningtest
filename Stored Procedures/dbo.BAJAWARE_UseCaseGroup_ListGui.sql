SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCaseGroup_ListGui]
AS
SELECT ucg.IdUseCaseGroup, ucg.Active, ucg.Name, app.Name AS IdApplication, ucg.SortOrder, ucg.StringKey, ucg.Description
FROM dbo.BAJAWARE_UseCaseGroup ucg
LEFT OUTER JOIN dbo.BAJAWARE_Application app ON ucg.IdApplication = app.IdApplication;
GO

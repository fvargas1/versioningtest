SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0456_006]
AS
BEGIN
-- El porcentaje cubierto ecpm debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	PrctECPM
FROM dbo.RW_VW_R04C0456_INC
WHERE ISNULL(PrctECPM,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_EntidadColumna_Insert]
	@Nombre VARCHAR(25),
	@Descripcion VARCHAR(100) = NULL,
	@IdEntidad INT,
	@NombreCampo VARCHAR(50),
	@TipoDato VARCHAR(50),
	@IsRequerido BIT = NULL,
	@IsKey BIT = NULL,
	@IsIdentidadBusqueda BIT = NULL,
	@IdCatalogo INT = NULL,
	@NombreIdCatalogo VARCHAR(50) = NULL
AS
INSERT INTO dbo.MIGRACION_EntidadColumns (
	Nombre,
	Descripcion,
	IdEntidad,
	NombreCampo,
	TipoDato,
	IsRequerido,
	IsKey,
	IsIdentidadBusqueda,
	IdCatalogo,
	NombreIdCatalogo
) VALUES (
	@Nombre,
	@Descripcion,
	@IdEntidad,
	@NombreCampo,
	@TipoDato,
	@IsRequerido,
	@IsKey,
	@IsIdentidadBusqueda,
	@IdCatalogo,
	@NombreIdCatalogo
);

SELECT SCOPE_IDENTITY();
GO

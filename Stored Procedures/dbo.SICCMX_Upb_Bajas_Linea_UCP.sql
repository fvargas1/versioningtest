SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Upb_Bajas_Linea_UCP]
AS
UPDATE lin
SET IdTipoBaja = linBaja.IdTipobaja,
	IdTipoBajaMA = linBaja.IdTipobajaMA,
	SaldoInicial = linBaja.SaldoInicial,
	ResTotalInicioPer = linBaja.ResTotalInicioPer,
	MontoPagEfeCap = linBaja.MontoPagEfeCap,
	MontoPagEfeInt = linBaja.MontoPagEfeInt,
	MontoPagEfeCom = linBaja.MontoPagEfeCom,
	MontoPagEfeIntMor = linBaja.MontoPagEfeIntMor,
	MontoQuitasCastQue = linBaja.MontoQuitasCastQue,
	MontoBonificacionDesc = linBaja.MontoBonificacionDesc
FROM dbo.SICCMX_LineaCredito lin
LEFT OUTER JOIN (
	SELECT cre.IdLineaCredito,
	MAX(info.IdTipoBaja) AS IdTipobaja,
	MAX(info.IdTipoBajaMA) AS IdTipobajaMA,
	SUM(ISNULL(info.SaldoInicial,0)) AS SaldoInicial,
	SUM(ISNULL(info.ResTotalInicioPer,0)) AS ResTotalInicioPer,
	SUM(ISNULL(info.MontoPagEfeCap,0)) AS MontoPagEfeCap,
	SUM(ISNULL(info.MontoPagEfeInt,0)) AS MontoPagEfeInt,
	SUM(ISNULL(info.MontoPagEfeCom,0)) AS MontoPagEfeCom,
	SUM(ISNULL(info.MontoPagEfeIntMor,0)) AS MontoPagEfeIntMor,
	SUM(ISNULL(info.MontoQuitasCastQue,0)) AS MontoQuitasCastQue,
	SUM(ISNULL(info.MontoBonificacionDesc,0)) AS MontoBonificacionDesc
	FROM dbo.SICCMX_Credito cre
	INNER JOIN dbo.SICCMX_CreditoInfo info ON cre.IdCredito = info.IdCredito
	WHERE info.IdTipoBaja IS NOT NULL
	GROUP BY cre.IdLineaCredito
) linBaja ON lin.IdLineaCredito = linBaja.IdLineaCredito
LEFT OUTER JOIN (
	SELECT cre2.IdLineaCredito
	FROM dbo.SICCMX_Credito cre2
	INNER JOIN dbo.SICCMX_CreditoInfo info2 ON cre2.IdCredito = info2.IdCredito
	WHERE info2.IdTipoBaja IS NULL
	GROUP BY cre2.IdLineaCredito
) linNoBaja ON lin.IdLineaCredito = linNoBaja.IdLineaCredito
WHERE linBaja.IdLineaCredito IS NOT NULL AND linNoBaja.IdLineaCredito IS NULL;
GO

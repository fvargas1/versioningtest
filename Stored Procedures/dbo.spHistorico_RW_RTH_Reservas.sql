SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_RTH_Reservas]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_RTH_Reservas);

DELETE FROM Historico.RW_RTH_Reservas WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_RTH_Reservas (
	IdPeriodoHistorico, Codigo, SaldoCapitalVigente, InteresVigente, SaldoCapitalVencido, InteresVencido, Constante, FactorATR,
	ATR, FactorVECES, Veces, FactorPorPago, PorPago, [PI], SP, E, Reserva, PorReserva, Calificacion
)
SELECT
	@IdPeriodoHistorico,
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	Constante,
	FactorATR,
	ATR,
	FactorVECES,
	Veces,
	FactorPorPago,
	PorPago,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion
FROM dbo.RW_RTH_Reservas
WHERE IdReporteLog = @IdReporteLog;
GO

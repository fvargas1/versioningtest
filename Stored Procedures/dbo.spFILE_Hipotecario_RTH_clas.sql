SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_RTH_clas]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Hipotecario_RTH_clas';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Hipotecario
SET errorCatalogo = 1
WHERE ISNULL(EsProductoInfonavit,'') = '2'
 AND CAST(ISNULL(Subcvi,'0') AS DECIMAL(23,2)) < (
 CAST(ISNULL(SaldoCapitalVigente,'0') AS DECIMAL(23,2))
 +CAST(ISNULL(SaldoInteresVigente,'0') AS DECIMAL(23,2))
 +CAST(ISNULL(SaldoCapitalVencido,'0') AS DECIMAL(23,2))
 +CAST(ISNULL(SaldoInteresVencido,'0') AS DECIMAL(23,2)))
 AND ISNULL(errorCatalogo,0) <> 1 AND ISNULL(errorFormato,0) <> 1;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoCredito,
 'EsProductoInfonavit',
 '[SaldoTotal=' + CAST(CAST(ISNULL(SaldoCapitalVigente,'0') AS DECIMAL(23,2))
 +CAST(ISNULL(SaldoInteresVigente,'0') AS DECIMAL(23,2))
 +CAST(ISNULL(SaldoCapitalVencido,'0') AS DECIMAL(23,2))
 +CAST(ISNULL(SaldoInteresVencido,'0') AS DECIMAL(23,2)) AS VARCHAR) + '][Subcuenta=' + ISNULL(Subcvi,'0') + ']',
 2,
 @Detalle
FROM dbo.FILE_Hipotecario
WHERE ISNULL(EsProductoInfonavit,'') = '2'
 AND CAST(ISNULL(Subcvi,'0') AS DECIMAL(23,2)) < (
 CAST(ISNULL(SaldoCapitalVigente,'0') AS DECIMAL(23,2))
 +CAST(ISNULL(SaldoInteresVigente,'0') AS DECIMAL(23,2))
 +CAST(ISNULL(SaldoCapitalVencido,'0') AS DECIMAL(23,2))
 +CAST(ISNULL(SaldoInteresVencido,'0') AS DECIMAL(23,2)))
 AND ISNULL(errorFormato,0) <> 1;
GO

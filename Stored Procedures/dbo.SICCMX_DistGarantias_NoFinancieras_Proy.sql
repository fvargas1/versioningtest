SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_NoFinancieras_Proy]
AS
-- GARANTIAS REALES NO FINANCIERAS (CREDITO) CON VALORES PROYECTADOS
UPDATE can
SET
 C = vw.ValorGarantiaProyectado,
 PrctCobSinAju = CASE WHEN cre.ExposicionIncumplimiento > 0 THEN CAST(vw.ValorGarantiaProyectado / cre.ExposicionIncumplimiento AS DECIMAL(18,8)) ELSE 0 END,
 PrctCobAjust = CASE WHEN cre.ExposicionIncumplimiento > 0 THEN dbo.MIN2VAR(1, (CAST(vw.ValorGarantiaProyectado / cre.ExposicionIncumplimiento AS DECIMAL(18,10)) / (vw.NivelMaximo / 100))) ELSE 0 END,
 MontoCobAjust = dbo.MIN2VAR(cre.ExposicionIncumplimiento, (vw.ValorGarantiaProyectado / (vw.NivelMaximo / 100)))
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_CreditoInfo cre ON can.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_VW_Garantias_RNF vw ON can.IdCredito = vw.IdCredito AND vw.IdTipoGarantia = can.IdTipoGarantia
INNER JOIN dbo.SICC_TipoGarantia tg ON vw.IdTipoGarantia = tg.IdTipoGarantia
WHERE tg.Codigo IN ('EYM-01','EYM-02','EYM-03');
GO

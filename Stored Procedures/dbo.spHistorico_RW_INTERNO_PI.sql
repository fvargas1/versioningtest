SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_INTERNO_PI]
 @IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_INTERNO_PI);

DELETE FROM Historico.RW_INTERNO_PI WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_INTERNO_PI (
 IdPeriodoHistorico, Periodo, Codigo, Nombre, Metodologia, Clasificacion, FactorCuantitativo, FactorCualitativo,
 Factor_Alpha, Factor_1mAlpha, PonderadoCuantitativo, PonderadoCualitativo, FactorTotal, [PI]
)
SELECT
 @IdPeriodoHistorico,
 Periodo,
 Codigo,
 Nombre,
 Metodologia,
 Clasificacion,
 FactorCuantitativo,
 FactorCualitativo,
 Factor_Alpha,
 Factor_1mAlpha,
 PonderadoCuantitativo,
 PonderadoCualitativo,
 FactorTotal,
 [PI]
FROM dbo.RW_INTERNO_PI
WHERE IdReporteLog = @IdReporteLog;
GO

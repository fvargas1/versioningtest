SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCaseGroup_List]
AS
SELECT IdUseCaseGroup, Active, Name, IdApplication, SortOrder, StringKey, Description
FROM dbo.BAJAWARE_UseCaseGroup;
GO

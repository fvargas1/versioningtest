SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_Garantia_Hipotecario_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_Garantia_Hipotecario_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_Garantia_Hipotecario_Hst (
 IdPeriodoHistorico,
 CodigoGarantia,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 Descripcion,
 PIGarante,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoGarantia,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 Descripcion,
 PIGarante,
 Fuente
FROM dbo.FILE_Garantia_Hipotecario_Hst;
GO

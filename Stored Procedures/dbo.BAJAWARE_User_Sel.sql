SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_User_Sel]
	@IdUser BAJAWARE_utID
AS
SELECT
IdUser, Active, Description, Blocked, [Name], FechaNacimiento, Email, Telefono, Mobil, NumeroControl, Username, Password, UsuarioWindows, UltimaFechaLogin, NeverLogedIn
FROM dbo.BAJAWARE_User
WHERE IdUser = @IdUser;
GO

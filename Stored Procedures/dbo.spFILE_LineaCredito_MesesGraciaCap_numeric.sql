SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_LineaCredito_MesesGraciaCap_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_LineaCredito_MesesGraciaCap_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_LineaCredito
SET errorFormato = 1
WHERE LEN(MesesGraciaCap)>0 AND (ISNUMERIC(ISNULL(MesesGraciaCap,'0')) = 0 OR MesesGraciaCap LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_LineaCredito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 NumeroLinea,
 'MesesGraciaCap',
 MesesGraciaCap,
 1,
 @Detalle
FROM dbo.FILE_LineaCredito
WHERE LEN(MesesGraciaCap)>0 AND (ISNUMERIC(ISNULL(MesesGraciaCap,'0')) = 0 OR MesesGraciaCap LIKE '%[^0-9 .]%');
GO

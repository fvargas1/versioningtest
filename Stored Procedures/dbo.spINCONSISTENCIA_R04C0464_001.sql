SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_001]
AS

BEGIN

-- El CURP debe estar relacionado con el acreditado que se reporta.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	NumeroDisposicion
FROM dbo.RW_VW_R04C0464_INC
WHERE LEN(ISNULL(NumeroDisposicion,'')) = 0;

END

GO

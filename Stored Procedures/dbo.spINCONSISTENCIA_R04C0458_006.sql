SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_006]
AS
BEGIN
-- Si el Tipo de Cartera (cve_tipo_cartera) es <> 299 y 270, entonces el RFC (dat_rfc) debe iniciar con guion bajo "_".

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoCartera,
	RFC
FROM dbo.RW_VW_R04C0458_INC
WHERE ISNULL(TipoCartera,'') NOT IN ('299','270') AND SUBSTRING(ISNULL(RFC,''),1,1) <> '_';

END
GO

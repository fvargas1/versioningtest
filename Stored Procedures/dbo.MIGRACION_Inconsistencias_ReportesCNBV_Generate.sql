SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Inconsistencias_ReportesCNBV_Generate]
AS
DECLARE @IdInconsistencia BIGINT;
DECLARE @CodenameCount VARCHAR(500);

DECLARE CursorInconsistencia CURSOR FOR
SELECT IdInconsistencia, CodenameCount
FROM dbo.MIGRACION_VW_Inconsistencia
WHERE Activo = 1
ORDER BY CategoriaNombre, Nombre;

OPEN CursorInconsistencia;

FETCH NEXT FROM CursorInconsistencia INTO @IdInconsistencia, @CodenameCount;
WHILE @@FETCH_STATUS = 0
BEGIN
EXEC @CodenameCount @IdInconsistencia;

FETCH NEXT FROM CursorInconsistencia INTO @IdInconsistencia, @CodenameCount;
END

CLOSE CursorInconsistencia;
DEALLOCATE CursorInconsistencia;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0442_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(10);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0442';
SELECT @Entidad = [Value] FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_R04C0442;

INSERT INTO dbo.RW_R04C0442 (
 IdReporteLog, Entidad, Formulario, NumeroSecuencia, CodigoPersona, RFC, NombrePersona, GrupoRiesgo, TipoAcreditadoRel, PersonalidadJuridica, SectorEconomico,
 ActividadEconomica, NumeroEmpleados, IngresosBrutos, Localidad, NumeroConsulta, TipoAltaCredito, SpecsDisposicionCredito, CodigoCredito,
 CodigoCreditoCNBV, CodigoAgrupacion, DestinoCredito, SaldoCredito, DenominacionCredito, FechaVencimiento, PeriodicidadPagosCapital,
 PeriodicidadPagosInteres, TasaRef, AjusteTasaRef, FrecuenciaRevisionTasa, MontoFondeo, CodigoBancoFondeador, Comisiones, PorcentajeCubierto,
 PorcentajeCubiertoFondeo, CodigoBancoGarantia, PorcentajeCubiertoAval, TipoGarantia, Municipio, Estado
)
SELECT DISTINCT
 @IdReporteLog,
 @Entidad,
 '0442',
 DENSE_RANK() OVER ( ORDER BY lin.Codigo ASC ),
 per.Codigo AS CodigoPersona,
 per.RFC AS RFC,
 REPLACE(pInfo.NombreCNBV,',','') AS NombrePersona,
 CASE WHEN LEN(ISNULL(pInfo.GrupoRiesgo,'')) = 0 OR pInfo.GrupoRiesgo LIKE '%SIN GRUPO%' THEN REPLACE(pInfo.NombreCNBV, ',', '') ELSE pInfo.GrupoRiesgo END AS GrupoRiesgo,
 deud.CodigoCNBV AS TipoAcreditadoRel,
 tpoPer.CodigoCNBV AS PersonalidadJuridica,
 sect.Codigo AS SectorEconomico,
 actEco.CodigoCNBV AS ActividadEconomica,
 pInfo.NumeroEmpleados AS NumeroEmpleados,
 pInfo.IngresosBrutos AS IngresosBrutos,
 mun.CodigoCNBV AS Localidad,
 NULLIF(lin.FolioConsultaBuro,'') AS NumeroConsulta,
 tpoAltaMA.CodigoCNBV AS TipoAltaCredito,
 dspCreMA.CodigoCNBV AS SpecsDisposicionCredito,
 lin.Codigo AS CodigoCredito,
 cnbv.CNBV AS CodigoCreditoCNBV,
 NULLIF(lin.NumeroAgrupacion,'') AS CodigoAgrupacion,
 dest.CodigoCNBV AS DestinoCredito,
 lin.MontoLinea AS SaldoCredito,
 mon.CodigoCNBV AS DenominacionCredito,
 CASE WHEN lin.FecVenLinea IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,lin.FecVenLinea,102),'.',''),1,6) END AS FechaVencimiento,
 perCap.CodigoCNBV AS PeriodicidadPagosCapital,
 perInt.CodigoCNBV AS PeriodicidadPagosInteres,
 tasa.Codigo AS TasaRef,
 NULLIF(lin.AjusteTasaReferencia,'') AS AjusteTasaRef,
 lin.FrecuenciaRevisionTasa AS FrecuenciaRevisionTasa,
 lin.MontoBancaDesarrollo AS MontoFondeo,
 fond.Codigo AS CodigoBancoFondeador,
 lin.ComisionesCobradas AS Comisiones,
 gar.PorcentajeCubierto * 100 AS PorcentajeCubierto,
 gar.PorcentajeFondo * 100 AS PorcentajeCubiertoFondeo,
 gar.InstitucionFondo AS CodigoBancoGarantia,
 gar.PorcentajeAval * 100 AS PorcentajeCubiertoAval,
 gar.TipoGarReal AS TipoGarantia,
 NULLIF(pInfo.CodigoPostal,'') AS Municipio,
 NULLIF(pInfo.Estado,'') AS Estado
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_LineaCredito lin ON pInfo.IdPersona = lin.IdPersona
LEFT OUTER JOIN dbo.SICC_DeudorRelacionadoMA deud ON per.IdDeudorRelacionadoMA = deud.IdDeudorRelacionado
LEFT OUTER JOIN dbo.SICC_TipoPersonaMA tpoPer ON pInfo.IdTipoPersonaMA = tpoPer.IdTipoPersona
LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor sect ON pInfo.IdSectorEconomico = sect.IdSectorEconomico
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEco ON per.IdActividadEconomica = actEco.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_Localidad2015 mun ON per.IdLocalidad = mun.IdLocalidad
LEFT OUTER JOIN dbo.SICC_TipoAltaMA tpoAltaMA ON lin.IdTipoAltaMA = tpoAltaMA.IdTipoAlta
LEFT OUTER JOIN dbo.SICC_DisposicionCreditoMA dspCreMA ON lin.IdDisposicionMA = dspCreMA.IdDisposicion
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCreditosCNBV cnbv ON lin.Codigo = cnbv.NumeroLinea
LEFT OUTER JOIN dbo.SICC_DestinoCreditoMA dest ON lin.IdDestinoCreditoMA = dest.IdDestinoCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON lin.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalMA perCap ON lin.IdPeriodicidadCapitalMA = perCap.IdPeriodicidadCapital
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteresMA perInt ON lin.IdPeriodicidadInteresMA = perInt.IdPeriodicidadInteres
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasa ON lin.IdTasaReferencia = tasa.IdTasaReferencia
LEFT OUTER JOIN dbo.SICC_Institucion fond ON lin.IdInstitucionFondea = fond.IdInstitucion
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_0442 gar ON lin.IdLineaCredito = gar.IdLineaCredito
WHERE lin.IdTipoAltaMA IS NOT NULL AND lin.IdTipoBajaMA IS NULL;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0442 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ValidaInfoAnx22_GP]
AS
DECLARE @udis_mayor MONEY;
DECLARE @IdMoneda BIGINT;
DECLARE @IdPeriodo BIGINT;
DECLARE @TipoCambioUdis MONEY;
DECLARE @monto_udis_mayor MONEY;

SELECT @udis_mayor = CONVERT(MONEY,Value) FROM BAJAWARE_Config where CodeName='A22_UDIS_MAYOR';
IF @udis_mayor IS NULL SET @udis_mayor = 216000000

SELECT @IdMoneda = IdMoneda FROM SICC_Moneda WHERE Codigo = (SELECT VALUE FROM BAJAWARE_Config WHERE Codename='MonedaUdis');
SELECT @IdPeriodo = IdPeriodo FROM SICC_Periodo WHERE Activo=1;
SELECT @TipoCambioUdis = Valor FROM SICC_TipoCambio WHERE IdPeriodo = @IdPeriodo AND IdMoneda = @IdMoneda;

SET @monto_udis_mayor = @udis_mayor * @TipoCambioUdis;


UPDATE piDet
SET PuntosActual = 57
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_Anexo22_GP anx ON ppi.IdGP = anx.IdGP AND ppi.EsGarante = anx.EsGarante
INNER JOIN dbo.SICCMX_Persona_PI_Detalles_GP piDet ON anx.IdGP = piDet.IdGP AND anx.EsGarante = piDet.EsGarante
INNER JOIN dbo.SICCMX_PI_Variables variab ON piDet.IdVariable = variab.Id AND variab.Codigo='22_NUM_INST_REP'
WHERE anx.VentNetTotAnuales >= @monto_udis_mayor AND anx.NumInstRepUlt12Meses >= 12;


INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT ppi.IdGP, ppi.EsGarante, GETDATE(), '', 'Se actualiza puntaje de ''' + variab.Nombre + ''' a ''57'', ya que sus ingresos anuales son mayores a ' + CONVERT(VARCHAR, @udis_mayor, 1) + ' UDIS y cuenta con mas de 12 instituciones reportadas'
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_Anexo22_GP anx ON ppi.IdGP = anx.IdGP AND ppi.EsGarante = anx.EsGarante
INNER JOIN dbo.SICCMX_Persona_PI_Detalles_GP piDet ON anx.IdGP = piDet.IdGP AND anx.EsGarante = piDet.EsGarante
INNER JOIN dbo.SICCMX_PI_Variables variab ON piDet.IdVariable = variab.Id AND variab.Codigo='22_NUM_INST_REP'
WHERE anx.VentNetTotAnuales >= @monto_udis_mayor AND anx.NumInstRepUlt12Meses >= 12;


UPDATE piDet
SET PuntosActual = 54
FROM SICCMX_Anexo20_GP anx
INNER JOIN SICCMX_Persona_PI_GP ppi ON ppi.IdGP = anx.IdGP AND ppi.EsGarante = anx.EsGarante
INNER JOIN dbo.SICCMX_Persona_PI_Detalles_GP piDet ON anx.IdGP = piDet.IdGP AND anx.EsGarante = piDet.EsGarante
INNER JOIN dbo.SICCMX_PI_Variables variab ON piDet.IdVariable = variab.Id AND variab.Codigo='22_REND_CAP_ROE'
WHERE ISNULL(anx.UtilNetaTrimestre,0) < 0 AND ISNULL(anx.CapitalContableProm,0) < 0;


INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT ppi.IdGP, ppi.EsGarante, GETDATE(), '', 'Se actualiza puntaje de ''' + variab.Nombre + ''' a ''54'', ya que la "Utilidad Neta Trimestral" y el "Capital Contable Promedio" son menores a 0'
FROM SICCMX_Anexo20_GP anx
INNER JOIN SICCMX_Persona_PI_GP ppi ON ppi.IdGP = anx.IdGP AND ppi.EsGarante = anx.EsGarante
INNER JOIN dbo.SICCMX_Persona_PI_Detalles_GP piDet ON anx.IdGP = piDet.IdGP AND anx.EsGarante = piDet.EsGarante
INNER JOIN dbo.SICCMX_PI_Variables variab ON piDet.IdVariable = variab.Id AND variab.Codigo='22_REND_CAP_ROE'
WHERE ISNULL(anx.UtilNetaTrimestre,0) < 0 AND ISNULL(anx.CapitalContableProm,0) < 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Prepara_Datos_A18]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo = '18';

INSERT INTO dbo.SICCMX_Persona_PI (IdPersona, IdMetodologia, IdClasificacion)
SELECT DISTINCT cr.IdPersona, cr.IdMetodologia, NULL
FROM dbo.SICCMX_Credito cr
WHERE cr.IdMetodologia=@IdMetodologia;
GO

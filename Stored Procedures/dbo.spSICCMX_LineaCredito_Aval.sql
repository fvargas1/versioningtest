SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_LineaCredito_Aval]
	@IdPersona BIGINT
AS
SELECT
 0 AS IdAval,
 '' AS NumeroLinea,
 '' AS Nombre,
 0 AS PI,
 0 AS Monto,
 0 AS Porcentaje,
 0 AS Reserva
FROM dbo.BAJAWARE_Config
WHERE 1=2
/*
SELECT
 aval.IdAval,
 credito.NumeroLinea,
 aval.nombre AS Nombre,
 ISNULL(avalCal.PI,0) AS PI,
 SUM(ISNULL(crAvalCal.Monto,0)) AS Monto,
 CASE WHEN resVar.Exposicion > 0 THEN SUM(ISNULL(crAvalCal.Monto,0)) / resVar.Exposicion ELSE 0 END AS Porcentaje,
 SUM(ISNULL(crAvalCal.Reserva,0)) AS Reserva
FROM dbo.SICCMX_CreditoAval crAval
INNER JOIN dbo.SICCMX_Credito credito ON crAval.IdCredito = credito.IdCredito
INNER JOIN dbo.SICCMX_Aval aval ON crAval.IdAval = aval.IdAval
INNER JOIN dbo.SICCMX_AvalCalificacion avalCal ON crAval.IdAval = avalCal.IdAval
INNER JOIN dbo.SICCMX_CreditoAvalCalificacion crAvalCal ON crAval.IdAval = crAvalCal.IdAval AND crAval.IdCredito = crAvalCal.IdCredito
INNER JOIN dbo.SICCMX_LineaCredito_Reservas_Variables resVar ON credito.IdLineaCredito = resVar.IdLineaCredito
WHERE credito.IdPersona = @IdPersona
GROUP BY aval.IdAval, credito.NumeroLinea, aval.nombre, avalCal.PI, resVar.Exposicion
ORDER BY credito.NumeroLinea
*/
GO

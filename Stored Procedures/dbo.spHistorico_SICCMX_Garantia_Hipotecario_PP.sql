SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Garantia_Hipotecario_PP]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Garantia_Hipotecario_PP WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Garantia_Hipotecario_PP (
	IdPeriodoHistorico,
	Garantia,
	MontoCubierto,
	MontoExpuesto,
	PIGarante,
	SPGarante,
	ReservaCubierto,
	ReservaExpuesto,
	ReservaFinal,
	PrctCubierto
)
SELECT
	@IdPeriodoHistorico,
	ga.Codigo,
	pp.MontoCubierto,
	pp.MontoExpuesto,
	pp.PIGarante,
	pp.SPGarante,
	pp.ReservaCubierto,
	pp.ReservaExpuesto,
	pp.ReservaFinal,
	pp.PrctCubierto
FROM dbo.SICCMX_Garantia_Hipotecario_PP pp
INNER JOIN dbo.SICCMX_Garantia_Hipotecario ga ON pp.IdGarantia = ga.IdGarantia;
GO

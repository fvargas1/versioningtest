SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Hipotecario_Migracion]
AS
UPDATE gar
SET
	IdTipoGarantia = tg.IdTipoGarantia,
	ValorGarantia = NULLIF(f.ValorGarantia,''),
	IdMoneda = mon.IdMoneda,
	Descripcion = LTRIM(RTRIM(f.Descripcion)),
	PIGarante = NULLIF(f.PIGarante,'')
FROM dbo.SICCMX_Garantia_Hipotecario gar
INNER JOIN dbo.FILE_Garantia_Hipotecario f ON gar.Codigo = LTRIM(f.CodigoGarantia)
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON tg.Codigo = LTRIM(f.TipoGarantia)
LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.Codigo = LTRIM(f.Moneda)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_Garantia_Hipotecario (
	Codigo,
	IdTipoGarantia,
	ValorGarantia,
	IdMoneda,
	Descripcion,
	PIGarante
)
SELECT
	LTRIM(RTRIM(f.CodigoGarantia)),
	tg.IdTipoGarantia,
	NULLIF(f.ValorGarantia,''),
	mon.IdMoneda,
	LTRIM(RTRIM(f.Descripcion)),
	NULLIF(f.PIGarante,'')
FROM dbo.FILE_Garantia_Hipotecario f
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON tg.Codigo = LTRIM(f.TipoGarantia)
LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.Codigo = LTRIM(f.Moneda)
LEFT OUTER JOIN dbo.SICCMX_Garantia_Hipotecario gar ON gar.Codigo = LTRIM(f.CodigoGarantia)
WHERE gar.Codigo IS NULL AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO

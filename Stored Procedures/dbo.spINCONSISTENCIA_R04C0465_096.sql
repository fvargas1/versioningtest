SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_096]
AS

BEGIN

-- Validar que el Puntaje Crediticio Cuantitativo (dat_ptaje_credit_cuantit) sea >= 204 pero <=913

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PuntajeCuantitativo
FROM dbo.RW_VW_R04C0465_INC
WHERE CAST(ISNULL(NULLIF(PuntajeCuantitativo,''),'-1') AS DECIMAL) NOT BETWEEN 204 AND 913;

END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_GENERATE_ArchivosReportes]
	@Grupo VARCHAR(50),
	@Nombre VARCHAR(50)
AS
DECLARE @qry VARCHAR(3000);
DECLARE @qry2 VARCHAR(3000);
DECLARE @sp_get VARCHAR(50);
DECLARE @nomArchivo VARCHAR(50);
DECLARE @db_server VARCHAR(50);
DECLARE @ruta_reportes VARCHAR(200);
DECLARE @folder VARCHAR(50);
DECLARE @ruta_csv VARCHAR(200);
DECLARE @ruta_txt VARCHAR(200);

SELECT @db_server = [Value] FROM dbo.BAJAWARE_Config WHERE CodeName = 'DB_SERVER';
SELECT @ruta_reportes = [Value] FROM dbo.BAJAWARE_Config WHERE CodeName = 'RUTA_REPORTES';

DECLARE crs CURSOR FOR
SELECT SProcGet, NombreTabla
FROM dbo.RW_Reporte
WHERE (NULLIF(@Grupo,'') IS NULL OR GrupoReporte = @Grupo) AND (NULLIF(@Nombre,'') IS NULL OR Nombre LIKE @Nombre + '%') AND Activo = 1;

OPEN crs;

FETCH NEXT FROM crs INTO @sp_get, @nomArchivo;

EXEC sp_configure 'show advanced options', 1;
RECONFIGURE;
EXEC sp_configure 'xp_cmdshell', 1;
RECONFIGURE;

SET @folder = SUBSTRING(REPLACE(CONVERT(VARCHAR,GETDATE(),102),'.',''),1,8) + '_' + REPLACE(CONVERT(VARCHAR, GETDATE(), 108),':','');;
SET @ruta_csv = @ruta_reportes + @folder + '\' + 'CSV\';
SET @ruta_txt = @ruta_reportes + @folder + '\' + 'TXT\';
EXEC master.dbo.xp_create_subdir @ruta_csv;
EXEC master.dbo.xp_create_subdir @ruta_txt;

WHILE @@FETCH_STATUS = 0
BEGIN

-- CSV
SET @qry = 'bcp "EXEC ' + DB_NAME() + '.dbo.' + @sp_get + ' 0" queryout ' + @ruta_csv + @nomArchivo + '.csv -c -t, -S ' + @db_server + ' -T';

-- TXT
SET @qry2 = 'bcp "EXEC ' + DB_NAME() + '.dbo.' + @sp_get + ' 0" queryout ' + @ruta_txt + @nomArchivo + '.txt -c -t\; -S ' + @db_server + ' -T';

EXEC master..xp_cmdshell @qry;
EXEC master..xp_cmdshell @qry2;

FETCH NEXT FROM crs INTO @sp_get, @nomArchivo;

END

EXEC sp_configure 'xp_cmdshell', 0;
RECONFIGURE;
EXEC sp_configure 'show advanced options', 0;
RECONFIGURE;

CLOSE crs;
DEALLOCATE crs;
GO

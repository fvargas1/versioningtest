SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Consumo_Periodicidad_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Consumo_Periodicidad_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Consumo f
LEFT OUTER JOIN dbo.SICC_PeriodicidadCalificacionConsumo rst ON LTRIM(f.Periodicidad) = rst.Codigo
WHERE rst.IdPeriodicidadCalificacionConsumo IS NULL AND LEN(f.Periodicidad) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCredito,
 'Periodicidad',
 f.Periodicidad,
 2,
 @Detalle
FROM dbo.FILE_Consumo f
LEFT OUTER JOIN dbo.SICC_PeriodicidadCalificacionConsumo rst ON LTRIM(f.Periodicidad) = rst.Codigo
WHERE rst.IdPeriodicidadCalificacionConsumo IS NULL AND LEN(f.Periodicidad) > 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_NoFinancieras_Consumo]
AS
-- GARANTIAS REALES NO FINANCIERAS (CREDITO)
UPDATE can
SET
 C = vw.ValorGarantia,
 PrctCobSinAju = CASE WHEN crv.E > 0 THEN CAST(vw.ValorGarantia / crv.E AS DECIMAL(18,8)) ELSE 0 END,
 PrctCobAjust = CASE WHEN crv.E > 0 THEN dbo.MIN2VAR(1, (CAST(vw.ValorGarantia / crv.E AS DECIMAL(18,10)) / (vw.NivelMaximo / 100))) ELSE 0 END,
 MontoCobAjust = dbo.MIN2VAR(crv.E, (vw.ValorGarantia / (vw.NivelMaximo / 100))),
 [PI] = crv.[PI]
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON can.IdConsumo = crv.IdConsumo
INNER JOIN dbo.SICCMX_VW_Garantias_RNF_Consumo vw ON can.IdConsumo = vw.IdConsumo AND vw.IdTipoGarantia=can.IdTipoGarantia;
GO

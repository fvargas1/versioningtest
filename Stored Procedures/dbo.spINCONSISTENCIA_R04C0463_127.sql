SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_127]
AS

BEGIN

-- El Monto de las Primas Anuales de todos los Seguros obligatorios que la Institución cobra al Acreditado (dat_monto_seguros_obligat),
-- debe ser menor o igual al Monto de la Línea de Crédito Autorizado (dat_monto_linea_credito_autori).

SELECT
	CodigoCredito,
	MontoPrimasAnuales,
	MonLineaCred
FROM dbo.RW_VW_R04C0463_INC
WHERE ISNULL(NULLIF(MontoPrimasAnuales,''),0) > ISNULL(NULLIF(MonLineaCred,''),0)

END

GO

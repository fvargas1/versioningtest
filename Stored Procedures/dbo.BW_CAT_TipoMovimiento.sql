SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_TipoMovimiento]
	@filtro VARCHAR(50)
AS
SELECT DISTINCT
	cat.Codigo,
	cat.Descripcion
FROM dbo.SICC_TipoMovimiento cat
WHERE ISNULL(cat.Codigo,'')+'|'+ISNULL(cat.Descripcion,'') LIKE '%' + @filtro + '%'
ORDER BY cat.Codigo;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Consumo_EsIPC_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_Consumo_EsIPC_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Garantia_Consumo
SET errorCatalogo = 1
WHERE LEN(EsIPC)>0 AND LTRIM(EsIPC) NOT IN ('0','1');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	CodigoGarantia,
	'EsIPC',
	EsIPC,
	2,
	@Detalle
FROM dbo.FILE_Garantia_Consumo
WHERE LEN(EsIPC)>0 AND LTRIM(EsIPC) NOT IN ('0','1');
GO

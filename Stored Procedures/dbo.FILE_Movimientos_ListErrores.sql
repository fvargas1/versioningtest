SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Movimientos_ListErrores]
AS
SELECT
 CodigoCredito,
 TipoMovimiento,
 Monto,
 Fecha,
 Fuente
FROM dbo.FILE_Movimientos
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO

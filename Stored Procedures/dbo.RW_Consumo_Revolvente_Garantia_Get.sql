SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_Revolvente_Garantia_Get]
	@IdReporteLog BIGINT
AS
SELECT
	FolioCredito,
	Garantia,
	GarantiaReal,
	ImporteGarantia,
	PI_Garante,
	SP,
	SP_SG,
	Hc,
	Hfx,
	PrctCobertura,
	Exposicion,
	EI_Ajustada,
	Reservas,
	ClaveGarante,
	RUGM,
	IdPortafolio
FROM dbo.RW_VW_Consumo_Revolvente_Garantia;
GO

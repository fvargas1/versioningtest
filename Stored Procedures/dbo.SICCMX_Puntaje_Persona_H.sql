SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Puntaje_Persona_H]
	@CodigoPersona VARCHAR(50),
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Persona,
	Factor,
	SubFactor,
	Campos,
	PuntosActual,
	FactorCuantitativo,
	FactorCualitativo,
	Alpha,
	MAlpha,
	PonderadoCuantitativo,
	PonderadoCualitativo,
	FactorTotal,
	[PI],
	ValorActual
FROM (
SELECT
	'VARIABLES DEL DEUDOR' AS Persona,
	fact.Nombre AS Factor,
	(sub.Codigo + ' ' + sub.Nombre) AS SubFactor,
	piVar.Descripcion AS Campos,
	CAST(piDet.PuntosActual AS DECIMAL) AS PuntosActual,
	ppi.FactorCuantitativo,
	ppi.FactorCualitativo,
	ppi.Alpha,
	ppi.MAlpha,
	ppi.PonderadoCuantitativo,
	ppi.PonderadoCualitativo,
	ppi.FactorTotal,
	ppi.[PI],
	1 AS Position,
	fact.Id AS FactorOrden,
	piVar.Orden VariableOrden,
	ISNULL(CASE WHEN rel.IdVariable IS NULL THEN CAST(CAST(piDet.ValorActual AS DECIMAL(27,6)) AS VARCHAR) ELSE CAST(CAST(piDet.ValorActual AS DECIMAL) AS VARCHAR) END, 'Sin Información') AS ValorActual
FROM dbo.SICCMX_PI_Variables piVar
INNER JOIN Historico.SICCMX_Persona_PI_Detalles piDet ON piVar.Codigo = piDet.Variable
INNER JOIN Historico.RW_CedulaNMC_Info ppi ON piDet.Persona = ppi.Codigo AND piDet.IdPeriodoHistorico = ppi.IdPeriodoHistorico
INNER JOIN dbo.SICCMX_PI_SubFactor sub ON piVar.IdSubFactor = sub.Id
INNER JOIN dbo.SICCMX_PI_Factores fact ON sub.IdFactor = fact.Id
LEFT OUTER JOIN dbo.SICCMX_Rel_Variable_Catalogo rel ON piVar.Id = rel.IdVariable
WHERE piDet.IdPeriodoHistorico=@IdPeriodoHistorico AND piDet.Persona=@CodigoPersona
UNION
SELECT
	'VARIABLES DEL AVAL : ' + rw.NombreAval + ' ( ' + rw.MetodologiaAval + ' )',
	fact.Nombre AS Factor,
	(sub.Codigo + ' ' + sub.Nombre) AS SubFactor,
	piVar.Descripcion AS Campos,
	CAST(piDet.PuntosActual AS DECIMAL) AS PuntosActual,
	rw.FactorCuantitativoAval,
	rw.FactorCualitativoAval,
	rw.AlphaAval,
	rw.MAlphaAval,
	rw.PonderadoCuantitativoAval,
	rw.PonderadoCualitativoAval,
	rw.FactorTotalAval,
	rw.PIAval,
	2 AS Position,
	fact.Id AS FactorOrden,
	piVar.Orden VariableOrden,
	ISNULL(CASE WHEN rel.IdVariable IS NULL THEN CAST(CAST(piDet.ValorActual AS DECIMAL(27,6)) AS VARCHAR) ELSE CAST(CAST(piDet.ValorActual AS DECIMAL) AS VARCHAR) END, 'Sin Información') AS ValorActual
FROM Historico.RW_CedulaNMC_Info rw
INNER JOIN Historico.SICCMX_Persona_PI_Detalles_GP piDet ON rw.CodigoAval = piDet.GP AND rw.IdPeriodoHistorico = piDet.IdPeriodoHistorico
INNER JOIN dbo.SICCMX_PI_Variables piVar ON piDet.Variable = piVar.Codigo
INNER JOIN dbo.SICCMX_PI_SubFactor sub ON piVar.IdSubFactor = sub.Id
INNER JOIN dbo.SICCMX_PI_Factores fact ON sub.IdFactor = fact.Id
LEFT OUTER JOIN dbo.SICCMX_Rel_Variable_Catalogo rel ON piVar.Id = rel.IdVariable
WHERE rw.IdPeriodoHistorico=@IdPeriodoHistorico AND rw.Codigo=@CodigoPersona
) AS tab
ORDER BY Position, Persona, FactorOrden, VariableOrden;
GO

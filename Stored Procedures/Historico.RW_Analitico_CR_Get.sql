SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Analitico_CR_Get]
 @IdPeriodoHistorico BIGINT
AS
SELECT
 CodigoPersona,
 CodigoCredito,
 SaldoTotal,
 MontoGarantia,
 [PI],
 SP,
 EI,
 Reserva,
 PrctReserva,
 GradoRiesgo,
 Zi,
 B0,
 B1,
 ACT,
 B2,
 HIST,
 B3,
 PrctUSO,
 B4,
 PrctPago,
 B5,
 Alto,
 B6,
 Medio,
 B7,
 Bajo,
 B8,
 GVeces1,
 B9,
 GVeces2,
 B10,
 GVeces3,
 B11,
 BKATR,
 LimiteCredito,
 Tipo_EX
FROM Historico.RW_Analitico_CR
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY CodigoPersona, CodigoCredito;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_030]
AS

BEGIN

-- La Periodicidad de Pagos Interes debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.PeriodicidadPagosInteres
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteresMA cat ON ISNULL(r.PeriodicidadPagosInteres,'') = cat.CodigoCNBV
WHERE cat.IdPeriodicidadInteres IS NULL;

END
GO

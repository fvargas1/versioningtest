SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Aval_TipoCobertura_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Aval_TipoCobertura_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET errorCatalogo = 1
FROM dbo.FILE_Aval f
LEFT OUTER JOIN dbo.SICC_TipoCobertura cat ON LTRIM(f.TipoCobertura) = cat.Codigo
WHERE cat.IdTipoCobertura IS NULL AND LEN(f.TipoCobertura)>0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Aval_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoAval,
 'TipoCobertura',
 f.TipoCobertura,
 2,
 @Detalle
FROM dbo.FILE_Aval f
LEFT OUTER JOIN dbo.SICC_TipoCobertura cat ON LTRIM(f.TipoCobertura) = cat.Codigo
WHERE cat.IdTipoCobertura IS NULL AND LEN(f.TipoCobertura)>0;
GO

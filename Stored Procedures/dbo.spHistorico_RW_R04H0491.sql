SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04H0491]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04H0491);

DELETE FROM Historico.RW_R04H0491 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04H0491 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, ProductoHipotecario, CategoriaCredito, TipoAlta,
	DestinoCredito, FechaOtorgamiento, FechaVencimiento, DenominacionCredito, MontoOriginal, Comisiones, MontoSubsidioFederal, EntidadCoFin, MontoSubCuenta,
	MontoOtorgadoCoFin, Apoyo, ValorOriginalVivienda, ValorAvaluo, NumeroAvaluo, Localidad, FechaFirmaReestructura, FechaVencimientoReestructura, MontoReestructura,
	DenominacionCreditoReestructura, IngresosMensuales, TipoComprobacionIngresos, SectorLaboral, NumeroConsultaSIC, PeriodicidadAmortizaciones, TipoTasaInteres,
	TasaRef, AjusteTasaRef, SeguroAcreditado, TipoSeguro, EntidadSeguro, PorcentajeCubiertoSeguro, MontoSubCuentaGarantia, INTEXP, SDES, Municipio, Estado
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	ProductoHipotecario,
	CategoriaCredito,
	TipoAlta,
	DestinoCredito,
	FechaOtorgamiento,
	FechaVencimiento,
	DenominacionCredito,
	MontoOriginal,
	Comisiones,
	MontoSubsidioFederal,
	EntidadCoFin,
	MontoSubCuenta,
	MontoOtorgadoCoFin,
	Apoyo,
	ValorOriginalVivienda,
	ValorAvaluo,
	NumeroAvaluo,
	Localidad,
	FechaFirmaReestructura,
	FechaVencimientoReestructura,
	MontoReestructura,
	DenominacionCreditoReestructura,
	IngresosMensuales,
	TipoComprobacionIngresos,
	SectorLaboral,
	NumeroConsultaSIC,
	PeriodicidadAmortizaciones,
	TipoTasaInteres,
	TasaRef,
	AjusteTasaRef,
	SeguroAcreditado,
	TipoSeguro,
	EntidadSeguro,
	PorcentajeCubiertoSeguro,
	MontoSubCuentaGarantia,
	INTEXP,
	SDES,
	Municipio,
	Estado
FROM dbo.RW_R04H0491
WHERE IdReporteLog = @IdReporteLog;
GO

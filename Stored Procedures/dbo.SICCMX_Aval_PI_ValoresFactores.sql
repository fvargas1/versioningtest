SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Aval_PI_ValoresFactores]
 @IdPersona BIGINT,
 @Factor VARCHAR(20)
AS
IF @Factor = 'FAC_CUANTI'
BEGIN
-- INFORMACION DE AVALES
SELECT DISTINCT
 per.IdGP AS IdPersona,
 variables.Nombre,
 CASE WHEN cat.Nombre IS NULL THEN CASE WHEN per.PuntosActual IS NULL THEN '' ELSE CASE WHEN per.ValorActual=9999 THEN 'Sin Pagos' ELSE
 ISNULL(CAST(CAST(per.ValorActual AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END END ELSE 
 cat.Codigo + '. ' +CASE WHEN LEN(cat.Nombre) > 50 THEN SUBSTRING(cat.Nombre,1,46) + ' ...' ELSE cat.Nombre END END AS ValorActual,
 CAST(per.PuntosActual AS INT) AS PuntosActual,
 CASE WHEN catH1.Nombre IS NULL THEN CASE WHEN per.PuntosH1 IS NULL THEN '' ELSE CASE WHEN per.ValorH1=9999 THEN 'Sin Pagos' ELSE
 ISNULL(CAST(CAST(per.ValorH1 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END END ELSE 
 catH1.Codigo + '. ' +CASE WHEN LEN(catH1.Nombre) > 50 THEN SUBSTRING(catH1.Nombre,1,46) + ' ...' ELSE catH1.Nombre END END AS ValorH1,
 CAST(per.PuntosH1 AS INT) AS PuntosH1,
 CASE WHEN catH2.Nombre IS NULL THEN CASE WHEN per.PuntosH2 IS NULL THEN '' ELSE CASE WHEN per.ValorH2=9999 THEN 'Sin Pagos' ELSE
 ISNULL(CAST(CAST(per.ValorH2 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END END ELSE 
 catH2.Codigo + '. ' +CASE WHEN LEN(catH2.Nombre) > 50 THEN SUBSTRING(catH2.Nombre,1,46) + ' ...' ELSE catH2.Nombre END END AS ValorH2,
 CAST(per.PuntosH2 AS INT) AS PuntosH2,
 CASE WHEN catH3.Nombre IS NULL THEN CASE WHEN per.PuntosH3 IS NULL THEN '' ELSE CASE WHEN per.ValorH3=9999 THEN 'Sin Pagos' ELSE
 ISNULL(CAST(CAST(per.ValorH3 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END END ELSE 
 catH3.Codigo + '. ' +CASE WHEN LEN(catH3.Nombre) > 50 THEN SUBSTRING(catH3.Nombre,1,46) + ' ...' ELSE catH3.Nombre END END AS ValorH3,
 CAST(per.PuntosH3 AS INT) AS PuntosH3,
 per.FechaCalculo,
 per.UsuarioCalculo,
 CASE WHEN cat.Nombre IS NULL THEN CASE WHEN per.ValorActual=9999 THEN 'Sin Pagos' ELSE ISNULL(CAST(CAST(per.ValorActual AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END
 ELSE cat.Codigo + '. ' + cat.Nombre END AS Descripcion,
 aval.Codigo AS CodigoAval,
 aval.Nombre AS NombreAval,
 aval.IdAval AS IdAval,
 per.EsGarante
FROM dbo.SICCMX_Persona_PI_Detalles_GP per
INNER JOIN dbo.SICCMX_PI_Variables variables ON variables.Id = per.IdVariable
INNER JOIN dbo.SICCMX_PI_SubFactor subfac ON subfac.Id = variables.IdSubFactor
INNER JOIN dbo.SICCMX_PI_Factores fac ON fac.Id = subfac.IdFactor
INNER JOIN dbo.SICC_EsGarante esg ON per.EsGarante = esg.IdEsGarante
INNER JOIN dbo.SICCMX_Aval aval ON per.IdGP = aval.IdAval
INNER JOIN dbo.SICCMX_CreditoAval ca ON aval.IdAval = ca.IdAval AND ca.Aplica = 1
INNER JOIN dbo.SICCMX_Credito cre ON ca.IdCredito = cre.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Rel_Variable_Catalogo rel ON per.IdVariable = rel.IdVariable
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos cat ON rel.IdCatalogo = cat.IdCatalogo AND CAST(CAST(per.ValorActual AS INT) AS VARCHAR) = cat.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH1 ON rel.IdCatalogo = catH1.IdCatalogo AND CAST(CAST(per.ValorH1 AS INT) AS VARCHAR) = catH1.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH2 ON rel.IdCatalogo = catH2.IdCatalogo AND CAST(CAST(per.ValorH2 AS INT) AS VARCHAR) = catH2.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH3 ON rel.IdCatalogo = catH3.IdCatalogo AND CAST(CAST(per.ValorH3 AS INT) AS VARCHAR) = catH3.Codigo
WHERE fac.Codigo = @Factor AND cre.IdPersona = @IdPersona AND esg.Layout = 'AVAL'

UNION ALL 

SELECT TOP 1
 per.IdGP AS IdPersona,
 'Se suman 90 puntos al Factor Cuantitativo' AS Nombre,
 '' AS ValorActual,
 '90' AS PuntosActual,
 '' AS ValorH1,
 CASE WHEN per.PuntosH1 IS NULL THEN NULL ELSE 90 END,
 '' AS ValorH2,
 CASE WHEN per.PuntosH2 IS NULL THEN NULL ELSE 90 END,
 '' AS ValorH3,
 CASE WHEN per.PuntosH3 IS NULL THEN NULL ELSE 90 END,
 '',
 '',
 '',
 aval.Codigo AS CodigoAval,
 aval.Nombre AS NombreAval,
 per.IdGP AS IdAval,
 per.EsGarante
FROM dbo.SICCMX_Persona_PI_Detalles_GP per
INNER JOIN dbo.SICCMX_PI_Variables variables ON variables.Id = per.IdVariable
INNER JOIN dbo.SICCMX_Anexo22_GP anx ON per.IdGP = anx.IdGP AND per.EsGarante = anx.EsGarante
INNER JOIN dbo.SICCMX_Aval aval ON per.IdGP = aval.IdAval
INNER JOIN dbo.SICCMX_CreditoAval ca ON aval.IdAval = ca.IdAval AND ca.Aplica = 1
INNER JOIN dbo.SICCMX_Credito cre ON ca.IdCredito = cre.IdCredito
WHERE cre.IdPersona = @IdPersona

UNION ALL

-- INFORMACION DE AVALES QUE SON ACREDITADOS
SELECT DISTINCT
 per.IdPersona,
 variables.Nombre,
 CASE WHEN cat.Nombre IS NULL THEN CASE WHEN per.PuntosActual IS NULL THEN '' ELSE CASE WHEN per.ValorActual=9999 THEN 'Sin Pagos' ELSE
 ISNULL(CAST(CAST(per.ValorActual AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END END ELSE 
 cat.Codigo + '. ' +CASE WHEN LEN(cat.Nombre) > 50 THEN SUBSTRING(cat.Nombre,1,46) + ' ...' ELSE cat.Nombre END END AS ValorActual,
 CAST(per.PuntosActual AS INT) AS PuntosActual,
 CASE WHEN catH1.Nombre IS NULL THEN CASE WHEN per.PuntosH1 IS NULL THEN '' ELSE CASE WHEN per.ValorH1=9999 THEN 'Sin Pagos' ELSE
 ISNULL(CAST(CAST(per.ValorH1 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END END ELSE 
 catH1.Codigo + '. ' +CASE WHEN LEN(catH1.Nombre) > 50 THEN SUBSTRING(catH1.Nombre,1,46) + ' ...' ELSE catH1.Nombre END END AS ValorH1,
 CAST(per.PuntosH1 AS INT) AS PuntosH1,
 CASE WHEN catH2.Nombre IS NULL THEN CASE WHEN per.PuntosH2 IS NULL THEN '' ELSE CASE WHEN per.ValorH2=9999 THEN 'Sin Pagos' ELSE
 ISNULL(CAST(CAST(per.ValorH2 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END END ELSE 
 catH2.Codigo + '. ' +CASE WHEN LEN(catH2.Nombre) > 50 THEN SUBSTRING(catH2.Nombre,1,46) + ' ...' ELSE catH2.Nombre END END AS ValorH2,
 CAST(per.PuntosH2 AS INT) AS PuntosH2,
 CASE WHEN catH3.Nombre IS NULL THEN CASE WHEN per.PuntosH3 IS NULL THEN '' ELSE CASE WHEN per.ValorH3=9999 THEN 'Sin Pagos' ELSE
 ISNULL(CAST(CAST(per.ValorH3 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END END ELSE 
 catH3.Codigo + '. ' +CASE WHEN LEN(catH3.Nombre) > 50 THEN SUBSTRING(catH3.Nombre,1,46) + ' ...' ELSE catH3.Nombre END END AS ValorH3,
 CAST(per.PuntosH3 AS INT) AS PuntosH3,
 per.FechaCalculo,
 per.UsuarioCalculo,
 CASE WHEN cat.Nombre IS NULL THEN CASE WHEN per.ValorActual=9999 THEN 'Sin Pagos' ELSE ISNULL(CAST(CAST(per.ValorActual AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END
 ELSE cat.Codigo + '. ' + cat.Nombre END AS Descripcion,
 aval.Codigo AS CodigoAval,
 aval.Nombre AS NombreAval,
 aval.IdPersona AS IdAval,
 0 AS EsGarante
FROM dbo.SICCMX_Persona_PI_Detalles per
INNER JOIN dbo.SICCMX_PI_Variables variables ON variables.Id = per.IdVariable
INNER JOIN dbo.SICCMX_PI_SubFactor subfac ON subfac.Id = variables.IdSubFactor
INNER JOIN dbo.SICCMX_PI_Factores fac ON fac.Id = subfac.IdFactor
INNER JOIN dbo.SICCMX_Aval aval ON per.IdPersona = aval.IdPersona
INNER JOIN dbo.SICCMX_CreditoAval ca ON aval.IdAval = ca.IdAval AND ca.Aplica = 1
INNER JOIN dbo.SICCMX_Credito cre ON ca.IdCredito = cre.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Rel_Variable_Catalogo rel ON per.IdVariable = rel.IdVariable
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos cat ON rel.IdCatalogo = cat.IdCatalogo AND CAST(CAST(per.ValorActual AS INT) AS VARCHAR) = cat.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH1 ON rel.IdCatalogo = catH1.IdCatalogo AND CAST(CAST(per.ValorH1 AS INT) AS VARCHAR) = catH1.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH2 ON rel.IdCatalogo = catH2.IdCatalogo AND CAST(CAST(per.ValorH2 AS INT) AS VARCHAR) = catH2.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH3 ON rel.IdCatalogo = catH3.IdCatalogo AND CAST(CAST(per.ValorH3 AS INT) AS VARCHAR) = catH3.Codigo
WHERE fac.Codigo = @Factor AND cre.IdPersona = @IdPersona

UNION ALL 

SELECT TOP 1
 per.IdPersona,
 'Se suman 90 puntos al Factor Cuantitativo' AS Nombre,
 '' AS ValorActual,
 '90' AS PuntosActual,
 '' AS ValorH1,
 CASE WHEN per.PuntosH1 IS NULL THEN NULL ELSE 90 END,
 '' AS ValorH2,
 CASE WHEN per.PuntosH2 IS NULL THEN NULL ELSE 90 END,
 '' AS ValorH3,
 CASE WHEN per.PuntosH3 IS NULL THEN NULL ELSE 90 END,
 '',
 '',
 '',
 aval.Codigo AS CodigoAval,
 aval.Nombre AS NombreAval,
 aval.IdPersona AS IdAval,
 0 AS EsGarante
FROM dbo.SICCMX_Persona_PI_Detalles per
INNER JOIN dbo.SICCMX_PI_Variables variables ON variables.Id = per.IdVariable
INNER JOIN dbo.SICCMX_Anexo22 anx ON per.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Aval aval ON per.IdPersona = aval.IdPersona
INNER JOIN dbo.SICCMX_CreditoAval ca ON aval.IdAval = ca.IdAval AND ca.Aplica = 1
INNER JOIN dbo.SICCMX_Credito cre ON ca.IdCredito = cre.IdCredito
WHERE cre.IdPersona = @IdPersona

END
ELSE
BEGIN

-- INFORMACION DE AVALES
SELECT
 per.IdGP AS IdPersona,
 variables.Nombre,
 CASE WHEN cat.Nombre IS NULL THEN CASE WHEN per.PuntosActual IS NULL THEN '' ELSE ISNULL(CAST(CAST(per.ValorActual AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END ELSE 
 cat.Codigo + '. ' +CASE WHEN LEN(cat.Nombre) > 50 THEN SUBSTRING(cat.Nombre,1,46) + ' ...' ELSE cat.Nombre END END AS ValorActual,
 CAST(per.PuntosActual AS INT) AS PuntosActual,
 CASE WHEN catH1.Nombre IS NULL THEN CASE WHEN per.PuntosH1 IS NULL THEN '' ELSE ISNULL(CAST(CAST(per.ValorH1 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END ELSE 
 catH1.Codigo + '. ' +CASE WHEN LEN(catH1.Nombre) > 50 THEN SUBSTRING(catH1.Nombre,1,46) + ' ...' ELSE catH1.Nombre END END AS ValorH1,
 CAST(per.PuntosH1 AS INT) AS PuntosH1,
 CASE WHEN catH2.Nombre IS NULL THEN CASE WHEN per.PuntosH2 IS NULL THEN '' ELSE ISNULL(CAST(CAST(per.ValorH2 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END ELSE 
 catH2.Codigo + '. ' +CASE WHEN LEN(catH2.Nombre) > 50 THEN SUBSTRING(catH2.Nombre,1,46) + ' ...' ELSE catH2.Nombre END END AS ValorH2,
 CAST(per.PuntosH2 AS INT) AS PuntosH2,
 CASE WHEN catH3.Nombre IS NULL THEN CASE WHEN per.PuntosH3 IS NULL THEN '' ELSE ISNULL(CAST(CAST(per.ValorH3 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END ELSE 
 catH3.Codigo + '. ' +CASE WHEN LEN(catH3.Nombre) > 50 THEN SUBSTRING(catH3.Nombre,1,46) + ' ...' ELSE catH3.Nombre END END AS ValorH3,
 CAST(per.PuntosH3 AS INT) AS PuntosH3,
 per.FechaCalculo,
 per.UsuarioCalculo,
 CASE WHEN cat.Nombre IS NULL THEN ISNULL(CAST(CAST(per.ValorActual AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') ELSE cat.Codigo + '. ' + cat.Nombre END AS Descripcion,
 aval.Codigo AS CodigoAval,
 aval.Nombre AS NombreAval,
 aval.IdAval AS IdAval,
 per.EsGarante
FROM dbo.SICCMX_Persona_PI_Detalles_GP per
INNER JOIN dbo.SICCMX_PI_Variables variables ON variables.Id = per.IdVariable
INNER JOIN dbo.SICCMX_PI_SubFactor subfac ON subfac.Id = variables.IdSubFactor
INNER JOIN dbo.SICCMX_PI_Factores fac ON fac.Id = subfac.IdFactor
INNER JOIN dbo.SICC_EsGarante esg ON per.EsGarante = esg.IdEsGarante
INNER JOIN dbo.SICCMX_Aval aval ON per.IdGP = aval.IdAval
INNER JOIN dbo.SICCMX_CreditoAval ca ON aval.IdAval = ca.IdAval AND ca.Aplica = 1
INNER JOIN dbo.SICCMX_Credito cre ON ca.IdCredito = cre.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Rel_Variable_Catalogo rel ON per.IdVariable = rel.IdVariable
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos cat ON rel.IdCatalogo = cat.IdCatalogo AND CAST(CAST(per.ValorActual AS INT) AS VARCHAR) = cat.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH1 ON rel.IdCatalogo = catH1.IdCatalogo AND CAST(CAST(per.ValorH1 AS INT) AS VARCHAR) = catH1.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH2 ON rel.IdCatalogo = catH2.IdCatalogo AND CAST(CAST(per.ValorH2 AS INT) AS VARCHAR) = catH2.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH3 ON rel.IdCatalogo = catH3.IdCatalogo AND CAST(CAST(per.ValorH3 AS INT) AS VARCHAR) = catH3.Codigo
WHERE fac.Codigo = @Factor AND cre.IdPersona = @IdPersona AND esg.Layout = 'AVAL'

UNION ALL

-- INFORMACION DE AVALES QUE SON ACREDITADOS
SELECT
 per.IdPersona,
 variables.Nombre,
 CASE WHEN cat.Nombre IS NULL THEN CASE WHEN per.PuntosActual IS NULL THEN '' ELSE ISNULL(CAST(CAST(per.ValorActual AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END ELSE 
 cat.Codigo + '. ' +CASE WHEN LEN(cat.Nombre) > 50 THEN SUBSTRING(cat.Nombre,1,46) + ' ...' ELSE cat.Nombre END END AS ValorActual,
 CAST(per.PuntosActual AS INT) AS PuntosActual,
 CASE WHEN catH1.Nombre IS NULL THEN CASE WHEN per.PuntosH1 IS NULL THEN '' ELSE ISNULL(CAST(CAST(per.ValorH1 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END ELSE 
 catH1.Codigo + '. ' +CASE WHEN LEN(catH1.Nombre) > 50 THEN SUBSTRING(catH1.Nombre,1,46) + ' ...' ELSE catH1.Nombre END END AS ValorH1,
 CAST(per.PuntosH1 AS INT) AS PuntosH1,
 CASE WHEN catH2.Nombre IS NULL THEN CASE WHEN per.PuntosH2 IS NULL THEN '' ELSE ISNULL(CAST(CAST(per.ValorH2 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END ELSE 
 catH2.Codigo + '. ' +CASE WHEN LEN(catH2.Nombre) > 50 THEN SUBSTRING(catH2.Nombre,1,46) + ' ...' ELSE catH2.Nombre END END AS ValorH2,
 CAST(per.PuntosH2 AS INT) AS PuntosH2,
 CASE WHEN catH3.Nombre IS NULL THEN CASE WHEN per.PuntosH3 IS NULL THEN '' ELSE ISNULL(CAST(CAST(per.ValorH3 AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') END ELSE 
 catH3.Codigo + '. ' +CASE WHEN LEN(catH3.Nombre) > 50 THEN SUBSTRING(catH3.Nombre,1,46) + ' ...' ELSE catH3.Nombre END END AS ValorH3,
 CAST(per.PuntosH3 AS INT) AS PuntosH3,
 per.FechaCalculo,
 per.UsuarioCalculo,
 CASE WHEN cat.Nombre IS NULL THEN ISNULL(CAST(CAST(per.ValorActual AS DECIMAL(22,4)) AS VARCHAR), 'Sin información') ELSE cat.Codigo + '. ' + cat.Nombre END AS Descripcion,
 aval.Codigo AS CodigoAval,
 aval.Nombre AS NombreAval,
 aval.IdPersona AS IdAval,
 0 AS EsGarante
FROM dbo.SICCMX_Persona_PI_Detalles per
INNER JOIN dbo.SICCMX_PI_Variables variables ON variables.Id = per.IdVariable
INNER JOIN dbo.SICCMX_PI_SubFactor subfac ON subfac.Id = variables.IdSubFactor
INNER JOIN dbo.SICCMX_PI_Factores fac ON fac.Id = subfac.IdFactor
INNER JOIN dbo.SICCMX_Aval aval ON per.IdPersona = aval.IdPersona
INNER JOIN dbo.SICCMX_CreditoAval ca ON aval.IdAval = ca.IdAval AND ca.Aplica = 1
INNER JOIN dbo.SICCMX_Credito cre ON ca.IdCredito = cre.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Rel_Variable_Catalogo rel ON per.IdVariable = rel.IdVariable
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos cat ON rel.IdCatalogo = cat.IdCatalogo AND CAST(CAST(per.ValorActual AS INT) AS VARCHAR) = cat.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH1 ON rel.IdCatalogo = catH1.IdCatalogo AND CAST(CAST(per.ValorH1 AS INT) AS VARCHAR) = catH1.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH2 ON rel.IdCatalogo = catH2.IdCatalogo AND CAST(CAST(per.ValorH2 AS INT) AS VARCHAR) = catH2.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos catH3 ON rel.IdCatalogo = catH3.IdCatalogo AND CAST(CAST(per.ValorH3 AS INT) AS VARCHAR) = catH3.Codigo
WHERE fac.Codigo = @Factor AND cre.IdPersona = @IdPersona

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0481_011]
AS

BEGIN

-- Si se reporta algún número de no garantías financieras, entonces el valor de éstas debe ser mayor a cero.
-- Si dat_num_gtias_reales_no_fin>0, entonces  dat_valor_gtia_derechos_cobro > 0 o dat_valor_gtia_inmuebles >0 o dat_valor_gtia_muebles > 0

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	NumeroGarRealNoFin,
	ValorDerechosCobro,
	ValorBienesInmuebles,
	ValorBienesMuebles
FROM dbo.RW_VW_R04C0481_INC
WHERE CAST(NumeroGarRealNoFin AS DECIMAL) > 0 AND (CAST(ValorDerechosCobro AS DECIMAL) + CAST(ValorBienesInmuebles AS DECIMAL) + CAST(ValorBienesMuebles AS DECIMAL)) = 0;

END


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_CreditoAval_Credito_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_CreditoAval_Credito_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_CreditoAval f
LEFT OUTER JOIN dbo.SICCMX_Credito cre ON LTRIM(f.CodigoCredito) = cre.Codigo
WHERE cre.IdCredito IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_CreditoAval_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoAval,
 'CodigoCredito',
 f.CodigoCredito,
 2,
 @Detalle
FROM dbo.FILE_CreditoAval f
LEFT OUTER JOIN dbo.SICCMX_Credito cre ON LTRIM(f.CodigoCredito) = cre.Codigo
WHERE cre.IdCredito IS NULL;
GO

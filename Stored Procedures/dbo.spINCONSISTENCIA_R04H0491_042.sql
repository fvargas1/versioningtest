SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_042]
AS

BEGIN

-- La clave de localidad en la que se encuentra la Vivienda es igual a cero (0) si y solo si “Tipo de Alta del Crédito” es igual a 3 (columna 9).

SELECT CodigoCredito, CodigoCreditoCNBV, Localidad, TipoAlta
FROM dbo.RW_R04H0491
WHERE Localidad = '0' AND TipoAlta <> '3';

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX3_Persona_Totales]
	@IdPersona BIGINT,
	@IdMetodologia INT
AS
SELECT
	per.IdPersona,
	per.Codigo,
	per.Nombre,
	ISNULL(SUM(crv.EI_Total),0) AS TotalResponsabilidad,
	ISNULL(SUM(crv.ReservaFinal),0) AS TotalReserva,
	ISNULL(SUM(pc.ValorGarantia),0) AS TotalGarantia,
	ISNULL(SUM(crv.EI_Expuesta),0) AS MontoExpuesto,
	ISNULL(SUM(crv.EI_Cubierta),0) AS MontoCubierto,
	ISNULL(SUM(crv.ReservaExpuesta),0) AS ReservaExpuesto,
	ISNULL(SUM(crv.ReservaCubierta),0) AS ReservaCubierto,
	ISNULL(crv.PI_Total,0) AS [PI]
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Proyecto pry ON per.IdPersona = pry.IdPersona
INNER JOIN dbo.SICCMX_ProyectoCalificacion pc ON pry.IdProyecto = pc.IdProyecto
WHERE per.IdPersona = @IdPersona AND cre.IdMetodologia = @IdMetodologia
GROUP BY per.IdPersona, per.Codigo, per.Nombre, crv.PI_Total
ORDER BY per.Nombre;
GO

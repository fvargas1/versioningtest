SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_061]
AS

BEGIN

-- El "TIPO DE SEGURO DE CRÉDITO A LA VIVIENDA" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.TipoSeguro
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_TipoSeguro cat ON ISNULL(r.TipoSeguro,'') = cat.CodigoCNBV
WHERE cat.IdTipoSeguro IS NULL;

END
GO

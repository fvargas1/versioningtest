SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ProyectoLinea_Migracion]
AS

INSERT INTO dbo.SICCMX_ProyectoLinea (
	IdProyecto,
	IdLineaCredito
)
SELECT
	(SELECT IdProyecto FROM dbo.SICCMX_Proyecto WHERE CodigoProyecto = LTRIM(f.CodigoProyecto)),
	(SELECT IdLineaCredito FROM dbo.SICCMX_LineaCredito WHERE Codigo = LTRIM(f.NumeroLinea))
FROM dbo.FILE_ProyectoLinea f
WHERE f.CodigoProyecto+'-'+f.NumeroLinea
NOT IN (
	SELECT p.CodigoProyecto+'-'+l.Codigo
	FROM dbo.SICCMX_ProyectoLinea pl
	INNER JOIN dbo.SICCMX_Proyecto p ON pl.IdProyecto = p.IdProyecto
	INNER JOIN dbo.SICCMX_LineaCredito l ON pl.IdLineaCredito = l.IdLineaCredito
) AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_101]
AS

BEGIN

-- Validar que si la Responsabilidad Total (dat_responsabilidad_total) es menor o igual a 0, entonces las Reservas Totales (dat_reservas_totales) son igual a 0.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	SaldoInsoluto,
	ReservaTotal
FROM dbo.RW_VW_R04C0464_INC
WHERE CAST(ISNULL(NULLIF(SaldoInsoluto,''),'0') AS DECIMAL) <= 0 AND CAST(ISNULL(NULLIF(ReservaTotal,''),'0') AS DECIMAL) > 0

END

GO

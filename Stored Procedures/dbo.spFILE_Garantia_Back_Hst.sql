SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Back_Hst]
AS

INSERT INTO dbo.FILE_Garantia_Hst (
 CodigoGarantia,
 TipoGarantiaMA,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 FechaValuacion,
 Descripcion,
 BancoGarantia,
 ValorGarantiaProyectado,
 RegGarantiaMob,
 Hc,
 VencimientoRestante,
 GradoRiesgo,
 AgenciaCalificadora,
 Calificacion,
 Emisor,
 Escala,
 EsIPC,
 PIGarante,
 NumRPPC,
 RFCGarante,
 NombreGarante,
 IdGarante,
 ActividaEcoGarante,
 LocalidadGarante,
 MunicipioGarante,
 EstadoGarante,
 TipoGarante,
 LEI,
 IndPerMorales,
 CodigoCliente,
 CodigoPostalGarante
)
SELECT
 gar.CodigoGarantia,
 gar.TipoGarantiaMA,
 gar.TipoGarantia,
 gar.ValorGarantia,
 gar.Moneda,
 gar.FechaValuacion,
 gar.Descripcion,
 gar.BancoGarantia,
 gar.ValorGarantiaProyectado,
 gar.RegGarantiaMob,
 gar.Hc,
 gar.VencimientoRestante,
 gar.GradoRiesgo,
 gar.AgenciaCalificadora,
 gar.Calificacion,
 gar.Emisor,
 gar.Escala,
 gar.EsIPC,
 gar.PIGarante,
 gar.NumRPPC,
 gar.RFCGarante,
 gar.NombreGarante,
 gar.IdGarante,
 gar.ActividaEcoGarante,
 gar.LocalidadGarante,
 gar.MunicipioGarante,
 gar.EstadoGarante,
 gar.TipoGarante,
 gar.LEI,
 gar.IndPerMorales,
 gar.CodigoCliente,
 gar.CodigoPostalGarante
FROM dbo.FILE_Garantia gar
LEFT OUTER JOIN dbo.FILE_Garantia_Hst hst ON LTRIM(gar.CodigoGarantia) = LTRIM(hst.CodigoGarantia)
WHERE hst.CodigoGarantia IS NULL;
GO

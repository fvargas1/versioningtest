SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_LineaCredito_errores_ClearLog]
AS
UPDATE dbo.FILE_LineaCredito
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_LineaCredito_errores;
GO

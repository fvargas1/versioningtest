SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_InsumoRC15]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.RW_InsumoRC15 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_InsumoRC15 (
	IdPeriodoHistorico,
	CodigoCredito,
	CodigoPersona,
	RFC,
	MontoGarantiasReales,
	MontoReservas,
	MontoAvales,
	FechaDisposicion,
	SituacionCredito,
	Exposicion,
	ImporteRC3,
	ClienteRelacionado
)
SELECT
	@IdPeriodoHistorico,
	CodigoCredito,
	CodigoPersona,
	RFC,
	MontoGarantiasReales,
	MontoReservas,
	MontoAvales,
	FechaDisposicion,
	SituacionCredito,
	Exposicion,
	ImporteRC3,
	ClienteRelacionado
FROM dbo.RW_InsumoRC15;
GO

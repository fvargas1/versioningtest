SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Movimientos_TipoMovimiento_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Movimientos_TipoMovimiento_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Movimientos f
LEFT OUTER JOIN dbo.SICC_TipoMovimiento cat ON LTRIM(ISNULL(f.TipoMovimiento,'')) = cat.Codigo
WHERE cat.IdTipoMovimiento IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Movimientos_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	f.CodigoCredito,
	'TipoMovimiento',
	f.TipoMovimiento,
	2,
	@Detalle
FROM dbo.FILE_Movimientos f
LEFT OUTER JOIN dbo.SICC_TipoMovimiento cat ON LTRIM(ISNULL(f.TipoMovimiento,'')) = cat.Codigo
WHERE cat.IdTipoMovimiento IS NULL;
GO

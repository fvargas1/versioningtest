SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0411Datos_CalculandoConceptos_2016]
AS
UPDATE dat
SET Concepto = conf.Concepto
FROM R04.[0411Datos_2016] dat
INNER JOIN R04.[0411Configuracion_2016] conf ON conf.TipoProducto = dat.CodigoProducto;
GO

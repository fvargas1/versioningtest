SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_Financieras]
AS
-- CALCULAMOS SP PARA GARANTIAS REALES FINANCIERAS
UPDATE can
SET SeveridadCorresp = CASE WHEN crv.EI_Total = 0 THEN 0 ELSE crv.SP_Expuesta * CAST(can.EI_Ajust / crv.EI_Total AS DECIMAL(18,12)) END
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
WHERE can.IdTipoGarantia IS NULL AND can.EsDescubierto IS NULL;
GO

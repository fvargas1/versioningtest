SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Garantia_Sel]
	@IdGarantia BIGINT
AS
SELECT
	g.IdGarantia,
	g.Codigo,
	g.FechaValuacion,
	0 AS GarantiaAsegurada,
	SortOrder AS GradoPrelacion,
	g.Descripcion,
	g.ValorGarantia,
	g.IdTipoGarantia,
	'' AS Linea,
	0 AS Bursatilidad,
	tg.Codigo,
	tg.Nombre as DescripcionTipoGarantia,
	0 AS Cobertura
FROM dbo.SICCMX_Garantia g
INNER JOIN dbo.SICC_TipoGarantia tg ON g.IdTipoGarantia = tg.IdTipoGarantia
WHERE g.IdGarantia = @IdGarantia;
GO

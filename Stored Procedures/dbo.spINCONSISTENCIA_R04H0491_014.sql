SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_014]
AS

BEGIN

-- Se deberá validar la correspondencia para las siguientes claves en "Tipo de Alta" y "Categoría del Crédito":
-- - Cuando el "Tipo de Alta del Crédito" sea 2, la "Categoría del Crédito" debe ser 1.
-- - Cuando el "Tipo de Alta del Crédito" sea 3, la "Categoría del Crédito" debe ser 1 o 2.
-- - Cuando el "Tipo de Alta del Crédito" sea 5, la "Categoría del Crédito" debe ser 1 o 2.
-- - Cuando el "Tipo de Alta del Crédito" sea 6, la "Categoría del Crédito" debe ser 2.
-- - Cuando el "Tipo de Alta del Crédito" sea 10, la "Categoría del Crédito" debe ser 2.

SELECT CodigoCredito, CodigoCreditoCNBV, TipoAlta, CategoriaCredito
FROM dbo.RW_R04H0491
WHERE (CategoriaCredito = '1' AND TipoAlta NOT IN ('2','3','5')) OR (CategoriaCredito = '2' AND TipoAlta NOT IN ('3','5','6','10'));

END
GO

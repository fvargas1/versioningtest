SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0478]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0478);

DELETE FROM Historico.RW_R04C0478 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0478 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, TipoCartera, ActividadEconomica, GrupoRiesgo, LocalidadDeudor,
	DomicilioMunicipio, DomicilioEstado, NumInfoCrediticia, LEI, TipoAlta, TipoProducto, TipoOperacion, DestinoCredito, CodigoCredito,
	CodigoCreditoCNBV, GrupalCNBV, MontoLineaAutorizado, FechaMaxDisponer, FechaVencLinea, Moneda, FormaDisposicion, TipoLinea, PrelacionPago,
	NoRUGM, PrctPartFed, InstAgenciaExt, TasaReferencia, AjusteTasaReferencia, OperacionTasaReferencia, FrecRevTasa, PeriodicidadCapital,
	PeriodicidadInteres, MesesGraciaCapital, MesesGraciaInteres, ComAperturaTasa, ComAperturaMonto, ComDispTasa, ComDispMonto, LocalidadDestinoCredito,
	MunicipioDestinoCredito, EstadoDestinoCredito, ActividadDestinoCredito
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	TipoCartera,
	ActividadEconomica,
	GrupoRiesgo,
	LocalidadDeudor,
	DomicilioMunicipio,
	DomicilioEstado,
	NumInfoCrediticia,
	LEI,
	TipoAlta,
	TipoProducto,
	TipoOperacion,
	DestinoCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	GrupalCNBV,
	MontoLineaAutorizado,
	FechaMaxDisponer,
	FechaVencLinea,
	Moneda,
	FormaDisposicion,
	TipoLinea,
	PrelacionPago,
	NoRUGM,
	PrctPartFed,
	InstAgenciaExt,
	TasaReferencia,
	AjusteTasaReferencia,
	OperacionTasaReferencia,
	FrecRevTasa,
	PeriodicidadCapital,
	PeriodicidadInteres,
	MesesGraciaCapital,
	MesesGraciaInteres,
	ComAperturaTasa,
	ComAperturaMonto,
	ComDispTasa,
	ComDispMonto,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM dbo.RW_R04C0478
WHERE IdReporteLog = @IdReporteLog;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoPI_Total]
AS
UPDATE dbo.SICCMX_Credito_Reservas_Variables
SET PI_Total = CASE WHEN (EI_Total * SP_Total) = 0 THEN PI_Expuesta ELSE ReservaFinal / CAST(EI_Total * SP_Total AS DECIMAL(25,4)) END;


INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 IdCredito,
 GETDATE(),
 '',
 'Se calculó una PI Total de ' + CONVERT(VARCHAR, CONVERT(DECIMAL(10,2), ISNULL(PI_Total*100, 0))) +'%'
FROM dbo.SICCMX_Credito_Reservas_Variables;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spGetInstitucion]
AS
SELECT Nombre
FROM dbo.SICC_Institucion
WHERE Codigo IN (SELECT Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion')
GO

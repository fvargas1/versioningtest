SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_027]
AS

BEGIN

-- La denominación del crédito debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.DenominacionCredito
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_Moneda cat ON ISNULL(r.DenominacionCredito,'') = cat.CodigoCNBV
WHERE cat.IdMoneda IS NULL;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_CedulaDatos_Anexo19_Historico]
	@IdPeriodoHistorico BIGINT
AS
SELECT DISTINCT
	per.Codigo AS CodigoPersona,
	dbo.fnFormat_Name(per.Nombre) AS 'Description',
	dbo.fnFormat_Name(per.Nombre) AS NombrePersona,
	pry.Proyecto AS CodigoProyecto,
	ISNULL(@IdPeriodoHistorico, -1 ) AS IdPeriodoHistorico
FROM Historico.SICCMX_Persona per
INNER JOIN Historico.SICCMX_Proyecto pry ON per.Codigo = pry.Persona AND per.IdPeriodoHistorico = pry.IdPeriodoHistorico
WHERE per.IdPeriodoHistorico = @IdPeriodoHistorico
ORDER BY NombrePersona, CodigoProyecto;
GO

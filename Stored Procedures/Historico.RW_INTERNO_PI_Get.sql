SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_INTERNO_PI_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Codigo,
	Nombre,
	Metodologia,
	Clasificacion,
	FactorCuantitativo,
	FactorCualitativo,
	Factor_Alpha,
	Factor_1mAlpha,
	PonderadoCuantitativo,
	PonderadoCualitativo,
	FactorTotal,
	[PI]
FROM Historico.RW_INTERNO_PI
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Metodologia, Nombre;
GO

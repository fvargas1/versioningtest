SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_037]
AS

BEGIN

-- Se validará que la clave de banca de desarrollo que otorgó los recursos exista en el catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.CodigoBancoFondeador
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_Institucion cat ON ISNULL(r.CodigoBancoFondeador,'') = cat.Codigo
WHERE LEN(R.CodigoBancoFondeador) > 0 AND cat.IdInstitucion IS NULL;

END
GO

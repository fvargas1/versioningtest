SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_SP_RTH]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaHipotecario FROM dbo.SICCMX_Hipotecario_Metodologia WHERE Codigo = '2';

-- Calculamos la SP cuando el ATR es mayor a lo maximo para cada Metodologia
UPDATE hrv
SET SP = CASE WHEN vp.ATR >= const.ATRSP THEN 1
	ELSE
	CASE WHEN ISNULL(vp.SUBCVi,0) >= ISNULL(hrv.E,0) THEN 0.10 ELSE 0.65 * 0.8 * ISNULL(tr.FactorAjuste,1) END
	END
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp ON vp.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia
INNER JOIN dbo.SICCMX_HipotecarioInfo inf ON vp.IdHipotecario = inf.IdHipotecario
LEFT OUTER JOIN dbo.SICC_TipoRegimen tr ON inf.IdTipoRegimenCredito = tr.IdTipoRegimen
WHERE const.IdMetodologia = @IdMetodologia;


UPDATE res
SET SPExpuesto = hrv.SP,
	SPCubierto = CASE WHEN vp.ATR >= const.ATRSP THEN 1 ELSE const.SPVALOR END
FROM dbo.SICCMX_HipotecarioReservas res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp ON vp.IdHipotecario = res.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON vp.IdHipotecario = hrv.IdHipotecario
WHERE const.IdMetodologia = @IdMetodologia;
GO

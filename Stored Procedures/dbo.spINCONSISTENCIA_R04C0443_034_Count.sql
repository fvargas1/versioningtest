SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_034_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El tipo de baja del crédito deberá existir en catálogo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.CodigoCredito)
FROM dbo.RW_R04C0443 r
LEFT OUTER JOIN dbo.SICC_TipoBajaMA cat ON ISNULL(r.TipoBaja,'') = cat.CodigoCNBV
WHERE LEN(r.TipoBaja) > 0 AND cat.IdTipoBaja IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

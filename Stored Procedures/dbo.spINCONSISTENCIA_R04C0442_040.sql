SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_040]
AS

BEGIN

-- El porcentaje cubierto con garantía de fondos y BD deberá ser mayor o igual a 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, PorcentajeCubiertoFondeo
FROM dbo.RW_R04C0442
WHERE CAST(ISNULL(NULLIF(PorcentajeCubiertoFondeo,''),'-1') AS DECIMAL(10,2)) < 0;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Hipotecario_Persona_List]
 @IdPersona BIGINT
AS
SELECT
 c.IdHipotecario,
 c.Codigo,
 ISNULL(c.SaldoCapitalVigenteValorizado,0) AS 'Capital Vigente',
 ISNULL(c.InteresVigenteValorizado,0) AS 'Interes Vigente',
 ISNULL(c.SaldoCapitalVencidoValorizado,0) AS 'Capital Vencido',
 ISNULL(c.InteresVencidoValorizado,0) AS 'Interes Vencido',
 ISNULL(c.SaldoTotalValorizado,0) AS 'Cartera Total',
 met.Descripcion AS 'Esquema',
 '' AS 'No. Periodos Vencidos',
-- CASE WHEN ISNULL(cc.MontoCubierto,0)>0 THEN
-- CONVERT(MONEY,cc.MontoCubierto)/CONVERT(MONEY,(ISNULL(c.SaldoCapitalVigente,0)+ISNULL(c.InteresVigente,0)+ISNULL(c.SaldoCapitalVencido,0)+ISNULL(c.InteresVencido,0)))
-- ELSE
-- CONVERT(MONEY,0)
-- END AS 'Porcentaje Cubierto',
 CASE WHEN ISNULL(cc.MontoExpuesto,0)>0 THEN
 CONVERT(MONEY,cc.MontoExpuesto)/CONVERT(MONEY,(ISNULL(c.SaldoCapitalVigente,0)+ISNULL(c.InteresVigente,0)+ISNULL(c.SaldoCapitalVencido,0)+ISNULL(c.InteresVencido,0)))
 ELSE
 CONVERT(MONEY,0)
 END AS 'Porcentaje Expuesto',
 ISNULL(cc.MontoCubierto,0) AS 'Monto Cubierto',
 ISNULL(cc.MontoExpuesto,0) AS 'Monto Expuesto',
 ISNULL(cc.ReservaCubierto,0) AS 'Reserva Cubierto',
 ISNULL(cc.ReservaExpuesto,0) AS 'Reserva Expuesto',
 ISNULL(cc.ReservaCubierto,0)+ISNULL(cc.ReservaExpuesto,0) AS 'Total Reservas',
 CASE WHEN ISNULL(cc.MontoCubierto,0)>0 THEN
 ISNULL(calc.Codigo,'0')
 ELSE
 '0' END AS 'Grado Riesgo Cubierto',
 CASE WHEN ISNULL(cc.MontoExpuesto,0)>0 THEN
 ISNULL(cale.Codigo,'0')
 ELSE
 '0' END AS 'Grado Riesgo Expuesto'
FROM dbo.SICCMX_VW_Hipotecario c
INNER JOIN dbo.SICCMX_HipotecarioInfo ci ON ci.IdHipotecario = c.IdHipotecario
INNER JOIN dbo.SICCMX_Persona p ON c.IdPersona = p.IdPersona
INNER JOIN dbo.SICCMX_HipotecarioReservas cc ON c.IdHipotecario = cc.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON cc.IdHipotecario = pre.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaHipotecario
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calc ON cc.IdCalificacionCubierto = calc.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 cale ON cc.IdCalificacionExpuesto = cale.IdCalificacion
WHERE ISNULL(c.SaldoTotalValorizado,0) > 0 AND p.IdPersona = @IdPersona;
GO

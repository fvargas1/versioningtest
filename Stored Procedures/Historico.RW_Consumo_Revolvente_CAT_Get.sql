SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Consumo_Revolvente_CAT_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Fecha,
	FolioCredito,
	CatContrato,
	LimCredito,
	Producto
FROM Historico.RW_Consumo_Revolvente_CAT
WHERE IdPeriodoHistorico=@IdPeriodoHistorico;
GO

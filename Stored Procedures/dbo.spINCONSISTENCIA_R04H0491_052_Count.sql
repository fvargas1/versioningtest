SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_052_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se deberá validar la correspondencia para las siguientes claves en "Sector Laboral del Acreditado" y "Tipo de Comprobación" de acuerdo a la siguiente tabla:
-- - Si el "Sector Laboral del Acreditado" es 1,2,3 o 4, el "Tipo de Comprobación de Ingresos" debe ser 2.
-- - Si el "Sector Laboral del Acreditado" es 5, el "Tipo de Comprobación de Ingresos" debe ser 1.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE (SectorLaboral IN ('1','2','3','4') AND TipoComprobacionIngresos <> '2') OR (SectorLaboral = '5' AND TipoComprobacionIngresos <> '1');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

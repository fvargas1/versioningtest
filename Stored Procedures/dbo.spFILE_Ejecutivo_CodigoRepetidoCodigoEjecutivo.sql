SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Ejecutivo_CodigoRepetidoCodigoEjecutivo]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Ejecutivo_CodigoRepetidoCodigoEjecutivo';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Ejecutivo f
WHERE f.CodigoEjecutivo IN (
	SELECT f.CodigoEjecutivo
	FROM dbo.FILE_Ejecutivo f
	GROUP BY f.CodigoEjecutivo
	HAVING COUNT(f.CodigoEjecutivo)>1
) AND LEN(f.CodigoEjecutivo) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Ejecutivo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	f.CodigoEjecutivo,
	'CodigoEjecutivo',
	f.CodigoEjecutivo,
	1,
	@Detalle
FROM dbo.FILE_Ejecutivo f
WHERE f.CodigoEjecutivo IN (
	SELECT f.CodigoEjecutivo
	FROM dbo.FILE_Ejecutivo f
	GROUP BY f.CodigoEjecutivo
	HAVING COUNT(f.CodigoEjecutivo)>1
) AND LEN(f.CodigoEjecutivo) > 0;
GO

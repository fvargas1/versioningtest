SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoGradoRiesgo]
AS
UPDATE gar
SET
 IdGradoRiesgo = CASE esc.Codigo
 WHEN '1' THEN cal.GradoRiesgoGlobal -- Para escala global
 WHEN '2' THEN cal.GradoRiesgoMX-- Para escala local
 ELSE NULL END
FROM dbo.SICCMX_Garantia gar
LEFT OUTER JOIN dbo.SICC_Escala esc ON gar.IdEscala = esc.IdEscala
LEFT OUTER JOIN dbo.SICC_AgenciaCalificacionGarantias cal ON gar.IdAgenciaCalificadora = cal.IdAgencia AND gar.Calificacion = cal.Calificacion;
GO

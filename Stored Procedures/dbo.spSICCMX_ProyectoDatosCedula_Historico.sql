SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_ProyectoDatosCedula_Historico]
	@CodigoProyecto VARCHAR(50),
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Nombre,
	Descripcion
FROM Historico.SICCMX_Proyecto
WHERE Proyecto = @CodigoProyecto AND IdPeriodoHistorico = @IdPeriodoHistorico;
GO

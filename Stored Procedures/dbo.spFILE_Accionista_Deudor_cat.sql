SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Accionista_Deudor_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Accionista_Deudor_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET errorCatalogo = 1
FROM dbo.FILE_Accionista f
LEFT OUTER JOIN dbo.SICCMX_Persona per ON LTRIM(ISNULL(f.Deudor,'')) = per.Codigo
WHERE per.IdPersona IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Accionista_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	Deudor,
	'Deudor',
	Deudor,
	1,
	@Detalle
FROM dbo.FILE_Accionista f
LEFT OUTER JOIN dbo.SICCMX_Persona per ON LTRIM(ISNULL(f.Deudor,'')) = per.Codigo
WHERE per.IdPersona IS NULL;
GO

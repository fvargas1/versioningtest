SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_016]
AS
BEGIN
-- La localidad del acreditado debe existir en catálogo

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.Localidad
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_Localidad2015 cat ON ISNULL(r.Localidad,'') = cat.CodigoCNBV
WHERE cat.IdLocalidad IS NULL;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_024]
AS

BEGIN

-- Para créditos nuevos y cartas de crédito la fecha de otorgamiento dentro del ID MET CNBV debe ser igual al periodo que se reporta

SELECT CodigoCredito, CodigoCreditoCNBV, MontoOriginal, TipoAlta
FROM dbo.RW_R04H0491
WHERE MontoOriginal = '0' AND TipoAlta <> '3';

END
GO

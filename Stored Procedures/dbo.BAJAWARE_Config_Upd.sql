SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Config_Upd]
	@IdConfig BAJAWARE_utID,
	@Name BAJAWARE_utDescripcion50,
	@Description BAJAWARE_utDescripcion100,
	@CodeName BAJAWARE_utDescripcion50,
	@Value BAJAWARE_utDescripcion250
AS
UPDATE dbo.BAJAWARE_Config
SET
	[Name] = @Name,
	Description = @Description,
	CodeName = @CodeName,
	Value = @Value
WHERE IdConfig = @IdConfig;
GO

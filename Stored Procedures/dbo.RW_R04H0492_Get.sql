SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04H0492_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	DenominacionCredito,
	SaldoPrincipalInicio,
	TasaInteres,
	ComisionesCobradasTasa,
	ComisionesCobradasMonto,
	MontoPagoExigible,
	MontoPagoRealizado,
	MontoBonificacion,
	SaldoPrincipalFinal,
	ResponsabilidadTotal,
	FechaUltimoPago,
	SituacionCredito,
	[PI],
	SP,
	DiasAtraso,
	MAXATR,
	PorVPAGO,
	SUBCV,
	ConvenioJudicial,
	PorCobPaMed,
	PorCobPP,
	EntidadCobertura,
	ReservasSaldoFinal,
	PerdidaEsperada,
	ReservaPreventiva,
	PIInterna,
	SPInterna,
	EInterna,
	PerdidaEsperadaInterna
FROM dbo.RW_VW_R04H0492
ORDER BY NumeroSecuencia;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0424Datos_CalculandoConceptos_2016]
AS
DECLARE @PeriodoAnterior INT;

SELECT @PeriodoAnterior = ant.IdPeriodo
FROM dbo.SICC_Periodo act
INNER JOIN dbo.SICC_Periodo ant ON YEAR(ant.Fecha) = YEAR(DATEADD(MONTH, -1, act.Fecha)) AND MONTH(ant.Fecha) = MONTH(DATEADD(MONTH, -1, act.Fecha))
WHERE act.Activo = 1;


TRUNCATE TABLE R04.[0424Conceptos_2016];

-- SALDOS HISTORICOS
INSERT INTO R04.[0424Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 conf.Concepto,
 CASE WHEN mon.CodigoISO = 'MXN'
 THEN CAST((CAST(ISNULL(dat.SaldoHistorico,0) AS DECIMAL) + CAST(ISNULL(InteresVigenteHistorico,0) AS DECIMAL) + CAST(ISNULL(InteresVencidoHistorico,0) AS DECIMAL)) * tc.Valor AS DECIMAL)
 ELSE CAST((ISNULL(dat.SaldoHistorico,0) + ISNULL(InteresVigenteHistorico,0) + ISNULL(InteresVencidoHistorico,0)) * tc.Valor AS DECIMAL)
 END AS SaldoHistorico
FROM [R04].[0424Datos_2016] dat
INNER JOIN R04.[0424Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'SDOINICIAL'
INNER JOIN dbo.SICC_Moneda mon ON mon.Codigo = dat.Moneda
INNER JOIN dbo.SICC_TipoCambio tc ON mon.IdMoneda = tc.IdMoneda AND tc.IdPeriodo = @PeriodoAnterior
WHERE dat.SituacionHistorica = '1';


-- SALDOS ACTUALES
INSERT INTO R04.[0424Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 conf.Concepto,
 CASE WHEN mon.CodigoISO = 'MXN'
 THEN CAST((CAST(ISNULL(dat.SaldoActual,0) AS DECIMAL) + CAST(ISNULL(dat.InteresVencido,0) AS DECIMAL) + CAST(ISNULL(dat.InteresVigente,0) AS DECIMAL)) * tc.Valor AS DECIMAL)
 ELSE CAST((ISNULL(dat.SaldoActual,0) + ISNULL(dat.InteresVencido,0) + ISNULL(dat.InteresVigente,0)) * tc.Valor AS DECIMAL)
 END AS SaldoActual
FROM [R04].[0424Datos_2016] dat
INNER JOIN R04.[0424Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'SDOFINAL'
INNER JOIN dbo.SICC_Moneda mon ON dat.Moneda = mon.Codigo
INNER JOIN dbo.SICC_TipoCambio tc ON mon.IdMoneda = tc.IdMoneda
INNER JOIN dbo.SICC_Periodo per ON per.IdPeriodo = tc.IdPeriodo AND per.Activo = 1
WHERE dat.SituacionActual = '1';


-- OTORGAMIENTOS Y COMPRA DE CARTERA
INSERT INTO R04.[0424Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 conf.Concepto,
 CASE WHEN mon.CodigoISO = 'MXN'
 THEN CAST((CAST(ISNULL(dat.SaldoActual,0) AS DECIMAL)) * tc.Valor AS DECIMAL)
 ELSE CAST((ISNULL(dat.SaldoActual,0)) * tc.Valor AS DECIMAL)
 END AS SaldoActual
FROM [R04].[0424Datos_2016] dat
INNER JOIN R04.[0424Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = dat.TipoAlta
INNER JOIN dbo.SICC_Moneda mon ON mon.Codigo = dat.Moneda
INNER JOIN dbo.SICC_TipoCambio tc ON mon.IdMoneda = tc.IdMoneda AND tc.IdPeriodo = @PeriodoAnterior
LEFT OUTER JOIN dbo.SICCMX_VW_A0424CreditosTAVEN_2016 taven ON dat.Codigo = taven.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_A0424CreditosTAVIG_2016 tavig ON dat.Codigo = tavig.Codigo
WHERE taven.Codigo IS NULL AND tavig.Codigo IS NULL AND dat.TipoBaja IS NULL AND dat.SituacionActual = '1';


-- INTERESES DEVENGADOS
INSERT INTO R04.[0424Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 conf.Concepto,
 dat.IntDevengado
FROM [R04].[0424Datos_2016] dat
INNER JOIN R04.[0424Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'INTDEV'
LEFT OUTER JOIN dbo.SICCMX_VW_A0424CreditosTAVEN_2016 taven ON dat.Codigo = taven.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_A0424CreditosTAVIG_2016 tavig ON dat.Codigo = tavig.Codigo
WHERE taven.Codigo IS NULL AND dat.TipoBaja IS NULL AND dat.SituacionActual = '1';


-- TRASPASOS DE CARTERA VENCIDA A VIGENTE
INSERT INTO R04.[0424Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 conf.Concepto,
 CASE WHEN mon.CodigoISO = 'MXN'
 THEN CAST((CAST(ISNULL(dat.SaldoHistorico,0) AS DECIMAL) + CAST(ISNULL(InteresVigenteHistorico,0) AS DECIMAL) + CAST(ISNULL(InteresVencidoHistorico,0) AS DECIMAL)) * tc.Valor AS DECIMAL)
 ELSE CAST((ISNULL(dat.SaldoHistorico,0) + ISNULL(InteresVigenteHistorico,0) + ISNULL(InteresVencidoHistorico,0)) * tc.Valor AS DECIMAL)
 END AS SaldoHistorico
FROM [R04].[0424Datos_2016] dat
INNER JOIN R04.[0424Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'TAVIG'
INNER JOIN dbo.SICC_Moneda mon ON mon.Codigo = dat.Moneda
INNER JOIN dbo.SICC_TipoCambio tc ON mon.IdMoneda = tc.IdMoneda AND tc.IdPeriodo = @PeriodoAnterior
INNER JOIN dbo.SICCMX_VW_A0424CreditosTAVIG_2016 tavig ON dat.Codigo = tavig.Codigo;


-- TRASPASOS DE CARTERA VIGENTE A VENCIDA
INSERT INTO R04.[0424Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 conf.Concepto,
 CASE WHEN mon.CodigoISO = 'MXN'
 THEN CAST((CAST(ISNULL(dat.SaldoHistorico,0) AS DECIMAL) + CAST(ISNULL(InteresVigenteHistorico,0) AS DECIMAL) + CAST(ISNULL(InteresVencidoHistorico,0) AS DECIMAL)) * tc.Valor AS DECIMAL)
 ELSE CAST((ISNULL(dat.SaldoHistorico,0) + ISNULL(InteresVigenteHistorico,0) + ISNULL(InteresVencidoHistorico,0)) * tc.Valor AS DECIMAL)
 END AS SaldoActual
FROM [R04].[0424Datos_2016] dat
INNER JOIN R04.[0424Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'TAVEN'
INNER JOIN dbo.SICC_Moneda mon ON mon.Codigo = dat.Moneda
INNER JOIN dbo.SICC_TipoCambio tc ON mon.IdMoneda = tc.IdMoneda
INNER JOIN dbo.SICC_Periodo prd ON tc.IdPeriodo = prd.IdPeriodo AND prd.Activo = 1
INNER JOIN dbo.SICCMX_VW_A0424CreditosTAVEN_2016 taven ON dat.Codigo = taven.Codigo;


-- BAJAS POR REESTRUCTURA O VENTA DE CARTERA
INSERT INTO R04.[0424Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 conf.Concepto,
 CASE WHEN mon.CodigoISO = 'MXN'
 THEN CAST((CAST(ISNULL(dat.SaldoHistorico,0) AS DECIMAL) + CAST(ISNULL(InteresVigenteHistorico,0) AS DECIMAL) + CAST(ISNULL(InteresVencidoHistorico,0) AS DECIMAL)) * tc.Valor AS DECIMAL)
 ELSE CAST((ISNULL(dat.SaldoHistorico,0) + ISNULL(InteresVigenteHistorico,0) + ISNULL(InteresVencidoHistorico,0)) * tc.Valor AS DECIMAL)
 END AS SaldoHistorico
FROM [R04].[0424Datos_2016] dat
INNER JOIN R04.[0424Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = dat.TipoBaja
INNER JOIN dbo.SICC_Moneda mon ON mon.Codigo = dat.Moneda
INNER JOIN dbo.SICC_TipoCambio tc ON mon.IdMoneda = tc.IdMoneda AND tc.IdPeriodo = @PeriodoAnterior
LEFT OUTER JOIN dbo.SICCMX_VW_A0424CreditosTAVEN_2016 taven ON dat.Codigo = taven.Codigo
LEFT OUTER JOIN dbo.SICCMX_VW_A0424CreditosTAVIG_2016 tavig ON dat.Codigo = tavig.Codigo
WHERE dat.TipoAlta IS NULL AND taven.Codigo IS NULL AND tavig.Codigo IS NULL AND dat.SituacionActual = '1';


-- MOVIMIENTOS
INSERT INTO R04.[0424Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 conf.Concepto,
 CAST(SUM(mov.Monto) AS DECIMAL)
FROM [R04].[0424Datos_2016] dat
INNER JOIN dbo.SICCMX_VW_0424_Movimientos mov ON dat.Codigo = mov.Codigo
INNER JOIN R04.[0424Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND mov.TipoMovimiento = conf.TipoMovimiento
LEFT OUTER JOIN dbo.SICCMX_VW_A0424CreditosTAVEN_2016 taven ON dat.Codigo = taven.Codigo
WHERE taven.Codigo IS NULL AND dat.TipoAlta IS NULL AND dat.TipoBaja IS NULL AND dat.SituacionActual = '1' AND mov.TipoMovimiento NOT IN ('REEST','TAVEN','TAVIG')
GROUP BY dat.Codigo, conf.Concepto;


-- AJUSTE CAMBIARIO
INSERT INTO R04.[0424Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 conf.Concepto,
 dat.AjusteCambiario
FROM R04.[0424Datos_2016] dat
INNER JOIN R04.[0424Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'AJCCVIG'
LEFT OUTER JOIN dbo.SICCMX_VW_A0424CreditosTAVEN_2016 taven ON dat.Codigo = taven.Codigo
WHERE taven.Codigo IS NULL AND dat.TipoAlta IS NULL AND dat.TipoBaja IS NULL AND dat.SituacionActual = '1';
GO

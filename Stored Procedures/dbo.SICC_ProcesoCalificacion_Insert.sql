SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ProcesoCalificacion_Insert]
	@Nombre varchar(100),
	@Descripcion varchar(200),
	@Codename varchar(150),
	@IdMetodologia int,
	@Position int,
	@Activo bit
AS
INSERT INTO dbo.SICC_ProcesoCalificacion (
	Nombre,
	Descripcion,
	Codename,
	IdMetodologia,
	Position,
	Activo
) VALUES (
	@Nombre,
	@Descripcion,
	@Codename,
	@IdMetodologia,
	@Position,
	@Activo
)
 
SELECT SCOPE_IDENTITY();
GO

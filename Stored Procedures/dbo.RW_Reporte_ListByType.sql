SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Reporte_ListByType]
	@ReportType VARCHAR(50)
AS
SELECT
	vw.IdReporte,
	vw.GrupoReporte,
	vw.Nombre,
	vw.Descripcion,
	vw.ReglamentoOficial,
	vw.ReglamentoOficialPath,
	vw.Periodicidad,
	vw.NombreTabla,
	vw.SProcGenerate,
	vw.SProcGet,
	vw.LayoutPath,
	vw.NombreSSRS,
	vw.Activo
FROM dbo.RW_VW_Reporte vw
WHERE ( vw.ReglamentoOficial = @ReportType OR @ReportType IS NULL ) AND vw.Activo=1
ORDER BY vw.GrupoReporte,vw.Nombre;
GO

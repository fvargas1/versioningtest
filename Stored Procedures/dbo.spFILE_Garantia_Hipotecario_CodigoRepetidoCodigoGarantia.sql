SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Hipotecario_CodigoRepetidoCodigoGarantia]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_Hipotecario_CodigoRepetidoCodigoGarantia';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Garantia_Hipotecario f
WHERE f.CodigoGarantia IN (
 SELECT f2.CodigoGarantia
 FROM dbo.FILE_Garantia_Hipotecario f2
 GROUP BY f2.CodigoGarantia
 HAVING COUNT(f2.CodigoGarantia) > 1
) AND LEN(ISNULL(f.CodigoGarantia,'')) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoGarantia,
 'CodigoGarantia',
 CodigoGarantia,
 1,
 @Detalle
FROM dbo.FILE_Garantia_Hipotecario f
WHERE LEN(ISNULL(f.CodigoGarantia,'')) > 0
GROUP BY f.CodigoGarantia
HAVING COUNT(f.CodigoGarantia) > 1;
GO

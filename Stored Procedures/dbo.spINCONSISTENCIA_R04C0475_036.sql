SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_036]
AS

BEGIN

-- Si el Puntaje Asignado por la Tasa de Retención Laboral (cve_ptaje_tasa_retenc_laboral) es = 31,
-- entonces la Tasa de Retención Laboral (dat_tasa_retencion_laboral) debe ser >= 0 y < 0.5

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	TasaRetLab,
	P_TasaRetLab AS Puntos_TasaRetLab
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_TasaRetLab,'') = '31' AND (CAST(TasaRetLab AS DECIMAL(10,6)) < 0 OR CAST(TasaRetLab AS DECIMAL(10,6)) >= 0.5);

END


GO

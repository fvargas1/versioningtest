SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_TasaReferencia]
	@filtro VARCHAR(50)
AS
SELECT DISTINCT
	hm.CodigoBanco,
	CAST(cat.Codigo AS INT) AS Codigo,
	cat.Codename,
	cat.Nombre,
	SUM(CASE WHEN hm.CodigoBanco IS NULL THEN 0 ELSE 1 END) OVER() AS vwCB
FROM dbo.SICC_TasaReferencia cat
LEFT OUTER JOIN dbo.FILE_Credito_Homologacion hm ON cat.Codigo = hm.CodigoBW AND hm.Campo = 'TasaReferencia' AND hm.Activo = 1
WHERE ISNULL(hm.CodigoBanco,'')+'|'+ISNULL(cat.Codigo,'')+'|'+ISNULL(cat.Nombre,'')+'|'+ISNULL(cat.Codename,'') LIKE '%' + @filtro + '%'
ORDER BY CAST(cat.Codigo AS INT);
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Archivo_Insert] (
 @Nombre varchar(250) = NULL,
 @Codename varchar(50) = NULL,
 @JobCodename varchar(50) = NULL,
 @ErrorCodename varchar(50) = NULL,
 @LogCodename varchar(50) = NULL,
 @ClearLogCodename varchar(50) = NULL,
 @Position int = NULL,
 @Activo bit = NULL,
 @ViewName varchar(250) = NULL,
 @ValidationStatus varchar(50) = NULL,
 @LastValidationExecuted datetime = NULL,
 @InitCodename varchar(50) = NULL
) 
AS
INSERT INTO dbo.MIGRACION_Archivo (
 Nombre,
 Codename,
 JobCodename,
 ErrorCodename,
 LogCodename,
 ClearLogCodename,
 Position,
 Activo,
 ViewName,
 ValidationStatus,
 LastValidationExecuted, 
 InitCodename
) VALUES (
 @Nombre,
 @Codename,
 @JobCodename,
 @ErrorCodename,
 @LogCodename,
 @ClearLogCodename,
 @Position,
 @Activo,
 @ViewName,
 @ValidationStatus,
 @LastValidationExecuted,
 @InitCodename
)

SELECT SCOPE_IDENTITY();
GO

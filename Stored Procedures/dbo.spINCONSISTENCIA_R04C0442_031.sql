SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_031]
AS

BEGIN

-- La tasa de referencia debe existir en el catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.TasaRef
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_TasaReferencia cat ON ISNULL(r.TasaRef,'') = cat.Codigo
WHERE cat.IdTasaReferencia IS NULL;

END
GO

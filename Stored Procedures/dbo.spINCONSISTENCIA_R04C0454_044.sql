SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_044]
AS
BEGIN
-- Validar que el Monto Bonificado por la Institución Financiera en el Periodo (dat_monto_bonificado) sea MAYOR O IGUAL a cero

SELECT
	CodigoCredito,
	MontoBonificado
FROM dbo.RW_VW_R04C0454_INC
WHERE CAST(ISNULL(NULLIF(MontoBonificado,''),'-1') AS DECIMAL) < 0;

END
GO

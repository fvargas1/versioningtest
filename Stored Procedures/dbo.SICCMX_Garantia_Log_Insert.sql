SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Garantia_Log_Insert]
AS
TRUNCATE TABLE dbo.SICCMX_Garantia_Log;

INSERT INTO dbo.SICCMX_Garantia_Log(IdGarantia, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 IdGarantia,
 GETDATE(),
 '',
 'He : 0.00 % (Factor de Ajuste para el importe de la operación)'
FROM dbo.SICCMX_VW_Garantia
WHERE IdClasificacionNvaMet=1;


INSERT INTO dbo.SICCMX_Garantia_Log(IdGarantia, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 IdGarantia,
 GETDATE(),
 '',
 'Hc : '
 + CAST(CAST(ISNULL(Hc*100, '0.00') AS DECIMAL(18,2)) AS VARCHAR)
 + ' % (Factor de Ajuste correspondiente a la garantía real financiera recibida)'
FROM dbo.SICCMX_VW_Garantia
WHERE IdClasificacionNvaMet=1;


INSERT INTO dbo.SICCMX_Garantia_Log(IdGarantia, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 g.IdGarantia,
 GETDATE(),
 '',
 'Hfx: '
 + CAST(CASE WHEN (SELECT TOP 1 cr.IdMoneda FROM dbo.SICCMX_Credito cr INNER JOIN dbo.SICCMX_CreditoGarantia cg ON cr.IdCredito=cg.IdCredito WHERE cg.IdGarantia=g.IdGarantia) = g.IdMoneda THEN '0.00' ELSE '8.00' END AS VARCHAR)
 + ' % (Factor de Ajuste por diferencia entre las monedas del importe de la exposición y la garantia)'
FROM dbo.SICCMX_VW_Garantia g
WHERE g.IdClasificacionNvaMet=1;


INSERT INTO dbo.SICCMX_Garantia_Log(IdGarantia, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 IdGarantia,
 GETDATE(),
 '',
 'C : $ '
 + CONVERT(VARCHAR, CONVERT(MONEY, ISNULL(ValorGtiaValorizado, '0.00'), 1), 1)
 + ' (Valor contable de la garantía real financiera)'
FROM dbo.SICCMX_VW_Garantia
WHERE IdClasificacionNvaMet=1;



INSERT INTO dbo.SICCMX_Garantia_Log(IdGarantia, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 vw.IdGarantia,
 GETDATE(),
 '',
 'C* : '
 + CAST(CAST(sp.NivelMinimo AS INT) AS VARCHAR)
 + '% (Nivel mínimo de cobertura admisible)'
FROM dbo.SICCMX_VW_Garantia vw
INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON vw.IdTipoGarantia = sp.IdTipoGarantia
WHERE IdClasificacionNvaMet=2;


INSERT INTO dbo.SICCMX_Garantia_Log(IdGarantia, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 vw.IdGarantia,
 GETDATE(),
 '',
 'C** : '
 + CAST(CAST(sp.NivelMaximo AS INT) AS VARCHAR)
 + '% (Nivel de sobre cobertura para reconocer una menor SP)'
FROM dbo.SICCMX_VW_Garantia vw
INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON vw.IdTipoGarantia = sp.IdTipoGarantia
WHERE IdClasificacionNvaMet=2;


INSERT INTO dbo.SICCMX_Garantia_Log(IdGarantia, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 vw.IdGarantia,
 GETDATE(),
 '',
 'SP** : '
 + CAST(CAST(sp.SeveridadPerdida * 100 AS INT) AS VARCHAR)
 + '% (Severidad de la Pérdida mínima correspondiente a C**)'
FROM dbo.SICCMX_VW_Garantia vw
INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON vw.IdTipoGarantia = sp.IdTipoGarantia
WHERE IdClasificacionNvaMet=2;


INSERT INTO dbo.SICCMX_Garantia_Log(IdGarantia, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 IdGarantia,
 GETDATE(),
 '',
 'C : $ '
 + CONVERT(VARCHAR, CONVERT(MONEY, ISNULL(ValorGtiaValorizado, 0.00), 1), 1)
 + ' (Valor contable de la garantía real no financiera)'
FROM dbo.SICCMX_VW_Garantia
WHERE IdClasificacionNvaMet=2;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Group_Upd]
	@IdGroup BAJAWARE_utID,
	@Active BAJAWARE_utActivo,
	@Description BAJAWARE_utDescripcion100,
	@IdGroupType BAJAWARE_utID
AS
UPDATE dbo.BAJAWARE_Group
SET
	Active = @Active,
	Description = @Description,
	Codename= @Description,
	IdGroupType = @IdGroupType
WHERE IdGroup = @IdGroup;
GO

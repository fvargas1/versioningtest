SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_ConsumoAval_errores_ClearLog]
AS
UPDATE dbo.FILE_ConsumoAval
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_ConsumoAval_errores;
GO

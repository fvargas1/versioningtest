SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Movimientos_Valoriza_Montos]
AS
-- VALORIZANDO MONTO DE CREDITOS COMERCIALES
UPDATE mov
SET Monto = f.Monto * tc.Valor
FROM dbo.SICCMX_Credito_Movimientos mov
INNER JOIN dbo.SICCMX_Credito cre ON mov.IdCredito = cre.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tm ON mov.IdTipoMovimiento = tm.IdTipoMovimiento
INNER JOIN dbo.FILE_Movimientos f ON cre.Codigo = LTRIM(f.CodigoCredito) AND tm.Codigo = LTRIM(f.TipoMovimiento)
INNER JOIN dbo.SICC_TipoCambio tc ON cre.IdMoneda = tc.IdMoneda
INNER JOIN dbo.SICC_Periodo prd ON tc.IdPeriodo = prd.IdPeriodo AND prd.Activo = 1
WHERE ISNUMERIC(f.Monto) = 1;

-- VALORIZANDO MONTO DE CREDITOS DE CONSUMO
UPDATE mov
SET Monto = f.Monto * tc.Valor
FROM dbo.SICCMX_Consumo_Movimientos mov
INNER JOIN dbo.SICCMX_Consumo cre ON mov.IdConsumo = cre.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tm ON mov.IdTipoMovimiento = tm.IdTipoMovimiento
INNER JOIN dbo.FILE_Movimientos f ON cre.Codigo = LTRIM(f.CodigoCredito) AND tm.Codigo = LTRIM(f.TipoMovimiento)
INNER JOIN dbo.SICCMX_ConsumoInfo inf ON cre.IdConsumo = inf.IdConsumo
INNER JOIN dbo.SICC_TipoCambio tc ON inf.IdMoneda = tc.IdMoneda
INNER JOIN dbo.SICC_Periodo prd ON tc.IdPeriodo = prd.IdPeriodo AND prd.Activo = 1
WHERE ISNUMERIC(f.Monto) = 1;

-- VALORIZANDO MONTO DE CREDITOS HIPOTECARIOS
UPDATE mov
SET Monto = f.Monto * tc.Valor
FROM dbo.SICCMX_Hipotecario_Movimientos mov
INNER JOIN dbo.SICCMX_Hipotecario cre ON mov.IdHipotecario = cre.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tm ON mov.IdTipoMovimiento = tm.IdTipoMovimiento
INNER JOIN dbo.FILE_Movimientos f ON cre.Codigo = LTRIM(f.CodigoCredito) AND tm.Codigo = LTRIM(f.TipoMovimiento)
INNER JOIN dbo.SICCMX_HipotecarioInfo inf ON cre.IdHipotecario = inf.IdHipotecario
INNER JOIN dbo.SICC_TipoCambio tc ON inf.IdMoneda = tc.IdMoneda
INNER JOIN dbo.SICC_Periodo prd ON tc.IdPeriodo = prd.IdPeriodo AND prd.Activo = 1
WHERE ISNUMERIC(f.Monto) = 1;
GO

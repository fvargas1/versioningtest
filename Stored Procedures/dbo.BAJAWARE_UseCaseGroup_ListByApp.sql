SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCaseGroup_ListByApp]
	@App BAJAWARE_utID
AS
SELECT
IdUseCaseGroup AS id, Active, [Name], IdApplication, SortOrder, StringKey, Description
FROM dbo.BAJAWARE_UseCaseGroup
WHERE IdApplication = @App
ORDER BY SortOrder
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Auditoria_Avales_BANCREA]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Auditoria_Avales_BANCREA);

DELETE FROM Historico.RW_Auditoria_Avales_BANCREA WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Auditoria_Avales_BANCREA (
	IdPeriodoHistorico, [ID], [ID_Linea], [LC], [S_Total], [IDevNC_B], [Estatus_B6], [IDC_C], [N_A_C],
	[ING_C], [PI_C], [IDC_G], [N_A_G], [OS_AG], [ING_G], [GP_PC], [PI_G], [TA_GTE], [B1]
)
SELECT
	@IdPeriodoHistorico,
	[ID],
	[ID_Linea],
	[LC],
	[S_Total],
	[IDevNC_B],
	[Estatus_B6],
	[IDC_C],
	[N_A_C],
	[ING_C],
	[PI_C],
	[IDC_G],
	[N_A_G],
	[OS_AG],
	[ING_G],
	[GP_PC],
	[PI_G],
	[TA_GTE],
	[B1]
FROM dbo.RW_Auditoria_Avales_BANCREA
WHERE IdReporteLog = @IdReporteLog;
GO

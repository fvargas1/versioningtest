SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UserAccessLog_Ins]
	@Username AS VARCHAR(250),
	@FechaAcceso AS DATETIME,
	@Status AS SMALLINT,
	@IPAddress AS VARCHAR(250)
AS
INSERT INTO dbo.BAJAWARE_UserAccesLog (Username, FechaAcceso, [Status], IPAddress)
VALUES (@Username, @FechaAcceso, @Status, @IPAddress);
GO

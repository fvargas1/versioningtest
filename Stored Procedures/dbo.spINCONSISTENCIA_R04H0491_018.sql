SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_018]
AS

BEGIN

-- La "FECHA DE OTORGAMIENTO DEL CRÉDITO" deberá anotarse cero (0) si y solo si "Tipo de Alta del Crédito" es igual 3 (columna 9).

SELECT CodigoCredito, CodigoCreditoCNBV, FechaOtorgamiento, TipoAlta
FROM dbo.RW_R04H0491
WHERE FechaOtorgamiento = '0' AND TipoAlta <> '3';

END
GO

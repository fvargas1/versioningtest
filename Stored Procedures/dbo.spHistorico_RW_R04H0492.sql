SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04H0492]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04H0492);

DELETE FROM Historico.RW_R04H0492 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04H0492 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, NumeroAvaluo, DenominacionCredito, SaldoPrincipalInicio,
	TasaInteres, ComisionesCobradasTasa, ComisionesCobradasMonto, MontoPagoExigible, MontoPagoRealizado, MontoBonificacion, SaldoPrincipalFinal,
	ResponsabilidadTotal, FechaUltimoPago, SituacionCredito, [PI], SP, DiasAtraso, MAXATR, PorVPAGO, SUBCV, ConvenioJudicial, PorCobPaMed, PorCobPP,
	EntidadCobertura, ReservasSaldoFinal, PerdidaEsperada, ReservaPreventiva, PIInterna, SPInterna, EInterna, PerdidaEsperadaInterna
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	DenominacionCredito,
	SaldoPrincipalInicio,
	TasaInteres,
	ComisionesCobradasTasa,
	ComisionesCobradasMonto,
	MontoPagoExigible,
	MontoPagoRealizado,
	MontoBonificacion,
	SaldoPrincipalFinal,
	ResponsabilidadTotal,
	FechaUltimoPago,
	SituacionCredito,
	[PI],
	SP,
	DiasAtraso,
	MAXATR,
	PorVPAGO,
	SUBCV,
	ConvenioJudicial,
	PorCobPaMed,
	PorCobPP,
	EntidadCobertura,
	ReservasSaldoFinal,
	PerdidaEsperada,
	ReservaPreventiva,
	PIInterna,
	SPInterna,
	EInterna,
	PerdidaEsperadaInterna
FROM dbo.RW_R04H0492
WHERE IdReporteLog = @IdReporteLog;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ConsumoReservas_Select]
	@IdConsumo BIGINT
AS
SELECT
	vw.IdConsumo,
	vw.FechaCalificacion,
	vw.IdCalificacionExpuesto,
	vw.IdCalificacionExpuestoNombre,
	vw.MontoExpuesto,
	vw.ReservaExpuesto,
	vw.PorcentajeExpuesto,
	vw.IdCalificacionCubierto,
	vw.IdCalificacionCubiertoNombre,
	vw.MontoCubierto,
	vw.ReservaCubierto,
	vw.PorcentajeCubierto,
	vw.IdCalificacionAdicional,
	vw.IdCalificacionAdicionalNombre,
	vw.MontoAdicional,
	vw.ReservaAdicional,
	vw.PorcentajeAdicional
FROM dbo.SICCMX_VW_ConsumoReservas vw
WHERE vw.IdConsumo = @IdConsumo;
GO

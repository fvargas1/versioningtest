SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0419Datos_AjusteCambiario_2016]
AS
UPDATE dat
SET AjusteCambiario = ISNULL(((CASE WHEN dat.Moneda = '0' THEN dat.SaldoActual ELSE dat.SaldoActualOrigen END * tcHst.Valor) - dat.SaldoActual) * dat.PrctReservasHistorico,0)
FROM R04.[0419Datos_2016] dat
INNER JOIN dbo.SICC_Moneda mon ON dat.Moneda = mon.Codigo
LEFT OUTER JOIN (
 SELECT mon1.Codigo AS Moneda, tc1.Valor
 FROM dbo.SICC_TipoCambio tc1
 INNER JOIN dbo.SICC_Moneda mon1 ON tc1.IdMoneda = mon1.IdMoneda
 INNER JOIN dbo.SICC_Periodo prdAct ON prdAct.Activo = 1
 INNER JOIN dbo.SICC_Periodo prdHst ON tc1.IdPeriodo = prdHst.IdPeriodo AND YEAR(prdHst.Fecha) = YEAR(DATEADD(MONTH, -1, prdAct.Fecha)) AND MONTH(prdHst.Fecha) = MONTH(DATEADD(MONTH, -1, prdAct.Fecha))
) AS tcHst ON dat.Moneda = tcHst.Moneda
WHERE mon.CodigoISO <> 'MXN';
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_044]
AS

BEGIN

-- Fecha de firma de la reestructura del crédito es cero (0) si y solo si el crédito no es reestructurado ("Tipo de Alta del Crédito" distinto de 3 en columna 9).

SELECT CodigoCredito, CodigoCreditoCNBV, FechaFirmaReestructura, TipoAlta
FROM dbo.RW_R04H0491
WHERE FechaFirmaReestructura = '0' AND TipoAlta = '3';

END
GO

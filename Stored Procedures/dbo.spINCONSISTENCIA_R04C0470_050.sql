SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_050]
AS

BEGIN

-- Validar que el resultado de la siguiente operación
-- (Ventas totales Anuales(dat_ventas_tot_anuales)/Activo Total Anual(dat_activo_tot_anual))
-- sea igual al dato reportado en la Rotación de Activos Totales (dat_rotac_activos_tot)

SELECT
 CodigoPersona AS CodigoDeudor,
 REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
 VentasNetasTotales,
 ActivoTotalAnual,
 CAST(CAST(VentasNetasTotales AS DECIMAL) / CAST(ActivoTotalAnual AS DECIMAL) AS DECIMAL(18,2)) AS [VentasNetasTotales/ActivoTotalAnual],
 RotActTot
FROM dbo.RW_VW_R04C0470_INC
WHERE CAST(CASE WHEN CAST(ActivoTotalAnual AS DECIMAL) = 0 THEN -999 ELSE CAST(VentasNetasTotales AS DECIMAL) / CAST(ActivoTotalAnual AS DECIMAL) END AS DECIMAL(18,2)) <> CAST(RotActTot AS DECIMAL(18,2));

END


GO

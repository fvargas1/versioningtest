SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0420Datos_CalculandoConceptos]
AS
DECLARE @PeriodoAnterior INT;

SELECT @PeriodoAnterior = ant.IdPeriodo
FROM dbo.SICC_Periodo act
INNER JOIN dbo.SICC_Periodo ant ON YEAR(ant.Fecha) = YEAR(DATEADD(MONTH, -1, act.Fecha)) AND MONTH(ant.Fecha) = MONTH(DATEADD(MONTH, -1, act.Fecha))
WHERE act.Activo = 1;


TRUNCATE TABLE R04.[0420Conceptos];

/* Creditos de comerciales
En este insert se espera que solo exista un movto taven o tavig.
Los movimientos de creditos con movto taven o tavig se insertan mas abajo*/
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 cr.Codigo,
 conf.Concepto,
 SUM(mov.Monto)
FROM dbo.SICCMX_Credito_Movimientos mov
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = mov.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0420Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0420CreditosMovimientos_PosteriorTaven_Suma taven ON taven.Codigo = cr.Codigo AND taven.Monto > 0
LEFT OUTER JOIN dbo.SICC_VW_R04A0420CreditosMovimientos_AnteriorTavig_Suma tavig ON cr.Codigo = tavig.Codigo AND tavig.Monto > 0
WHERE taven.Codigo IS NULL AND tavig.Codigo IS NULL AND conf.TipoMovimiento NOT IN ('TAVEN','TAVIG')
GROUP BY cr.Codigo, conf.Concepto

UNION ALL
-- Pagos creditos de comerciales de creditos con movto taven
SELECT
 dat.Codigo,
 taven.Concepto,
 SUM(taven.Monto)
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICC_VW_R04A0420CreditosMovimientos_PosteriorTaven_Suma taven ON taven.Codigo = dat.Codigo
WHERE taven.Monto <> 0
GROUP BY dat.Codigo, taven.Concepto

UNION ALL
-- Pagos creditos de comerciales con movto tavig
SELECT
 dat.Codigo,
 pagos.Concepto,
 SUM(pagos.Monto)
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICC_VW_R04A0420CreditosMovimientos_AnteriorTavig_Pagos pagos ON pagos.Codigo = dat.Codigo
WHERE pagos.Monto <> 0
GROUP BY dat.Codigo, pagos.Concepto;


-- Insert traspaso tavencredito
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 cr.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Credito_Movimientos mov
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = mov.IdCredito 
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento AND tmov.Codigo = 'TAVEN'
INNER JOIN R04.[0420Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE datos.SituacionActual='2' -- Credito actual vencido
 AND datos.SituacionHistorica='1' -- Historico vigente

UNION ALL
-- Movimientos tavig comercial
SELECT
 cr.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Credito_Movimientos mov
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = mov.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0420Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE tmov.Codigo = 'TAVIG';


-- Insert traspaso tavencredito por reestructuras de vigente a vencido
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 datos.Codigo,
 conf.Concepto,
 datos.SaldoActual
FROM R04.[0420Datos] datos
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = 'TAVEN'
WHERE datos.SituacionActual = '2' -- Credito actual vencido
 AND datos.SituacionHistorica IS NULL -- no existia
 AND datos.Reestructurado = 1;


/* Creditos de consumo
En este insert se espera que solo exista un movto taven o tavig.
Los movimientos de creditos con movto taven o tavig se insertan mas abajo*/
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)SELECT
 cr.Codigo,
 conf.Concepto,
 SUM(mov.Monto)
FROM dbo.SICCMX_Consumo_Movimientos mov
INNER JOIN dbo.SICCMX_Consumo cr ON cr.IdConsumo = mov.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0420Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0420ConsumoMovimientos_PosteriorTaven_Suma taven ON taven.Codigo = cr.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0420ConsumoMovimientos_AnteriorTavig_Suma tavig ON cr.Codigo = tavig.Codigo
WHERE taven.Codigo IS NULL AND tavig.Codigo IS NULL AND conf.TipoMovimiento NOT IN ('TAVEN','TAVIG')
GROUP BY cr.Codigo, conf.Concepto

-- Pagos creditos de consumo
UNION ALL
SELECT
 dat.Codigo,
 taven.Concepto,
 SUM(taven.Monto)
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICC_VW_R04A0420ConsumoMovimientos_PosteriorTaven_Suma taven ON taven.Codigo = dat.Codigo
WHERE taven.Monto <> 0
GROUP BY dat.Codigo, taven.Concepto

UNION ALL
-- Pagos creditos de Consumo con movto tavig
SELECT
 dat.Codigo,
 pagos.Concepto,
 SUM(pagos.Monto)
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICC_VW_R04A0420ConsumoMovimientos_AnteriorTavig_Pagos pagos ON pagos.Codigo = dat.Codigo
GROUP BY dat.Codigo, pagos.Concepto;


-- Insert tavenconsumo
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 datos.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Consumo_Movimientos mov
INNER JOIN dbo.SICCMX_Consumo cr ON cr.IdConsumo = mov.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento AND tmov.Codigo='TAVEN'
INNER JOIN R04.[0420Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE datos.SituacionActual = '2' -- Credito actual vencido
 AND datos.SituacionHistorica = '1' -- Historico vigente

UNION ALL
-- Insert de movimientos tavig
SELECT
 cr.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Consumo_Movimientos mov
INNER JOIN dbo.SICCMX_Consumo cr ON cr.IdConsumo = mov.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0420Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE tmov.Codigo = 'TAVIG';


/* Creditos hipotecarios
En este insert se espera que solo exista un movto taven o tavig.
Los movimientos de creditos con movto taven o tavig se insertan mas abajo*/
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 cr.Codigo,
 conf.Concepto,
 SUM(mov.Monto)
FROM dbo.SICCMX_Hipotecario_Movimientos mov
INNER JOIN dbo.SICCMX_Hipotecario cr ON cr.IdHipotecario = mov.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0420Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0420HipotecarioMovimientos_PosteriorTaven_Suma taven ON taven.Codigo = cr.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0420HipotecarioMovimientos_AnteriorTavig_Suma tavig ON cr.Codigo = tavig.Codigo
WHERE taven.Codigo IS NULL AND tavig.Codigo IS NULL AND conf.TipoMovimiento NOT IN ('TAVEN','TAVIG')
GROUP BY cr.Codigo, conf.Concepto;


-- Pagos creditos hipotecarios
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 taven.Concepto,
 SUM(taven.Monto)
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICC_VW_R04A0420HipotecarioMovimientos_PosteriorTaven_Suma taven ON taven.Codigo = dat.Codigo
WHERE taven.Monto <> 0
GROUP BY dat.Codigo, taven.Concepto

UNION ALL
-- Pagos creditos Hipotecario con movto tavig
SELECT
 dat.Codigo,
 pagos.Concepto,
 SUM(pagos.Monto)
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICC_VW_R04A0420HipotecarioMovimientos_AnteriorTavig_Pagos pagos ON pagos.Codigo = dat.Codigo
GROUP BY dat.Codigo, pagos.Concepto;


-- Insert tavenHipotecario
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 datos.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Hipotecario_Movimientos mov
INNER JOIN dbo.SICCMX_Hipotecario cr ON cr.IdHipotecario = mov.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON info.IdHipotecario = cr.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento AND tmov.Codigo='TAVEN'
INNER JOIN R04.[0420Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE datos.SituacionActual = '2' -- Credito actual vencido
 AND datos.SituacionHistorica = '1' -- Historico vigente 

UNION ALL
-- Traspaso tavig
SELECT
 cr.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Hipotecario_Movimientos mov
INNER JOIN dbo.SICCMX_Hipotecario cr ON cr.IdHipotecario = mov.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0420Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE tmov.Codigo = 'TAVIG';


-- Valorizamos todos los montos anteriores
UPDATE conceptos
SET conceptos.Monto = conceptos.Monto * tc.Valor
FROM R04.[0420Conceptos] conceptos
INNER JOIN R04.[0420Datos] dat ON dat.Codigo = conceptos.Codigo
INNER JOIN dbo.SICC_Moneda mon ON dat.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo p ON p.IdPeriodo = tc.IdPeriodo AND p.Activo = 1
WHERE conceptos.Concepto NOT LIKE '856%'; -- El ajuste cambiario ya esta valorizado


-- Intereses devengados
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 datos.Codigo,
 CASE
 WHEN datos.CodigoProducto LIKE '0101%' THEN '854004101100'
 WHEN datos.CodigoProducto LIKE '0102%' THEN '854004102100'
 WHEN datos.CodigoProducto LIKE '0103%' THEN '854004103100'
 WHEN datos.CodigoProducto LIKE '02%' THEN '854004200000'
 WHEN datos.CodigoProducto LIKE '03%' THEN '854004300000'
 END Concepto,
 ISNULL(adi.InteresDevengadoValorizado,0)
FROM R04.[0420Datos] datos
INNER JOIN dbo.SICCMX_Credito cr ON datos.Codigo = cr.Codigo
INNER JOIN dbo.SICCMX_VW_CreditoAdicional adi ON cr.IdCredito = adi.IdCredito
WHERE datos.SituacionActual ='2' AND ISNULL(adi.InteresDevengadoValorizado,0) > 0

UNION ALL
SELECT
 datos.Codigo,
 CASE
 WHEN datos.CodigoProducto LIKE '0101%' THEN '854004101100'
 WHEN datos.CodigoProducto LIKE '0102%' THEN '854004102100'
 WHEN datos.CodigoProducto LIKE '0103%' THEN '854004103100'
 WHEN datos.CodigoProducto LIKE '02%' THEN '854004200000'
 WHEN datos.CodigoProducto LIKE '03%' THEN '854004300000'
 END Concepto,
 ISNULL(adi.InteresDevengadoValorizado,0)
FROM R04.[0420Datos] datos
INNER JOIN dbo.SICCMX_Consumo cr ON datos.Codigo = cr.Codigo
INNER JOIN dbo.SICCMX_VW_ConsumoAdicional adi ON cr.IdConsumo = adi.IdConsumo
WHERE datos.SituacionActual ='2' AND ISNULL(adi.InteresDevengadoValorizado,0) > 0

UNION ALL
SELECT
 datos.Codigo,
 CASE
 WHEN datos.CodigoProducto LIKE '0101%' THEN '854004101100'
 WHEN datos.CodigoProducto LIKE '0102%' THEN '854004102100'
 WHEN datos.CodigoProducto LIKE '0103%' THEN '854004103100'
 WHEN datos.CodigoProducto LIKE '02%' THEN '854004200000'
 WHEN datos.CodigoProducto LIKE '03%' THEN '854004300000'
 END Concepto,
 ISNULL(adi.InteresesDevengadosValorizado,0)
FROM R04.[0420Datos] datos
INNER JOIN dbo.SICCMX_Hipotecario cr ON datos.Codigo = cr.Codigo
INNER JOIN dbo.SICCMX_VW_HipotecarioAdicional adi ON cr.IdHipotecario = adi.IdHipotecario
WHERE datos.SituacionActual ='2' AND ISNULL(adi.InteresesDevengadosValorizado,0) > 0;


/*Cancelacion de Creditos*/
UPDATE R04.[0420Conceptos]
SET Monto = CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(datos.SaldoHistorico,0) AS DECIMAL) + CAST(ISNULL(datos.InteresVencidoHistorico,0) AS DECIMAL) ELSE ISNULL(datos.SaldoHistorico,0) + ISNULL(datos.InteresVencidoHistorico,0) END
FROM R04.[0420Conceptos]con
INNER JOIN R04.[0420Datos] datos ON con.Codigo = datos.Codigo
INNER JOIN R04.[0420Configuracion] conf ON con.Concepto = conf.Concepto AND conf.TipoMovimiento = 'CAST'
INNER JOIN dbo.SICCMX_Credito cr ON cr.Codigo = datos.Codigo
INNER JOIN SICCMX_CreditoInfo info ON info.IdCredito = cr.IdCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cr.IdMoneda = mon.IdMoneda
WHERE datos.SituacionActual = '2' --AND info.IdTipoBaja=5 --Bonificación, Quita o Castigo
 AND ISNULL(datos.SaldoHistorico,0) + ISNULL(datos.InteresVencidoHistorico,0) > 0
 AND ISNULL(datos.SaldoActual,0) = 0;


/*Cancelacion de Creditos que no haya subido sistemas el movimiento*/
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 datos.Codigo,
 conf.Concepto,
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(datos.SaldoHistorico,0) AS DECIMAL) + CAST(ISNULL(datos.InteresVencidoHistorico,0) AS DECIMAL) ELSE ISNULL(datos.SaldoHistorico,0) + ISNULL(datos.InteresVencidoHistorico,0) END
FROM R04.[0420Datos] datos
INNER JOIN R04.[0420Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = 'CAST'
INNER JOIN dbo.SICCMX_Credito cr ON cr.Codigo = datos.Codigo
INNER JOIN SICCMX_CreditoInfo info ON info.IdCredito = cr.IdCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cr.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN R04.[0420Conceptos] con ON con.Codigo = datos.Codigo
WHERE datos.SituacionActual = '2' --AND info.IdTipoBaja=5 --Bonificación, Quita o Castigo
 AND ISNULL(datos.SaldoHistorico,0) + ISNULL(datos.InteresVencidoHistorico,0) > 0
 AND ISNULL(datos.SaldoActual,0) = 0 AND con.Codigo IS NULL;

/*** Ajuste Cambiario DLLR/UDIS***/
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '856010110000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '856010210000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '856010310000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '856020000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '856030000000'
 END ConceptoActual,
 ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencidaHistorico,0)) * tc.Valor) -
 ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencidaHistorico,0)) * tch.Valor) AS AjusteCambiario
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = dat.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo per ON per.IdPeriodo = tc.IdPeriodo AND per.Activo = 1
INNER JOIN dbo.SICC_TipoCambio tch ON tch.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo perh ON perh.IdPeriodo = tch.IdPeriodo AND perh.IdPeriodo = @PeriodoAnterior
WHERE dat.CodigoProducto IS NOT NULL
 AND ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencidaHistorico,0)) * tc.Valor) -
 ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencidaHistorico,0)) * tch.Valor) <> 0
 AND mon.Codigo <> '0'
 AND dat.SituacionActual = '2' AND dat.SituacionHistorica = '2';


/*** Ajuste Cambiario para creditos vencidos liquidados por pago DLLR/UDIS***/
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '856010110000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '856010210000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '856010310000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '856020000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '856030000000'
 END ConceptoActual,
 ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencidaHistorico,0)) * tc.Valor) -
 ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencidaHistorico,0)) * tch.Valor) AS AjusteCambiario
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = dat.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo per ON per.IdPeriodo = tc.IdPeriodo AND per.Activo = 1
INNER JOIN dbo.SICC_TipoCambio tch ON tch.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo perh ON perh.IdPeriodo = tch.IdPeriodo AND perh.IdPeriodo = @PeriodoAnterior
WHERE dat.CodigoProducto IS NOT NULL
 AND ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencidaHistorico,0)) * tc.Valor) -
 ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencidaHistorico,0)) * tch.Valor) <> 0
 AND mon.Codigo <> '0' AND ISNULL(dat.SaldoActual,0) = 0
 AND dat.SituacionActual = '1' AND dat.SituacionHistorica = '2';


-- Saldos Historicos
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '851011000000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '851021000000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '851031000000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '852000000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '853000000000'
 END ConceptoHistorico, 
 CASE WHEN mon.CodigoISO = 'MXN'
 THEN CAST((CAST(ISNULL(dat.SaldoHistorico,0) AS DECIMAL) + CAST(ISNULL(InteresVigenteHistorico,0) AS DECIMAL) + CAST(ISNULL(InteresVencidoHistorico,0) AS DECIMAL)) * tc.Valor AS DECIMAL)
 ELSE CAST((ISNULL(dat.SaldoHistorico,0) + ISNULL(InteresVigenteHistorico,0) + ISNULL(InteresVencidoHistorico,0)) * tc.Valor AS DECIMAL)
 END AS SaldoHistorico
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = dat.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN R04.A0420CreditosTAVEN taven ON taven.Codigo = dat.Codigo
WHERE tc.IdPeriodo = @PeriodoAnterior
 AND dat.CodigoProducto IS NOT NULL
 AND taven.Codigo IS NULL -- Omitir los saldos historicos taven
 AND SituacionHistorica = '2'

UNION ALL
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '861011000000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '861021000000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '861031000000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '862000000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '863000000000'
 END ConceptoActual,
 CASE WHEN mon.CodigoISO = 'MXN'
 THEN CAST((CAST(ISNULL(dat.SaldoActual,0) AS DECIMAL) + CAST(ISNULL(dat.InteresVencido,0) AS DECIMAL) + CAST(ISNULL(dat.InteresVigente,0) AS DECIMAL)) * tc.Valor AS DECIMAL)
 ELSE CAST((ISNULL(dat.SaldoActual,0) + ISNULL(dat.InteresVencido,0) + ISNULL(dat.InteresVigente,0)) * tc.Valor AS DECIMAL)
 END AS SaldoActual
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = dat.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo per ON per.IdPeriodo = tc.IdPeriodo AND per.Activo = 1
WHERE dat.CodigoProducto IS NOT NULL AND dat.SituacionActual = '2';


-- Los creditos TAVIG deben tener saldo cero en el reporte 420
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '861011000000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '861021000000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '861031000000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '862000000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '863000000000'
 END ConceptoActual,
 0 SaldoActual
FROM R04.[0420Datos] dat
WHERE dat.CodigoProducto IS NOT NULL AND dat.SituacionActual = '1' AND dat.SituacionHistorica = '2';


/*Insertamos almenos un registro en cero como saldo final si es que no
hay ningun registro en 420Conceptos que no sea saldo historico en cero*/
INSERT INTO R04.[0420Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '861011000000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '861021000000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '861031000000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '862000000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '863000000000'
 END ConceptoActual,
 0 SaldoActual
FROM R04.[0420Datos] dat
WHERE dat.CodigoProducto IS NOT NULL
 AND (SELECT COUNT(Codigo) FROM R04.[0420Conceptos] WHERE Concepto LIKE '86%') = 0;
GO

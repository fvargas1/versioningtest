SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ConsumoGarantia_Back_Hst]
AS

INSERT INTO dbo.FILE_ConsumoGarantia_Hst (
	CodigoGarantia,
	CodigoCredito,
	Porcentaje
)
SELECT
	cg.CodigoGarantia,
	cg.CodigoCredito,
	cg.Porcentaje
FROM dbo.FILE_ConsumoGarantia cg
LEFT OUTER JOIN dbo.FILE_ConsumoGarantia_Hst hst ON LTRIM(cg.CodigoGarantia) = LTRIM(hst.CodigoGarantia) AND LTRIM(cg.CodigoCredito) = LTRIM(hst.CodigoCredito)
WHERE hst.CodigoGarantia IS NULL;
GO

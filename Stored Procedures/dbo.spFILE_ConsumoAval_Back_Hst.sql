SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ConsumoAval_Back_Hst]
AS

INSERT INTO dbo.FILE_ConsumoAval_Hst (
	CodigoAval,
	CodigoCredito,
	Porcentaje
)
SELECT
	cg.CodigoAval,
	cg.CodigoCredito,
	cg.Porcentaje
FROM dbo.FILE_ConsumoAval cg
LEFT OUTER JOIN dbo.FILE_ConsumoAval_Hst hst ON LTRIM(cg.CodigoAval) = LTRIM(hst.CodigoAval) AND LTRIM(cg.CodigoCredito) = LTRIM(hst.CodigoCredito)
WHERE hst.CodigoAval IS NULL;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_060_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El campo "SEGUROS A CARGO DEL ACREDITADO" debe ser un código valido del catálogo disponible en el SITI.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.IdReporteLog)
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_SeguroAcreditado cat ON ISNULL(r.SeguroAcreditado,'') = cat.CodigoCNBV
WHERE cat.IdSeguroAcreditado IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

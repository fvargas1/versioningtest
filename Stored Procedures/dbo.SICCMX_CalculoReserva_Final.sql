SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoReserva_Final]
AS
UPDATE dbo.SICCMX_Credito_Reservas_Variables SET ReservaFinal = ReservaCubierta_GarPer + ReservaExpuesta_GarPer;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCase_Del]
	@IdUseCase BAJAWARE_utID
AS
DELETE FROM dbo.BAJAWARE_UseCase WHERE IdUseCase=@IdUseCase;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Hipotecario_Movimientos]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Hipotecario_Movimientos WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Hipotecario_Movimientos (
	IdPeriodoHistorico,
	Hipotecario,
	TipoMovimiento,
	Monto,
	Fecha,
	CalificacionAfecto,
	Concepto0419,
	Concepto0420,
	Concepto0424
)
SELECT
	@IdPeriodoHistorico,
	hip.Codigo,
	tm.Codigo,
	cm.Monto,
	cm.Fecha,
	cm.CalificacionAfecto,
	cm.Concepto0419,
	cm.Concepto0420,
	cm.Concepto0424
FROM dbo.SICCMX_Hipotecario_Movimientos cm
INNER JOIN dbo.SICCMX_Hipotecario hip ON cm.IdHipotecario = hip.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tm ON cm.IdTipoMovimiento = tm.IdTipoMovimiento;
GO

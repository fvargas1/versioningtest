SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_024_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Para créditos nuevos y cartas de crédito la fecha de otorgamiento dentro del ID MET CNBV debe ser igual al periodo que se reporta

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE MontoOriginal = '0' AND TipoAlta <> '3';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_013_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por Días atrasados Infonavit en el último bimestre (cve_ptaje_dias_atra_infonavit) es = 32,
-- entonces los Días atrasados Infonavit en el último bimestre (dat_dias_mora_infonavit) debe ser >= 7.33 y < 52

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_DiasAtrInfonavit,'') = '32' AND (CAST(DiasAtrInfonavit AS DECIMAL(10,6)) < 7.33 OR CAST(DiasAtrInfonavit AS DECIMAL(10,6)) >= 52);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO

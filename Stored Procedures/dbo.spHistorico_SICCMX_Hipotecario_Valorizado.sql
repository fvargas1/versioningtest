SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Hipotecario_Valorizado]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Hipotecario_Valorizado WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Hipotecario_Valorizado (
	IdPeriodoHistorico,
	Hipotecario,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	MontoGarantia,
	InteresCarteraVencida
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	Codigo,
	SaldoCapitalVigenteValorizado,
	InteresVigenteValorizado,
	SaldoCapitalVencidoValorizado,
	InteresVencidoValorizado,
	MontoGarantiaValorizado,
	InteresCarteraVencidaValorizado
FROM dbo.SICCMX_VW_Hipotecario;
GO

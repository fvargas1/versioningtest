SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Get_Status_Historicos]
AS
DECLARE @Status INT;
DECLARE @IdPeriodoActivo INT;
DECLARE @NumProcesos INT;
DECLARE @NumHistoricos INT;
DECLARE @FechaUltimaCalificacion DATETIME;
DECLARE @FechaUltimoHistorico DATETIME;

SET @Status = 0;
SELECT @IdPeriodoActivo = IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;

SELECT @NumProcesos = COUNT(IdProcesoLog)
FROM dbo.SICCMX_ProcesoLog
WHERE Categoria = 'calificacion' AND IdPeriodo = @IdPeriodoActivo;

IF @NumProcesos = 0
BEGIN
SET @Status = 1;
END
ELSE
BEGIN

SELECT @NumHistoricos = COUNT(per.IdPeriodo)
FROM dbo.SICC_Periodo per
INNER JOIN dbo.SICC_PeriodoHistorico hst ON per.Fecha = hst.Fecha
WHERE per.Activo = 1

IF @NumHistoricos = 0
BEGIN
	SET @Status = 2;
END
ELSE
BEGIN
	SELECT @FechaUltimaCalificacion = MAX(Fecha)
	FROM dbo.SICCMX_ProcesoLog
	WHERE Categoria = 'calificacion' AND IdPeriodo = @IdPeriodoActivo;
	
	SELECT @FechaUltimoHistorico = MAX(hst.FechaCreacion)
	FROM dbo.SICC_Periodo per
	INNER JOIN dbo.SICC_PeriodoHistorico hst ON per.Fecha = hst.Fecha
	WHERE per.Activo = 1;
	
	IF @FechaUltimoHistorico < @FechaUltimaCalificacion
	BEGIN
	SET @Status = 3;
	END

END

END

SELECT @Status AS Estatus;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_EntidadColumns_SelectIdBusqueda]
	@IdEntidad INT
AS
SELECT
	IdEntidadColumns,
	Nombre,
	Descripcion,
	IdEntidad,
	IdEntidadNombre,
	NombreCampo,
	TipoDato,
	IsRequerido,
	IsKey,
	IsIdentidadBusqueda,
	IdCatalogo,
	IdCatalogoNombre,
	NombreIdCatalogo
FROM dbo.MIGRACION_VW_EntidadColumna
WHERE IdEntidad = @IdEntidad AND IsIdentidadBusqueda = 1
ORDER BY Nombre ASC;
GO

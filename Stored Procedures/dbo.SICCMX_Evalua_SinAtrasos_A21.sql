SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Evalua_SinAtrasos_A21]
AS
UPDATE anx
SET SinAtrasos = 0
FROM dbo.SICCMX_Anexo21 anx
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON anx.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo info ON cre.IdCredito = info.IdCredito
WHERE anx.SinAtrasos = 1 AND info.DiasVencidos > 0 AND cre.MontoValorizado > 0;

INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT anx.IdPersona, GETDATE(), '', 'El acreditado cuenta con créditos con Días Vencidos.'
FROM dbo.SICCMX_Anexo21 anx
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON anx.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo info ON cre.IdCredito = info.IdCredito
WHERE info.DiasVencidos > 0 AND cre.MontoValorizado > 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoReserva_Expuesta]
AS
UPDATE crv
SET ReservaExpuesta = can.Reserva
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Garantia_Canasta can ON crv.IdCredito = can.IdCredito
WHERE can.EsDescubierto=1;

UPDATE dbo.SICCMX_Credito_Reservas_Variables
SET ReservaExpuesta_GarPer = CASE WHEN ExpProyectada > 0 THEN EI_Total ELSE EI_Expuesta_GarPer END * PI_Expuesta * SP_Expuesta;
GO

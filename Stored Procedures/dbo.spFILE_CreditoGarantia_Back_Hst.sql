SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_CreditoGarantia_Back_Hst]
AS

INSERT INTO dbo.FILE_CreditoGarantia_Hst (
	CodigoGarantia,
	CodigoCredito,
	Porcentaje
)
SELECT
	cg.CodigoGarantia,
	cg.CodigoCredito,
	cg.Porcentaje
FROM dbo.FILE_CreditoGarantia cg
LEFT OUTER JOIN dbo.FILE_CreditoGarantia_Hst hst ON LTRIM(cg.CodigoCredito) = LTRIM(hst.CodigoCredito) AND LTRIM(cg.CodigoGarantia) = LTRIM(hst.CodigoGarantia)
WHERE hst.CodigoGarantia IS NULL;
GO

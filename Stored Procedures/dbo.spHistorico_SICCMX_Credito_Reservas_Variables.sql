SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Credito_Reservas_Variables]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Credito_Reservas_Variables WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Credito_Reservas_Variables (
	IdPeriodoHistorico,
	Credito,
	ExpProyectada,
	PI_Total,
	PI_Expuesta,
	PI_Cubierta,
	SP_Total,
	SP_Expuesta,
	SP_Cubierta,
	EI_Total,
	EI_Expuesta,
	EI_Cubierta,
	ReservaFinal,
	ReservaExpuesta,
	ReservaCubierta,
	PorReservaFinal,
	PorCubierto,
	PorExpuesta,
	CalifFinal,
	CalifCubierta,
	CalifExpuesta,
	PIAval,
	ReservaFinalOriginal,
	ReservaExpuestaOriginal,
	ReservaCubiertaOriginal,
	EI_Expuesta_GarPer,
	EI_Cubierta_GarPer,
	ReservaExpuesta_GarPer,
	ReservaCubierta_GarPer,
	ECubiertaPP,
	ReservaCubPP,
	PorCubiertoPP,
	MontoAdicional,
	ReservaAdicional,
	PorcentajeAdicional,
	CalificacionAdicional
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	cre.Codigo, -- Credito
	crv.ExpProyectada,
	crv.PI_Total,
	crv.PI_Expuesta,
	crv.PI_Cubierta,
	crv.SP_Total,
	crv.SP_Expuesta,
	crv.SP_Cubierta,
	crv.EI_Total,
	crv.EI_Expuesta,
	crv.EI_Cubierta,
	crv.ReservaFinal,
	crv.ReservaExpuesta,
	crv.ReservaCubierta,
	crv.PorReservaFinal,
	crv.PorCubierto,
	crv.PorExpuesta,
	cTot.Codigo, -- CalifFinal
	cCub.Codigo, -- CalifCubierta
	cExp.Codigo, -- CalifExpuesta
	crv.PIAval,
	crv.ReservaFinalOriginal,
	crv.ReservaExpuestaOriginal,
	crv.ReservaCubiertaOriginal,
	crv.EI_Expuesta_GarPer,
	crv.EI_Cubierta_GarPer,
	crv.ReservaExpuesta_GarPer,
	crv.ReservaCubierta_GarPer,
	crv.ECubiertaPP,
	crv.ReservaCubPP,
	crv.PorCubiertoPP,
	crv.MontoAdicional,
	crv.ReservaAdicional,
	crv.PorcentajeAdicional,
	cAdi.Codigo
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Credito cre ON crv.IdCredito = cre.IdCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cTot ON crv.CalifFinal = cTot.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cCub ON crv.CalifCubierta = cCub.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cExp ON crv.CalifExpuesta = cExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cAdi ON crv.IdCalificacionAdicional = cAdi.IdCalificacion;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Hipotecario_Select]
 @IdHipotecario BIGINT
AS
SELECT
 vw.IdHipotecario,
 vw.IdPersona,
 vw.NombrePersona AS IdPersonaNombre,
 vw.Codigo,
 vw.SaldoCapitalVigente,
 vw.InteresVigente,
 vw.SaldoCapitalVencido,
 vw.InteresVencido,
 NULL AS PeriodosIncumplimiento,
 NULL AS IdTipoCreditoHipotecario,
 NULL AS IdTipoCreditoHipotecarioNombre,
 NULL AS Restructurado,
 NULL AS InteresRefinanciados,
 NULL AS TasaVariable,
 NULL AS Porcentaje,
 NULL AS PeriodosAjuste,
 NULL AS Garantia,
 ISNULL(vw.MontoGarantia,0) AS MontoGarantia,
 NULL AS CarteraVencida,
 NULL AS TipoCartera,
 hg.MontoUsadoGarantia,
 tg.Nombre AS TipoGarantia
FROM dbo.SICCMX_VW_Hipotecario vw
LEFT OUTER JOIN dbo.SICCMX_HipotecarioGarantia hg ON vw.IdHipotecario = hg.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_Garantia_Hipotecario gar ON hg.IdGarantia = gar.IdGarantia
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON gar.IdTipoGarantia = tg.IdTipoGarantia
WHERE vw.IdHipotecario = @IdHipotecario; 
GO

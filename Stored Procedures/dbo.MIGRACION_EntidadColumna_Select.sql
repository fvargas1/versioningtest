SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_EntidadColumna_Select]
	@IdEntidadColumns INT
AS
SELECT
	vw.IdEntidadColumns,
	vw.Nombre,
	vw.Descripcion,
	vw.IdEntidad,
	vw.IdEntidadNombre,
	vw.NombreCampo,
	vw.TipoDato,
	vw.IsRequerido,
	vw.IsKey,
	vw.IsIdentidadBusqueda,
	vw.IdCatalogo,
	vw.IdCatalogoNombre,
	vw.NombreIdCatalogo
FROM dbo.MIGRACION_VW_EntidadColumna vw
WHERE vw.IdEntidadColumns = @IdEntidadColumns;
GO

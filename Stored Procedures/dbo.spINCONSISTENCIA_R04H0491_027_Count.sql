SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_027_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se deberá validar la correspondencia para las siguientes claves en "Monto del Subsidio Federal al Frente" y "Tipo de Alta del Crédito":
-- - Cuando el "Monto del Subsidio Federal al Frente" sea mayor a 0, el "Tipo de Alta del Crédito" debe ser 2,5,6 o 10.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE CAST(NULLIF(MontoSubsidioFederal,'') AS DECIMAL) > 0 AND TipoAlta NOT IN ('2','5','6','10');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

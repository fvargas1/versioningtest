SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_ReservasSaldoFinal_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Hipotecario_ReservasSaldoFinal_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Hipotecario
SET errorFormato = 1
WHERE LEN(ISNULL(ReservasSaldoFinal,'')) > 0 AND (ISNUMERIC(ISNULL(ReservasSaldoFinal,'0')) = 0 OR ReservasSaldoFinal LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCredito,
 'ReservasSaldoFinal',
 ReservasSaldoFinal,
 1,
 @Detalle
FROM dbo.FILE_Hipotecario
WHERE LEN(ISNULL(ReservasSaldoFinal,'')) > 0 AND (ISNUMERIC(ISNULL(ReservasSaldoFinal,'0')) = 0 OR ReservasSaldoFinal LIKE '%[^0-9 .]%');
GO

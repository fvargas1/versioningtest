SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_046_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La "FECHA DE VENCIMIENTO DEL CREDITO REESTRUCTURADO" deberá ser mayor a la fecha de firma de la reestructuración del crédito (columna 25).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE FechaVencimientoReestructura <= FechaFirmaReestructura;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Proyecto_Sobrecosto_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Proyecto_Sobrecosto_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Proyecto
SET errorFormato = 1
WHERE LEN(Sobrecosto) > 0 AND (ISNUMERIC(ISNULL(Sobrecosto,'0')) = 0 OR Sobrecosto LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Proyecto_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoProyecto,
 'Sobrecosto',
 Sobrecosto,
 1,
 @Detalle
FROM dbo.FILE_Proyecto
WHERE LEN(Sobrecosto) > 0 AND (ISNUMERIC(ISNULL(Sobrecosto,'0')) = 0 OR Sobrecosto LIKE '%[^0-9 .]%');
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_Total]
AS
UPDATE dbo.SICCMX_Credito_Reservas_Variables
SET SP_Total = CASE WHEN EI_Total = 0 THEN 0 ELSE (CAST(SP_Expuesta * EI_Expuesta_GarPer AS DECIMAL(22,10)) / EI_Total) + (CAST(SP_Cubierta * EI_Cubierta_GarPer AS DECIMAL(22,10)) / EI_Total) END;


INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 IdCredito,
 GETDATE(),
 '',
 'Se calculó una SP Total de ' + CONVERT(VARCHAR, CONVERT(DECIMAL(10,2), ISNULL(SP_Total*100, 0))) + '%'
FROM dbo.SICCMX_Credito_Reservas_Variables;
GO

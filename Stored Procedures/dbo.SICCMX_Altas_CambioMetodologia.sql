SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Altas_CambioMetodologia]
AS
DECLARE @IdTipoAlta INT;
SELECT @IdTipoAlta = IdTipoAlta FROM dbo.SICC_TipoAlta WHERE Codigo='138';


UPDATE dbo.SICCMX_CreditoInfo SET IdTipoAlta = NULL WHERE IdTipoAlta = @IdTipoAlta;
UPDATE dbo.SICCMX_LineaCredito SET IdTipoAlta = NULL WHERE IdTipoAlta = @IdTipoAlta;

UPDATE cInfo
SET IdTipoAlta = @IdTipoAlta
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
INNER JOIN dbo.SICCMX_VW_Persona_Ultima_Metodologia hcre ON cre.IdPersona = hcre.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
WHERE met.Codigo <> hcre.Metodologia AND cre.MontoValorizado > 0 AND cInfo.IdTipoAlta IS NULL;

UPDATE lin
SET IdTipoAlta = @IdTipoAlta
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_LineaCredito lin ON cre.IdLineaCredito = lin.IdLineaCredito
INNER JOIN dbo.SICCMX_VW_Persona_Ultima_Metodologia hcre ON cre.IdPersona = hcre.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
WHERE met.Codigo <> hcre.Metodologia AND cre.MontoValorizado > 0 AND lin.IdTipoAlta IS NULL;
GO

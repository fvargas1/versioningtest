SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_CalculoPuntos]
AS

--Primero se calcula el puntaje para aquellas variables donde no hay informacion
UPDATE det
SET
 det.PuntosActual = mat.Puntos,
 det.FechaCalculo = GETDATE(),
 det.UsuarioCalculo = ''
FROM dbo.SICCMX_Persona_PI_Detalles det
INNER JOIN dbo.SICCMX_PI_MatrizPuntaje mat ON mat.IdVariable = det.IdVariable
WHERE det.ValorActual IS NULL AND mat.RangoMenor IS NULL;


--Se calculan los valores cuando existe informacion
UPDATE det
SET
 det.PuntosActual = mat.Puntos,
 det.FechaCalculo = GETDATE(),
 det.UsuarioCalculo = ''
FROM dbo.SICCMX_Persona_PI_Detalles det
INNER JOIN dbo.SICCMX_PI_MatrizPuntaje mat ON mat.IdVariable = det.IdVariable AND ValorActual >= mat.RangoMenor AND ValorActual < mat.RangoMayor
WHERE det.ValorActual IS NOT NULL AND mat.RangoMenor <> mat.RangoMayor;


--Se calculan las variables cuando no existen diferencias entre el menor y mayor
UPDATE det
SET
 det.PuntosActual = mat.Puntos,
 det.FechaCalculo = GETDATE(),
 det.UsuarioCalculo = ''
FROM dbo.SICCMX_Persona_PI_Detalles det
INNER JOIN dbo.SICCMX_PI_MatrizPuntaje mat ON mat.IdVariable = det.IdVariable AND ValorActual = mat.RangoMenor
WHERE det.ValorActual IS NOT NULL AND mat.RangoMenor = mat.RangoMayor;


INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT IdPersona, GETDATE(), '', '********** SE CALCULAN LOS PUNTOS POR VARIABLE **********'
FROM dbo.SICCMX_Persona_PI;


-- Actualiza el puntaje a 24 a los clientes con Utilidad Neta Trimestral y Capital Contable Promedio menor a 0
EXEC SICCMX_ValidaInfoAnx20;

-- Actualiza el puntaje a 57 a los clientes con Ventas Totales Anuales mayor a 216 millones de UDIS y con mas de 12 instituciones reportadas
EXEC SICCMX_ValidaInfoAnx22;


-- LOG PARA ANEXO 20
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT ppi.IdPersona, GETDATE(), '', 'Activo Total : $ ' + CONVERT(VARCHAR, CAST(an20.ActivoTotal AS MONEY), 1)
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_Anexo20 an20 ON ppi.IdPersona = an20.IdPersona
UNION ALL
SELECT ppi.IdPersona, GETDATE(), '', 'Entidad Financiera ' + CASE WHEN ISNULL(an20.EntFinAcreOtorgantesCre, 0) = 0 THEN 'NO ' ELSE '' END + 'Otorgante de Crédito'
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_Anexo20 an20 ON ppi.IdPersona = an20.IdPersona
UNION ALL
SELECT ppi.IdPersona, GETDATE(), '', 'Tipo de Entidad Financiera : ' + vwCat.Nombre
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_Anexo20 an20 ON ppi.IdPersona = an20.IdPersona
INNER JOIN dbo.CAT_VW_EntFin_Sujeta vwCat ON CAST(an20.EntFinSujRegBan AS VARCHAR(5)) = vwCat.Codigo;


-- LOG PARA ANEXO 21
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT ppi.IdPersona, GETDATE(), '', 'Cliente ' + CASE WHEN ISNULL(an21.SinAtrasos, 0) = 1 THEN 'sin atrasos' ELSE 'con atrasos' END
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_Anexo21 an21 ON ppi.IdPersona = an21.IdPersona;


-- LOG PARA ANEXO 22
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT ppi.IdPersona, GETDATE(), '', 'Ventas Netas Anuales : $ ' + CONVERT(VARCHAR, CONVERT(MONEY, an22.VentNetTotAnuales), 1)
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_Anexo22 an22 ON ppi.IdPersona = an22.IdPersona;
GO

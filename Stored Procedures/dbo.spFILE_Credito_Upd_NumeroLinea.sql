SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_Upd_NumeroLinea]
AS
UPDATE dbo.FILE_Credito
SET NumeroLinea = CodigoCredito
WHERE LEN(ISNULL(NumeroLinea,'')) = 0;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_InconsistenciaResumen_List_ConInconsistencias]
AS
SELECT
	vw.IdInconsistenciaResumen,
	vw.IdInconsistencia,
	vw.InconsistenciaNombre,
	vw.Numero,
	vw.Fecha,
	vw.CategoriaNombre
FROM dbo.MIGRACION_VW_InconsistenciaResumen vw
WHERE ISNULL(vw.Numero,0) <> 0
ORDER BY vw.CategoriaNombre, vw.InconsistenciaNombre;
GO

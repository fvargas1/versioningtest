SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Proyecto]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Proyecto WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Proyecto (
	IdPeriodoHistorico,
	Proyecto,
	Persona,
	Nombre,
	Descripcion,
	LineaCredito,
	Moneda,
	FechaCreacion,
	Periodo,
	FechaInicioOper,
	TasaDescuentoVP,
	VentNetTotAnuales
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	pry.CodigoProyecto,
	per.Codigo,
	pry.Nombre,
	pry.Descripcion,
	lin.Codigo,
	mon.Codigo,
	pry.FechaCreacion,
	pry.Periodo,
	pry.FechaInicioOper,
	pry.TasaDescuentoVP,
	pry.VentNetTotAnuales
FROM dbo.SICCMX_Proyecto pry
INNER JOIN dbo.SICCMX_Persona per ON pry.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICCMX_LineaCredito lin ON pry.IdLineaCredito = lin.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON pry.IdMoneda = mon.IdMoneda;
GO

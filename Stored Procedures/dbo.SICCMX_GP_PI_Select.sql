SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_GP_PI_Select]
 @IdAval INT,
 @EsGarante INT
AS
SELECT
 vw.IdGP,
 vw.EsGarante,
 vw.FactorCuantitativo,
 vw.PonderadoCuantitativo,
 vw.FactorCualitativo,
 vw.PonderadoCualitativo,
 vw.FactorTotal,
 vw.[PI],
 vw.IdMetodologia,
 vw.IdMetodologiaNombre,
 vw.PrctCuantitativo,
 vw.PrctCualitativo
FROM dbo.SICCMX_VW_Persona_PI_GP vw
WHERE vw.IdGP = @IdAval AND vw.EsGarante = @EsGarante;
GO

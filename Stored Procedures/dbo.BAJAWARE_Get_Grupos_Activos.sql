SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Get_Grupos_Activos]
AS
SELECT
	grp.IdGroup,
	grp.Description,
	CASE WHEN cfg.Value IS NULL THEN 0 ELSE 1 END AS Activo,
	CASE WHEN cfg.Value IS NULL THEN 'blank' ELSE 'on' END AS ActivoImg,
	ISNULL(rel.Cantidad,0) AS Cantidad,
	ISNULL(tot.Total,0) AS Total,
	CAST(ISNULL(rel.Cantidad,0) AS VARCHAR) + '/' + CAST(ISNULL(tot.Total,0) AS VARCHAR) AS Configurados
FROM dbo.BAJAWARE_Group grp
LEFT OUTER JOIN (
	SELECT Value
	FROM dbo.BAJAWARE_Config
	WHERE CodeName = 'FRM_REP_CFG'
) AS cfg ON grp.IdGroup = cfg.Value
LEFT OUTER JOIN (
	SELECT r1.IdGroup, COUNT(DISTINCT r2.IdReporte) AS Cantidad
	FROM dbo.RW_Rel_Grupo_ReporteCampos r1
	INNER JOIN dbo.RW_ReporteCampos r2 ON r1.IdReporteCampos = r2.IdReporteCampos
	INNER JOIN dbo.RW_Reporte r3 ON r2.IdReporte = r3.IdReporte AND r3.Activo = 1
	GROUP BY r1.IdGroup
) AS rel ON grp.IdGroup = rel.IdGroup
LEFT OUTER JOIN (
	SELECT COUNT(DISTINCT r1.IdReporte) AS Total
	FROM dbo.RW_ReporteCampos r1
	INNER JOIN dbo.RW_Reporte r2 ON r1.IdReporte = r2.IdReporte AND r2.Activo = 1
) AS tot ON 1=1
WHERE grp.Active = 1
ORDER BY grp.Description;
GO

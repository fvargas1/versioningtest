SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_035_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se deberá validar la correspondencia para el monto en "Valor de la Vivienda al Momento de la Originación" y la clave de "Tipo de Alta del Crédito":
-- - Cuando el "Valor de la Vivienda al momento de la Originación" es mayor a 0, el "Tipo de Alta del Crédito" debe ser 2, 5, 6 o 10.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE CAST(ISNULL(NULLIF(ValorOriginalVivienda,''),'0') AS DECIMAL) > 0 AND TipoAlta NOT IN ('2','5','6','10');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_Institucion]
	@filtro VARCHAR(50)
AS
SELECT DISTINCT
	hm.CodigoBanco,
	cat.Codigo AS Codigo,
	cat.Nombre,
	CAST(cat.Fondeadora AS INT) AS Fondeadora,
	CAST(cat.Otorgante AS INT) AS Otorgante,
	SUM(CASE WHEN hm.CodigoBanco IS NULL THEN 0 ELSE 1 END) OVER() AS vwCB
FROM dbo.SICC_Institucion cat
LEFT OUTER JOIN dbo.FILE_Credito_Homologacion hm ON cat.Codigo = hm.CodigoBW AND hm.Campo = 'InstitutoFondea' AND hm.Activo = 1
WHERE ISNULL(hm.CodigoBanco,'')+'|'+ISNULL(cat.Codigo,'')+'|'+ISNULL(cat.Nombre,'') LIKE '%' + @filtro + '%'
ORDER BY cat.Codigo;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ProyectoLinea_NumeroLinea_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_ProyectoLinea_NumeroLinea_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_ProyectoLinea f
LEFT OUTER JOIN dbo.SICCMX_LineaCredito lin ON LTRIM(f.NumeroLinea) = lin.Codigo
WHERE lin.IdLineaCredito IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_ProyectoLinea_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoProyecto,
 'NumeroLinea',
 f.NumeroLinea,
 2,
 @Detalle
FROM dbo.FILE_ProyectoLinea f
LEFT OUTER JOIN dbo.SICCMX_LineaCredito lin ON LTRIM(f.NumeroLinea) = lin.Codigo
WHERE lin.IdLineaCredito IS NULL;
GO

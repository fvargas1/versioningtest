SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_Reservas_A19_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Fecha,
	CodigoPersona,
	NombrePersona,
	CodigoProyecto,
	NombreProyecto,
	CodigoCredito,
	SaldoCredito,
	FechaVecimiento,
	Moneda,
	MontoGarantia,
	MontoReserva,
	PrctReserva,
	Calificacion
FROM dbo.RW_VW_Analitico_Reservas_A19
ORDER BY CodigoPersona, CodigoProyecto, CodigoCredito;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ProyectoLinea_CodigoProyecto_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_ProyectoLinea_CodigoProyecto_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_ProyectoLinea f
LEFT OUTER JOIN dbo.SICCMX_Proyecto pry ON LTRIM(f.CodigoProyecto) = pry.CodigoProyecto
WHERE pry.IdProyecto IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_ProyectoLinea_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoProyecto,
 'CodigoProyecto',
 f.CodigoProyecto,
 2,
 @Detalle
FROM dbo.FILE_ProyectoLinea f
LEFT OUTER JOIN dbo.SICCMX_Proyecto pry ON LTRIM(f.CodigoProyecto) = pry.CodigoProyecto
WHERE pry.IdProyecto IS NULL;
GO

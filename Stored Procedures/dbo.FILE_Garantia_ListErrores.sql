SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Garantia_ListErrores]
AS
SELECT
 CodigoGarantia,
 TipoGarantiaMA,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 FechaValuacion,
 Descripcion,
 BancoGarantia,
 ValorGarantiaProyectado,
 RegGarantiaMob,
 Hc,
 VencimientoRestante,
 GradoRiesgo,
 AgenciaCalificadora,
 Calificacion,
 Emisor,
 Escala,
 EsIPC,
 PIGarante,
 NumRPPC,
 RFCGarante,
 NombreGarante,
 IdGarante,
 ActividaEcoGarante,
 LocalidadGarante,
 MunicipioGarante,
 EstadoGarante,
 TipoGarante,
 LEI,
 IndPerMorales,
 CodigoCliente,
 CodigoPostalGarante,
 Fuente
FROM dbo.FILE_Garantia
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO

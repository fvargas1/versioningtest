SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_ConsumoAdicional]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_ConsumoAdicional WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_ConsumoAdicional (
	IdPeriodoHistorico,
	Consumo,
	SaldoPromedio,
	InteresDevengado,
	Comision
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	con.Codigo,
	cad.SaldoPromedio,
	cad.InteresDevengado,
	cad.Comision
FROM dbo.SICCMX_Consumo con
INNER JOIN dbo.SICCMX_ConsumoAdicional cad ON con.IdConsumo = cad.IdConsumo;
GO

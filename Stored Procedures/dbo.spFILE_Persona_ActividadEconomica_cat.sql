SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_ActividadEconomica_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_ActividadEconomica_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON LTRIM(f.ActividadEconomica) = act.Codigo
WHERE act.Codigo IS NULL AND LEN(f.ActividadEconomica) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	CodigoCliente,
	'ActividadEconomica',
	f.ActividadEconomica,
	2,
	@Detalle
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON LTRIM(f.ActividadEconomica) = act.Codigo
WHERE act.Codigo IS NULL AND LEN(f.ActividadEconomica) > 0;
GO

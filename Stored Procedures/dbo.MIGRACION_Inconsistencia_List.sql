SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Inconsistencia_List]
AS
SELECT
 vw.IdInconsistencia,
 vw.Nombre,
 vw.Descripcion,
 vw.CodenameCount,
 vw.Codename,
 vw.IdCategoria,
 vw.CategoriaNombre,
 vw.Activo,
 vw.Regulatorio,
 vw.FechaCreacion,
 vw.FechaActualizacion,
 vw.Username,
 CASE WHEN vw.Activo = 1 THEN 'on' ELSE 'blank' END AS ActivoImg,
 CASE WHEN vw.Regulatorio = 1 THEN 'on' ELSE 'blank' END AS RegulatorioImg
FROM dbo.MIGRACION_VW_Inconsistencia vw
WHERE vw.Activo = 1
ORDER BY vw.CategoriaNombre, vw.Nombre;
GO

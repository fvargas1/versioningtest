SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_Variables_Manuales]
AS
UPDATE piDet
SET ValorActual = man.Valor
FROM dbo.SICCMX_Persona_PI_Detalles piDet
INNER JOIN dbo.SICCMX_Variables_Buro_Manual man ON piDet.IdPersona = man.IdPersona AND piDet.IdVariable = man.IdVariable;

INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT IdPersona, GETDATE(), '', 'Se ajustan variables manualmente'
FROM dbo.SICCMX_Variables_Buro_Manual;
GO

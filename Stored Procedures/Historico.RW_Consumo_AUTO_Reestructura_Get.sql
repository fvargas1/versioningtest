SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Consumo_AUTO_Reestructura_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	FolioCredito,
	FechaReestructuraCredito,
	Reestructura,
	QuitasCondonacionesBonificacionesDescuentos,
	Folio2Credito
FROM Historico.RW_Consumo_AUTO_Reestructura
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY FolioCredito;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_CreditoGarantia_PP]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_CreditoGarantia_PP WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_CreditoGarantia_PP (
	IdPeriodoHistorico,
	Credito,
	Garantia,
	PrctReserva,
	Reserva,
	ReservaCubierta,
	ReservaExpuesta
)
SELECT
	@IdPeriodoHistorico,
	cr.Codigo,
	ga.Codigo,
	pp.PrctReserva,
	pp.Reserva,
	pp.ReservaCubierta,
	pp.ReservaExpuesta
FROM dbo.SICCMX_CreditoGarantia_PP pp
INNER JOIN dbo.SICCMX_Credito cr ON pp.IdCredito = cr.IdCredito
INNER JOIN dbo.SICCMX_Garantia ga ON pp.IdGarantia = ga.IdGarantia;
GO

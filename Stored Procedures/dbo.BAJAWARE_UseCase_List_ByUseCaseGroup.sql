SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCase_List_ByUseCaseGroup]
	@IdGroupCaseGroup BAJAWARE_utID
AS
SELECT IdUseCase as [id], Active, Description, Name, CodeName, Visible, PageLink, IdUseCaseGroup, SortOrder, StringKey
FROM dbo.BAJAWARE_UseCase
WHERE IdUseCaseGroup = @IdGroupCaseGroup AND Active=1
ORDER BY SortOrder;
GO

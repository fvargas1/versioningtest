SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_PI_CalcularValorFinal_GP]
AS

--Se calcula el PI
UPDATE dbo.SICCMX_Persona_PI_GP
SET PI = 1 / (1 + EXP( -1 * ( ( val.X - per.FactorTotal ) * LOG(2)/40)))
FROM dbo.SICCMX_Persona_PI_GP per
INNER JOIN dbo.SICCMX_PI_ValoresDelta val ON val.IdMetodologia = per.IdMetodologia AND ISNULL(val.IdClasificacion, '') = ISNULL(per.IdClasificacion, '');


-- LOG de calculo...
INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdGP,
 ppi.EsGarante,
 GETDATE(),
 '',
 'El valor del PI fue de : '
 + CONVERT(VARCHAR, CONVERT(DECIMAL(18,8), ppi.PI * 100))
 + '% y calculado segun la formula 1 / (1 + EXP( -1 * ( ( '
 + CONVERT(VARCHAR, CONVERT(INT, val.X))
 + ' - '
 + CONVERT(VARCHAR, CONVERT(INT, ppi.FactorTotal))
 + ' ) * LOG(2)/40)))'
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_PI_ValoresDelta val ON val.IdMetodologia = ppi.IdMetodologia AND ISNULL(val.IdClasificacion, '') = ISNULL(ppi.IdClasificacion, '');
GO

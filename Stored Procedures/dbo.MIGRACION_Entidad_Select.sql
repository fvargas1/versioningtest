SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Entidad_Select]
	@IdEntidad INT
AS
SELECT
	vw.IdEntidad,
	vw.Nombre,
	vw.Descripcion,
	vw.NombreView
FROM dbo.MIGRACION_VW_Entidad vw
WHERE vw.IdEntidad = @IdEntidad;
GO

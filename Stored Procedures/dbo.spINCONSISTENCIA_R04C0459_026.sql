SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_026]
AS
BEGIN
-- Institución Banca de Desarrollo o Fondo de Fomento que otorgó el Fondeo debe ser diferente de 0.

SELECT DISTINCT
 CodigoCredito,
 REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
 NumeroDisposicion,
 InstitucionFondeo,
 MontoBancaDesarrollo
FROM dbo.RW_VW_R04C0459_INC
WHERE ISNULL(NULLIF(InstitucionFondeo,''),'0') = '0' AND CAST(ISNULL(NULLIF(MontoBancaDesarrollo,''),'0') AS DECIMAL(23,2)) <> 0;

END


GO

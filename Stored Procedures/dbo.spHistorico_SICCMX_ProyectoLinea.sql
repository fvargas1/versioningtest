SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_ProyectoLinea]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_ProyectoLinea WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_ProyectoLinea (
	IdPeriodoHistorico,
	Proyecto,
	LineaCredito
)
SELECT
	@IdPeriodoHistorico,
	p.CodigoProyecto,
	l.Codigo
FROM dbo.SICCMX_ProyectoLinea pl
INNER JOIN dbo.SICCMX_Proyecto p ON pl.IdProyecto = p.IdProyecto
INNER JOIN dbo.SICCMX_LineaCredito l ON pl.IdLineaCredito = l.IdLineaCredito;
GO

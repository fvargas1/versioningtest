SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_NoFinancieras]
AS
-- CALCULAMOS SP PARA GARANTIAS REALES NO FINANCIERAS
UPDATE can
SET SeveridadCorresp = CASE
 WHEN ISNULL(can.PrctCobSinAju, 0) < (vw.NivelMinimo/100) THEN canExp.SeveridadCorresp
 ELSE vw.SeveridadPerdida END 
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Garantias_RNF vw ON can.IdCredito = vw.IdCredito AND vw.IdTipoGarantia=can.IdTipoGarantia
INNER JOIN dbo.SICCMX_Garantia_Canasta canExp ON can.IdCredito = canExp.IdCredito AND ISNULL(canExp.EsDescubierto, 0) = 1;
GO

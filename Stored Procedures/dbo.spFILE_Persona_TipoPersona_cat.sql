SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_TipoPersona_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_TipoPersona_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_TipoPersona tp ON LTRIM(f.PersonalidadJuridica) = tp.Codigo AND tp.Activo = 1
WHERE tp.Codigo IS NULL AND LEN(f.PersonalidadJuridica) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCliente,
 'PersonalidadJuridica',
 f.PersonalidadJuridica,
 2,
 @Detalle
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_TipoPersona tp ON LTRIM(f.PersonalidadJuridica) = tp.Codigo AND tp.Activo = 1
WHERE tp.Codigo IS NULL AND LEN(f.PersonalidadJuridica) > 0;
GO

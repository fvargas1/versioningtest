SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_Persona_Fondos]
AS

UPDATE crv
SET EI_Cubierta = 0,
 EI_Cubierta_GarPer = 0,
 EI_Expuesta = EI_Total,
 EI_Expuesta_GarPer = EI_Total,
 SP_Cubierta = 0,
 SP_Expuesta = 0.45,
 SP_Total = 0.45,
 PI_Cubierta = 0,
 PI_Expuesta = 0.0111111111,
 PI_Total = 0.0111111111,
 ReservaFinal = EI_Total * 0.45 * 0.0111111111,
 ReservaExpuesta = EI_Total * 0.45 * 0.0111111111,
 ReservaExpuesta_GarPer = EI_Total * 0.45 * 0.0111111111,
 ReservaCubierta = 0,
 ReservaCubierta_GarPer = 0
FROM dbo.SICCMX_Persona p
INNER JOIN dbo.SICCMX_Credito c ON p.IdPersona = c.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON c.IdCredito = crv.IdCredito
WHERE p.EsFondo IS NOT NULL;


-- Insertamos Log
INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log( IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT crv.IdCredito,
GETDATE(),
'',
'Se actualiza PI a '
+ CONVERT(VARCHAR, CAST(PI_Total * 100.00 AS DECIMAL(4,2)))
+ '% y la SP a '
+ CONVERT(VARCHAR, CAST(SP_Total * 100.00 AS DECIMAL(4,2)))
+ '% ya que esta respaldado por un Fondo Federal.'
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Credito c ON crv.IdCredito = c.IdCredito
INNER JOIN dbo.SICCMX_Persona p ON c.IdPersona = p.IdPersona
WHERE P.EsFondo IS NOT NULL;
GO

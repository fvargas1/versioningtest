SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0443_SOFOM_Get]
 @IdReporteLog BIGINT
AS
SELECT
 Formulario,
 --NumeroSecuencia,
 CodigoPersona,
 RFC,
 NombrePersona,
 CodigoCredito,
 CodigoCreditoCNBV,
 CodigoAgrupacion,
 --NumeroDisposicion,
 FechaDisposicion,
 CalificacionPersona,
 CalificacionCreditoCubierta,
 CalificacionCreditoExpuesta,
 CalificacionPersonaCNBV,
 CalificacionCreditoCubiertaCNBV,
 CalificacionCreditoExpuestaCNBV,
 SituacionCredito,
 SaldoInicial,
 TasaInteres,
 MontoDispuesto,
 MontoExigible,
 MontoPagado,
 MontoInteresesCobrados,
 MontoComisionDevengada,
 DiasVencido,
 SaldoFinal,
 ResponsabilidadTotal,
 TipoBaja,
 MontoReconocido,
 ProductoComercial
FROM dbo.RW_VW_R04C0443_SOFOM
ORDER BY NombrePersona;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CedulaNMC_Canasta_Get]
AS
TRUNCATE TABLE dbo.RW_CedulaNMC_Canasta;

INSERT INTO dbo.RW_CedulaNMC_Canasta(
 IdPersona,
 IdCredito,
 NumeroLinea,
 CodigoCredito,
 TipoGarantia,
 PrctSinAjust,
 PrctAjust,
 [PI],
 SP,
 MontoRecuperacion,
 Reserva,
 Posicion
)
SELECT
 rw.IdPersona,
 rw.IdCredito,
 rw.NumeroLinea,
 rw.CodigoCredito,
 cnt.Tipo,
 cnt.PorcCobSinAjust,
 cnt.PorcCobAjust,
 cnt.[PI],
 cnt.SP,
 cnt.MontoRecuperacion,
 cnt.Reserva,
 cnt.Posicion
FROM dbo.RW_CedulaNMC_Creditos rw
INNER JOIN (
 SELECT can.IdCredito,
 CASE WHEN can.IdTipoGarantia IS NULL THEN 'Garantias Financieras' ELSE tg.Nombre END AS Tipo,
 ISNULL(can.PrctCobSinAju, 0) AS PorcCobSinAjust,
 ISNULL(can.PrctCobAjust, 0) AS PorcCobAjust,
 ISNULL(can.[PI], 0) AS [PI],
 ISNULL(can.SeveridadCorresp, 0) AS SP,
 ISNULL(can.MontoRecuperacion, 0) AS MontoRecuperacion,
 ISNULL(can.Reserva, 0) AS Reserva,
 1 AS Posicion
 FROM dbo.SICCMX_Garantia_Canasta can
 LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
 WHERE ISNULL(can.EsDescubierto, 0) <> 1 AND can.PrctCobSinAju > 0
 UNION ALL
 SELECT can.IdCredito,
 'Expuesto' AS Tipo,
 can.PrctCobSinAju AS PorcCobSinAjust,
 ISNULL(can.PrctCobAjust, 0) AS PorcCobAjust,
 ISNULL(can.[PI], 0) AS [PI],
 ISNULL(can.SeveridadCorresp, 0) AS SP,
 ISNULL(can.MontoRecuperacion, 0) AS MontoRecuperacion,
 ISNULL(can.Reserva, 0) AS Reserva,
 3 AS Posicion
 FROM dbo.SICCMX_Garantia_Canasta can
 WHERE ISNULL(can.EsDescubierto, 0) = 1
) AS cnt ON rw.IdCredito = cnt.IdCredito;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CedulaNMC_Creditos_Get]
AS
TRUNCATE TABLE dbo.RW_CedulaNMC_Creditos;

INSERT INTO dbo.RW_CedulaNMC_Creditos(
 IdPersona,
 IdCredito,
 NumeroLinea,
 CodigoCredito,
 SaldoTotal,
 MontoCubierto,
 MontoExpuesto,
 EI,
 TipoLinea,
 PI_Ponderada,
 SP_Ponderada,
 Reserva,
 PrctReserva,
 Calificacion
)
SELECT
 rw.IdPersona,
 vw.IdCredito,
 vw.NumeroLinea,
 vw.CodigoCredito,
 vw.MontoValorizado,
 crv.EI_Cubierta,
 crv.EI_Expuesta,
 crv.EI_Total,
 tl.Nombre,
 crv.PI_Total,
 crv.SP_Total,
 crv.ReservaFinal,
 crv.PorReservaFinal,
 cal.Codigo
FROM dbo.RW_CedulaNMC_Info rw
INNER JOIN dbo.SICCMX_VW_Credito_NMC vw ON rw.IdPersona = vw.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON vw.IdCredito = crv.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoLinea tl ON vw.TipoLinea = tl.Codigo
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON crv.CalifFinal = cal.IdCalificacion;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_011]
AS

BEGIN

-- El "PRODUCTO HIPOTECARIO DE LA ENTIDAD" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.ProductoHipotecario
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_ProductoHipotecario cat ON ISNULL(r.ProductoHipotecario,'') = cat.CodigoCNBV
WHERE cat.IdProductoHipotecario IS NULL;

END
GO

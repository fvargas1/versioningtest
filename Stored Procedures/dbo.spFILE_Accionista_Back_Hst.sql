SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Accionista_Back_Hst]
AS

INSERT INTO dbo.FILE_Accionista_Hst (
	Deudor,
	NombreAccionista,
	Porcentaje
)
SELECT
	acc.Deudor,
	acc.NombreAccionista,
	acc.Porcentaje
FROM dbo.FILE_Accionista acc
LEFT OUTER JOIN dbo.FILE_Accionista_Hst hst ON LTRIM(acc.Deudor) = LTRIM(hst.Deudor)
WHERE hst.Deudor IS NULL;
GO

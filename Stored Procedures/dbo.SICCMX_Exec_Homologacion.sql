SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Exec_Homologacion]
 @tFile VARCHAR(100),
 @tHomologacion VARCHAR(100),
 @Campo VARCHAR(50),
 @Tipo INT,
 @Fuente VARCHAR(50)
AS
DECLARE @QRY VARCHAR(500);

IF @Tipo = 1
BEGIN

SET @QRY =
'UPDATE f ' +
'SET ' + @Campo +' = h.CodigoBW ' +
'FROM dbo.' + @tFile + ' f ' +
'INNER JOIN dbo.' + @tHomologacion + ' h ON LTRIM(f.' + @Campo + ') = h.CodigoBanco ' +
'WHERE h.Campo=''' + @Campo + ''' AND ISNULL(f.Fuente,'''') = ISNULL(h.Fuente,'''') AND ISNULL(f.Homologado,0) = 0';
EXEC (@QRY);

END
ELSE IF @Tipo = 2
BEGIN

SET @QRY =
'UPDATE f ' +
'SET f.' + @Campo + ' = CAST(f.' + @Campo + ' / tc.Valor AS DECIMAL(23,2)) ' +
'FROM dbo.' + @tFile + ' f ' +
'INNER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo ' +
'INNER JOIN dbo.SICC_TipoCambio tc ON mon.IdMoneda = tc.IdMoneda ' +
'INNER JOIN dbo.SICC_Periodo prd ON tc.IdPeriodo = prd.IdPeriodo AND prd.Activo = 1 ' +
'WHERE ISNUMERIC(f.' + @Campo + ') = 1 AND ISNULL(f.Fuente,'''') = ''' + ISNULL(@Fuente,'') + ''' AND ISNULL(f.Homologado,0) = 0';
EXEC (@QRY);

END

ELSE IF @Tipo = 3
BEGIN

SET @QRY =
'UPDATE f ' +
'SET f.' + @Campo + ' = CAST(f.' + @Campo + ' * tc.Valor AS DECIMAL(23,2)) ' +
'FROM dbo.' + @tFile + ' f ' +
'INNER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo ' +
'INNER JOIN dbo.SICC_TipoCambio tc ON mon.IdMoneda = tc.IdMoneda ' +
'INNER JOIN dbo.SICC_Periodo prd ON tc.IdPeriodo = prd.IdPeriodo AND prd.Activo = 1 ' +
'WHERE ISNUMERIC(f.' + @Campo + ') = 1 AND ISNULL(f.Fuente,'''') = ''' + ISNULL(@Fuente,'') + ''' AND ISNULL(f.Homologado,0) = 0';
EXEC (@QRY);

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_002_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "ID ACREDITADO ASIGNADO INST" solo debe contener letras en mayúsculas y números, sin caracteres distintos a estos.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(CodigoCredito)
FROM dbo.RW_R04C0442
WHERE CodigoPersona LIKE '%[^A-Z0-9]%' OR BINARY_CHECKSUM(CodigoPersona) <> BINARY_CHECKSUM(UPPER(CodigoPersona));

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

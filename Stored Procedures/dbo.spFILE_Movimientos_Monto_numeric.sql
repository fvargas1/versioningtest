SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Movimientos_Monto_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Movimientos_Monto_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Movimientos
SET errorFormato = 1
WHERE ISNUMERIC(Monto) = 0 OR Monto LIKE '%[^0-9 .-]%';

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Movimientos_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoCredito,
	'Monto',
	Monto,
	1,
	@Detalle
FROM dbo.FILE_Movimientos
WHERE ISNUMERIC(Monto) = 0 OR Monto LIKE '%[^0-9 .-]%';
GO

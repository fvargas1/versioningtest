SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_038_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Tipo de Operación es 254, el Destino de Crédito deberá ser 330, 332, 333, 430, 432 ó 433

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(TipoOperacion,'') = '254' AND ISNULL(DestinoCredito,'') NOT IN ('330', '332', '333', '430', '432', '433');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO

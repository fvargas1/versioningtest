SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_A21_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @query VARCHAR(8000);
DECLARE @Ids VARCHAR(2000);

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte='INTERNO' AND Nombre = '_Analitico_A21';
SELECT @Ids = STUFF((SELECT'],[' + LTRIM(C.Nombre) + '_V],[' + LTRIM(C.Nombre) + '_P'
FROM SICCMX_PI_Variables C
WHERE C.IdMetodologia=21 GROUP BY C.Nombre ORDER BY MIN(C.Orden) FOR XML PATH('')), 1, 2, '') + ']';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();

SET @query =
'SELECT * FROM(
SELECT ' + CAST(@IdReporteLog AS VARCHAR(10)) + ' AS IdReporteLog, per.Codigo, REPLACE(per.Nombre,'','','''') AS Cte_Nombre, piv.Nombre+''_V'' AS Nombre,
CASE WHEN cat.Nombre IS NULL THEN CASE WHEN det.PuntosActual IS NULL THEN '''' ELSE ISNULL(CAST(CAST(det.ValorActual AS DECIMAL(22,4)) AS VARCHAR), ''Sin información'') END ELSE
cat.Codigo + ''. '' + REPLACE(cat.Nombre,'','','''') END AS Dato,
CAST(ppi.PonderadoCuantitativo AS INT) AS PonderadoCuantitativo,
CAST(ppi.PonderadoCualitativo AS INT) AS PonderadoCualitativo,
CAST(ppi.FactorTotal AS INT) AS FactorTotal,
CAST(ppi.PI * 100 AS DECIMAL(10,6)) AS PI,
anx.SinAtrasos
FROM dbo.SICCMX_Persona per
INNER JOIN SICCMX_Persona_PI_Detalles det ON det.IdPersona=per.IdPersona
INNER JOIN SICCMX_PI_Variables piv ON piv.Id=det.IdVariable
INNER JOIN SICCMX_Persona_PI ppi ON ppi.IdPersona=per.IdPersona
INNER JOIN SICCMX_VW_Credito_NMC cre ON per.IdPersona=cre.IdPersona
INNER JOIN SICCMX_Anexo21 anx ON ppi.IdPersona = anx.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Rel_Variable_Catalogo rel ON det.IdVariable=rel.IdVariable
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos cat ON rel.IdCatalogo=cat.IdCatalogo AND SUBSTRING(REPLACE(CAST(det.ValorActual AS VARCHAR),''.'',''''),1,3)=cat.Codigo
WHERE piv.IdMetodologia=21 AND cre.MontoValorizado>0
UNION ALL
SELECT ' + CAST(@IdReporteLog AS VARCHAR(10)) + ' AS IdReporteLog, per.Codigo, REPLACE(per.Nombre,'','','''') AS Cte_Nombre, piv.Nombre+''_P'' AS Nombre,
ISNULL(CAST(det.PuntosActual AS VARCHAR),'''') AS Dato,
CAST(ppi.PonderadoCuantitativo AS INT) AS PonderadoCuantitativo,
CAST(ppi.PonderadoCualitativo AS INT) AS PonderadoCualitativo,
CAST(ppi.FactorTotal AS INT) AS FactorTotal,
CAST(ppi.PI * 100 AS DECIMAL(10,6)) AS PI,
anx.SinAtrasos
FROM dbo.SICCMX_Persona per
INNER JOIN SICCMX_Persona_PI_Detalles det ON det.IdPersona=per.IdPersona
INNER JOIN SICCMX_PI_Variables piv ON piv.Id=det.IdVariable
INNER JOIN SICCMX_Persona_PI ppi ON ppi.IdPersona=per.IdPersona
INNER JOIN SICCMX_VW_Credito_NMC cre ON per.IdPersona=cre.IdPersona
INNER JOIN SICCMX_Anexo21 anx ON ppi.IdPersona = anx.IdPersona
WHERE piv.IdMetodologia=21 AND cre.MontoValorizado>0
) t PIVOT (max(Dato) FOR Nombre IN ('+@Ids+')) AS pvt';


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_Analitico_A21;

-- Informacion para reporte
INSERT INTO dbo.RW_Analitico_A21 (
 IdReporteLog, Codigo, Nombre, PonderadoCuantitativo, PonderadoCualitativo, FactorTotal, [PI], SinAtrasos, AntSocInfCred_V, AntSocInfCred_P, QuitasCastReest_V,
 QuitasCastReest_P, PrctPagoInstNoBanc_V, PrctPagoInstNoBanc_P, PrctPagosEntComer_V, PrctPagosEntComer_P, CuentasCredAbiertosInstFin_V,
 CuentasCredAbiertosInstFin_P, MontoMaxInstFin_V, MontoMaxInstFin_P, MesesUltCredAbierto_V, MesesUltCredAbierto_P, PrctPagoInstFinBanc_V,
 PrctPagoInstFinBanc_P, PrctPagoInstFin29Atr_V, PrctPagoInstFin29Atr_P, PrctPagoInstFin90Atr_V, PrctPagoInstFin90Atr_P, DiasMoraInstFinBanc_V,
 DiasMoraInstFinBanc_P, PagosInstFinBanc_V, PagosInstFinBanc_P, AportInfonavit_V, AportInfonavit_P, DiasAtrInfonavit_V, DiasAtrInfonavit_P,
 TasaRetLab_V, TasaRetLab_P, IndPerMoralFid_V, IndPerMoralFid_P, ProcOrigAdmon_V, ProcOrigAdmon_P
)
EXECUTE (@query);


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Analitico_A21 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO

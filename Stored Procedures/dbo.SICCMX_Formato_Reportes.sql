SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Formato_Reportes]
 @IdReporte INT
AS
DECLARE @qry VARCHAR(1000);
DECLARE @tabla VARCHAR(50);
DECLARE @campo VARCHAR(100);
DECLARE @tipo INT;
DECLARE @longitud INT;
DECLARE @decimales INT;
DECLARE @predeterminado VARCHAR(250);
DECLARE @aplicaFormatoVP BIT;

DECLARE crs CURSOR FOR
SELECT
 rep.TablaOrigen,
 cmp.Codename,
 CASE WHEN rel.IdRel IS NULL THEN cmp.TipoDato ELSE rel.TipoDato END,
 ISNULL(CASE WHEN rel.IdRel IS NULL THEN cmp.Longitud ELSE rel.Longitud END, 0),
 ISNULL(CASE WHEN rel.IdRel IS NULL THEN cmp.Decimales ELSE rel.Decimales END, 0),
 CASE WHEN rel.IdRel IS NULL THEN cmp.Predeterminado ELSE rel.Predeterminado END,
 ISNULL(CASE WHEN rel.IdRel IS NULL THEN cmp.AplicaFormatoVP ELSE rel.AplicaFormatoVP END, 0)
FROM dbo.RW_Reporte rep
INNER JOIN dbo.RW_ReporteCampos cmp ON rep.IdReporte = cmp.IdReporte
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.CodeName = 'FRM_REP_CFG'
LEFT OUTER JOIN dbo.RW_Rel_Grupo_ReporteCampos rel ON cmp.IdReporteCampos = rel.IdReporteCampos AND rel.IdGroup = cfg.Value
WHERE cmp.IdReporte = @IdReporte;

OPEN crs;

FETCH NEXT FROM crs INTO @tabla, @campo, @tipo, @longitud, @decimales, @predeterminado, @aplicaFormatoVP;

WHILE @@FETCH_STATUS = 0
BEGIN

IF @tipo = 1 -- ALFANUMERICO
BEGIN

SET @qry =
'UPDATE dbo.' + @tabla + ' ' +
'SET ' + @campo
+ ' = ISNULL(NULLIF(' + @campo + ',''''), NULLIF(''' + ISNULL(@predeterminado,'') + ''',''''))'
EXEC(@qry);

END
ELSE IF @tipo = 2 -- NUMERICO
BEGIN

SET @qry =
'UPDATE dbo.' + @tabla + ' ' +
'SET ' + @campo
+ ' = CASE WHEN LEN(ISNULL(' + @campo + ','''')) = 0 AND 0 = ' + CAST(@aplicaFormatoVP AS VARCHAR(1)) + ' THEN '''
+ ISNULL(@predeterminado,'') + ''' ELSE CAST(CAST(ISNULL(NULLIF(' + @campo + ',''''), NULLIF(''' + ISNULL(@predeterminado,'') + ''','''')) AS '
+ 'DECIMAL(' + CAST(@longitud + @decimales AS VARCHAR) + ',' + CAST(@decimales AS VARCHAR) + ')) AS VARCHAR) END '
+ 'WHERE ISNUMERIC(ISNULL(NULLIF(' + @campo + ',''''), NULLIF(''' + ISNULL(@predeterminado,'') + ''',''''))) = 1'
EXEC(@qry);

END

FETCH NEXT FROM crs INTO @tabla, @campo, @tipo, @longitud, @decimales, @predeterminado, @aplicaFormatoVP;

END

CLOSE crs;
DEALLOCATE crs;
GO

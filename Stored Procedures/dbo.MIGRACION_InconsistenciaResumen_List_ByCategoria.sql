SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_InconsistenciaResumen_List_ByCategoria]
	@IdCategoria BIGINT = NULL
AS
SELECT vw.IdInconsistenciaResumen,
	vw.IdInconsistencia,
	vw.InconsistenciaNombre,
	vw.Numero,
	vw.Fecha,
	vw.IdCategoria,
	vw.CategoriaNombre
FROM dbo.MIGRACION_VW_InconsistenciaResumen vw
WHERE vw.IdCategoria = ISNULL(@IdCategoria,vw.IdCategoria) AND vw.Numero > 0
ORDER BY vw.CategoriaNombre, vw.InconsistenciaNombre;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Inconsistencia_Delete]
	@IdInconsistencia INT
AS
DELETE FROM dbo.MIGRACION_Inconsistencia WHERE IdInconsistencia = @IdInconsistencia;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_ClasificacionContable_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_ClasificacionContable_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_ClasificacionContable sit ON LTRIM(f.ClasificacionContable) = sit.Codigo
WHERE LEN(ISNULL(f.ClasificacionContable,'')) > 0 AND sit.IdClasificacionContable IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCredito,
 'ClasificacionContable',
 f.ClasificacionContable,
 2,
 @Detalle
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_ClasificacionContable sit ON LTRIM(f.ClasificacionContable) = sit.Codigo
WHERE LEN(ISNULL(f.ClasificacionContable,'')) > 0 AND sit.IdClasificacionContable IS NULL;
GO

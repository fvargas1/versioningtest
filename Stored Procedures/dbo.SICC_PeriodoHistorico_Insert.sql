SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_PeriodoHistorico_Insert] (
	@Fecha datetime = NULL,
	@Version varchar(50) = NULL,
	@Username VARCHAR(50) = NULL,
	@CarteraComercial MONEY,
	@CastigosNetos MONEY,
	@Activo BIT,
	@Reporte BIT
)
AS
DECLARE @IdPeriodoHistorico BIGINT;

UPDATE dbo.SICC_PeriodoHistorico SET Activo=NULL WHERE Fecha=@Fecha;

INSERT INTO dbo.SICC_PeriodoHistorico (
	Fecha,
	Version,
	FechaCreacion,
	Username,
	Activo
) VALUES (
	@Fecha,
	@Version,
	GETDATE(),
	@Username,
	1
)

SET @IdPeriodoHistorico=SCOPE_IDENTITY();

IF NOT EXISTS (SELECT IdPeriodoHistorico FROM dbo.SICC_PeriodoHistorico WHERE ISNULL(Reporte,0)=1 AND Fecha=@Fecha)
BEGIN
UPDATE dbo.SICC_PeriodoHistorico SET Reporte=1 WHERE IdPeriodoHistorico=@IdPeriodoHistorico;
END

SELECT @IdPeriodoHistorico;
GO

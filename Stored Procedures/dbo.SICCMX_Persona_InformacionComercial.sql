SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_InformacionComercial]
 @IdPersona BIGINT
AS
SELECT
 rw.Codigo,
 rw.Nombre,
 rw.RFC,
 rw.MetodologiaNombre,
 rw.MetodologiaDesc,
 rw.Promotor,
 rw.Domicilio,
 rw.Telefono,
 rw.Correo,
 rw.ActividadEconomica,
 rw.SocioPrincipal,
 rw.GrupoEconomico,
 rw.EI,
 rw.Reserva,
 rw.Calificacion
FROM dbo.RW_CedulaNMC_Info rw
WHERE rw.IdPersona=@IdPersona;
GO

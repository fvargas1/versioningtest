SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_010_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La "COMISIONES COBRADAS AL ACREDITADO (DENOMINADAS EN TASA)" debe ser en base 100 y a dos decimales.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0492
WHERE LEN(SUBSTRING(ComisionesCobradasTasa,CHARINDEX('.',ComisionesCobradasTasa)+1,LEN(ComisionesCobradasTasa))) <> 2;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO

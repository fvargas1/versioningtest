SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Actualiza_PI_Banco]
AS
UPDATE hrv
SET [PI] = inf.[PI]
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_HipotecarioInfo inf ON inf.IdHipotecario = hrv.IdHipotecario
WHERE inf.[PI] IS NOT NULL;
GO

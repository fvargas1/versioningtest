SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnFormat_Name](
	@texto VARCHAR(250)
)
RETURNS VARCHAR(250)
AS
BEGIN
SET @texto =
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	REPLACE(
	@texto
	,'\',' ')
	,'/',' ')
	,':',' ')
	,'*',' ')
	,'?',' ')
	,'"',' ')
	,'<',' ')
	,'>',' ')
	,'|',' ')
	,'Ã“','O')
	,'Ã‰','E')
	,'Ã','I')
	,'Ã‘','N')
	,'Ãš','U')
	,'IA‘','')
	,'I','Á')
	,'I','Í')
	,'I‘','Ñ')
	,'Iš','Ú')
	,'ÃƒÂ','A')
	,'-',' ')
	,'.','');
RETURN @texto;
END;
GO

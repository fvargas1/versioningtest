SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_R04A0424CreditosMovimientos_AnteriorTaven_Suma]
AS
SELECT
	cr.Codigo,
	SUM(CAST(mov.Monto AS DECIMAL)) AS Monto
FROM dbo.SICCMX_Credito cr
INNER JOIN dbo.SICCMX_Credito_Movimientos mov ON mov.IdCredito = cr.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN dbo.SICC_VW_R04ACreditosMovimiento_FechaPrimerTaven taven ON taven.IdCredito = cr.IdCredito
WHERE cr.IdSituacionCredito=2 AND mov.Fecha <= taven.Fecha AND tm.Codigo NOT IN ('TAVEN','TAVIG')
GROUP BY cr.Codigo
UNION ALL
-- Seleccionar los creditos con movimiento taven sin pagos antes del movto
SELECT
	cr.Codigo,
	0 AS Monto
FROM dbo.SICCMX_Credito cr
INNER JOIN R04.[0424Datos] dat ON cr.Codigo = dat.Codigo
WHERE 0 = (
	SELECT COUNT(m.IdTipoMovimiento)
	FROM dbo.SICCMX_Credito_Movimientos m
	INNER JOIN dbo.SICC_VW_R04ACreditosMovimiento_FechaPrimerTaven taven ON taven.IdCredito = cr.IdCredito
	INNER JOIN dbo.SICC_TipoMovimiento tm ON m.IdTipoMovimiento = tm.IdTipoMovimiento AND tm.Codigo NOT IN ('TAVEN','TAVIG')
	WHERE m.IdCredito = cr.IdCredito AND m.Fecha <= taven.Fecha
) AND dat.SituacionActual = '2' AND dat.SituacionHistorica = '1';
GO

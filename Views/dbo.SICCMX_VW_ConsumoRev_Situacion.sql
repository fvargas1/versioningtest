SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_ConsumoRev_Situacion]
AS
SELECT
	crv.IdConsumo,
	CASE WHEN inf.TarjetaActiva = 1 THEN
	CASE crv.Tipo_EX
	WHEN 0 THEN 6
	WHEN 3.1 THEN 7
	WHEN 3.2 THEN 8
	WHEN 4 THEN 9
	END
	ELSE
	CASE crv.Tipo_EX
	WHEN 0 THEN 10
	WHEN 3.1 THEN 11
	WHEN 3.2 THEN 12
	WHEN 4 THEN 13
	END
	END AS Situacion
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_ConsumoInfo inf ON crv.IdConsumo = inf.IdConsumo;
GO

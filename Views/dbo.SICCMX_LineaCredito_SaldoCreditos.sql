SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_LineaCredito_SaldoCreditos]
AS
SELECT IdLineaCredito, SUM(ISNULL(MontoValorizado,0)) AS Monto
FROM dbo.SICCMX_VW_Credito_NMC
GROUP BY IdLineaCredito
GO

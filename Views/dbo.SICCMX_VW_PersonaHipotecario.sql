SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_PersonaHipotecario]
AS
SELECT DISTINCT TOP 1000
	p.IdPersona AS Id,
	p.Codigo,
	p.Nombre AS Description,
	p.Nombre AS Keywords,
	'persona_hipotecario' AS ObjectType
FROM dbo.SICCMX_Persona p
INNER JOIN dbo.SICCMX_Hipotecario c ON c.IdPersona = p.IdPersona
ORDER BY p.Nombre;
GO

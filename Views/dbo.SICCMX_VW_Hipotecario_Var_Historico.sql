SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Hipotecario_Var_Historico]
AS
SELECT
	hip.IdHipotecario,
	pre.ATR AS ATR,
	pre.VPago AS PrctPagoEx,
	1 AS RN
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo hInfo ON hInfo.IdHipotecario = hip.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hip.IdHipotecario = pre.IdHipotecario

UNION ALL

SELECT
	hipAct.IdHipotecario,
	pre.ATR,
	pre.VPago AS PrctPagoEx,
	(ROW_NUMBER() OVER(PARTITION BY pre.Hipotecario ORDER BY pre.IdPeriodoHistorico DESC)) + 1 AS RN
FROM Historico.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN Historico.SICCMX_Hipotecario hip ON pre.Hipotecario = hip.Hipotecario AND pre.IdPeriodoHistorico = hip.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_HipotecarioInfo hInfo ON pre.Hipotecario = hInfo.Hipotecario AND pre.IdPeriodoHistorico = hInfo.IdPeriodoHistorico
INNER JOIN dbo.SICCMX_Hipotecario hipAct ON pre.Hipotecario = hipAct.Codigo
INNER JOIN dbo.SICC_PeriodoHistorico hst ON pre.IdPeriodoHistorico = hst.IdPeriodoHistorico AND hst.Activo = 1
INNER JOIN dbo.SICC_Periodo per ON hst.Fecha < per.Fecha AND per.Activo = 1;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_ACM_SICC]
AS
SELECT
	Periodo_Actual,
	Tipo_Cred_R04A,
	Fecha_Ven,
	Sector_Lab,
	Situacion_Cred,
	Interes_Vig,
	Interes_Ven,
	Saldo_Total,
	Moneda,
	ISNULL(Tipo_Cred_R04A,'')+'|'+ISNULL(Sector_Lab,'')+'|'+ISNULL(Situacion_Cred,'')+'|'+ISNULL(Moneda,'') AS EVERYTHING
FROM dbo.RW_ACM_SICC;
GO

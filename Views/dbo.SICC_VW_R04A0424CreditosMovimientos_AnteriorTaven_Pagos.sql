SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_R04A0424CreditosMovimientos_AnteriorTaven_Pagos]
AS
SELECT
	cr.Codigo,
	conf.Concepto,
	mov.Monto
FROM dbo.SICCMX_Credito cr
INNER JOIN dbo.SICCMX_Credito_Movimientos mov ON mov.IdCredito = cr.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN dbo.SICCMX_CreditoInfo info ON info.IdCredito = cr.IdCredito
INNER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4
INNER JOIN R04.[0424Configuracion] conf ON tp.Codigo LIKE conf.CodigoProducto AND conf.TipoMovimiento = tm.Codigo
INNER JOIN dbo.SICC_VW_R04ACreditosMovimiento_FechaPrimerTaven taven ON taven.IdCredito = cr.IdCredito
LEFT OUTER JOIN dbo.SICC_SituacionCredito sit ON cr.IdSituacionCredito = sit.IdSituacionCredito
WHERE sit.Codigo = '2' AND mov.Fecha < taven.Fecha AND tm.Codigo NOT IN ('TAVEN','TAVIG');
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Personas_Creditos_Vencidos_PI100]
AS
SELECT DISTINCT per.IdPersona
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN SICCMX_Persona per ON cre.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN (
	SELECT vwCr.IdPersona, SUM(CASE WHEN sit.Codigo='2' THEN vwCr.MontoValorizado ELSE 0 END) AS SaldoVencido, SUM(vwCr.MontoValorizado) * .05 AS Total_5_Prct
	FROM dbo.SICCMX_VW_Credito_NMC vwCr
	LEFT OUTER JOIN SICC_SituacionCredito sit ON vwCr.IdSituacionCredito = sit.IdSituacionCredito
	GROUP BY vwCr.IdPersona
) AS total ON cre.IdPersona = total.IdPersona
WHERE ISNULL(met.Codigo, '0') IN ('20','21', '22') AND cre.MontoValorizado>0 AND total.SaldoVencido >= total.Total_5_Prct;
GO

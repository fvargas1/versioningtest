SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_CreditosCNBV]
AS
SELECT cre.IdCredito, cnbv.NumeroLinea, cnbv.CNBV
FROM dbo.SICCMX_CreditoCNBV cnbv
INNER JOIN dbo.SICCMX_Credito cre ON cnbv.CodigoCredito = cre.Codigo;
GO

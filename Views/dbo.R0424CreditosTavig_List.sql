SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[R0424CreditosTavig_List]
AS
-- Seleccionar los creditos con movimiento tavig
SELECT cr.Codigo
FROM dbo.SICCMX_Credito_Movimientos mov
INNER JOIN dbo.SICCMX_Credito cr ON mov.IdCredito = cr.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tm ON mov.IdTipoMovimiento = tm.IdTipoMovimiento
WHERE tm.Codigo = 'TAVIG'
UNION ALL
SELECT cr.Codigo
FROM dbo.SICCMX_Consumo_Movimientos mov
INNER JOIN dbo.SICCMX_Consumo cr ON mov.IdConsumo = cr.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tm ON mov.IdTipoMovimiento = tm.IdTipoMovimiento
WHERE tm.Codigo = 'TAVIG'
UNION ALL
SELECT cr.Codigo
FROM dbo.SICCMX_Hipotecario_Movimientos mov
INNER JOIN dbo.SICCMX_Hipotecario cr ON mov.IdHipotecario = cr.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tm ON mov.IdTipoMovimiento = tm.IdTipoMovimiento
WHERE tm.Codigo = 'TAVIG';
GO

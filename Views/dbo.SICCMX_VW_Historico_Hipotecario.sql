SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Historico_Hipotecario]
AS
SELECT
 vw.IdHipotecario,
 MAX(CASE WHEN vw.RN <= 4 THEN vw.ATR ELSE 0 END) AS MAXATR,
 (SUM(CASE WHEN vw.RN <= 7 THEN vw.PrctPagoEx ELSE 0 END) + dbo.MAX2VAR(7-MAX(vw.RN),0)) / 7 AS PrctPagoEx,
 (SUM(CASE WHEN vw.RN <= 4 THEN vw.PrctPagoEx ELSE 0 END) + dbo.MAX2VAR(4-MAX(vw.RN),0)) / 4 AS PrctPagoIM,
 MAX(CASE WHEN vw.RN <= 7 THEN vw.ATR ELSE 0 END) AS MAXATR_REA_PRO
FROM dbo.SICCMX_VW_Hipotecario_Var_Historico vw
GROUP BY vw.IdHipotecario;
GO

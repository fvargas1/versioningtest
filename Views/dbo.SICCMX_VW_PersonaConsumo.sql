SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_PersonaConsumo]
AS
SELECT DISTINCT TOP 1000
	p.IdPersona AS Id,
	p.Codigo,
	p.Nombre AS Description,
	p.Nombre AS Keywords,
	'persona_consumo' AS ObjectType
FROM dbo.SICCMX_Persona p
INNER JOIN dbo.SICCMX_Consumo c ON c.IdPersona = p.IdPersona
ORDER BY p.Nombre;
GO

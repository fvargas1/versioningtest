SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_A0420CreditosTAVEN_2016]
AS
-- Creditos con SituacionHistorica Vigente y Actual Vencida
SELECT dat.Codigo
FROM R04.[0420Datos_2016] dat
WHERE (dat.SituacionHistorica = '1' AND dat.SituacionActual = '2');
GO

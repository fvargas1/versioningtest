SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_0419_MovCargos]
AS
-- CREDITO DADOS DE BAJA
SELECT
 dat.Codigo,
 conf.Concepto,
 dat.SaldoReservaCubHistorico+dat.SaldoReservaExpHistorico+dat.ReservasAdicOrdenadasCNBVHistorico+dat.InteresCarteraVencidaHistorico AS Reserva
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CANCELACION'
WHERE dat.IdTipoBaja IS NOT NULL
AND dat.SaldoReservaCubHistorico+dat.SaldoReservaExpHistorico+dat.ReservasAdicOrdenadasCNBVHistorico+dat.InteresCarteraVencidaHistorico > 0

UNION ALL
-- RESERVAS ADICIONALES
SELECT
 dat.Codigo,
 '139309000000' AS Concepto,
 (dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico) - (dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida)
FROM R04.[0419Datos_2016] dat
WHERE dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico > dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida
AND dat.IdTipoBaja IS NULL AND SaldoActual > 0
AND (dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico) - (dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida) > 0

UNION ALL
--AJUSTE CAMBIARIO
SELECT
 dat.Codigo,
 conf.Concepto,
 ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'AJCAMBCARGO'
WHERE dat.IdTipoBaja IS NULL AND dat.AjusteCambiario < 0
AND ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual > 0

UNION ALL
-- CREDITOS SIN SALDO Y SIN TIPO DE BAJA
SELECT
 dat.Codigo,
 conf.Concepto,
 dat.SaldoReservaCubHistorico+dat.SaldoReservaExpHistorico+dat.ReservasAdicOrdenadasCNBVHistorico+dat.InteresCarteraVencidaHistorico
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CANCELACION'
WHERE dat.IdTipoBaja IS NULL AND dat.SaldoActual = 0
AND dat.SaldoReservaCubHistorico+dat.SaldoReservaExpHistorico+dat.ReservasAdicOrdenadasCNBVHistorico+dat.InteresCarteraVencidaHistorico > 0

UNION ALL
SELECT
 dat.Codigo,
 conf.Concepto,
 ABS(ABS(dat.ReservasDif) - CAST(ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual AS DECIMAL))
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CANCELACION'
WHERE dat.SaldoHistorico > 0 AND dat.Moneda = '0' AND dat.IdTipoBaja IS NULL AND dat.ReservasDif < 0
AND ABS(dat.ReservasDif) - ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual < 0
AND ABS(ABS(dat.ReservasDif) - ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual) > 0

UNION ALL
SELECT
 dat.Codigo,
 conf.Concepto,
 ABS(ABS(dat.ReservasDif) + CAST(ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual AS DECIMAL))
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CANCELACION'
WHERE dat.SaldoHistorico > 0 AND dat.Moneda = '0' AND dat.IdTipoBaja IS NULL AND dat.ReservasDif > 0 AND dat.SaldoActual > 0
AND ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual > dat.ReservasDif

UNION ALL
-- MOVIMIENTOS CREDITOS VIEJOS
SELECT
 dat.Codigo,
 conf.Concepto,
 mov.Monto * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual) / dat.SaldoActual
FROM R04.[0419Datos_2016] dat
INNER JOIN dbo.SICCMX_VW_0419_Movimientos mov ON dat.Codigo = mov.Codigo
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND mov.TipoMovimiento = conf.TipoMovimiento
WHERE dat.IdTipoBaja IS NULL AND dat.SaldoActual > 0 AND mov.TipoMovimiento NOT IN ('PAGO','PAGOC','PAGOI')

UNION ALL
SELECT
 dat.Codigo,
 conf.Concepto,
 dat.ReservasDif
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'PAGO'
WHERE dat.SaldoHistorico = dat.SaldoActual AND dat.Moneda = '0' AND dat.ReservasDif > 0

UNION ALL
-- AJUSTE EN MOVIMIENTOS CONTRA DIFERENCIA DE SALDOS
SELECT
 dat.Codigo,
 conf.Concepto,
 dat.ReservasDif
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CANCELACION'
WHERE dat.IdTipoBaja IS NULL AND dat.Moneda = '0' AND dat.ReservasDif <= dat.ReservasCargos AND dat.ReservasDif > 0

UNION ALL
SELECT
 dat.Codigo,
 conf.Concepto,
 dat.ReservasCargos
FROM R04.[0419Datos_2016] dat
INNER JOIN (
	SELECT DISTINCT Codigo
	FROM dbo.SICCMX_VW_0419_Movimientos
	WHERE TipoMovimiento NOT IN ('PAGO','PAGOC','PAGOI')
) AS mov ON dat.Codigo = mov.Codigo
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'PAGO'
WHERE dat.IdTipoBaja IS NULL AND dat.SaldoActual > 0 AND dat.ReservasCargos > 0
GO

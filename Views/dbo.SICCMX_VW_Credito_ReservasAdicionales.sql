SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Credito_ReservasAdicionales]
AS
SELECT
	cre.IdCredito,
	ISNULL(cre.InteresCarteraVencidaValorizado,0) AS InteresCarteraVencidaValorizado,
	ISNULL(inf.ReservasAdicionales,0) AS ReservasAdicionales,
	ISNULL(cre.InteresCarteraVencidaValorizado,0)
	+ ISNULL(inf.ReservasAdicionales,0) AS ReservasAdicTotal
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_CreditoInfo inf ON cre.IdCredito = inf.IdCredito;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Persona_PI_GP]
AS
SELECT
 ppi.IdGP,
 ppi.EsGarante,
 ppi.FactorCuantitativo,
 ppi.PonderadoCuantitativo,
 ppi.FactorCualitativo,
 ppi.PonderadoCualitativo,
 ppi.FactorTotal,
 ppi.[PI],
 ppi.IdMetodologia,
 met.Nombre AS IdMetodologiaNombre,
 val.FactorCuantitativo AS PrctCuantitativo,
 val.FactorCualitativo AS PrctCualitativo
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_Metodologia met ON ppi.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICCMX_PI_ValoresDelta val ON val.IdMetodologia = ppi.IdMetodologia AND ISNULL(val.IdClasificacion, '') = ISNULL(ppi.IdClasificacion, '');
GO

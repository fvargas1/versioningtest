SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_R04A0420ConsumoMovimientos_AnteriorTavig_Suma]
AS
SELECT
	cr.Codigo,
	SUM(CAST(mov.Monto AS DECIMAL)) AS Monto
FROM dbo.SICCMX_Consumo cr
INNER JOIN dbo.SICCMX_Consumo_Movimientos mov ON mov.IdConsumo = cr.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN dbo.SICC_VW_R04AConsumoMovimiento_FechaPrimerTavig tavig ON tavig.IdConsumo = cr.IdConsumo
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sit ON cr.IdSituacionCredito = sit.IdSituacionConsumo
WHERE sit.Codigo = '1' AND mov.Fecha <= tavig.Fecha AND tm.Codigo NOT IN ('TAVEN','TAVIG')
GROUP BY cr.Codigo

UNION ALL 
-- Seleccionar los creditos con movimiento taven sin pagos antes del movto
SELECT
	cr.Codigo,
	0 AS Monto
FROM dbo.SICCMX_Consumo cr
INNER JOIN R04.[0420Datos] dat ON cr.Codigo = dat.Codigo
WHERE 0 = (
	SELECT COUNT(m.IdTipoMovimiento)
	FROM dbo.SICCMX_Consumo_Movimientos m
	INNER JOIN dbo.SICC_VW_R04AConsumoMovimiento_FechaPrimerTavig tavig ON tavig.IdConsumo = m.IdConsumo
	INNER JOIN dbo.SICC_TipoMovimiento tm ON m.IdTipoMovimiento = tm.IdTipoMovimiento AND tm.Codigo NOT IN ('TAVEN','TAVIG')
	WHERE m.IdConsumo = cr.IdConsumo AND m.Fecha <= tavig.Fecha
) AND dat.SituacionActual = '1' AND dat.SituacionHistorica = '2';
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Credito_RCCalificacion]
AS
SELECT
	cre.IdCredito,
	DATEDIFF(MONTH, cre.FechaDisposicion, cre.FechaVencimiento) AS MesesVencimiento,
	CASE WHEN DATEDIFF(MONTH, cre.FechaDisposicion, cre.FechaVencimiento) <= 12 THEN inf.IdRCCalificacionCortoPlazo ELSE inf.IdRCCalificacion END AS IdRCCalificacion
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo inf ON per.IdPersona = inf.IdPersona;
GO

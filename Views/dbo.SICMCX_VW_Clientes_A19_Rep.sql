SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICMCX_VW_Clientes_A19_Rep]
AS
SELECT DISTINCT
 cre.IdPersona,
 CASE
 WHEN anx20.IdPersona IS NOT NULL THEN '20'
 WHEN anx21.IdPersona IS NOT NULL THEN CASE WHEN ISNULL(anx21.OrgDescPartidoPolitico,0) = 0 THEN '21' ELSE '21OD' END
 WHEN anx22.IdPersona IS NOT NULL THEN CASE WHEN ISNULL(anx22.OrgDescPartidoPolitico,0) = 0 THEN '22' ELSE '22OD' END
 WHEN tpoPer.A18 IS NOT NULL THEN '18'
 WHEN pInfo.IngresosBrutos < tpoC.MinUDIS THEN CASE WHEN tpoPer.A21OD IS NULL THEN '21' ELSE '21OD' END
 WHEN pInfo.IngresosBrutos >= tpoC.MinUDIS THEN CASE WHEN tpoPer.A22OD is NULL THEN '22' ELSE '22OD' END
 ELSE 'SM' END AS Metodologia,
 tpoPer.Codigo AS TipoPersona,
 pInfo.IngresosBrutos
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON cre.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICCMX_Anexo20 anx20 ON pInfo.IdPersona = anx20.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Anexo21 anx21 ON pInfo.IdPersona = anx21.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Anexo22 anx22 ON pInfo.IdPersona = anx22.IdPersona
LEFT OUTER JOIN dbo.SICC_TipoPersona tpoPer ON pInfo.IdTipoPersona = tpoPer.IdTipoPersona
LEFT OUTER JOIN (
 SELECT tc.Valor * CAST(cfgMIN.[Value] AS DECIMAL(23,2)) AS MinUDIS
 FROM dbo.SICC_TipoCambio tc
 INNER JOIN dbo.SICC_Periodo prd ON tc.IdPeriodo = prd.IdPeriodo AND prd.Activo = 1
 INNER JOIN dbo.SICC_Moneda mon ON tc.IdMoneda = mon.IdMoneda
 INNER JOIN dbo.BAJAWARE_Config cfg ON mon.Codigo = cfg.[Value] AND cfg.CodeName = 'MonedaUDIS'
 INNER JOIN dbo.BAJAWARE_Config cfgMIN ON cfgMIN.CodeName='A22_UDIS_MENOR1'
) AS tpoC ON 1=1
WHERE met.Codigo = '4';
GO

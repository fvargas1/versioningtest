SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_ReporteApoyo]
AS
SELECT
	ReporteApoyo.IdReporteApoyo,
	ReporteApoyo.IdReporte,
	idreporte.Nombre AS IdReporteNombre,
	ReporteApoyo.Nombre,
	ReporteApoyo.Descripcion,
	ReporteApoyo.Activo,
	ReporteApoyo.OnDemand,
	ReporteApoyo.NombreArchivo,
	ReporteApoyo.SSRSTemplate,
	ReporteApoyo.IdUsuarioCreacion,
	'' AS UsuarioCreacionNombre,
	ReporteApoyo.FechaCreacion,
	ReporteApoyo.IdUsuarioModificacion,
	'' AS UsuarioModificacionNombre,
	ReporteApoyo.FechaModificacion
FROM dbo.RW_ReporteApoyo AS ReporteApoyo
INNER JOIN dbo.RW_Reporte idreporte ON ReporteApoyo.IdReporte = idreporte.IdReporte;
GO

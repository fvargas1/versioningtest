SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_ReporteLog]
AS
SELECT
	ReporteLog.IdReporteLog,
	ReporteLog.IdReporte,
	idreporte.Nombre AS IdReporteNombre,
	ReporteLog.Descripcion,
	ReporteLog.FechaCreacion,
	ReporteLog.UsuarioCreacion,
	ReporteLog.FolderArchivo,
	ReporteLog.IdFuenteDatos,
	'' AS IdFuenteDatosNombre,
	ReporteLog.NombreArchivo,
	ReporteLog.FechaImportacionDatos,
	ReporteLog.FechaCalculoProcesos,
	ReporteLog.TotalRegistros,
	ReporteLog.TotalSaldos,
	ReporteLog.TotalIntereses
FROM dbo.RW_ReporteLog AS ReporteLog
INNER JOIN dbo.RW_Reporte idreporte ON ReporteLog.IdReporte = idreporte.IdReporte;
GO

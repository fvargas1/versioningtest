SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_R04AHipotecarioMovimiento_FechaPrimerTavig]
AS
SELECT
	mov.IdHipotecario,
	MAX(mov.Fecha) AS Fecha
FROM dbo.SICCMX_Hipotecario_Movimientos mov
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
WHERE tm.Codigo='TAVIG'
GROUP BY mov.IdHipotecario;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [R04].[VW_0419Abonos_CalculoDiferente]
AS
/* creditos que en el periodo anterior tenian una calif y actualmente dos, y una de ellas se ha mantenido igual. Se calculan diferente.*/
SELECT dat.Codigo
FROM R04.[0419Datos] dat
INNER JOIN dbo.RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo =  dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE dat.CalifCubHistorico IS NULL AND dat.CalifCubActual IS NOT NULL AND dat.CalifExpHistorico = CalifExpActual AND SaldoReservaExpHistorico - SaldoReservaExpActual > 0

UNION ALL
/**
Caso 2: Cuando la calificacion Expuesta Actual es diferente de la anterior y En este periodo
se crea una calificaicon Cubierta
**/
SELECT dat.Codigo
FROM R04.[0419Datos] dat
INNER JOIN dbo.RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE ((CalifExpHistorico<>CalifExpActual AND CalifCubHistorico IS NULL AND CalifCubActual IS NOT NULL)
	OR (CalifCubHistorico<>CalifCubActual AND CalifExpHistorico IS NULL AND CalifExpActual IS NOT NULL))
	AND (SaldoReservaExpActual+SaldoReservaCubActual)-(SaldoReservaExpHistorico+SaldoReservaCubHistorico) > 0;
GO

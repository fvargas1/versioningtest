SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Persona_PI_Factores]
AS
SELECT
 det.IdPersona,
 SUM(CASE WHEN fac.Codigo = 'FAC_CUALI' THEN ISNULL(det.PuntosActual,0) ELSE 0 END) FAC_CUALI,
 SUM(CASE WHEN fac.Codigo = 'FAC_CUANTI' THEN ISNULL(det.PuntosActual,0) ELSE 0 END) FAC_CUANTI
FROM dbo.SICCMX_Persona_PI_Detalles det
INNER JOIN dbo.SICCMX_PI_Variables v ON v.Id = det.IdVariable
INNER JOIN dbo.SICCMX_PI_SubFactor sub ON sub.Id = v.IdSubFactor
INNER JOIN dbo.SICCMX_PI_Factores fac ON fac.Id = sub.IdFactor
GROUP BY det.IdPersona;
GO

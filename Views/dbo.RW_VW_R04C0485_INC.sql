SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0485_INC]
AS
SELECT
	IdReporteLog,
	Formulario,
	CodigoCreditoCNBV,
	TipoBaja,
	SaldoPrincipalInicio,
	SaldoInsoluto,
	MontoPagado,
	MontoCastigos,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificaciones,
	MontoDescuentos,
	MontoBienDacion,
	ReservasCalifCanceladas,
	ReservasAdicCanceladas
FROM dbo.RW_R04C0485_2016
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.CodeName = 'ANIO_REP' AND cfg.[Value] = '2016';
GO

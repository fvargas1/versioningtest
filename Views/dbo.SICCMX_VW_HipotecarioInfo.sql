SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_HipotecarioInfo]
AS
SELECT
 hInfo.IdHipotecario,
 NULL AS IdProducto, --hInfo.IdProducto,
 NULL AS IdProductoNombre, --prod.Nombre AS IdProductoNombre,
 hInfo.IdCategoria,
 cat.Nombre AS IdCategoriaNombre,
 hInfo.IdTipoAlta,
 tpoAlt.Nombre AS IdTipoAltaNombre,
 hInfo.IdDestinoHipotecario AS IdDestino,
 dest.Nombre AS IdDestinoNombre,
 hInfo.FechaOtorgamiento,
 hInfo.FechaVencimiento,
 hInfo.IdMoneda,
 idmoneda.Nombre AS IdMonedaNombre,
 hInfo.MontoOriginal,
 hInfo.GastosOriginacion,
 hInfo.MontoSubsidio,
 hInfo.IdEntidadCofinanciadora,
 NULL AS IdEntidadCofinanciadoraNombre, --ent.Nombre AS IdEntidadCofinanciadoraNombre,
 hInfo.MontoSubcuenta,
 hInfo.MontoCofinanciador,
 NULL AS MontoBancaDesarrollo, --hInfo.MontoBancaDesarrollo,
 NULL AS IdApoyoBanca, --hInfo.IdApoyoBanca,
 NULL AS IdApoyoBancaNombre, --apy.Nombre AS IdApoyoBancaNombre,
 NULL AS ValorViviendaOriginacion, --hInfo.ValorViviendaOriginacion,
 NULL AS ValorInmuebleAvaluo, --hInfo.ValorInmuebleAvaluo,
 hInfo.NumeroAvaluo,
 hInfo.IdLocalidad,
 loc.NombreLocalidad AS IdLocalidadNombre,
 NULL AS FechaFirmaReestructuracion, --hInfo.FechaFirmaReestructuracion,
 NULL AS FechaVencimientoReestructurado, --hInfo.FechaVencimientoReestructurado,
 NULL AS MontoReestructurado, --hInfo.MontoReestructurado,
 hInfo.IdMonedaReestructurado,
 monRst.Nombre AS IdMonedaReestructuradoNombre,
 hInfo.IdPeriodicidadCapital,
 perCap.Nombre AS IdPeriodicidadCapitalNombre,
 NULL AS IdTipoTasa, --hInfo.IdTipoTasa,
 NULL AS IdTipoTasaNombre, --tasa.Nombre AS IdTipoTasaNombre,
 hInfo.IdTasaReferencia,
 tasaRef.Nombre AS IdTasaReferenciaNombre,
 NULL AS AjusteTasa, --hInfo.AjusteTasa,
 NULL AS IdSeguro, --hInfo.IdSeguro,
 NULL AS IdSeguroNombre, --idseguro.Nombre AS IdSeguroNombre,
 hInfo.IdTipoSeguro,
 seg.Nombre AS IdTipoSeguroNombre,
 hInfo.IdEntidadAseguradora,
 entAsg.Nombre AS IdEntidadAseguradoraNombre,
 NULL AS PorcentajeSeguro, --hInfo.PorcentajeSeguro,
 hInfo.TasaInteres,
 NULL AS ComisionesTasa, --hInfo.ComisionesTasa,
 NULL AS ComisionesMonto, --hInfo.ComisionesMonto,
 NULL AS MontoPagoExigible, --hInfo.MontoPagoExigible,
 NULL AS MontoPagosRealizados, --hInfo.MontoPagosRealizados,
 hInfo.MontoBonificacion,
 NULL AS FechaPago, --hInfo.FechaPago,
 hInfo.IdTipoBaja,
 tpoBaj.Nombre AS IdTipoBajaNombre,
 hInfo.ValorBienAdjudicado,
 NULL AS SaldoInicial, --hInfo.SaldoInicial,
 NULL AS SaldoFinal, --hInfo.SaldoFinal,
 NULL AS MontoAdeudado, --hInfo.MontoAdeudado,
 NULL AS MontoLiquidado, --hInfo.MontoLiquidado,
 NULL AS NumeroConsulta, --hInfo.NumeroConsulta
 hInfo.ReservasSaldoFinal,
 hInfo.ATR,
 hInfo.MaxATR,
 hInfo.VPago,
 hInfo.PorPago,
 hInfo.PorCLTVi,
 hInfo.PPagoIM,
 hInfo.TRi,
 hInfo.MON,
 hInfo.RAi,
 hInfo.CodigoCNBV,
 hInfo.PromedioDeRet,
 hInfo.IdTipoRegimenCredito,
 tr.Nombre AS NombreTipoRegimen,
 tr.FactorAjuste AS FactorAjusteRegimen
FROM dbo.SICCMX_HipotecarioInfo hInfo
LEFT OUTER JOIN dbo.SICC_CategoriaCredito cat ON hInfo.IdCategoria = cat.IdCategoria
LEFT OUTER JOIN dbo.SICC_TipoAlta tpoAlt ON hInfo.IdTipoAlta = tpoAlt.IdTipoAlta
LEFT OUTER JOIN dbo.SICC_DestinoHipotecario dest ON hInfo.IdDestinoHipotecario = dest.IdDestinoHipotecario
LEFT OUTER JOIN dbo.SICC_Moneda idmoneda ON hInfo.IdMoneda = idmoneda.IdMoneda
LEFT OUTER JOIN dbo.SICC_Localidad2015 loc ON hInfo.IdLocalidad = loc.IdLocalidad
LEFT OUTER JOIN dbo.SICC_Moneda monRst ON hInfo.IdMonedaReestructurado = monRst.IdMoneda
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON hInfo.IdPeriodicidadCapital = perCap.IdPeriodicidadCapital
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasaRef ON hInfo.IdTasaReferencia = tasaRef.IdTasaReferencia
LEFT OUTER JOIN dbo.SICC_TipoSeguro seg ON hInfo.IdTipoSeguro = seg.IdTipoSeguro
LEFT OUTER JOIN dbo.SICC_EntidadAseguradora entAsg ON hInfo.IdEntidadAseguradora = entAsg.IdEntidadAseguradora
LEFT OUTER JOIN dbo.SICC_TipoBaja tpoBaj ON hInfo.IdTipoBaja = tpoBaj.IdTipoBaja
LEFT OUTER JOIN dbo.SICC_TipoRegimen tr ON hInfo.IdTipoRegimenCredito = tr.IdTipoRegimen;
GO

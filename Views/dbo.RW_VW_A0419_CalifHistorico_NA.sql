SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_A0419_CalifHistorico_NA]
AS
--US:18112011 Creditos con sola una calificacion historica y dos calificaciones actuales
SELECT Codigo
FROM R04.[0419Datos]
WHERE (CalifCubHistorico IS NULL OR CalifExpHistorico IS NULL) AND NOT (CalifCubHistorico IS  NULL AND CalifExpHistorico IS NULL) AND (CalifCubActual IS NOT NULL AND CalifExpActual IS NOT NULL);
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_ProcesosMigracion]
AS
SELECT
 0 AS IdProceso,
 'Migración de Datos' AS Nombre,
 1 AS Nivel,
 'MIG' AS Codigo,
 MIN(arc.FechaInicio) AS FechaInicio,
 MAX(arc.FechaFin) AS FechaFin,
 MAX(arc.ValidationStatus) AS Estatus,
 2 AS MaxNivel,
 CAST(
 (SELECT CAST(COUNT(arc.IdArchivo) AS DECIMAL(5,2))
 FROM dbo.MIGRACION_Archivo arc
 INNER JOIN dbo.MIGRACION_Validacion val ON arc.Codename = val.CategoryName
 WHERE arc.Activo=1 AND val.Activo=1 AND val.FechaFin IS NOT NULL) /
 (SELECT CAST(COUNT(arc.IdArchivo) AS DECIMAL(5,2))
 FROM dbo.MIGRACION_Archivo arc
 INNER JOIN dbo.MIGRACION_Validacion val ON arc.Codename = val.CategoryName
 WHERE arc.Activo=1 AND val.Activo=1)
 * 100 AS INT) AS Progreso,
 0 AS Position
FROM dbo.MIGRACION_Archivo arc
WHERE arc.Activo=1
UNION ALL
SELECT
 arc.IdArchivo,
 arc.Nombre,
 2,
 arc.Codename,
 arc.FechaInicio,
 arc.FechaFin,
 arc.ValidationStatus,
 2,
 CASE WHEN (SELECT CAST(COUNT(pr.IdValidacion) AS DECIMAL(5,2)) FROM dbo.MIGRACION_Validacion pr WHERE pr.CategoryName=arc.Codename AND pr.Activo=1) = 0 THEN 0
 ELSE
 CAST(
 (SELECT CAST(COUNT(pr.IdValidacion) AS DECIMAL(5,2)) FROM dbo.MIGRACION_Validacion pr WHERE pr.CategoryName=arc.Codename AND pr.Activo=1 AND pr.FechaFin IS NOT NULL) /
 (SELECT CAST(COUNT(pr.IdValidacion) AS DECIMAL(5,2)) FROM dbo.MIGRACION_Validacion pr WHERE pr.CategoryName=arc.Codename AND pr.Activo=1)
 * 100 AS INT) END AS Progreso,
 arc.Position
FROM dbo.MIGRACION_Archivo AS arc
WHERE arc.Activo=1
GO

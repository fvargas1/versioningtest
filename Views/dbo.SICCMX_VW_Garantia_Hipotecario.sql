SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantia_Hipotecario]
AS
SELECT
	gar.IdGarantia,
	gar.Codigo,
	ISNULL(gar.ValorGarantia, 0) AS ValorGarantia,
	gar.IdTipoGarantia,
	tg.Codigo AS IdTipoGarantiaCodigo,
	gar.IdMoneda,
	mon.Codigo AS IdMonedaCodigo,
	gar.PIGarante,
	CAST(ISNULL(gar.ValorGarantia, 0) * tc.Valor AS DECIMAL(23,2)) AS ValorGtiaValorizado
FROM dbo.SICCMX_Garantia_Hipotecario AS gar
INNER JOIN dbo.SICC_TipoGarantia tg ON gar.IdTipoGarantia = tg.IdTipoGarantia
INNER JOIN dbo.SICC_Moneda mon ON gar.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo per ON per.IdPeriodo = tc.IdPeriodo AND per.Activo=1;
GO

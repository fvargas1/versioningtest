SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04AComercialCastigados]
AS
SELECT c.Codigo, m.Monto
FROM dbo.SICCMX_Credito c
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables r ON r.IdCredito = c.IdCredito
INNER JOIN dbo.SICCMX_Credito_Movimientos m ON m.IdCredito = c.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = m.IdTipoMovimiento
WHERE r.ReservaCubierta + r.ReservaExpuesta = 0 AND tm.Codigo = 'CAST';
GO

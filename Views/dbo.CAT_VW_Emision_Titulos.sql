SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CAT_VW_Emision_Titulos]
AS
SELECT
 item.IdItemCatalogo,
 item.Codigo,
 item.Nombre,
 item.Padre,
 item.Orden
FROM Bajaware.ItemCatalogo item
INNER JOIN Bajaware.Catalogos cat ON cat.IdCatalogo=item.IdCatalogo
WHERE cat.Codename='A20_EMISION_TITULOS';
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_DetalleReservas]
AS
SELECT DISTINCT
	Fecha,
	CodigoCredito,
	TipoProducto,
	ReservaInicial,
	ReservaFinal,
	Cargo,
	Abono,
	GradoRiesgo,
	ISNULL(CodigoCredito,'')+'|'+ISNULL(TipoProducto,'')+'|'+ISNULL(GradoRiesgo,'') AS EVERYTHING
FROM dbo.RW_DetalleReservas;
GO

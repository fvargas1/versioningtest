SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_PeriodoHistorico]
AS
SELECT
	PeriodoHistorico.IdPeriodoHistorico,
	PeriodoHistorico.Fecha,
	PeriodoHistorico.Version,
	PeriodoHistorico.FechaCreacion,
	PeriodoHistorico.Username,
	PeriodoHistorico.CarteraComercial,
	PeriodoHistorico.CastigosNetos,
	PeriodoHistorico.Activo,
	PeriodoHistorico.Reporte
FROM dbo.SICC_PeriodoHistorico AS PeriodoHistorico;
GO

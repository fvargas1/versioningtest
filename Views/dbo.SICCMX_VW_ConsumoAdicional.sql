SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_ConsumoAdicional]
AS
SELECT
	adi.IdConsumo,
	ISNULL(adi.SaldoPromedio,0) AS SaldoPromedio,
	CAST(ISNULL(adi.SaldoPromedio,0) * tc.Valor AS DECIMAL(23,2)) AS SaldoPromedioValorizado,
	CAST(CAST(ISNULL(adi.SaldoPromedio,0) * tc.Valor AS DECIMAL(23,2)) / usd.Valor AS DECIMAL(23,2)) AS SaldoPromedioValorizadoUSD,
	ISNULL(adi.InteresDevengado,0) AS InteresDevengado,
	CAST(ISNULL(adi.InteresDevengado,0) * tc.Valor AS DECIMAL(23,2)) AS InteresDevengadoValorizado,
	CAST(CAST(ISNULL(adi.InteresDevengado,0) * tc.Valor AS DECIMAL(23,2)) / usd.Valor AS DECIMAL(23,2)) AS InteresDevengadoValorizadoUSD,
	ISNULL(adi.Comision,0) AS Comision,
	CAST(ISNULL(adi.Comision,0) * tc.Valor AS DECIMAL(23,2)) AS ComisionValorizado,
	CAST(CAST(ISNULL(adi.Comision,0) * tc.Valor AS DECIMAL(23,2)) / usd.Valor AS DECIMAL(23,2)) AS ComisionValorizadoUSD
FROM dbo.SICCMX_ConsumoAdicional adi
INNER JOIN dbo.SICCMX_ConsumoInfo info ON adi.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = info.IdMoneda
INNER JOIN dbo.SICC_Periodo periodo ON periodo.IdPeriodo = tc.IdPeriodo AND periodo.Activo=1
LEFT OUTER JOIN (
	SELECT tcUSD.Valor
	FROM dbo.SICC_TipoCambio tcUSD
	INNER JOIN dbo.SICC_Moneda monUSD ON tcUSD.IdMoneda = monUSD.IdMoneda AND monUSD.Codigo='1'
	INNER JOIN dbo.SICC_Periodo perUSD ON tcUSD.IdPeriodo = perUSD.IdPeriodo AND perUSD.Activo=1
)usd ON 1=1;
GO

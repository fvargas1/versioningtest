SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_InsumoRC15]
AS
SELECT
	CodigoCredito,
	CodigoPersona,
	RFC,
	MontoGarantiasReales,
	MontoReservas,
	MontoAvales,
	FechaDisposicion,
	SituacionCredito,
	Exposicion,
	ImporteRC3,
	ClienteRelacionado,
	ISNULL(CodigoCredito,'')+'|'+ISNULL(CodigoPersona,'')+'|'+ISNULL(RFC,'')+'|'+ISNULL(SituacionCredito,'')+'|'+ISNULL(ClienteRelacionado,'') AS EVERYTHING
FROM dbo.RW_InsumoRC15;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_R04A0420ConsumoMovimientos_AnteriorTavig_Pagos]
AS
SELECT
	cr.Codigo,
	conf.Concepto,
	mov.Monto
FROM dbo.SICCMX_Consumo cr
INNER JOIN dbo.SICCMX_Consumo_Movimientos mov ON mov.IdConsumo = cr.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN dbo.SICCMX_ConsumoInfo info ON info.IdConsumo = cr.IdConsumo
INNER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4
INNER JOIN R04.[0420Configuracion] conf ON tp.Codigo LIKE conf.CodigoProducto AND conf.TipoMovimiento = tm.Codigo
INNER JOIN dbo.SICC_VW_R04AConsumoMovimiento_FechaPrimerTavig tavig ON tavig.IdConsumo = cr.IdConsumo
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sit ON cr.IdSituacionCredito = sit.IdSituacionConsumo
WHERE sit.Codigo = '1' AND mov.Fecha <= tavig.Fecha AND tm.Codigo NOT IN ('TAVEN','TAVIG');
GO

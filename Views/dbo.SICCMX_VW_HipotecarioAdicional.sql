SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_HipotecarioAdicional]
AS
SELECT
	adi.IdHipotecario,
	adi.SaldoPromedio,
	CAST(adi.SaldoPromedio * tc.Valor AS DECIMAL(23,2)) AS SaldoPromedioValorizado,
	CAST(CAST(ISNULL(adi.SaldoPromedio,0) * tc.Valor AS DECIMAL(23,2)) / usd.Valor AS DECIMAL(23,2)) AS SaldoPromedioValorizadoUSD,
	adi.InteresesDevengados,
	CAST(adi.InteresesDevengados * tc.Valor AS DECIMAL(23,2)) AS InteresesDevengadosValorizado,
	CAST(CAST(ISNULL(adi.InteresesDevengados,0) * tc.Valor AS DECIMAL(23,2)) / usd.Valor AS DECIMAL(23,2)) AS InteresesDevengadosValorizadoUSD,
	adi.Comisiones,
	CAST(adi.Comisiones * tc.Valor AS DECIMAL(23,2)) AS ComisionesValorizado,
	CAST(CAST(ISNULL(adi.Comisiones,0) * tc.Valor AS DECIMAL(23,2)) / usd.Valor AS DECIMAL(23,2)) AS ComisionesValorizadoUSD
FROM dbo.SICCMX_HipotecarioAdicional adi
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON adi.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = info.IdMoneda
INNER JOIN dbo.SICC_Periodo periodo ON periodo.IdPeriodo = tc.IdPeriodo AND periodo.Activo=1
LEFT OUTER JOIN (
	SELECT tcUSD.Valor
	FROM dbo.SICC_TipoCambio tcUSD
	INNER JOIN dbo.SICC_Moneda monUSD ON tcUSD.IdMoneda = monUSD.IdMoneda AND monUSD.Codigo='1'
	INNER JOIN dbo.SICC_Periodo perUSD ON tcUSD.IdPeriodo = perUSD.IdPeriodo AND perUSD.Activo=1
)usd ON 1=1;
GO

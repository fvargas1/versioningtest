SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_A0420CreditosTAVIG_2016]
AS
-- Creditos con SituacionHistorica Vencida y Actual Vigente
SELECT dat.Codigo
FROM R04.[0420Datos_2016] dat
WHERE (dat.SituacionHistorica = '2' AND dat.SituacionActual = '1');
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [Historico].[SICCMX_VW_Credito_ReservasAdicionales]
AS
SELECT
	cre.IdPeriodoHistorico,
	cre.Credito,
	ISNULL(cre.InteresCarteraVencida,0) AS InteresCarteraVencidaValorizado,
	ISNULL(inf.ReservasAdicionales,0) AS ReservasAdicionales,
	ISNULL(cre.InteresCarteraVencida,0)
	+ ISNULL(inf.ReservasAdicionales,0) AS ReservasAdicTotal
FROM Historico.SICCMX_Credito_Valorizado cre
INNER JOIN Historico.SICCMX_CreditoInfo inf ON cre.IdPeriodoHistorico = inf.IdPeriodoHistorico AND cre.Credito = inf.Credito;
GO

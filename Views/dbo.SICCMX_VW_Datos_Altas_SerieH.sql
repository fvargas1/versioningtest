SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Datos_Altas_SerieH]
AS
SELECT
 'A' AS TipoRegistro,
 CodigoCredito,
 CodigoCreditoCNBV,
 NumeroAvaluo,
 DenominacionCredito,
 DenominacionCreditoReestructura
FROM dbo.RW_R04H0491

UNION 

SELECT
 'H',
 r.CodigoCredito,
 r.CodigoCreditoCNBV,
 r.NumeroAvaluo,
 r.DenominacionCredito,
 r.DenominacionCreditoReestructura
FROM Historico.RW_R04H0491 r
INNER JOIN dbo.SICC_PeriodoHistorico ph ON r.IdPeriodoHistorico = ph.IdPeriodoHistorico
WHERE ph.Activo = 1;
GO

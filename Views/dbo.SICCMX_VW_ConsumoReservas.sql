SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_ConsumoReservas]
AS
SELECT
	con.IdConsumo,
	con.FechaCalificacion,
	con.ECubierta AS MontoCubierto,
	con.ReservaCubierta AS ReservaCubierto,
	con.PorReservaCubierta AS PorcentajeCubierto,
	con.IdCalificacionCubierta AS IdCalificacionCubierto,
	calCub.Codigo AS IdCalificacionCubiertoNombre,
	con.EExpuesta AS MontoExpuesto,
	con.ReservaExpuesta AS ReservaExpuesto,
	con.PorReservaExpuesta AS PorcentajeExpuesto,
	con.IdCalificacionExpuesta AS IdCalificacionExpuesto,
	calExp.Codigo AS IdCalificacionExpuestoNombre,
	con.MontoAdicional,
	con.ReservaAdicional,
	con.PorcentajeAdicional,
	con.IdCalificacionAdicional,
	calAdi.Codigo AS IdCalificacionAdicionalNombre
FROM dbo.SICCMX_Consumo_Reservas_Variables con
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calExp ON con.IdCalificacionExpuesta = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calCub ON con.IdCalificacionCubierta = calCub.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calAdi ON con.IdCalificacionAdicional = calAdi.IdCalificacion;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_PP]
AS
SELECT
 cg.IdCredito,
 SUM(gpp.MontoCubierto) AS Monto,
 SUM(gpp.PrctCubierto) AS Porcentaje,
 SUM(cg.ReservaCubierta) AS ReservaCubierto,
 SUM(cg.ReservaExpuesta) AS ReservaExpuesto,
 gar.NombreGarante AS NomGar,
 gar.Codigo AS CodigoGarantia
FROM dbo.SICCMX_CreditoGarantia_PP cg
INNER JOIN dbo.SICCMX_Garantia_PP gpp ON cg.IdGarantia = gpp.IdGarantia
CROSS APPLY (
	SELECT TOP 1 g.NombreGarante, g.Codigo
	FROM dbo.SICCMX_Garantia g
	WHERE g.IdGarantia = gpp.IdGarantia
	ORDER BY g.ValorGarantia DESC
) AS gar
GROUP BY cg.IdCredito, gar.NombreGarante, gar.Codigo;
GO

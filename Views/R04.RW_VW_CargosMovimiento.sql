SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [R04].[RW_VW_CargosMovimiento]
AS
-- Consultar los movimientos (CAST) de creditos
SELECT
	dat.Codigo,
	SUM(mov.Monto) AS CargoMovimiento,
	SUM(dat.SaldoReservaCubHistorico + dat.SaldoReservaExpHistorico) AS ReservaHistorico,
	SUM(dat.SaldoReservaCubActual + dat.SaldoReservaExpActual) AS ReservaActual,
	tm.Codigo AS Movimiento
FROM R04.[0419Datos] dat
INNER JOIN dbo.SICCMX_Credito cre ON dat.Codigo = cre.Codigo
INNER JOIN dbo.SICCMX_Credito_Movimientos mov ON cre.IdCredito = mov.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tm ON mov.IdTipoMovimiento = tm.IdTipoMovimiento
WHERE tm.Codigo IN ('CAST','BONIF')
GROUP BY dat.Codigo, tm.Codigo

UNION ALL

SELECT
	dat.Codigo,
	SUM(mov.Monto) AS CargoMovimiento,
	SUM(dat.SaldoReservaCubHistorico + dat.SaldoReservaExpHistorico) AS ReservaHistorico,
	SUM(dat.SaldoReservaCubActual + dat.SaldoReservaExpActual) AS ReservaActual,
	tm.Codigo AS Movimiento
FROM R04.[0419Datos] dat
INNER JOIN dbo.SICCMX_Consumo con ON dat.Codigo = con.Codigo
INNER JOIN dbo.SICCMX_Consumo_Movimientos mov ON con.IdConsumo = mov.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
WHERE tm.Codigo IN ('CAST','BONIF')
GROUP BY dat.Codigo, tm.Codigo

UNION ALL

SELECT
	dat.Codigo,
	SUM(mov.Monto) AS CargoMovimiento,
	SUM(dat.SaldoReservaCubHistorico + dat.SaldoReservaExpHistorico) AS ReservaHistorico,
	SUM(dat.SaldoReservaCubActual + dat.SaldoReservaExpActual) AS ReservaActual,
	tm.Codigo AS Movimiento
FROM R04.[0419Datos] dat
INNER JOIN dbo.SICCMX_Hipotecario hip ON dat.Codigo = hip.Codigo
INNER JOIN dbo.SICCMX_Hipotecario_Movimientos mov ON hip.IdHipotecario = mov.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
WHERE tm.Codigo IN ('CAST','BONIF')
GROUP BY dat.Codigo, tm.Codigo;
GO

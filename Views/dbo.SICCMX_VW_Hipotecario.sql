SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Hipotecario]
AS
SELECT
 hip.IdHipotecario,
 hip.IdPersona,
 per.Codigo AS CodigoPersona,
 per.Nombre AS NombrePersona,
 hip.Codigo,
 hip.SaldoCapitalVigente,
 CAST(hip.SaldoCapitalVigente * tc.Valor AS DECIMAL(23,2)) AS SaldoCapitalVigenteValorizado,
 hip.InteresVigente,
 CAST(hip.InteresVigente * tc.Valor AS DECIMAL(23,2)) AS InteresVigenteValorizado,
 hip.SaldoCapitalVencido,
 CAST(hip.SaldoCapitalVencido * tc.Valor AS DECIMAL(23,2)) AS SaldoCapitalVencidoValorizado,
 hip.InteresVencido,
 CAST(hip.InteresVencido * tc.Valor AS DECIMAL(23,2)) AS InteresVencidoValorizado,
 ISNULL(hip.MontoGarantia,0) AS MontoGarantia,
 CAST(ISNULL(hip.MontoGarantia,0) * tc.Valor AS DECIMAL(23,2)) AS MontoGarantiaValorizado,
 ISNULL(hip.InteresCarteraVencida,0) AS InteresCarteraVencida, 
 CAST(ISNULL(hip.InteresCarteraVencida,0) * tc.Valor AS DECIMAL(23,2)) AS InteresCarteraVencidaValorizado,
 hip.IdTipoCreditoR04A,
 hip.IdEntidadCobertura,
 ent.Codigo AS CodigoEntidadCobertura,
 CAST(hip.SaldoCapitalVigente + hip.InteresVigente + hip.SaldoCapitalVencido + hip.InteresVencido AS DECIMAL(23,2)) AS SaldoTotal,
 CAST((hip.SaldoCapitalVigente*tc.Valor)+(hip.InteresVigente*tc.Valor)+(hip.SaldoCapitalVencido*tc.Valor)+(hip.InteresVencido*tc.Valor) AS DECIMAL(23,2)) AS SaldoTotalValorizado,
 CAST(CAST(hip.SaldoCapitalVigente*tc.Valor AS DECIMAL)+CAST(hip.InteresVigente*tc.Valor AS DECIMAL)+CAST(hip.SaldoCapitalVencido*tc.Valor AS DECIMAL)+CAST(hip.InteresVencido*tc.Valor AS DECIMAL) AS DECIMAL(23,2)) AS SaldoTotalValorizadoRound,
 hip.IdTipoCreditoR04A_2016
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_Persona per ON hip.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICC_TipoCambio tc ON info.IdMoneda = tc.IdMoneda
INNER JOIN dbo.SICC_Periodo prd ON tc.IdPeriodo = prd.IdPeriodo AND prd.Activo = 1
LEFT OUTER JOIN dbo.SICC_EntidadCobertura ent ON hip.IdEntidadCobertura = ent.IdEntidadCobertura;
GO

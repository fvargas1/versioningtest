SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_Metodologia]
AS
SELECT
 Metodologia.IdMetodologia,
 Metodologia.Codigo,
 Metodologia.Nombre
FROM dbo.SICCMX_Metodologia AS Metodologia;
GO

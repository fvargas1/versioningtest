SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_HipotecarioReservas]
AS
SELECT
	res.IdHipotecario,
	res.FechaCalificacion,
	res.IdCalificacionExpuesto,
	calExp.Codigo AS IdCalificacionExpuestoNombre,
	res.MontoExpuesto,
	res.SPExpuesto,
	res.ReservaExpuesto,
	res.PorcentajeExpuesto,
	res.IdCalificacionCubierto,
	calCub.Codigo AS IdCalificacionCubiertoNombre,
	res.MontoCubierto,
	res.SPCubierto,
	res.ReservaCubierto,
	res.PorcentajeCubierto,
	res.IdCalificacionAdicional,
	calAd.Codigo AS IdCalificacionAdicionalNombre,
	res.MontoAdicional,
	res.ReservaAdicional,
	res.PorcentajeAdicional
FROM dbo.SICCMX_HipotecarioReservas AS res
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calExp ON res.IdCalificacionExpuesto = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calCub ON res.IdCalificacionCubierto = calCub.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calAd ON res.IdCalificacionAdicional = calAd.IdCalificacion;
GO

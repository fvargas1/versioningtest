SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Credito_SP]
AS
SELECT
 cre.IdCredito,
 CASE WHEN 1 - (CAST(SUM(ISNULL(can.MontoRecuperacion, 0)) AS DECIMAL(25,4)) / crv.EI_Expuesta_GarPer) < 0 THEN 0
 ELSE CAST(1 - (CAST(SUM(ISNULL(can.MontoRecuperacion, 0)) AS DECIMAL(25,4)) / crv.EI_Expuesta_GarPer) AS DECIMAL(18,12)) END AS SP
FROM dbo.SICCMX_VW_Credito_NMC AS cre
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Garantia_Canasta AS can ON cre.IdCredito = can.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE ISNULL(crv.EI_Expuesta_GarPer, 0) > 0 AND ISNULL(tg.Codigo,'') NOT IN ('NMC-05','GP')
GROUP BY cre.IdCredito, crv.EI_Expuesta_GarPer;
GO

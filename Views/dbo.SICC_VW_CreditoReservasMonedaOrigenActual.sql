SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_CreditoReservasMonedaOrigenActual]
AS
SELECT
	dat.Codigo,
	ROUND(ISNULL(dat.SaldoReservaExpActual,0)/tc.Valor,2) AS SaldoReservaExpActual,
	ROUND(ISNULL(dat.SaldoReservaCubActual,0)/tc.Valor,2) AS SaldoReservaCubActual,
	tc.Valor AS ValorActual
FROM R04.[0419Datos] dat
INNER JOIN dbo.SICC_Moneda mon ON mon.Codigo = dat.Moneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo p ON p.IdPeriodo = tc.IdPeriodo AND p.Activo = 1;
GO

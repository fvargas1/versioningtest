SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantia]
AS
SELECT
 gar.IdGarantia,
 gar.Codigo,
 gar.IdTipoGarantiaMA,
 gar.IdTipoGarantia,
 gar.ValorGarantia,
 gar.IdMoneda,
 gar.FechaValuacion,
 gar.Descripcion,
 gar.IdBancoGarantia,
 gar.ValorGarantiaProyectado,
 gar.RegGarantiaMob,
 gar.Hc,
 gar.VencimientoRestante,
 gar.IdGradoRiesgo,
 gar.IdAgenciaCalificadora,
 gar.Calificacion,
 gar.IdEmisor,
 gar.IdEscala,
 gar.EsIPC,
 gar.PIGarante,
 gar.NumRPPC,
 gar.RFCGarante,
 gar.NombreGarante,
 gar.IdGarante,
 gar.IdActividaEcoGarante,
 gar.IdLocalidadGarante,
 gar.MunicipioGarante,
 gar.EstadoGarante,
 gar.IdTipoGarante,
 gar.LEI,
 gar.Aplica,
 mon.Codigo AS CodigoMoneda,
 tg.Codigo AS CodigoTipoGarantia,
 tg.SortOrder,
 tg.IdClasificacionNvaMet,
 tg.EsEfectivo,
 CAST(ISNULL(gar.ValorGarantia, 0) * tc.Valor AS DECIMAL(23,2)) AS ValorGtiaValorizado,
 tg.AplicaDist
FROM dbo.SICCMX_Garantia AS gar
INNER JOIN dbo.SICC_Moneda mon ON gar.IdMoneda = mon.IdMoneda
INNER JOIN SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN SICC_Periodo per ON per.IdPeriodo = tc.IdPeriodo AND per.Activo = 1
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON gar.IdTipoGarantia = tg.IdTipoGarantia;
GO

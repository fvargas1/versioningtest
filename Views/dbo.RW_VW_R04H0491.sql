SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04H0491]
AS
SELECT DISTINCT
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	ProductoHipotecario,
	CategoriaCredito,
	TipoAlta,
	DestinoCredito,
	FechaOtorgamiento,
	FechaVencimiento,
	DenominacionCredito,
	MontoOriginal,
	Comisiones,
	MontoSubsidioFederal,
	EntidadCoFin,
	MontoSubCuenta,
	MontoOtorgadoCoFin,
	Apoyo,
	ValorOriginalVivienda,
	ValorAvaluo,
	NumeroAvaluo,
	Localidad,
	FechaFirmaReestructura,
	FechaVencimientoReestructura,
	MontoReestructura,
	DenominacionCreditoReestructura,
	IngresosMensuales,
	TipoComprobacionIngresos,
	SectorLaboral,
	NumeroConsultaSIC,
	PeriodicidadAmortizaciones,
	TipoTasaInteres,
	TasaRef,
	AjusteTasaRef,
	SeguroAcreditado,
	TipoSeguro,
	EntidadSeguro,
	PorcentajeCubiertoSeguro,
	MontoSubCuentaGarantia,
	INTEXP,
	SDES,
	Municipio,
	Estado,
	ISNULL(CodigoCredito,'')+'|'+ISNULL(CodigoCreditoCNBV,'') AS EVERYTHING
FROM dbo.RW_R04H0491;
GO

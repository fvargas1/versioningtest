SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Analitico_Reservas_A19]
AS
SELECT DISTINCT
	Fecha,
	CodigoPersona,
	NombrePersona,
	CodigoProyecto,
	NombreProyecto,
	CodigoCredito,
	SaldoCredito,
	FechaVecimiento,
	Moneda,
	MontoGarantia,
	MontoReserva,
	PrctReserva,
	Calificacion,
	ISNULL(CodigoPersona,'')+'|'+ISNULL(NombrePersona,'')+'|'+ISNULL(CodigoProyecto,'')+'|'+ISNULL(NombreProyecto,'')+'|'+ISNULL(CodigoCredito,'') AS EVERYTHING
FROM dbo.RW_Analitico_Reservas_A19;
GO

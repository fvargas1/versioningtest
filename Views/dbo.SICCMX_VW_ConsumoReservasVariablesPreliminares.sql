SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_ConsumoReservasVariablesPreliminares]
AS
SELECT
 pre.IdConsumo,
 con.Codigo AS IdConsumoNombre,
 pre.IdMetodologia,
 met.Nombre AS IdMetodologiaNombre,
 met.Codigo AS Metodologia,
 pre.NumeroIntegrantes,
 pre.Ciclos,
 pre.LimiteCredito,
 pre.SaldoFavor,
 pre.PorcentajePagoRealizado,
 pre.ATR,
 pre.MaxATR,
 pre.INDATR,
 pre.VECES,
 pre.Impago,
 pre.NumeroImpagosConsecutivos,
 pre.NumeroImpagosHistoricos,
 pre.MesesTranscurridos,
 pre.PorPago,
 pre.PorSDOIMP,
 pre.PorPR,
 pre.PorcentajeUso,
 pre.TarjetaActiva,
 tcc.ClasificacionReportes,
 pre.Alto,
 pre.Medio,
 pre.Bajo
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_Consumo con ON pre.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaConsumo
LEFT OUTER JOIN dbo.SICC_TipoCreditoConsumo tcc ON con.IdTipoCredito = tcc.IdTipoCreditoConsumo;
GO

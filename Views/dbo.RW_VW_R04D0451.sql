SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04D0451]
AS
SELECT DISTINCT
	Concepto,
	Subreporte,
	Moneda,
	MetodoCalificacion,
	TipoCalificacion,
	TipoSaldo,
	Dato,
	ISNULL(Concepto,'')+'|'+ISNULL(Moneda,'')+'|'+ISNULL(MetodoCalificacion,'')+'|'+ISNULL(TipoCalificacion,'')+'|'+ISNULL(TipoSaldo,'') AS EVERYTHING
FROM dbo.RW_R04D0451;
GO

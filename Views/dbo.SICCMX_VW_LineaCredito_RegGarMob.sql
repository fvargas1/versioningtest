SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_LineaCredito_RegGarMob]
AS
SELECT lin.IdLineaCredito, ISNULL(NULLIF(g.RegGarantiaMob,''),'0') AS RegGarantiaMob, ISNULL(NULLIF(g.NumRPPC,''),'0') AS NumRPPC
FROM dbo.SICCMX_LineaCredito lin
CROSS APPLY (
	SELECT TOP 1 gar.RegGarantiaMob, gar.NumRPPC, SUM(cg.PorcCubiertoCredito) AS Porcentaje
	FROM dbo.SICCMX_Credito cre
	INNER JOIN dbo.SICCMX_CreditoGarantia cg ON cre.IdCredito = cg.IdCredito
	INNER JOIN dbo.SICCMX_Garantia gar ON cg.IdGarantia = gar.IdGarantia
	WHERE gar.Aplica = 1 AND cre.IdLineaCredito = lin.IdLineaCredito
	GROUP BY cre.IdLineaCredito, gar.RegGarantiaMob, gar.NumRPPC
	ORDER BY Porcentaje DESC
) AS g;
GO

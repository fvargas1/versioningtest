SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Datos_Reportes_Altas]
AS
SELECT cre.CNBV, calt.TipoAlta, calt.Moneda, calt.MontoLineaAut, calt.TipoLinea
FROM dbo.SICCMX_VW_CreditosCNBV cre
CROSS APPLY (
 SELECT TOP 1 tab.TipoAlta, tab.Moneda, tab.MontoLineaAut, tab.TipoLinea FROM (
 --dbo.RW_R04C0453
 SELECT ISNULL(NULLIF(r53.Periodo,''),'0') AS Periodo, r53.TipoAltaCredito AS TipoAlta, r53.Moneda, r53.MonLineaCred AS MontoLineaAut,
 r53.TipoLinea AS TipoLinea
 FROM dbo.RW_VW_R04C0453_INC r53 WHERE r53.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- Historico.RW_R04C0453
 SELECT ISNULL(NULLIF(r53.Periodo,''),'0'), r53.TipoAltaCredito, r53.Moneda, r53.MonLineaCred, r53.TipoLinea
 FROM Historico.RW_VW_R04C0453_INC r53 WHERE r53.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- dbo.RW_R04C0458
 SELECT ISNULL(NULLIF(r58.Periodo,''),'0') AS Periodo, r58.TipoAltaCredito AS TipoAlta, r58.Moneda AS Moneda, r58.MonLineaCredValorizado AS MontoLineaAut,
 r58.TipoLinea AS TipoLinea
 FROM dbo.RW_VW_R04C0458_INC r58 WHERE r58.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- Historico.RW_R04C0458
 SELECT ISNULL(NULLIF(r58.Periodo,''),'0'), r58.TipoAltaCredito, r58.Moneda, r58.MonLineaCredValorizado, r58.TipoLinea
 FROM Historico.RW_VW_R04C0458_INC r58 WHERE r58.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- dbo.RW_R04C0463
 SELECT ISNULL(NULLIF(r63.Periodo,''),'0') AS Periodo, r63.TipoAltaCredito AS TipoAlta, r63.Moneda, r63.MonLineaCredValorizado AS MontoLineaAut,
 r63.TipoLinea AS TipoLinea
 FROM dbo.RW_VW_R04C0463_INC r63 WHERE r63.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- Historico.RW_R04C0463
 SELECT ISNULL(NULLIF(r63.Periodo,''),'0'), r63.TipoAltaCredito, r63.Moneda, r63.MonLineaCredValorizado, r63.TipoLinea
 FROM Historico.RW_VW_R04C0463_INC r63 WHERE r63.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- dbo.RW_R04C0468
 SELECT ISNULL(NULLIF(r68.Periodo,''),'0') AS Periodo, r68.TipoAltaCredito AS TipoAlta, r68.Moneda, r68.MonLineaCredValorizado AS MontoLineaAut,
 r68.TipoLinea AS TipoLinea
 FROM dbo.RW_VW_R04C0468_INC r68 WHERE r68.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- Historico.RW_R04C0468
 SELECT ISNULL(NULLIF(r68.Periodo,''),'0'), r68.TipoAltaCredito, r68.Moneda, r68.MonLineaCredValorizado, r68.TipoLinea
 FROM Historico.RW_VW_R04C0468_INC r68 WHERE r68.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- dbo.RW_R04C0473
 SELECT ISNULL(NULLIF(r73.Periodo,''),'0') AS Periodo, r73.TipoAltaCredito AS TipoAlta, r73.Moneda, r73.MonLineaCredValorizado AS MontoLineaAut,
 r73.TipoLinea AS TipoLinea
 FROM dbo.RW_VW_R04C0473_INC r73 WHERE r73.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- Historico.RW_R04C0473
 SELECT ISNULL(NULLIF(r73.Periodo,''),'0'), r73.TipoAltaCredito, r73.Moneda, r73.MonLineaCredValorizado, r73.TipoLinea
 FROM Historico.RW_VW_R04C0473_INC r73 WHERE r73.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- dbo.RW_R04C0478
 SELECT ISNULL(NULLIF(r78.Periodo,''),'0') AS Periodo, r78.TipoAltaCredito AS TipoAlta, r78.Moneda, r78.MonLineaCredValorizado AS MontoLineaAut,
 r78.TipoLinea AS TipoLinea
 FROM dbo.RW_VW_R04C0478_INC r78 WHERE r78.CodigoCreditoCNBV = cre.CNBV
 
 UNION ALL -- Historico.RW_R04C0478
 SELECT ISNULL(NULLIF(r78.Periodo,''),'0'), r78.TipoAltaCredito, r78.Moneda, r78.MonLineaCredValorizado, r78.TipoLinea
 FROM Historico.RW_VW_R04C0478_INC r78 WHERE r78.CodigoCreditoCNBV = cre.CNBV
 ) AS tab
 ORDER BY CAST(tab.Periodo AS INT) DESC
) AS calt
GO

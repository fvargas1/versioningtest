SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0415MN_VW]
AS
SELECT
	concepto.Codigo,
	concepto.Nombre AS Concepto,
	concepto.Padre,
	CASE WHEN rep.TipoDeCartera = '0' AND rep.TipoDeSaldo = '4' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS NoAplicaInteresesPagados,
	CASE WHEN rep.TipoDeCartera = '0' AND rep.TipoDeSaldo = '5' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS NoAplicaComisiones,
	CASE WHEN rep.TipoDeCartera = '0' AND rep.TipoDeSaldo = '6' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS NoAplicaSaldoPromedioDiario
FROM dbo.ReportWare_VW_0415Concepto concepto
LEFT OUTER JOIN dbo.RW_R04A0415MN rep ON rep.Concepto = concepto.Codigo;
GO

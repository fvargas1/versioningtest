SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_LineaCreditoAdicional]
AS
SELECT
 lin.IdLineaCredito,
 ISNULL(lin.ComisionDelMes,0) AS Comision,
 CAST(ISNULL(lin.ComisionDelMes,0) * cg.TipoCambio AS DECIMAL(23,2)) AS ComisionValorizado,
 CAST(CAST(ISNULL(lin.ComisionDelMes,0) * cg.TipoCambio AS DECIMAL(23,2)) / usd.Valor AS DECIMAL(23,2)) AS ComisionValorizadoUSD
FROM dbo.SICCMX_LineaCredito lin
CROSS APPLY (
	SELECT TOP 1 lin.IdLineaCredito, tc.Valor AS TipoCambio
	FROM dbo.SICCMX_VW_Credito_NMC cre
	INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = cre.IdMoneda
	INNER JOIN dbo.SICC_Periodo periodo ON periodo.IdPeriodo = tc.IdPeriodo AND periodo.Activo=1
	WHERE cre.IdLineaCredito = lin.IdLineaCredito
	ORDER BY cre.MontoValorizado DESC
) AS cg
LEFT OUTER JOIN (
 SELECT tcUSD.Valor
 FROM dbo.SICC_TipoCambio tcUSD
 INNER JOIN dbo.SICC_Moneda monUSD ON tcUSD.IdMoneda = monUSD.IdMoneda AND monUSD.Codigo='1'
 INNER JOIN dbo.SICC_Periodo perUSD ON tcUSD.IdPeriodo = perUSD.IdPeriodo AND perUSD.Activo=1
) usd ON 1=1;
GO

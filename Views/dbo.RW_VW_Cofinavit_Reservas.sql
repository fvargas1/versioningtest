SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Cofinavit_Reservas]
AS
SELECT DISTINCT
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	TipoRegimen,
	Constante,
	FactorATR,
	ATR,
	FactorPorpago,
	PorPago,
	FactorPromRete,
	PromRete,
	FactorMaxATR,
	MaxATR,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion,
	ISNULL(Codigo,'') + '|' + ISNULL(TipoRegimen,'') AS EVERYTHING
FROM dbo.RW_Cofinavit_Reservas;
GO

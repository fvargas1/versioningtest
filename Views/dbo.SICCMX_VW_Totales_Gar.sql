SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Totales_Gar]
AS
SELECT
 vwNM.IdCredito,
 vwNM.He,
 SUM(CASE WHEN vwTot.MontoTotal > 0 THEN ((vwNM.ValorGarantia / vwTot.MontoTotal) * vwNM.Hc) ELSE 0 END) AS Hc,
 SUM(CASE WHEN vwTot.MontoTotal > 0 THEN ((vwNM.ValorGarantia / vwTot.MontoTotal) * vwNM.Hfx) ELSE 0 END) AS Hfx,
 vwTot.MontoTotal,
 vwNM.MontoCredito
FROM dbo.SICCMX_VW_Garantias_NM vwNM
INNER JOIN dbo.SICCMX_VW_Garantia_TotGRF vwTot ON vwNM.IdCredito = vwTot.IdCredito
WHERE vwNM.IdClasificacionNvaMet = 1 AND vwNM.Aplica=1
GROUP BY vwNM.IdCredito, vwNM.He, vwNM.MontoCredito, vwTot.MontoTotal;
GO

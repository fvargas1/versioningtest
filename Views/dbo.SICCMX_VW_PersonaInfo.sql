SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_PersonaInfo]
AS
SELECT
 PersonaInfo.IdPersona,
 PersonaInfo.IdTipoPersona,
 idtipopersona.Nombre AS IdTipoPersonaNombre,
 --PersonaInfo.IdSectorEconomico,
 --idsectoreconomico.Nombre AS IdSectorEconomicoNombre,
 0 AS IdSectorEconomico,
 '' AS IdSectorEconomicoNombre,
 PersonaInfo.NumeroEmpleados,
 PersonaInfo.IngresosBrutos,
 PersonaInfo.IdSectorLaboral,
 idsectorlaboral.Nombre AS IdSectorLaboralNombre,
 PersonaInfo.GrupoRiesgo,
 PersonaInfo.IdTipoIngreso,
 idtipoingreso.Nombre AS IdTipoIngresoNombre
FROM dbo.SICCMX_PersonaInfo AS PersonaInfo
LEFT OUTER JOIN dbo.SICC_TipoPersona idtipopersona ON PersonaInfo.IdTipoPersona = idtipopersona.IdTipoPersona
--LEFT OUTER JOIN dbo.SICC_SectorEconomico idsectoreconomico ON PersonaInfo.IdSectorEconomico = idsectoreconomico.IdSectorEconomico
LEFT OUTER JOIN dbo.SICC_SectorLaboral idsectorlaboral ON PersonaInfo.IdSectorLaboral = idsectorlaboral.IdSectorLaboral
LEFT OUTER JOIN dbo.SICC_TipoIngreso idtipoingreso ON PersonaInfo.IdTipoIngreso = idtipoingreso.IdTipoIngreso;
GO

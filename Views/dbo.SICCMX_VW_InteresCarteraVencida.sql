SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_InteresCarteraVencida]
AS
SELECT
	CodigoCredito AS Codigo,
	InteresCarteraVencidaValorizado AS InteresCarteraVencida
FROM dbo.SICCMX_VW_Credito_NMC
UNION ALL
SELECT
	Codigo,
	InteresCarteraVencidaValorizado
FROM dbo.SICCMX_VW_Consumo
UNION ALL
SELECT
	Codigo,
	InteresCarteraVencidaValorizado
FROM dbo.SICCMX_VW_Hipotecario;
GO

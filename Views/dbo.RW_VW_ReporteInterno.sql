SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_ReporteInterno]
AS
SELECT
 ReporteInterno.IdReporteInterno,
 ReporteInterno.GrupoReporte,
 ReporteInterno.Nombre,
 ReporteInterno.Descripcion,
 ReporteInterno.SProc,
 ReporteInterno.Activo
FROM dbo.RW_ReporteInterno AS ReporteInterno;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_R04A0424ConsumoMovimientos_AnteriorTaven_Suma]
AS
SELECT
	cr.Codigo,
	SUM(CAST(mov.Monto AS DECIMAL)) AS Monto
FROM dbo.SICCMX_Consumo cr
INNER JOIN dbo.SICCMX_Consumo_Movimientos mov ON mov.IdConsumo = cr.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN dbo.SICC_VW_R04AConsumoMovimiento_FechaPrimerTaven taven ON taven.IdConsumo = cr.IdConsumo
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sit ON cr.IdSituacionCredito = sit.IdSituacionConsumo
WHERE sit.Codigo = '2' AND mov.Fecha <= taven.Fecha AND tm.Codigo NOT IN ('TAVEN','TAVIG')
GROUP BY cr.Codigo
UNION ALL
-- Seleccionar los creditos con movimiento taven sin pagos antes del movto
SELECT
	cr.Codigo,
	0 AS Monto
FROM dbo.SICCMX_Consumo cr
INNER JOIN R04.[0424Datos] dat ON cr.Codigo = dat.Codigo
WHERE 0 = (
	SELECT COUNT(m.IdTipoMovimiento)
	FROM dbo.SICCMX_Consumo_Movimientos m
	INNER JOIN dbo.SICC_VW_R04AConsumoMovimiento_FechaPrimerTaven taven ON taven.IdConsumo = m.IdConsumo
	INNER JOIN dbo.SICC_TipoMovimiento tm ON m.IdTipoMovimiento = tm.IdTipoMovimiento AND tm.Codigo NOT IN ('TAVEN','TAVIG')
	WHERE m.IdConsumo = cr.IdConsumo AND m.Fecha <= taven.Fecha
) AND dat.SituacionActual = '2' AND dat.SituacionHistorica = '1';
GO

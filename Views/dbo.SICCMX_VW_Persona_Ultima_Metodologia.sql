SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Persona_Ultima_Metodologia]
AS
SELECT DISTINCT lin.IdPersona, hcr.IdPeriodoHistorico, hcr.Metodologia
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
CROSS APPLY (
	SELECT TOP 1 ppi.IdPeriodoHistorico, ppi.Metodologia
	FROM Historico.SICCMX_Persona_PI ppi
	WHERE ppi.Persona = per.Codigo
	ORDER BY ppi.IdPeriodoHistorico DESC
) AS hcr
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_Cub_Consumo]
AS
SELECT
 can.IdConsumo,
 SUM(ISNULL(can.MontoCobAjust, 0)) AS Cobertura,
 SUM(ISNULL(can.PrctCobSinAju, 0)) AS PrctCobSinAju,
 SUM(ISNULL(can.PrctCobAjust, 0)) AS PrctCobAjust,
 ISNULL(con.E, 0) AS MontoCredito
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables con ON can.IdConsumo = con.IdConsumo
WHERE can.EsDescubierto IS NULL
GROUP BY can.IdConsumo, con.E;
GO

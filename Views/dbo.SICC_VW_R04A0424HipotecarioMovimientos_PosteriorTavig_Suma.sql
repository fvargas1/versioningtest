SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_R04A0424HipotecarioMovimientos_PosteriorTavig_Suma]
AS
SELECT
	cr.Codigo,
	conf.Concepto,
	mov.Monto
FROM dbo.SICCMX_Hipotecario cr
INNER JOIN dbo.SICCMX_Hipotecario_Movimientos mov ON mov.IdHipotecario = cr.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON info.IdHipotecario = cr.IdHipotecario
INNER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = cr.IdTipoCreditoR04A
INNER JOIN R04.[0424Configuracion] conf ON tp.Codigo LIKE conf.CodigoProducto AND conf.TipoMovimiento = tm.Codigo
INNER JOIN dbo.SICC_VW_R04AHipotecarioMovimiento_FechaPrimerTavig tavig ON tavig.IdHipotecario = cr.IdHipotecario
LEFT OUTER JOIN dbo.SICC_SituacionHipotecario sit ON info.IdSituacionCredito = sit.IdSituacionHipotecario
WHERE mov.Fecha > tavig.Fecha AND sit.Codigo = '1' AND tm.Codigo NOT IN ('TAVEN','TAVIG')
UNION ALL
-- Seleccionar los creditos con movimiento taven sin pagos antes del movto
SELECT
	cr.Codigo,
	'',
	0 AS Monto
FROM dbo.SICCMX_Hipotecario cr
INNER JOIN R04.[0424Datos] dat ON cr.Codigo = dat.Codigo
WHERE 0 = (
	SELECT COUNT(m.IdTipoMovimiento)
	FROM dbo.SICCMX_Hipotecario_Movimientos m
	INNER JOIN dbo.SICC_VW_R04AHipotecarioMovimiento_FechaPrimerTavig tavig ON tavig.IdHipotecario = cr.IdHipotecario
	INNER JOIN dbo.SICC_TipoMovimiento tm ON m.IdTipoMovimiento = tm.IdTipoMovimiento AND tm.Codigo NOT IN ('TAVEN','TAVIG')
	WHERE m.IdHipotecario = cr.IdHipotecario AND m.Fecha > tavig.Fecha
) AND dat.SituacionActual = '1' AND dat.SituacionHistorica = '2';
GO

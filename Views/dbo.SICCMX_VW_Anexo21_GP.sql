SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Anexo21_GP]
AS
SELECT
 anx.IdGP,
 anx.AntSocInfCredCredito,
 anx.QuitasCastRest,
 anx.PorPagoTiemInstNoBanc,
 anx.PorPagEntCom60oMasAtraso,
 anx.CuenCredAbierInstBancUlt12mes,
 anx.MonMaxCredOtorInstBanExpUDIS / tca.Valor AS MonMaxCredOtorInstBanExpUDIS,
 anx.NumMesUltCredAbiUlt12Meses,
 anx.PorPagIntBan60oMasAtraso,
 anx.PorPagIntBan1a29Atraso,
 anx.PorPagIntBan90oMasAtraso,
 anx.NumDiasMoraPromInstBanc,
 anx.NumPagTiemIntBanUlt12Meses,
 anx.MonTotPagInfonavitUltBimestre / tca.Valor AS MonTotPagInfonavitUltBimestre,
 anx.NumDiasAtrInfonavitUltBimestre,
 anx.TasaRetLab,
 anx.ProcOriAdmonCred,
 anx.SinAtrasos,
 anx.VentNetTotAnuales,
 anx.OrgDescPartidoPolitico,
 anx.FechaInfoFinanc,
 anx.FechaInfoBuro,
 anx.CalCredAgenciaCal,
 anx.ExpNegativasPag,
 anx.IndPerMorales,
 anx.NumeroEmpleados,
 anx.LugarRadica,
 anx.EsGarante
FROM dbo.SICCMX_Anexo21_GP anx
INNER JOIN dbo.SICC_TipoCambio tca ON 1=1
INNER JOIN dbo.SICC_Periodo per ON tca.IdPeriodo = per.IdPeriodo AND per.Activo=1
INNER JOIN dbo.SICC_Moneda mon ON tca.IdMoneda = mon.IdMoneda
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.Value = mon.Codigo AND cfg.CodeName='MonedaUdis';
GO

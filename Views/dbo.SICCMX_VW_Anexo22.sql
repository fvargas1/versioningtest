SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Anexo22]
AS
SELECT
	anx.IdPersona,
	anx.NumDiasMoraPromInstBanc,
	anx.PorPagoTiemInstBanc,
	anx.NumInstRepUlt12Meses,
	anx.PorPagoTiemInstNoBanc,
	anx.MonTotPagInfonavitUltBimestre / tca.Valor AS MonTotPagInfonavitUltBimestre,
	anx.NumDiasAtrInfonavitUltBimestre,
	anx.TasaRetLab,
	anx.IndCalEstaEcono,
	anx.IntCarCompetencia,
	anx.Provedores,
	anx.Clientes,
	anx.EdoFinAudit,
	anx.NumAgeCalf,
	anx.IndConsAdmon,
	anx.EstrucOrg,
	anx.CompAccionaria,
	anx.GastosFinancieros,
	anx.VentNetTotAnuales,
	anx.PasivoCirculante,
	anx.ActivoCirculante,
	anx.ActTotalAnual,
	anx.CapitalContableProm,
	anx.UtilidadNeta,
	anx.UAFIR,
	anx.OrgDescPartidoPolitico,
	anx.FechaInfoFinanc,
	anx.FechaInfoBuro,
	anx.CalCredAgenciaCal,
	anx.ExpNegativasPag,
	anx.RotActTotales,
	anx.RotCapTrabajo,
	anx.RendimeintosCapitalROE,
	anx.UAFIR_GastosFin,
	anx.LiquidezOper,
	anx.NumeroEmpleados,
	anx.LugarRadica,
	anx.F_NumDiasMoraPromInstBanc,
	anx.F_PorPagoTiemInstBanc,
	anx.F_NumInstRepUlt12Meses,
	anx.F_PorPagoTiemInstNoBanc,
	anx.F_MonTotPagInfonavitUltBimestre,
	anx.F_NumDiasAtrInfonavitUltBimestre,
	anx.F_TasaRetLab,
	anx.F_RotActTotales,
	anx.F_RotCapTrabajo,
	anx.F_RendimeintosCapitalROE,
	anx.F_IndCalEstaEcono,
	anx.F_IntCarCompetencia,
	anx.F_Provedores,
	anx.F_Clientes,
	anx.F_EdoFinAudit,
	anx.F_NumAgeCalf,
	anx.F_IndConsAdmon,
	anx.F_EstrucOrg,
	anx.F_CompAccionaria,
	anx.F_LiquidezOper,
	anx.F_UAFIR_GastosFin,
	anx.SinAtrasos
FROM dbo.SICCMX_Anexo22 anx
INNER JOIN dbo.SICC_TipoCambio tca ON 1=1
INNER JOIN dbo.SICC_Periodo per ON tca.IdPeriodo = per.IdPeriodo AND per.Activo=1
INNER JOIN dbo.SICC_Moneda mon ON tca.IdMoneda = mon.IdMoneda
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.Value = mon.Codigo AND cfg.CodeName='MonedaUdis';
GO

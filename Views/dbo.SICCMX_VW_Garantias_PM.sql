SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_PM]
AS
SELECT
 can.IdCredito,
 can.MontoCobAjust AS Monto,
 can.PrctCobAjust AS Porcentaje,
 can.[PI],
 can.SeveridadCorresp AS SP,
 can.Reserva,
 gar.NombreGarante AS NomGar,
 gar.Codigo AS CodigoGarantia
FROM dbo.SICCMX_Garantia_Canasta can
CROSS APPLY (
	SELECT TOP 1 g.NombreGarante, g.Codigo
	FROM dbo.SICCMX_CreditoGarantia cg
	INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia AND can.IdTipoGarantia = g.IdTipoGarantia
	INNER JOIN dbo.SICC_TipoGarantia tg ON g.IdTipoGarantia = tg.IdTipoGarantia
	WHERE tg.Codigo = 'NMC-05' AND cg.IdCredito = can.IdCredito
	ORDER BY g.ValorGarantia DESC
) AS gar;
GO

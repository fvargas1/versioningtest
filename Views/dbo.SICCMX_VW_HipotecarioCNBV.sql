SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_HipotecarioCNBV]
AS
SELECT hip.IdHipotecario, cnbv.CNBV
FROM dbo.SICCMX_HipotecarioCNBV cnbv
INNER JOIN dbo.SICCMX_Hipotecario hip ON cnbv.Codigo = hip.Codigo
WHERE cnbv.IdPeriodoBaja IS NULL;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MIGRACION_VW_Validacion]
AS
SELECT
	Validacion.IdValidacion,
	Validacion.Description,
	Validacion.Codename,
	Validacion.Position,
	Validacion.CategoryName,
	Validacion.FieldName,
	Validacion.Activo,
	Validacion.AplicaUpd,
	Validacion.ReqCalificacion,
	Validacion.ReqReportes
FROM dbo.MIGRACION_Validacion AS Validacion;
GO

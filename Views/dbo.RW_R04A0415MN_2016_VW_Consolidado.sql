SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0415MN_2016_VW_Consolidado]
AS
SELECT
 Codigo,
 Concepto,
 Padre,
 SUM(InteresesPagados) AS InteresesPagados,
 SUM(Comisiones) AS Comisiones,
 SUM(SaldoPromedioDiario) AS SaldoPromedioDiario
FROM dbo.RW_R04A0415MN_2016_VW
GROUP BY Codigo, Concepto, Padre;
GO

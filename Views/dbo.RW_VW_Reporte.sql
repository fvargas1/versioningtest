SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Reporte]
AS
SELECT
	Reporte.IdReporte,
	Reporte.GrupoReporte,
	Reporte.Nombre,
	Reporte.Descripcion,
	Reporte.ReglamentoOficial,
	Reporte.ReglamentoOficialPath,
	Reporte.Periodicidad,
	Reporte.NombreTabla,
	Reporte.SProcGenerate,
	Reporte.SProcGet,
	Reporte.LayoutPath,
	Reporte.NombreSSRS,
	Reporte.Activo,
	Reporte.IdGrupoReporte
FROM dbo.RW_Reporte AS Reporte;
GO

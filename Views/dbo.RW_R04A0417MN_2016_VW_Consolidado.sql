SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0417MN_2016_VW_Consolidado]
AS
SELECT
 Codigo,
 Concepto,
 Padre,
 SUM(SaldoBase) AS SaldoBase,
 SUM(Estimacion) AS Estimacion
FROM dbo.RW_R04A0417MN_2016_VW
GROUP BY Codigo, Concepto, Padre;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_TiposCambio_SICC]
AS
SELECT
	prd.Fecha,
	mon.CodigoISO,
	tc.Valor
FROM dbo.SICC_Periodo prd
INNER JOIN dbo.SICC_TipoCambio tc ON prd.IdPeriodo = tc.IdPeriodo
INNER JOIN dbo.SICC_Moneda mon ON tc.IdMoneda = mon.IdMoneda;
GO

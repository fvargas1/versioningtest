SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Proyecto_DatosLinea]
AS
SELECT pry.IdProyecto, SUM(vw.MontoLinea) AS MontoLinea, SUM(vw.MontoValorizado) AS SaldoInsoluto
FROM dbo.SICCMX_Proyecto pry
INNER JOIN dbo.SICCMX_VW_Credito_NMC vw ON pry.IdLineaCredito = vw.IdLineaCredito
GROUP BY pry.IdProyecto

UNION ALL

SELECT pl.IdProyecto, SUM(vw.MontoLinea) AS MontoLinea, SUM(vw.MontoValorizado) AS SaldoInsoluto
FROM dbo.SICCMX_ProyectoLinea pl
INNER JOIN dbo.SICCMX_VW_Credito_NMC vw ON pl.IdLineaCredito = vw.IdLineaCredito
GROUP BY pl.IdProyecto;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_TasaValor]
AS
SELECT
	TasaValor.IdTasaValor,
	TasaValor.IdTasa,
	idtasa.Descripcion AS DescripcionTasa,
	TasaValor.IdPeriodo,
	idperiodo.Fecha AS PeriodoFecha,
	TasaValor.Valor
FROM dbo.SICC_TasaValor AS TasaValor
INNER JOIN dbo.SICC_Tasa idtasa ON TasaValor.IdTasa = idtasa.IdTasa
INNER JOIN dbo.SICC_Periodo idperiodo ON TasaValor.IdPeriodo = idperiodo.IdPeriodo;
GO

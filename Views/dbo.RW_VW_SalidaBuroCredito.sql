SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_SalidaBuroCredito]
AS
SELECT DISTINCT
	Periodo,
	BP,
	Cuenta,
	Calificacion,
	ISNULL(Periodo,'')+'|'+ISNULL(BP,'')+'|'+ISNULL(Cuenta,'')+'|'+ISNULL(Calificacion,'') AS EVERYTHING
FROM dbo.RW_SalidaBuroCredito;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_CedulaHistorico]
AS
SELECT
 CedulaHistorico.IdCedula,
 CedulaHistorico.Nombre,
 CedulaHistorico.Codename,
 CedulaHistorico.ReportFolder,
 CedulaHistorico.Reportname,
 CedulaHistorico.Parameters,
 CedulaHistorico.Prefix,
 CedulaHistorico.Activo,
 CedulaHistorico.OutputParams
FROM dbo.RW_CedulaHistorico AS CedulaHistorico;
GO

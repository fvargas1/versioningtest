SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_GarantiasPersReportes]
AS
SELECT
	tb.IdCredito,
	MAX(ISNULL(tb.NumeroGarPersonales,0)) AS NumeroGarPersonales,
	MAX(ISNULL(tb.PrctGarPersonales,0)) AS PrctGarPersonales,
	MAX(ISNULL(tb.MontoGarPersonales,0)) AS MontoGarPersonales,
	MAX(tb.NombreAval) AS NombreAval,
	MAX(ISNULL(tb.PrctAval,0)) AS PrctAval,
	MAX(tb.TipoAval) AS TipoAval,
	MAX(tb.RFCAval) AS RFCAval,
	MAX(tb.TipoGarante) AS TipoGarante,
	MIN(tb.PI_Garante) AS PI_Garante,
	MAX(tb.IdMoneda) AS IdMoneda
FROM (
SELECT DISTINCT
	ca.IdCredito,
	COUNT(a.IdAval) AS NumeroGarPersonales,
	SUM(ca.Porcentaje) AS PrctGarPersonales,
	SUM(ca.Monto) AS MontoGarPersonales,
	MAX(REPLACE(a.Nombre, ',', '')) AS NombreAval,
	SUM(ca.Porcentaje) AS PrctAval,
	MAX(tg.Codigo) AS TipoAval,
	MAX(a.RFC) AS RFCAval,
	MAX(tg.Codigo) AS TipoGarante,
	MAX(a.PIAval) AS PI_Garante,
	MAX(mon.CodigoCNBV) AS IdMoneda
FROM dbo.SICCMX_CreditoAval ca
INNER JOIN dbo.SICCMX_Aval a ON ca.IdAval = a.IdAval AND a.Aplica = 1
INNER JOIN dbo.SICC_TipoGarante tg ON a.IdTipoAval = tg.IdTipoGarante
LEFT OUTER JOIN dbo.SICC_Moneda mon ON a.IdMoneda = mon.IdMoneda
WHERE ca.Aplica = 1
GROUP BY ca.IdCredito
UNION
SELECT
	cgPP.IdCredito,
	COUNT(g.IdGarantia) OVER (PARTITION BY cgPP.IdCredito) AS NumeroGarPersonales,
	gPP.PrctCubierto AS PrctGarPersonales,
	gPP.MontoCubierto AS MontoGarPersonales,
	g.NombreGarante AS NombreAval,
	gPP.PrctCubierto AS PrctAval,
	tg.Codigo AS TipoAval,
	g.RFCGarante AS RFCAval,
	tg.Codigo AS TipoGarante,
	g.PIGarante AS PI_Garante,
	mon.CodigoCNBV AS IdMoneda
FROM dbo.SICCMX_CreditoGarantia_PP cgPP
INNER JOIN dbo.SICCMX_Garantia_PP gPP ON cgPP.IdGarantia = gPP.IdGarantia
INNER JOIN dbo.SICCMX_VW_Garantia g ON cgPP.IdGarantia = g.IdGarantia AND g.Aplica = 1 AND g.CodigoTipoGarantia = 'NMC-04'
LEFT OUTER JOIN dbo.SICC_TipoGarante tg ON g.IdTipoGarante = tg.IdTipoGarante
LEFT OUTER JOIN dbo.SICC_Moneda mon ON g.IdMoneda = mon.IdMoneda
UNION
SELECT
	cg.IdCredito,
	COUNT(g.IdGarantia) OVER (PARTITION BY cg.IdCredito) AS NumeroGarPersonales,
	can.PrctCobSinAju AS PrctGarPersonales,
	can.C AS MontoGarPersonales,
	g.NombreGarante AS NombreAval,
	can.PrctCobSinAju AS PrctAval,
	tg.Codigo AS TipoAval,
	g.RFCGarante AS RFCAval,
	tg.Codigo AS TipoGarante,
	g.PIGarante AS PI_Garante,
	mon.CodigoCNBV AS IdMoneda
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_VW_Garantia g ON cg.IdGarantia = g.IdGarantia AND g.Aplica = 1 AND g.CodigoTipoGarantia = 'NMC-05'
INNER JOIN dbo.SICCMX_Garantia_Canasta can ON cg.IdCredito = can.IdCredito AND g.IdTipoGarantia = can.IdTipoGarantia
LEFT OUTER JOIN dbo.SICC_TipoGarante tg ON g.IdTipoGarante = tg.IdTipoGarante
LEFT OUTER JOIN dbo.SICC_Moneda mon ON g.IdMoneda = mon.IdMoneda
) AS tb
GROUP BY tb.IdCredito;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Historico_Consumo]
AS
SELECT
 vw.IdConsumo,
 (SUM(CASE WHEN vw.RN <= 4 THEN vw.PorcentajePagoRealizado ELSE 0 END) + dbo.MAX2VAR(4-MAX(vw.RN),0)) / 4 AS PorPagoM,
 CASE WHEN imp.RN_Impago IS NULL OR imp.RN_Impago = 2 THEN 0 ELSE SUM(CASE WHEN vw.RN BETWEEN 2 AND imp.RN_Impago THEN vw.Impago ELSE 0 END) END AS ImpagoConsecutivos,
 SUM(CASE WHEN vw.RN <= 6 THEN ISNULL(vw.Impago,0) ELSE 0 END) AS ImpagosHistoricos
FROM dbo.SICCMX_VW_Consumo_Var_Historico vw
LEFT OUTER JOIN (
 SELECT IdConsumo, MIN(RN) AS RN_Impago
 FROM dbo.SICCMX_VW_Consumo_Var_Historico
 WHERE ISNULL(Impago,0) = 0 AND RN > 1
 GROUP BY IdConsumo
) imp ON vw.IdConsumo = imp.IdConsumo
GROUP BY vw.IdConsumo, imp.RN_Impago;
GO

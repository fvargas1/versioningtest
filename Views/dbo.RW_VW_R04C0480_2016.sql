SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0480_2016]
AS
SELECT DISTINCT
	Formulario,
	CodigoPersona,
	SinAtrasos,
	[PI],
	PuntajeTotal,
	PuntajeCuantitativo,
	PuntajeCualitativo,
	CreditoReportadoSIC,
	Alfa,
	HITenSIC,
	FechaConsultaSIC,
	FechaInfoFinanciera,
	MesesPI100,
	ID_PI100,
	GarantiaLeyFederal,
	CumpleCritContGral,
	P_DiasMoraInstBanc,
	P_PorcPagoInstBanc,
	P_NumInstRep,
	P_PorcPagoInstNoBanc,
	P_PagosInfonavit,
	P_DiasAtrInfonavit,
	P_TasaRetLab,
	P_RotActTot,
	P_RotCapTrabajo,
	P_RendCapROE,
	DiasMoraInstBanc,
	PorcPagoInstBanc,
	NumInstRep,
	PorcPagoInstNoBanc,
	PagosInfonavit,
	DiasAtrInfonavit,
	NumeroEmpleados,
	TasaRetLab,
	PasivoCirculante,
	UtilidadNeta,
	CapitalContable,
	ActivoTotalAnual,
	VentasNetasTotales,
	RotActTot,
	ActivoCirculante,
	RotCapTrabajo,
	RendCapROE,
	ISNULL(CodigoPersona,'') AS EVERYTHING
FROM dbo.RW_R04C0480_2016;
GO

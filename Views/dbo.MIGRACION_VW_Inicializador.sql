SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MIGRACION_VW_Inicializador]
AS
SELECT
	Inicializador.IdInicializador,
	Inicializador.Description,
	Inicializador.Codename,
	Inicializador.Position,
	Inicializador.GrupoArchivo,
	Inicializador.Activo
FROM dbo.MIGRACION_Inicializador AS Inicializador;
GO

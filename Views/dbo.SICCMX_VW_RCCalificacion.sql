SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_RCCalificacion]
AS
SELECT
	cal.IdRCCalificacion,
	cal.CodigoCNBV,
	tab.CodigoCNBV AS TablaAdeudoCNBV,
	grd.CodigoCNBV AS GradoRiesgoCNBV,
	esc.CodigoCNBV AS EscalaCNBV,
	agn.CodigoCNBV AS AgenciaCNBV
FROM dbo.SICC_RCCalificacion cal
INNER JOIN dbo.SICC_TablaAdeudo tab ON cal.IdTablaAdeudo = tab.IdTablaAdeudo
INNER JOIN dbo.SICC_RCGradoRiesgo grd ON cal.IdRCGradoRiesgo = grd.IdRCGradoRiesgo
INNER JOIN dbo.SICC_Escala esc ON cal.IdRCEscalaCal = esc.IdEscala
INNER JOIN dbo.SICC_AgenciaCalificadora agn ON cal.IdRCAgenciaCal = agn.IdAgencia
GO

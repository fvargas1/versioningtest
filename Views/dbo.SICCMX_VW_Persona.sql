SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Persona]
AS
SELECT
 per.IdPersona,
 per.Codigo,
 per.Nombre,
 per.RFC,
 per.Domicilio,
 pInfo.GrupoRiesgo AS IdGrupoEconomicoNombre,
 per.Telefono,
 per.Correo,
 per.IdEjecutivo,
 eje.Nombre AS IdEjecutivoNombre,
 per.IdInstitucion,
 inst.Nombre AS IdInstitucionNombre,
 per.IdActividadEconomica,
 act.Nombre AS IdActividadEconomicaNombre,
 per.IdLocalidad,
 loc.NombreLocalidad AS IdLocalidadNombre,
 per.IdDeudorRelacionado,
 drel.Nombre AS IdDeudorRelacionadoNombre,
 per.IdSucursal,
 suc.Nombre AS IdSucursalNombre,
 per.FechaCreacion,
 per.FechaActualizacion,
 per.Username
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON pInfo.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Ejecutivo eje ON per.IdEjecutivo = eje.IdEjecutivo
INNER JOIN dbo.SICC_Institucion inst ON per.IdInstitucion = inst.IdInstitucion
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON per.IdActividadEconomica = act.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_Localidad2015 loc ON per.IdLocalidad = loc.IdLocalidad
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado drel ON per.IdDeudorRelacionado = drel.IdDeudorRelacionado
LEFT OUTER JOIN dbo.SICC_Sucursal suc ON per.IdSucursal = suc.IdSucursal;
GO

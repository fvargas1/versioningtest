SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Credito_TiposAlta]
AS
SELECT cre.Codigo, ta.Codigo AS TipoAlta, prd.Fecha
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_CreditoInfo inf ON cre.IdCredito = inf.IdCredito
INNER JOIN dbo.SICC_TipoAlta ta ON inf.IdTipoAlta = ta.IdTipoAlta
INNER JOIN dbo.SICC_Periodo prd ON prd.Activo = 1
UNION ALL
SELECT inf.Credito, inf.TipoAlta, prd.Fecha
FROM Historico.SICCMX_CreditoInfo inf
INNER JOIN dbo.SICC_PeriodoHistorico prd ON inf.IdPeriodoHistorico = prd.IdPeriodoHistorico AND prd.Activo = 1
WHERE inf.TipoAlta IS NOT NULL;
GO

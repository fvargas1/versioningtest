SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantia_TotGRF_Consumo]
AS
SELECT
	IdConsumo,
	CAST(SUM(ValorGarantia) AS DECIMAL(23,2)) AS MontoTotal,
	MontoCredito
FROM dbo.SICCMX_VW_Garantias_NM_Consumo
WHERE IdClasificacionNvaMet = 1
GROUP BY IdConsumo, MontoCredito;
GO

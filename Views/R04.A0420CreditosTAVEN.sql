SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [R04].[A0420CreditosTAVEN]
AS
-- Creditos con SituacionHistorica Vigente y Actual Vencida, deben de tener movimientos taven
SELECT
	dat.Codigo,
	'COMERCIAL' TipoCredito
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICCMX_Credito cr ON cr.Codigo = dat.Codigo
INNER JOIN dbo.SICCMX_Credito_Movimientos mov ON cr.IdCredito = mov.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento AND tmov.Codigo = 'TAVEN'
WHERE (dat.SituacionHistorica = '1' AND dat.SituacionActual = '2')
UNION ALL
SELECT
	dat.Codigo,
	'CONSUMO'
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICCMX_Consumo cr ON cr.Codigo = dat.Codigo
INNER JOIN dbo.SICCMX_Consumo_Movimientos mov ON cr.IdConsumo = mov.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento AND tmov.Codigo = 'TAVEN'
WHERE (dat.SituacionHistorica = '1' AND dat.SituacionActual = '2')
UNION ALL
SELECT
	dat.Codigo,
	'HIPOTECARIO'
FROM R04.[0420Datos] dat
INNER JOIN dbo.SICCMX_Hipotecario cr ON cr.Codigo = dat.Codigo
INNER JOIN dbo.SICCMX_Hipotecario_Movimientos mov ON cr.IdHipotecario = mov.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento AND tmov.Codigo = 'TAVEN'
WHERE (dat.SituacionHistorica = '1' AND dat.SituacionActual = '2');
GO

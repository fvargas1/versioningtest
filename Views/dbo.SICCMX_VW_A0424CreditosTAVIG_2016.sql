SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_A0424CreditosTAVIG_2016]
AS
-- Creditos con Sitacion Historica Vencida y Actual Vigente
SELECT dat.Codigo
FROM R04.[0424Datos_2016] dat
WHERE (dat.SituacionHistorica = '2' AND dat.SituacionActual = '1');
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Credito_AjusteTasa]
AS
SELECT
 inf.IdCredito,
 CASE WHEN ISNULL(cre.EsMultimoneda,0) = 0 THEN '0' ELSE
 CASE WHEN SUBSTRING(inf.AjusteTasaReferencia,1,1) IN ('+','-','*') THEN SUBSTRING(inf.AjusteTasaReferencia,1,1) ELSE '' END +
 ISNULL(CAST(CAST(REPLACE(REPLACE(REPLACE(NULLIF(inf.AjusteTasaReferencia,''),'-',''),'*',''),'+','') AS DECIMAL(18,6)) AS VARCHAR(20)),'0') END AS AjusteTasa,
 CASE
 WHEN inf.AjusteTasaReferencia LIKE '%+%' OR SUBSTRING(inf.AjusteTasaReferencia,1,1) NOT IN ('+','-','*') THEN '110'
 WHEN inf.AjusteTasaReferencia LIKE '%*%' THEN '111'
 ELSE '112'
 END AS OperacionDiferencial,
 CASE WHEN ISNULL(cre.EsMultimoneda,0) = 0 THEN '0' ELSE cre.FrecuenciaRevisionTasa END AS FrecuenciaRevisionTasa
FROM dbo.SICCMX_CreditoInfo inf
INNER JOIN dbo.SICCMX_Credito cre ON inf.IdCredito = cre.IdCredito;
GO

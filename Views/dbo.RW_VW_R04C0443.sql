SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0443]
AS
SELECT DISTINCT
	Formulario,
	NumeroSecuencia,
	CodigoPersona,
	RFC,
	NombrePersona,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoAgrupacion,
	NumeroDisposicion,
	FechaDisposicion,
	CalificacionPersona,
	CalificacionCreditoCubierta,
	CalificacionCreditoExpuesta,
	CalificacionPersonaCNBV,
	CalificacionCreditoCubiertaCNBV,
	CalificacionCreditoExpuestaCNBV,
	SituacionCredito,
	SaldoInicial,
	TasaInteres,
	MontoDispuesto,
	MontoExigible,
	MontoPagado,
	MontoInteresPagado,
	MontoComisionDevengada,
	DiasVencido,
	SaldoFinal,
	ResponsabilidadTotal,
	TipoBaja,
	MontoReconocido,
	ProductoComercial,
	ISNULL(CodigoPersona,'')+'|'+ISNULL(RFC,'')+'|'+ISNULL(NombrePersona,'')+'|'+ISNULL(CodigoCredito,'')+'|'+ISNULL(CodigoCreditoCNBV,'') AS EVERYTHING
FROM dbo.RW_R04C0443;
GO

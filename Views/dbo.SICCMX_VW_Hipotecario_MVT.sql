SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Hipotecario_MVT]
AS
SELECT hip.IdHipotecario
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hip.IdHipotecario = pre.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaHipotecario
WHERE met.Codigo = 'MVT';
GO

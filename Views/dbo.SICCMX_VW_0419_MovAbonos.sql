SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_0419_MovAbonos]
AS
-- RESERVAS ADICIONALES
SELECT
 dat.Codigo,
 '139497000000' AS Concepto,
 (dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida) - (dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico) AS Reserva
FROM R04.[0419Datos_2016] dat
WHERE dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico < dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida
AND dat.SaldoHistorico > 0 AND IdTipoBaja IS NULL
AND (dat.ReservasAdicOrdenadasCNBV + dat.InteresCarteraVencida) - (dat.ReservasAdicOrdenadasCNBVHistorico + dat.InteresCarteraVencidaHistorico) > 0

UNION ALL
-- CREDITOS NUEVOS EN EL MES
SELECT
 dat.Codigo,
 conf.Concepto,
 dat.SaldoReservaCubActual+dat.SaldoReservaExpActual
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CREADI'
WHERE dat.SaldoHistorico = 0 AND dat.IdTipoBaja IS NULL
AND dat.SaldoReservaCubActual+dat.SaldoReservaExpActual > 0

UNION ALL
-- AJUSTE CAMBIARIO
SELECT
 dat.Codigo,
 conf.Concepto,
 ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'AJCAMBABONO'
WHERE dat.SaldoHistorico > 0 AND dat.IdTipoBaja IS NULL AND dat.AjusteCambiario > 0
AND ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual > 0

UNION ALL
SELECT
 dat.Codigo,
 conf.Concepto,
 ABS(dat.ReservasDif) - CAST(ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual AS DECIMAL)
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CRECAL'
WHERE dat.SaldoHistorico > 0 AND dat.Moneda <> '0' AND dat.IdTipoBaja IS NULL AND dat.ReservasDif < 0
AND ABS(dat.ReservasDif) - ABS(dat.AjusteCambiario) * (dat.SaldoReservaCubActual+dat.SaldoReservaExpActual+dat.ReservasAdicOrdenadasCNBV+dat.InteresCarteraVencida) / dat.SaldoActual > 0

UNION ALL
SELECT
 dat.Codigo,
 conf.Concepto,
 ABS(dat.ReservasDif)
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CRECAL'
WHERE dat.SaldoHistorico = dat.SaldoActual AND dat.Moneda = '0' AND dat.ReservasDif < 0;
GO

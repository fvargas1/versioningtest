SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_INTERNO_PI]
AS
SELECT DISTINCT TOP 1000
	Codigo,
	Nombre,
	Metodologia,
	Clasificacion,
	FactorCuantitativo,
	FactorCualitativo,
	Factor_Alpha,
	Factor_1mAlpha,
	PonderadoCuantitativo,
	PonderadoCualitativo,
	FactorTotal,
	[PI],
	ISNULL(Codigo,'')+'|'+ISNULL(Nombre,'') AS EVERYTHING
FROM dbo.RW_INTERNO_PI
ORDER BY Metodologia, Nombre;
GO

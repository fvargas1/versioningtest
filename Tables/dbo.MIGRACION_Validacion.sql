CREATE TABLE [dbo].[MIGRACION_Validacion]
(
[IdValidacion] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codename] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Position] [int] NOT NULL,
[CategoryName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NOT NULL CONSTRAINT [DF_MIGRACION_Validacion_Activo] DEFAULT ((1)),
[FieldName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaInicio] [datetime] NULL,
[FechaFin] [datetime] NULL,
[Estatus] [int] NULL,
[AplicaUpd] [int] NULL,
[Detalle] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorDetalle] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReqCalificacion] [bit] NULL,
[ReqReportes] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_Validacion] ADD CONSTRAINT [PK_MIGRACION_Validacion] PRIMARY KEY CLUSTERED  ([IdValidacion]) ON [PRIMARY]
GO

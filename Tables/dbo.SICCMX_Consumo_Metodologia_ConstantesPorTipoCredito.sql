CREATE TABLE [dbo].[SICCMX_Consumo_Metodologia_ConstantesPorTipoCredito]
(
[IdMetodologia] [int] NOT NULL,
[IdTipoCredito] [int] NOT NULL,
[Factor] [decimal] (8, 6) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Consumo_Metodologia_ConstantesPorTipoCredito] ADD CONSTRAINT [PK_SICCMX_Consumo_Metodologia_ConstantesPorTipoCredito] PRIMARY KEY CLUSTERED  ([IdMetodologia], [IdTipoCredito]) ON [PRIMARY]
GO

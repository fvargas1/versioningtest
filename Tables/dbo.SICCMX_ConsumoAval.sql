CREATE TABLE [dbo].[SICCMX_ConsumoAval]
(
[IdRel] [bigint] NOT NULL IDENTITY(1, 1),
[IdConsumo] [bigint] NULL,
[IdAval] [bigint] NULL,
[Porcentaje] [decimal] (14, 10) NULL,
[Monto] [decimal] (23, 2) NULL,
[Aplica] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_ConsumoAval] ADD CONSTRAINT [PK_SICCMX_ConsumoAval] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO

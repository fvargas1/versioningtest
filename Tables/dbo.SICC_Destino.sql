CREATE TABLE [dbo].[SICC_Destino]
(
[IdDestino] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV_A22] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBVEYM] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBVOD] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV_A20] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL,
[CNBV_2016] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CNBV_2016_EYM] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Destino] ADD CONSTRAINT [PK_SICCMX_Destino] PRIMARY KEY CLUSTERED  ([IdDestino]) ON [PRIMARY]
GO

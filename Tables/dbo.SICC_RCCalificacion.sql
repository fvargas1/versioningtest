CREATE TABLE [dbo].[SICC_RCCalificacion]
(
[IdRCCalificacion] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdTablaAdeudo] [int] NULL,
[IdRCGradoRiesgo] [int] NULL,
[IdRCEscalaCal] [int] NULL,
[IdRCAgenciaCal] [int] NULL
) ON [PRIMARY]
GO

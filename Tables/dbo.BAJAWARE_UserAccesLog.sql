CREATE TABLE [dbo].[BAJAWARE_UserAccesLog]
(
[IdUserLog] [int] NOT NULL IDENTITY(1, 1),
[Username] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FechaAcceso] [datetime] NOT NULL,
[Status] [smallint] NOT NULL,
[IPAddress] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_UserAccesLog] ADD CONSTRAINT [PK_BAJAWARE_UserAccesLog] PRIMARY KEY CLUSTERED  ([IdUserLog]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_UserAccesLog] ADD CONSTRAINT [FK_BAJAWARE_UserAccesLog_BAJAWARE_UserAccesLog] FOREIGN KEY ([IdUserLog]) REFERENCES [dbo].[BAJAWARE_UserAccesLog] ([IdUserLog])
GO

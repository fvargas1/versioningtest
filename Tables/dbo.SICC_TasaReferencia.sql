CREATE TABLE [dbo].[SICC_TasaReferencia]
(
[IdTasaReferencia] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TasaReferencia] ADD CONSTRAINT [PK_SICCMX_TasaReferencia] PRIMARY KEY CLUSTERED  ([IdTasaReferencia]) ON [PRIMARY]
GO

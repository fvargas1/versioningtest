CREATE TABLE [dbo].[SICC_Municipio]
(
[IdMunicipio] [int] NOT NULL IDENTITY(1, 1),
[CodigoEstado] [smallint] NOT NULL,
[Estado] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocalidadCNBV] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoMunicipio] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoColonia] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Municipio] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoRR] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Codigo2016] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Municipio] ADD CONSTRAINT [PK_SICC_Municipio] PRIMARY KEY CLUSTERED  ([IdMunicipio]) ON [PRIMARY]
GO

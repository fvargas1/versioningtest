CREATE TABLE [dbo].[RW_ReporteApoyo]
(
[IdReporteApoyo] [bigint] NOT NULL IDENTITY(1, 1),
[IdReporte] [bigint] NOT NULL,
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NOT NULL,
[OnDemand] [bit] NOT NULL,
[NombreArchivo] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSRSTemplate] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdUsuarioCreacion] [bigint] NOT NULL,
[FechaCreacion] [datetime] NOT NULL,
[IdUsuarioModificacion] [bigint] NULL,
[FechaModificacion] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RW_ReporteApoyo] ADD CONSTRAINT [PK_RW_ReporteApoyo] PRIMARY KEY CLUSTERED  ([IdReporteApoyo]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[RW_Cofinavit_Reservas]
(
[IdReporteLog] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoCapitalVigente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteresVigente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoCapitalVencido] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteresVencido] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoRegimen] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Constante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorATR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ATR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorPorpago] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorPago] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorPromRete] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PromRete] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorMaxATR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxATR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[E] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reserva] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorReserva] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

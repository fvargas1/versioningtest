CREATE TABLE [dbo].[RW_MAPEO_R01]
(
[IdMapeoR01] [int] NOT NULL IDENTITY(1, 1),
[SituacionCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TipoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Anexo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PersonalidadJuridica] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TipoSaldo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Cuenta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RW_MAPEO_R01] ADD CONSTRAINT [PK_RW_MAPEO_R01] PRIMARY KEY CLUSTERED  ([IdMapeoR01]) ON [PRIMARY]
GO

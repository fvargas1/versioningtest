CREATE TABLE [dbo].[SICC_TipoGarante]
(
[IdTipoGarante] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP] [decimal] (18, 10) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoGarante] ADD CONSTRAINT [PK_SICCMX_TipoGarante] PRIMARY KEY CLUSTERED  ([IdTipoGarante]) ON [PRIMARY]
GO

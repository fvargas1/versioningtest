CREATE TABLE [Historico].[SICCMX_PersonaCalificacion]
(
[IdPeriodoHistorico] [int] NULL,
[Persona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EI] [decimal] (23, 2) NULL,
[Reserva] [decimal] (23, 2) NULL,
[PrctReserva] [decimal] (18, 10) NULL,
[Calificacion] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI_Consumo] [decimal] (23, 2) NULL,
[Reserva_Consumo] [decimal] (23, 2) NULL,
[PrctReserva_Consumo] [decimal] (18, 10) NULL,
[Calificacion_Consumo] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI_Hipotecario] [decimal] (23, 2) NULL,
[Reserva_Hipotecario] [decimal] (23, 2) NULL,
[PrctReserva_Hipotecario] [decimal] (18, 10) NULL,
[Calificacion_Hipotecario] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

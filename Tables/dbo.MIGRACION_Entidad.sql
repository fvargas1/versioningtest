CREATE TABLE [dbo].[MIGRACION_Entidad]
(
[IdEntidad] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreView] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_Entidad] ADD CONSTRAINT [PK_MIGRACION_Entidad] PRIMARY KEY CLUSTERED  ([IdEntidad]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICCMX_Hipotecario_Metodologia_ValoresCalculoTR]
(
[IdValor] [int] NOT NULL IDENTITY(1, 1),
[Region] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Convenio] [int] NOT NULL,
[VariableA] [decimal] (5, 4) NOT NULL,
[VariableB] [decimal] (5, 4) NOT NULL,
[VariableC] [decimal] (5, 4) NOT NULL,
[IdMetodologia] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Hipotecario_Metodologia_ValoresCalculoTR] ADD CONSTRAINT [PK_SICCMX_Hipotecario_Metodologia_ValoresCalculoTR] PRIMARY KEY CLUSTERED  ([IdValor]) ON [PRIMARY]
GO

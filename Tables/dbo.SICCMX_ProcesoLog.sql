CREATE TABLE [dbo].[SICCMX_ProcesoLog]
(
[IdProcesoLog] [int] NOT NULL IDENTITY(1, 1),
[Categoria] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Proceso] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Fecha] [datetime] NOT NULL,
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdPeriodo] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_ProcesoLog] ADD CONSTRAINT [PK_SICCMX_ProcesoLog] PRIMARY KEY CLUSTERED  ([IdProcesoLog]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICC_SectorEconomico]
(
[IdSectorEconomico] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdCalificacionComercial] [int] NOT NULL,
[IdSubSector] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_SectorEconomico] ADD CONSTRAINT [PK_SICC_SectorEconomico] PRIMARY KEY CLUSTERED  ([IdSectorEconomico]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_SectorEconomico] WITH NOCHECK ADD CONSTRAINT [FK_SICC_SectorEconomico_SICC_CalificacionComercial] FOREIGN KEY ([IdCalificacionComercial]) REFERENCES [dbo].[SICC_CalificacionComercial] ([IdCalificacion])
GO
ALTER TABLE [dbo].[SICC_SectorEconomico] ADD CONSTRAINT [FK_SICC_SectorEconomico_SICC_SubSector] FOREIGN KEY ([IdSubSector]) REFERENCES [dbo].[SICC_SubSector] ([IdSubSector])
GO

CREATE TABLE [dbo].[SICC_AgenciaCalificacionGarantias]
(
[IdRel] [int] NOT NULL IDENTITY(1, 1),
[IdAgencia] [int] NOT NULL,
[Calificacion] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GradoRiesgoGlobal] [int] NOT NULL,
[GradoRiesgoMX] [int] NOT NULL,
[Activo] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_AgenciaCalificacionGarantias] ADD CONSTRAINT [PK_SICC_AgenciaCalificacionGarantias] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO

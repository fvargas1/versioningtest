CREATE TABLE [dbo].[SICC_TipoCambio]
(
[IdTipoCambio] [bigint] NOT NULL IDENTITY(1, 1),
[IdMoneda] [bigint] NOT NULL,
[IdPeriodo] [int] NOT NULL,
[Valor] [decimal] (18, 6) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoCambio] ADD CONSTRAINT [PK_SICC_TipoCambio] PRIMARY KEY CLUSTERED  ([IdTipoCambio]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoCambio] ADD CONSTRAINT [FK_SICC_TipoCambio_SICC_Moneda] FOREIGN KEY ([IdMoneda]) REFERENCES [dbo].[SICC_Moneda] ([IdMoneda])
GO
ALTER TABLE [dbo].[SICC_TipoCambio] ADD CONSTRAINT [FK_SICCHN_TipoCambio_SICCHN_Periodo] FOREIGN KEY ([IdPeriodo]) REFERENCES [dbo].[SICC_Periodo] ([IdPeriodo]) ON DELETE CASCADE
GO

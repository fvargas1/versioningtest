CREATE TABLE [dbo].[SICCMX_SPGarantiasReales]
(
[IdTipoGarantia] [int] NOT NULL,
[NivelMinimo] [decimal] (10, 4) NULL,
[NivelMaximo] [decimal] (10, 4) NULL,
[SeveridadPerdida] [decimal] (10, 4) NULL,
[SP_Consumo] [decimal] (10, 4) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_SPGarantiasReales] ADD CONSTRAINT [PK_SICCMX_SPGarantiasReales] PRIMARY KEY CLUSTERED  ([IdTipoGarantia]) ON [PRIMARY]
GO

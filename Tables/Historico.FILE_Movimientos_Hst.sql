CREATE TABLE [Historico].[FILE_Movimientos_Hst]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoMovimiento] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Monto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fecha] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

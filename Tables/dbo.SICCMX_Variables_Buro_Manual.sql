CREATE TABLE [dbo].[SICCMX_Variables_Buro_Manual]
(
[IdPersona] [int] NOT NULL,
[IdVariable] [int] NOT NULL,
[Valor] [decimal] (26, 10) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Variables_Buro_Manual] ADD CONSTRAINT [PK_SICCMX_Variables_Buro_Manual] PRIMARY KEY CLUSTERED  ([IdPersona], [IdVariable]) ON [PRIMARY]
GO

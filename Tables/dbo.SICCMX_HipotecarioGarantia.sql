CREATE TABLE [dbo].[SICCMX_HipotecarioGarantia]
(
[IdRel] [bigint] NOT NULL IDENTITY(1, 1),
[IdHipotecario] [bigint] NOT NULL,
[IdGarantia] [bigint] NOT NULL,
[PorUsadoGarantia] [decimal] (14, 10) NULL,
[MontoUsadoGarantia] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_HipotecarioGarantia] ADD CONSTRAINT [PK_SICCMX_HipotecarioGarantia] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO

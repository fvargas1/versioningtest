CREATE TABLE [Historico].[SICCMX_ProyectoLinea]
(
[IdPeriodoHistorico] [int] NULL,
[Proyecto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LineaCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

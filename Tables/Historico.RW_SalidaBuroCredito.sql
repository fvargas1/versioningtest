CREATE TABLE [Historico].[RW_SalidaBuroCredito]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Periodo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cuenta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cartera] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

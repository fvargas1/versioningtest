CREATE TABLE [Historico].[RW_Consumo_Revolvente_CAT]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Fecha] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FolioCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CatContrato] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LimCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Producto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

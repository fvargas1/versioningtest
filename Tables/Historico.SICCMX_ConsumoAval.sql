CREATE TABLE [Historico].[SICCMX_ConsumoAval]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Aval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Porcentaje] [decimal] (14, 10) NULL,
[Monto] [decimal] (23, 2) NULL,
[Aplica] [bit] NULL
) ON [PRIMARY]
GO

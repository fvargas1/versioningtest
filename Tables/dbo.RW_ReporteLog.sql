CREATE TABLE [dbo].[RW_ReporteLog]
(
[IdReporteLog] [bigint] NOT NULL IDENTITY(1, 1),
[IdReporte] [bigint] NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaCreacion] [datetime] NOT NULL,
[UsuarioCreacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FolderArchivo] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdFuenteDatos] [bigint] NULL,
[NombreArchivo] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaImportacionDatos] [datetime] NULL,
[FechaCalculoProcesos] [datetime] NULL,
[TotalRegistros] [bigint] NULL,
[TotalSaldos] [decimal] (23, 2) NULL,
[TotalIntereses] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RW_ReporteLog] ADD CONSTRAINT [PK_RW_ReporteLog] PRIMARY KEY CLUSTERED  ([IdReporteLog]) ON [PRIMARY]
GO

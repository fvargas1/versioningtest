CREATE TABLE [dbo].[RW_ReporteInterno]
(
[IdReporteInterno] [int] NOT NULL IDENTITY(1, 1),
[GrupoReporte] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SProc] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Activo] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RW_ReporteInterno] ADD CONSTRAINT [PK_RW_ReporteInterno] PRIMARY KEY CLUSTERED  ([IdReporteInterno]) ON [PRIMARY]
GO

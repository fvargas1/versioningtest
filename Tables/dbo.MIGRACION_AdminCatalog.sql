CREATE TABLE [dbo].[MIGRACION_AdminCatalog]
(
[IdAdminCatalog] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NombreTabla] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NombreIdentidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NombreBusqueda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_AdminCatalog] ADD CONSTRAINT [PK_MIGRACION_AdminCatalog] PRIMARY KEY CLUSTERED  ([IdAdminCatalog]) ON [PRIMARY]
GO

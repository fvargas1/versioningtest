CREATE TABLE [dbo].[SICCMX_Persona_PI_Detalles]
(
[IdPersona] [bigint] NOT NULL,
[IdVariable] [int] NOT NULL,
[ValorActual] [decimal] (26, 10) NULL,
[PuntosActual] [int] NULL,
[ValorH1] [decimal] (26, 10) NULL,
[PuntosH1] [int] NULL,
[ValorH2] [decimal] (26, 10) NULL,
[PuntosH2] [int] NULL,
[ValorH3] [decimal] (26, 10) NULL,
[PuntosH3] [int] NULL,
[ValorH4] [decimal] (26, 10) NULL,
[PuntosH4] [int] NULL,
[FechaValor] [datetime] NULL,
[FechaMigracion] [datetime] NULL,
[UsuarioMigracion] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaCalculo] [datetime] NULL,
[UsuarioCalculo] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Persona_PI_Detalles] ADD CONSTRAINT [PK_SICCMX_Persona_PI_Detalles_1] PRIMARY KEY CLUSTERED  ([IdPersona], [IdVariable]) ON [PRIMARY]
GO

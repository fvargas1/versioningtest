CREATE TABLE [dbo].[RW_ReporteSeccion]
(
[IdSeccion] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RW_ReporteSeccion] ADD CONSTRAINT [PK_RW_ReporteSeccion] PRIMARY KEY CLUSTERED  ([IdSeccion]) ON [PRIMARY]
GO

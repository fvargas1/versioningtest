CREATE TABLE [dbo].[SICCMX_GradoRiesgo_Ajuste]
(
[IdRel] [int] NOT NULL IDENTITY(1, 1),
[GradoRiesgo] [int] NOT NULL,
[RangoMenor] [decimal] (18, 10) NULL,
[RangoMayor] [decimal] (18, 10) NULL,
[Emisor] [int] NULL,
[Factor] [decimal] (18, 10) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_GradoRiesgo_Ajuste] ADD CONSTRAINT [PK_SICCMX_GradoRiesgo_Ajuste] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO

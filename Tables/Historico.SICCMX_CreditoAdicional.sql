CREATE TABLE [Historico].[SICCMX_CreditoAdicional]
(
[IdPeriodoHistorico] [int] NULL,
[Credito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoPromedio] [decimal] (23, 2) NULL,
[InteresDevengado] [decimal] (23, 2) NULL,
[Comision] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO

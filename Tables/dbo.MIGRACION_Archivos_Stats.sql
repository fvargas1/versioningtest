CREATE TABLE [dbo].[MIGRACION_Archivos_Stats]
(
[IdArchivo] [int] NOT NULL,
[Total] [int] NULL,
[Actualizados] [int] NULL,
[Nuevos] [int] NULL,
[Bajas] [int] NULL
) ON [PRIMARY]
GO

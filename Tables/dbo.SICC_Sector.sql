CREATE TABLE [dbo].[SICC_Sector]
(
[IdSector] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Sector] ADD CONSTRAINT [PK_SICC_Sector] PRIMARY KEY CLUSTERED  ([IdSector]) ON [PRIMARY]
GO

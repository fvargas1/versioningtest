CREATE TABLE [dbo].[SICC_TipoMovimiento]
(
[IdTipoMovimiento] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoMovimiento] ADD CONSTRAINT [PK_SICC_TipoMovimiento] PRIMARY KEY CLUSTERED  ([IdTipoMovimiento]) ON [PRIMARY]
GO

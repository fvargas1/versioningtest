CREATE TABLE [Historico].[SICCMX_Garantia_Consumo]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorGarantia] [decimal] (23, 2) NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaValuacion] [datetime] NULL,
[Descripcion] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BancoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorGarantiaProyectado] [decimal] (23, 2) NULL,
[Hc] [decimal] (18, 10) NULL,
[VencimientoRestante] [datetime] NULL,
[GradoRiesgo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgenciaCalificadora] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Emisor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Escala] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EsIPC] [bit] NULL,
[PIGarante] [decimal] (18, 10) NULL,
[CodigoCliente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndPerMorales] [bit] NULL,
[Aplica] [bit] NULL
) ON [PRIMARY]
GO

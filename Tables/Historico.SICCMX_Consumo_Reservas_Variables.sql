CREATE TABLE [Historico].[SICCMX_Consumo_Reservas_Variables]
(
[IdPeriodoHistorico] [int] NULL,
[Consumo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaCalificacion] [datetime] NULL,
[PI] [decimal] (18, 12) NULL,
[E] [decimal] (23, 2) NULL,
[MontoGarantia] [decimal] (23, 2) NULL,
[ReservaTotal] [decimal] (23, 2) NULL,
[PorReserva] [decimal] (18, 12) NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ECubierta] [decimal] (23, 2) NULL,
[SPCubierta] [decimal] (18, 12) NULL,
[ReservaCubierta] [decimal] (23, 2) NULL,
[PorReservaCubierta] [decimal] (18, 12) NULL,
[CalificacionCubierta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EExpuesta] [decimal] (23, 2) NULL,
[SPExpuesta] [decimal] (18, 12) NULL,
[ReservaExpuesta] [decimal] (23, 2) NULL,
[PorReservaExpuesta] [decimal] (18, 12) NULL,
[CalificacionExpuesta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoAdicional] [decimal] (23, 2) NULL,
[ReservaAdicional] [decimal] (23, 2) NULL,
[PorcentajeAdicional] [decimal] (18, 12) NULL,
[CalificacionAdicional] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tipo_EX] [decimal] (4, 2) NULL
) ON [PRIMARY]
GO

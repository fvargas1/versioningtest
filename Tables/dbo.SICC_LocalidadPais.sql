CREATE TABLE [dbo].[SICC_LocalidadPais]
(
[IdLocalidadPais] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdLocalidadZona] [bigint] NULL,
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdCalificacionComercial] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_LocalidadPais] ADD CONSTRAINT [PK_SICC_LocalidadPais] PRIMARY KEY CLUSTERED  ([IdLocalidadPais]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_LocalidadPais] WITH NOCHECK ADD CONSTRAINT [FK_SICC_LocalidadPais_SICC_CalificacionComercial] FOREIGN KEY ([IdCalificacionComercial]) REFERENCES [dbo].[SICC_CalificacionComercial] ([IdCalificacion])
GO
ALTER TABLE [dbo].[SICC_LocalidadPais] ADD CONSTRAINT [FK_SICC_LocalidadPais_SICC_LocalidadZona] FOREIGN KEY ([IdLocalidadZona]) REFERENCES [dbo].[SICC_LocalidadZona] ([IdLocalidadZona])
GO

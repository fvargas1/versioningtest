CREATE TABLE [dbo].[SICC_Institucion]
(
[IdInstitucion] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Fondeadora] [bit] NULL,
[Otorgante] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Institucion] ADD CONSTRAINT [PK_SICCMX_Institucion] PRIMARY KEY CLUSTERED  ([IdInstitucion]) ON [PRIMARY]
GO

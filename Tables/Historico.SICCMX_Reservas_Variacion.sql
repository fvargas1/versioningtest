CREATE TABLE [Historico].[SICCMX_Reservas_Variacion]
(
[IdPeriodoHistorico] [int] NULL,
[Credito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReservaCalConsDesc] [decimal] (23, 2) NULL,
[ReservaAdicConsDesc] [decimal] (23, 2) NULL,
[MesesSP100] [int] NULL,
[CreditoRes5Prct] [int] NULL,
[CreditoSustPI] [int] NULL,
[ReservaPeriodoAnt] [decimal] (23, 2) NULL,
[ReservaAdicPeriodoAnt] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO

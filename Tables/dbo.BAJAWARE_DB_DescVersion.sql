CREATE TABLE [dbo].[BAJAWARE_DB_DescVersion]
(
[IdDescVersion] [int] NOT NULL IDENTITY(1, 1),
[IdVersion] [int] NOT NULL,
[Orden] [int] NULL,
[Descripcion] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_DB_DescVersion] ADD CONSTRAINT [PK_BAJAWARE_DB_DescVersion] PRIMARY KEY CLUSTERED  ([IdDescVersion]) ON [PRIMARY]
GO

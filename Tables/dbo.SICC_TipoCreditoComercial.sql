CREATE TABLE [dbo].[SICC_TipoCreditoComercial]
(
[IdTipoCredito] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoCreditoComercial] ADD CONSTRAINT [PK_SICC_TipoCreditoComercial] PRIMARY KEY CLUSTERED  ([IdTipoCredito]) ON [PRIMARY]
GO

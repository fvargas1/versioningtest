CREATE TABLE [R04].[0419Configuracion_2016]
(
[IdConfiguracion] [int] NOT NULL IDENTITY(1, 1),
[TipoMovimiento] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CargoAbono] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0419Configuracion_2016] ADD CONSTRAINT [PK_0419Configuracion_2016] PRIMARY KEY CLUSTERED  ([IdConfiguracion]) ON [PRIMARY]
GO

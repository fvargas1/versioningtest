CREATE TABLE [dbo].[SICCMX_HipotecarioCNBV]
(
[IdHipotecarioCNBV] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[_Consecutivo] [int] NULL,
[FechaCreacion] [datetime] NULL CONSTRAINT [DF_SICCMX_HipotecarioCNBV_FechaCreacion] DEFAULT (getdate()),
[IdPeriodo] [bigint] NULL,
[IdPeriodoBaja] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_HipotecarioCNBV] ADD CONSTRAINT [PK_SICCMX_HipotecarioCNBV] PRIMARY KEY CLUSTERED  ([IdHipotecarioCNBV]) ON [PRIMARY]
GO

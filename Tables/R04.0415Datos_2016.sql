CREATE TABLE [R04].[0415Datos_2016]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TipoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SituacionCredito] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoPromedio] [decimal] (23, 2) NULL,
[SaldoPromedioUSD] [decimal] (23, 2) NULL,
[InteresMes] [decimal] (23, 2) NULL,
[InteresMesUSD] [decimal] (23, 2) NULL,
[ComisionMes] [decimal] (23, 2) NULL,
[ComisionMesUSD] [decimal] (23, 2) NULL,
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0415Datos_2016] ADD CONSTRAINT [PK_0415Datos_2016] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO

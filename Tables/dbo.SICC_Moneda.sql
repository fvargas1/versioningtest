CREATE TABLE [dbo].[SICC_Moneda]
(
[IdMoneda] [bigint] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MonedaOficial] [bit] NOT NULL,
[CodigoInterno] [bit] NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AplicaTC] [bit] NULL,
[CodigoCNBV_Hipo] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Codigo_Consumo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoISO] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClasSerieA] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoRR] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Moneda] ADD CONSTRAINT [PK_SICCMX_Moneda] PRIMARY KEY CLUSTERED  ([IdMoneda]) ON [PRIMARY]
GO

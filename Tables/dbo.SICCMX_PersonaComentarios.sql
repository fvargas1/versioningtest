CREATE TABLE [dbo].[SICCMX_PersonaComentarios]
(
[IdPersona] [bigint] NOT NULL,
[Comentarios] [varchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_PersonaComentarios] ADD CONSTRAINT [PK_SICCMX_PersonaComentarios] PRIMARY KEY CLUSTERED  ([IdPersona]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_PersonaComentarios] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_PersonaComentarios_SICCMX_Persona] FOREIGN KEY ([IdPersona]) REFERENCES [dbo].[SICCMX_Persona] ([IdPersona])
GO

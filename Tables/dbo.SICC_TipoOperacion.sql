CREATE TABLE [dbo].[SICC_TipoOperacion]
(
[IdTipoOperacion] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL,
[A18] [bit] NULL,
[A19] [bit] NULL,
[A20] [bit] NULL,
[A21] [bit] NULL,
[A22] [bit] NULL,
[A21OD] [bit] NULL,
[A22OD] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoOperacion] ADD CONSTRAINT [PK_SICC_TipoOperacion] PRIMARY KEY CLUSTERED  ([IdTipoOperacion]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICC_CalificacionConsumo2011]
(
[IdCalificacion] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RangoMenor] [decimal] (18, 6) NULL,
[RangoMayor] [decimal] (18, 6) NULL,
[IdMetodologia] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_CalificacionConsumo2011] ADD CONSTRAINT [PK_SICC_CalificacionConsumo2011] PRIMARY KEY CLUSTERED  ([IdCalificacion]) ON [PRIMARY]
GO

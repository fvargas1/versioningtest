CREATE TABLE [dbo].[SICC_TipoGarantiaConsumo]
(
[IdTipoGarantiaConsumo] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoBanxico] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SP] [decimal] (3, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoGarantiaConsumo] ADD CONSTRAINT [PK_SICC_TipoGarantiaConsumo] PRIMARY KEY CLUSTERED  ([IdTipoGarantiaConsumo]) ON [PRIMARY]
GO

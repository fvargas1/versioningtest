CREATE TABLE [dbo].[SICC_TipoPersonaMA]
(
[IdTipoPersona] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoPersonaMA] ADD CONSTRAINT [PK_SICC_TipoPersonaMA] PRIMARY KEY CLUSTERED  ([IdTipoPersona]) ON [PRIMARY]
GO

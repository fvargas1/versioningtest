CREATE TABLE [dbo].[MIGRACION_Archivos_Carga]
(
[IdArchivo] [int] NOT NULL,
[Fuente] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RutaCarga] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileCarga] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumRegistros] [int] NULL,
[MontoCC] [decimal] (23, 2) NULL,
[NumRegistrosBW] [int] NULL,
[MontoCCBW] [decimal] (23, 2) NULL,
[ValidacionMonto] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

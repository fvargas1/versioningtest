CREATE TABLE [R04].[0411Configuracion]
(
[IdConfiguracion] [int] NOT NULL IDENTITY(1, 1),
[TipoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SituacionCredito] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TipoCartera] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0411Configuracion] ADD CONSTRAINT [PK_0411Configuracion] PRIMARY KEY CLUSTERED  ([IdConfiguracion]) ON [PRIMARY]
GO

CREATE TABLE [Historico].[RW_RTH_Reservas]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoCapitalVigente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteresVigente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoCapitalVencido] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteresVencido] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Constante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorATR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ATR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorVECES] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Veces] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorPorPago] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorPago] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[E] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reserva] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorReserva] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

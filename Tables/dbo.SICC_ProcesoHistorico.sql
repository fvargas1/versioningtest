CREATE TABLE [dbo].[SICC_ProcesoHistorico]
(
[IdProcesoHistorico] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codename] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Position] [int] NOT NULL,
[Activo] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_ProcesoHistorico] ADD CONSTRAINT [PK_SICC_ProcesoHistorico] PRIMARY KEY CLUSTERED  ([IdProcesoHistorico]) ON [PRIMARY]
GO

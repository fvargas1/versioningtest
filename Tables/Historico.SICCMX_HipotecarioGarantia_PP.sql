CREATE TABLE [Historico].[SICCMX_HipotecarioGarantia_PP]
(
[IdPeriodoHistorico] [int] NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Garantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrctReserva] [decimal] (14, 10) NULL,
[Reserva] [decimal] (23, 2) NULL,
[ReservaCubierta] [decimal] (23, 2) NULL,
[ReservaExpuesta] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO

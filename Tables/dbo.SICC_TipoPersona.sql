CREATE TABLE [dbo].[SICC_TipoPersona]
(
[IdTipoPersona] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL,
[A18] [bit] NULL,
[A20] [bit] NULL,
[A21] [bit] NULL,
[A22] [bit] NULL,
[A21OD] [bit] NULL,
[A22OD] [bit] NULL,
[A19] [bit] NULL,
[IdRCGrupoRiesgo] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoPersona] ADD CONSTRAINT [PK_SICCMX_TipoPersona] PRIMARY KEY CLUSTERED  ([IdTipoPersona]) ON [PRIMARY]
GO

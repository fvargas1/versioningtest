CREATE TABLE [dbo].[SICC_TasaValor]
(
[IdTasaValor] [int] NOT NULL IDENTITY(1, 1),
[IdTasa] [int] NOT NULL,
[IdPeriodo] [int] NOT NULL,
[Valor] [decimal] (18, 6) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TasaValor] ADD CONSTRAINT [PK_SICC_TasaValor] PRIMARY KEY CLUSTERED  ([IdTasaValor]) ON [PRIMARY]
GO

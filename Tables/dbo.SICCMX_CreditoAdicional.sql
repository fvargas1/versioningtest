CREATE TABLE [dbo].[SICCMX_CreditoAdicional]
(
[IdCredito] [bigint] NOT NULL,
[SaldoPromedio] [decimal] (23, 2) NULL,
[InteresDevengado] [decimal] (23, 2) NULL,
[Comision] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO

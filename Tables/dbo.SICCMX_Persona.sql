CREATE TABLE [dbo].[SICCMX_Persona]
(
[IdPersona] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RFC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Domicilio] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Telefono] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Correo] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdEjecutivo] [bigint] NULL,
[IdInstitucion] [bigint] NOT NULL,
[IdActividadEconomica] [bigint] NULL,
[IdLocalidad] [bigint] NULL,
[IdDeudorRelacionadoMA] [bigint] NULL,
[IdDeudorRelacionado] [bigint] NULL,
[IdSucursal] [bigint] NULL,
[FechaCreacion] [datetime] NULL CONSTRAINT [DF_SICCMX_Persona_FechaCreacion] DEFAULT (getdate()),
[FechaActualizacion] [datetime] NULL,
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EsFondo] [int] NULL,
[PI100] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Persona] ADD CONSTRAINT [PK_SICCMX_Persona] PRIMARY KEY CLUSTERED  ([IdPersona]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Persona] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_Persona_SICC_ActividadEconomica] FOREIGN KEY ([IdActividadEconomica]) REFERENCES [dbo].[SICC_ActividadEconomica] ([IdActividadEconomica])
GO
ALTER TABLE [dbo].[SICCMX_Persona] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_Persona_SICC_DeudorRelacionado] FOREIGN KEY ([IdDeudorRelacionado]) REFERENCES [dbo].[SICC_DeudorRelacionado] ([IdDeudorRelacionado])
GO
ALTER TABLE [dbo].[SICCMX_Persona] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_Persona_SICC_Institucion] FOREIGN KEY ([IdInstitucion]) REFERENCES [dbo].[SICC_Institucion] ([IdInstitucion])
GO
ALTER TABLE [dbo].[SICCMX_Persona] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_Persona_SICC_Sucursal] FOREIGN KEY ([IdSucursal]) REFERENCES [dbo].[SICC_Sucursal] ([IdSucursal])
GO

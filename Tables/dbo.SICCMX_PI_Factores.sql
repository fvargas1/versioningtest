CREATE TABLE [dbo].[SICCMX_PI_Factores]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Orden] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_PI_Factores] ADD CONSTRAINT [PK_SICCMX_PI_Factores] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO

CREATE TABLE [Historico].[SICCMX_Persona_PI_GP]
(
[IdPeriodoHistorico] [int] NULL,
[GP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EsGarante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FactorCuantitativo] [decimal] (10, 6) NULL,
[PonderadoCuantitativo] [decimal] (10, 6) NULL,
[FactorCualitativo] [decimal] (10, 6) NULL,
[PonderadoCualitativo] [decimal] (10, 6) NULL,
[FactorTotal] [decimal] (10, 6) NULL,
[PI] [decimal] (18, 12) NULL,
[Metodologia] [int] NULL,
[IdClasificacion] [int] NULL,
[Comentarios] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

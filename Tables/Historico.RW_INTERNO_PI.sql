CREATE TABLE [Historico].[RW_INTERNO_PI]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Periodo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Metodologia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Clasificacion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorCuantitativo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorCualitativo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Factor_Alpha] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Factor_1mAlpha] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PonderadoCuantitativo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PonderadoCualitativo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorTotal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [Historico].[SICCMX_HipotecarioAdicional]
(
[IdPeriodoHistorico] [int] NULL,
[Hipotecario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SaldoPromedio] [decimal] (23, 2) NULL,
[InteresesDevengados] [decimal] (23, 2) NULL,
[Comisiones] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO

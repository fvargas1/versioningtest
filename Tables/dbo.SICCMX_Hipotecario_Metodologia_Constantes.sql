CREATE TABLE [dbo].[SICCMX_Hipotecario_Metodologia_Constantes]
(
[IdMetodologiaHipotecario] [int] NOT NULL IDENTITY(1, 1),
[IdMetodologia] [int] NULL,
[Constante] [decimal] (7, 4) NULL,
[ATR] [decimal] (7, 4) NULL,
[VECES] [decimal] (7, 4) NULL,
[MAXATR] [decimal] (7, 4) NULL,
[PorVPAGO] [decimal] (7, 4) NULL,
[PorCLTV] [decimal] (7, 4) NULL,
[INTEXP] [decimal] (7, 4) NULL,
[MON] [decimal] (7, 4) NULL,
[ATRPI] [decimal] (7, 4) NULL,
[ATRSP] [decimal] (7, 4) NULL,
[SPVALOR] [decimal] (7, 4) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Hipotecario_Metodologia_Constantes] ADD CONSTRAINT [PK_SICCMX_Hipotecario_Metodologia_Constantes] PRIMARY KEY CLUSTERED  ([IdMetodologiaHipotecario]) ON [PRIMARY]
GO

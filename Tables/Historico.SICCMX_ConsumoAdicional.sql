CREATE TABLE [Historico].[SICCMX_ConsumoAdicional]
(
[IdPeriodoHistorico] [int] NULL,
[Consumo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoPromedio] [decimal] (23, 2) NULL,
[InteresDevengado] [decimal] (23, 2) NULL,
[Comision] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO

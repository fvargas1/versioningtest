CREATE TABLE [dbo].[SICC_Localidad2015]
(
[IdLocalidad] [bigint] NOT NULL IDENTITY(1, 1),
[CodigoPais] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoEstado] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreEstado] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoMunicipio] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreMunicipio] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoLocalidad] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreLocalidad] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoRR] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Localidad2015] ADD CONSTRAINT [PK_SICC_Localidad2015] PRIMARY KEY CLUSTERED  ([IdLocalidad]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICCMX_LineaCredito_Reservas_Variables_Log]
(
[IdLineaCredito] [bigint] NOT NULL,
[Fecha] [datetime] NULL,
[Usuario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comentarios] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

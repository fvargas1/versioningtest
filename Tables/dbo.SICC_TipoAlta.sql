CREATE TABLE [dbo].[SICC_TipoAlta]
(
[IdTipoAlta] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL,
[GeneraCNBV] [smallint] NULL,
[Clasificacion] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoAlta] ADD CONSTRAINT [PK_SICCMX_TipoAlta] PRIMARY KEY CLUSTERED  ([IdTipoAlta]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICCMX_ConsumoInfo]
(
[IdConsumo] [bigint] NOT NULL,
[FechaOtorgamiento] [datetime] NULL,
[PagoExigible] [decimal] (23, 2) NULL,
[CodigoCreditoReestructurado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaDisposicion] [datetime] NULL,
[FechaVencimiento] [datetime] NULL,
[PlazoTotal] [int] NULL,
[MontoLineaAutorizada] [decimal] (23, 2) NULL,
[ValorOriginalBien] [decimal] (23, 2) NULL,
[IdMecanismoPago] [int] NULL,
[FechaCorte] [datetime] NULL,
[PlazoRemanente] [int] NULL,
[MontoExigible] [decimal] (23, 2) NULL,
[PagoRealizado] [decimal] (23, 2) NULL,
[DiasAtraso] [int] NULL,
[ExigibleTeorico] [decimal] (23, 2) NULL,
[CodigoAgrupacion] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdTipoGarantia] [int] NULL,
[ImporteGarantia] [decimal] (23, 2) NULL,
[RegistroUnicoGarantia] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuitasCondonaciones] [decimal] (23, 2) NULL,
[FechaBaja] [datetime] NULL,
[IdTipoBaja] [int] NULL,
[BonificacionesDescuentos] [decimal] (23, 2) NULL,
[NuevoFolioCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditoCodificado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClienteCodificado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdTipoProductoSerie4] [int] NULL,
[FolioConsultaBuro] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PagoRealizadoCal] [decimal] (23, 2) NULL,
[LimCredito] [decimal] (23, 2) NULL,
[PagoNGI] [decimal] (23, 2) NULL,
[SaldoRev] [decimal] (23, 2) NULL,
[SaldoPMSI] [decimal] (23, 2) NULL,
[PagoMin] [decimal] (23, 2) NULL,
[SaldoPCI] [decimal] (23, 2) NULL,
[InteresPCI] [decimal] (18, 10) NULL,
[IdMoneda] [int] NULL,
[PorcentajePagoRealizado] [decimal] (18, 10) NULL,
[ATR] [decimal] (18, 10) NULL,
[MaxATR] [decimal] (18, 10) NULL,
[INDATR] [bit] NULL,
[VECES] [decimal] (18, 10) NULL,
[Impago] [decimal] (18, 10) NULL,
[NumeroImpagosConsecutivos] [decimal] (18, 10) NULL,
[NumeroImpagosHistoricos] [decimal] (18, 10) NULL,
[MesesTranscurridos] [decimal] (18, 10) NULL,
[PorPago] [decimal] (18, 10) NULL,
[PorSDOIMP] [decimal] (18, 10) NULL,
[PorPR] [decimal] (18, 10) NULL,
[PorcentajeUso] [decimal] (18, 10) NULL,
[TarjetaActiva] [bit] NULL,
[Aforo] [decimal] (23, 2) NULL,
[Enganche] [decimal] (23, 2) NULL,
[ValorTotalCredito] [decimal] (23, 2) NULL,
[CAT] [decimal] (18, 10) NULL,
[ExigibleSinSegurosComisiones] [decimal] (23, 2) NULL,
[IdModalidadCAT] [int] NULL,
[PrimaSeguroObligatorio] [decimal] (23, 2) NULL,
[PrimaSeguroDanos] [decimal] (23, 2) NULL,
[IdTipoProductoSerie4_2016] [int] NULL,
[DiasVencidos] [int] NULL,
[AntiguedadAcreditado] [decimal] (10, 6) NULL,
[MonExiRepSIC] [decimal] (23, 2) NULL,
[MonExiIns] [decimal] (23, 2) NULL,
[Meses_desde_ult_atr_bk] [decimal] (10, 6) NULL,
[FechaBuro] [datetime] NULL,
[GVeces1] [int] NULL,
[GVeces2] [int] NULL,
[GVeces3] [int] NULL,
[VecesMontoBanco] [decimal] (18, 10) NULL,
[Alto] [int] NULL,
[Medio] [int] NULL,
[Bajo] [int] NULL,
[limcreditocalif] [decimal] (23, 2) NULL,
[idgarantia] [int] NULL,
[indicadorcat] [bit] NULL,
[comtotal] [decimal] (23, 2) NULL,
[comtardio] [decimal] (23, 2) NULL,
[pagonginicio] [decimal] (23, 2) NULL,
[pagoexigepsi] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_ConsumoInfo] ADD CONSTRAINT [PK_SICCMX_ConsumoInfo] PRIMARY KEY CLUSTERED  ([IdConsumo]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICC_IndicadorGarantia_Consumo]
(
[IdIndicador] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoBanxico] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_IndicadorGarantia_Consumo] ADD CONSTRAINT [PK_SICC_IndicadorGarantia_Consumo] PRIMARY KEY CLUSTERED  ([IdIndicador]) ON [PRIMARY]
GO

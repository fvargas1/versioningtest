CREATE TABLE [dbo].[SICC_SituacionHipotecario]
(
[IdSituacionHipotecario] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_SituacionHipotecario] ADD CONSTRAINT [PK_SICC_SituacionHipotecario] PRIMARY KEY CLUSTERED  ([IdSituacionHipotecario]) ON [PRIMARY]
GO

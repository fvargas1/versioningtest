CREATE TABLE [Historico].[SICCMX_ConsumoGarantia]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Garantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorUsadoGarantia] [decimal] (14, 10) NULL,
[MontoUsadoGarantia] [decimal] (23, 2) NULL,
[Aplica] [bit] NULL
) ON [PRIMARY]
GO

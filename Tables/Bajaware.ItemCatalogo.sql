CREATE TABLE [Bajaware].[ItemCatalogo]
(
[IdItemCatalogo] [bigint] NOT NULL IDENTITY(1, 1),
[IdCatalogo] [int] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [int] NOT NULL,
[Padre] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Orden] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Bajaware].[ItemCatalogo] ADD CONSTRAINT [PK_ItemCatalogo_1] PRIMARY KEY CLUSTERED  ([IdItemCatalogo]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICCMX_CreditosSinClasificar]
(
[CodigoPersona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombrePersona] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroLinea] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[BAJAWARE_Application]
(
[IdApplication] [dbo].[BAJAWARE_utID] NOT NULL IDENTITY(1, 1),
[Active] [dbo].[BAJAWARE_utActivo] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Folder] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HomePage] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [dbo].[BAJAWARE_utInt] NOT NULL,
[StringKey] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_BAJAWARE_Application_StringKey] DEFAULT ('B'),
[Version] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_Application] ADD CONSTRAINT [PK_BAJAWARE_Application] PRIMARY KEY CLUSTERED  ([IdApplication]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICCMX_Garantia_Log]
(
[IdGarantia] [bigint] NOT NULL,
[FechaCalculo] [datetime] NULL,
[Usuario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descripcion] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICC_CategoriaCredito]
(
[IdCategoria] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_CategoriaCredito] ADD CONSTRAINT [PK_SICCMX_CategoriaConsumo] PRIMARY KEY CLUSTERED  ([IdCategoria]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICC_TipoEntidad]
(
[IdTipoEntidad] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoEntidad] ADD CONSTRAINT [PK_SICC_TipoEntidad] PRIMARY KEY CLUSTERED  ([IdTipoEntidad]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[MIGRACION_TipoDato_Validacion]
(
[IdTipoDatoValidacion] [int] NOT NULL IDENTITY(1, 1),
[TipoDato] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[storedProcedure] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_TipoDato_Validacion] ADD CONSTRAINT [PK_MIGRACION_TipoDato_Validacion] PRIMARY KEY CLUSTERED  ([IdTipoDatoValidacion]) ON [PRIMARY]
GO

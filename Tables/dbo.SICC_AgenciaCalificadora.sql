CREATE TABLE [dbo].[SICC_AgenciaCalificadora]
(
[IdAgencia] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_AgenciaCalificadora] ADD CONSTRAINT [PK_SICCMX_AgenciaCalificadora] PRIMARY KEY CLUSTERED  ([IdAgencia]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[MIGRACION_InconsistenciaResumen]
(
[IdInconsistenciaResumen] [bigint] NOT NULL IDENTITY(1, 1),
[IdInconsistencia] [int] NOT NULL,
[Numero] [int] NOT NULL,
[Fecha] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_InconsistenciaResumen] ADD CONSTRAINT [PK_MIGRACION_InconsistenciaResumen] PRIMARY KEY CLUSTERED  ([IdInconsistenciaResumen]) ON [PRIMARY]
GO

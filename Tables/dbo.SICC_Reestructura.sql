CREATE TABLE [dbo].[SICC_Reestructura]
(
[IdReestructura] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Reestructura] ADD CONSTRAINT [PK_SICCMX_Reestructura] PRIMARY KEY CLUSTERED  ([IdReestructura]) ON [PRIMARY]
GO

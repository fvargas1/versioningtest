CREATE TABLE [dbo].[SICC_ProcesoCalificacion]
(
[IdProcesoCalificacion] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codename] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdMetodologia] [int] NULL,
[Position] [int] NOT NULL,
[Activo] [bit] NOT NULL,
[IdGrupoProceso] [int] NULL,
[IdMetodologiaGeneral] [int] NULL,
[Params] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_ProcesoCalificacion] ADD CONSTRAINT [PK_SICC_ProcesoCalificacion] PRIMARY KEY CLUSTERED  ([IdProcesoCalificacion]) ON [PRIMARY]
GO

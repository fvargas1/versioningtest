CREATE TABLE [dbo].[BAJAWARE_UseCase]
(
[IdUseCase] [dbo].[BAJAWARE_utID] NOT NULL IDENTITY(1, 1),
[Active] [dbo].[BAJAWARE_utActivo] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Visible] [dbo].[BAJAWARE_utActivo] NOT NULL,
[PageLink] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdUseCaseGroup] [dbo].[BAJAWARE_utID] NOT NULL,
[SortOrder] [dbo].[BAJAWARE_utInt] NOT NULL,
[StringKey] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_UseCase] ADD CONSTRAINT [PK_BAJAWARE_UseCase] PRIMARY KEY CLUSTERED  ([IdUseCase]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_UseCase] ADD CONSTRAINT [FK_BAJAWARE_UseCase_BAJAWARE_UseCaseGroup] FOREIGN KEY ([IdUseCaseGroup]) REFERENCES [dbo].[BAJAWARE_UseCaseGroup] ([IdUseCaseGroup])
GO

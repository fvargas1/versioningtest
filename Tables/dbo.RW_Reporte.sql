CREATE TABLE [dbo].[RW_Reporte]
(
[IdReporte] [int] NOT NULL IDENTITY(1, 1),
[GrupoReporte] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReglamentoOficial] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReglamentoOficialPath] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Periodicidad] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreTabla] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SProcGenerate] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SProcGet] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LayoutPath] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreSSRS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL,
[IdGrupoReporte] [int] NULL,
[TablaOrigen] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreCNBV] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RW_Reporte] ADD CONSTRAINT [PK_RW_Reporte] PRIMARY KEY CLUSTERED  ([IdReporte]) ON [PRIMARY]
GO

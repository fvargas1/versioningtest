CREATE TABLE [dbo].[SICCMX_CreditoGarantia_PP]
(
[IdRel] [bigint] NOT NULL IDENTITY(1, 1),
[IdCredito] [bigint] NOT NULL,
[IdGarantia] [bigint] NOT NULL,
[PrctReserva] [decimal] (18, 12) NULL,
[Reserva] [decimal] (23, 2) NULL,
[ReservaCubierta] [decimal] (23, 2) NULL,
[ReservaExpuesta] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_CreditoGarantia_PP] ADD CONSTRAINT [PK_SICCMX_CreditoGarantia_PP] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICC_GrupoProceso]
(
[IdGrupoProceso] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_GrupoProceso] ADD CONSTRAINT [PK_SICC_GrupoProceso] PRIMARY KEY CLUSTERED  ([IdGrupoProceso]) ON [PRIMARY]
GO

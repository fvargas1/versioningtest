CREATE TABLE [Historico].[SICCMX_HipotecarioReservas]
(
[IdPeriodoHistorico] [int] NULL,
[Hipotecario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FechaCalificacion] [datetime] NULL,
[MontoExpuesto] [decimal] (23, 2) NULL,
[SPExpuesto] [decimal] (18, 12) NULL,
[ReservaExpuesto] [decimal] (23, 2) NULL,
[PorcentajeExpuesto] [decimal] (18, 12) NULL,
[CalificacionExpuesto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoCubierto] [decimal] (23, 2) NULL,
[SPCubierto] [decimal] (18, 12) NULL,
[ReservaCubierto] [decimal] (23, 2) NULL,
[PorcentajeCubierto] [decimal] (18, 12) NULL,
[CalificacionCubierto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoAdicional] [decimal] (23, 2) NULL,
[ReservaAdicional] [decimal] (23, 2) NULL,
[PorcentajeAdicional] [decimal] (18, 12) NULL,
[CalificacionAdicional] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

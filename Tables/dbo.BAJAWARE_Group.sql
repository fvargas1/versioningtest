CREATE TABLE [dbo].[BAJAWARE_Group]
(
[IdGroup] [dbo].[BAJAWARE_utID] NOT NULL IDENTITY(1, 1),
[Active] [dbo].[BAJAWARE_utActivo] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdGroupType] [dbo].[BAJAWARE_utID] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_Group] ADD CONSTRAINT [PK_BAJAWARE_Group] PRIMARY KEY CLUSTERED  ([IdGroup]) ON [PRIMARY]
GO

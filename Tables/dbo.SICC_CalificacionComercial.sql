CREATE TABLE [dbo].[SICC_CalificacionComercial]
(
[IdCalificacion] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Orden] [int] NULL,
[ReservaInferior] [money] NULL,
[ReservaIntermedio] [money] NULL,
[ReservaSuperior] [money] NULL,
[MorosidadInferior] [int] NULL,
[MorosidadSuperior] [int] NULL,
[ReservaGobierno] [money] NULL,
[ReservaProyectosInferior] [money] NULL,
[ReservaProyectosSuperior] [money] NULL,
[PorMinimo] [decimal] (18, 6) NULL,
[PorMaximo] [decimal] (18, 6) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_CalificacionComercial] ADD CONSTRAINT [PK_SICCMX_CalificacionComercial] PRIMARY KEY CLUSTERED  ([IdCalificacion]) ON [PRIMARY]
GO

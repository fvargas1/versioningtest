CREATE TABLE [dbo].[SICC_EsGarante]
(
[IdEsGarante] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Layout] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

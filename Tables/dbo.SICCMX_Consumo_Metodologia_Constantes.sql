CREATE TABLE [dbo].[SICCMX_Consumo_Metodologia_Constantes]
(
[IdMetConstante] [int] NOT NULL IDENTITY(1, 1),
[IdMetodologia] [int] NOT NULL,
[ATR] [decimal] (8, 5) NOT NULL,
[ProPago] [decimal] (8, 5) NOT NULL,
[ProPagoGrupal] [decimal] (8, 5) NOT NULL,
[INDATR] [decimal] (8, 5) NOT NULL,
[ProPR] [decimal] (8, 5) NOT NULL,
[VECES] [decimal] (8, 5) NOT NULL,
[MAXATR] [decimal] (8, 5) NOT NULL,
[PorSDOIMP] [decimal] (8, 5) NOT NULL,
[PorSDOIMPTotal] [decimal] (8, 5) NOT NULL,
[CiclosPromedio] [decimal] (8, 5) NOT NULL,
[CiclosAcum] [decimal] (8, 5) NOT NULL,
[NumIntegrantes] [decimal] (8, 5) NULL,
[ATRPI] [decimal] (8, 5) NOT NULL,
[ATRSP] [decimal] (8, 5) NOT NULL,
[SPVALOR] [decimal] (8, 5) NULL,
[OTRO] [decimal] (8, 5) NULL,
[Constante] [decimal] (8, 5) NULL,
[Alto] [decimal] (8, 5) NULL,
[Medio] [decimal] (8, 5) NULL,
[Bajo] [decimal] (8, 5) NULL,
[GVeces1] [decimal] (8, 5) NULL,
[GVeces2] [decimal] (8, 5) NULL,
[GVeces3] [decimal] (8, 5) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Consumo_Metodologia_Constantes] ADD CONSTRAINT [PK_SICCMX_Consumo_Metodologia_Constantes] PRIMARY KEY CLUSTERED  ([IdMetConstante]) ON [PRIMARY]
GO

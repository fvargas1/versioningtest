CREATE TABLE [dbo].[SICC_Conceptos_Periodo]
(
[IdConceptoPeriodo] [int] NOT NULL IDENTITY(1, 1),
[IdConcepto] [int] NOT NULL,
[IdPeriodo] [int] NOT NULL,
[Monto] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Conceptos_Periodo] ADD CONSTRAINT [PK_SICC_Conceptos_0419] PRIMARY KEY CLUSTERED  ([IdConceptoPeriodo]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICC_Catalogo]
(
[IdCatalogo] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPGet] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Activo] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Catalogo] ADD CONSTRAINT [PK_SICC_Catalogo] PRIMARY KEY CLUSTERED  ([IdCatalogo]) ON [PRIMARY]
GO

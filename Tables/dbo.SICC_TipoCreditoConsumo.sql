CREATE TABLE [dbo].[SICC_TipoCreditoConsumo]
(
[IdTipoCreditoConsumo] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoBanxico] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClasificacionReportes] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoCreditoConsumo] ADD CONSTRAINT [PK_SICC_TipoCreditoConsumo] PRIMARY KEY CLUSTERED  ([IdTipoCreditoConsumo]) ON [PRIMARY]
GO

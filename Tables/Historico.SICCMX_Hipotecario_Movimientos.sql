CREATE TABLE [Historico].[SICCMX_Hipotecario_Movimientos]
(
[IdPeriodoHistorico] [int] NULL,
[Hipotecario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TipoMovimiento] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Monto] [decimal] (23, 2) NOT NULL,
[Fecha] [datetime] NOT NULL,
[CalificacionAfecto] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto0419] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto0420] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto0424] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [Historico].[SICCMX_CreditoGarantia_PP]
(
[IdPeriodoHistorico] [int] NULL,
[Credito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Garantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrctReserva] [decimal] (18, 12) NULL,
[Reserva] [decimal] (18, 12) NULL,
[ReservaCubierta] [decimal] (18, 12) NULL,
[ReservaExpuesta] [decimal] (18, 12) NULL
) ON [PRIMARY]
GO

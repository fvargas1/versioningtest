CREATE TABLE [dbo].[SICC_PeriodicidadInteres]
(
[IdPeriodicidadInteres] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL,
[CodigoCNBV_A20] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV_A22] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_PeriodicidadInteres] ADD CONSTRAINT [PK_SICCMX_PeriodicidadInteres] PRIMARY KEY CLUSTERED  ([IdPeriodicidadInteres]) ON [PRIMARY]
GO

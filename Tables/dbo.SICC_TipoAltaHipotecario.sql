CREATE TABLE [dbo].[SICC_TipoAltaHipotecario]
(
[IdTipoAltaHipotecario] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoAltaHipotecario] ADD CONSTRAINT [PK_SICC_TipoAltaHipotecario] PRIMARY KEY CLUSTERED  ([IdTipoAltaHipotecario]) ON [PRIMARY]
GO

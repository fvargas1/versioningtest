CREATE TABLE [dbo].[SICC_SituacionCredito]
(
[IdSituacionCredito] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_SituacionCredito] ADD CONSTRAINT [PK_SICCMX_SituacionCredito] PRIMARY KEY CLUSTERED  ([IdSituacionCredito]) ON [PRIMARY]
GO

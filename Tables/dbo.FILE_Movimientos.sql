CREATE TABLE [dbo].[FILE_Movimientos]
(
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoMovimiento] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Monto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fecha] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[errorFormato] [bit] NULL,
[errorCatalogo] [bit] NULL,
[Homologado] [bit] NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

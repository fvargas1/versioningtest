CREATE TABLE [Historico].[SICCMX_Hipotecario_Reservas_Variables]
(
[IdPeriodoHistorico] [int] NULL,
[Hipotecario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PI] [decimal] (18, 12) NULL,
[SP] [decimal] (18, 12) NULL,
[E] [decimal] (23, 2) NULL,
[Reserva] [decimal] (23, 2) NULL,
[PorReserva] [decimal] (18, 12) NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PerdidaEsperada] [decimal] (18, 12) NULL,
[ReservaOriginal] [decimal] (23, 2) NULL,
[PorCubiertoPP] [decimal] (18, 12) NULL,
[ECubiertaPP] [decimal] (23, 2) NULL,
[ReservaCubPP] [decimal] (23, 2) NULL,
[ReservaPP_Credito] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO

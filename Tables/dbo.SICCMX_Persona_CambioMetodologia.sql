CREATE TABLE [dbo].[SICCMX_Persona_CambioMetodologia]
(
[IdCambioMet] [int] NOT NULL IDENTITY(1, 1),
[Persona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NumeroDeLineaActual] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MetodologiaAnterior] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrgDescAnterior] [bit] NULL,
[MetodologiaActual] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrgDescActual] [bit] NULL,
[IdPeriodoHistorico] [bigint] NULL
) ON [PRIMARY]
GO

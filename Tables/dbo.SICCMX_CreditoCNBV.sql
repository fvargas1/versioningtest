CREATE TABLE [dbo].[SICCMX_CreditoCNBV]
(
[IdCreditoCNBV] [bigint] NOT NULL IDENTITY(1, 1),
[NumeroLinea] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoPersona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[_Consecutivo] [int] NULL,
[FechaCreacion] [datetime] NULL CONSTRAINT [DF_SICCMX_CreditoCNBV_FechaCreacion] DEFAULT (getdate()),
[IdPeriodo] [bigint] NULL,
[MontoLinea] [decimal] (23, 2) NULL,
[IdMoneda] [bigint] NULL,
[IdTipoProducto] [bigint] NULL,
[IdTipoOperacion] [int] NULL,
[IdTasaReferencia] [bigint] NULL,
[FechaVencimiento] [datetime] NULL,
[IdPeriodoBaja] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_CreditoCNBV] ADD CONSTRAINT [PK_SICCMX_CreditoCNBV] PRIMARY KEY CLUSTERED  ([IdCreditoCNBV]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SICCMX_PI_SubFactor]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdFactor] [int] NULL,
[Orden] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_PI_SubFactor] ADD CONSTRAINT [PK_SICCMX_PI_SubFactor] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO

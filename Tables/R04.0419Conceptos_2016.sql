CREATE TABLE [R04].[0419Conceptos_2016]
(
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Monto] [decimal] (23, 2) NULL,
[CargoAbono] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[RW_Rel_Grupo_ReporteCampos]
(
[IdRel] [int] NOT NULL IDENTITY(1, 1),
[IdGroup] [int] NOT NULL,
[IdReporteCampos] [int] NOT NULL,
[TipoDato] [int] NOT NULL,
[Longitud] [int] NULL,
[Decimales] [int] NULL,
[Predeterminado] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AplicaFormatoVP] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RW_Rel_Grupo_ReporteCampos] ADD CONSTRAINT [PK_RW_Rel_Grupo_ReporteCampos] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO

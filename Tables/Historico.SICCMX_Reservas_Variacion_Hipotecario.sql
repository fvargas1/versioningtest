CREATE TABLE [Historico].[SICCMX_Reservas_Variacion_Hipotecario]
(
[IdPeriodoHistorico] [int] NULL,
[Hipotecario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReservaCalConsDesc] [decimal] (23, 2) NULL,
[ReservaAdicConsDesc] [decimal] (23, 2) NULL,
[ReservaPeriodoAnt] [decimal] (23, 2) NULL,
[ReservaAdicPeriodoAnt] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[BAJAWARE_UserLog]
(
[IdUserLog] [int] NOT NULL IDENTITY(1, 1),
[IdUser] [int] NOT NULL,
[IdUserModified] [int] NOT NULL,
[FechaModificacion] [datetime] NOT NULL,
[ModificationType] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_UserLog] ADD CONSTRAINT [PK_BAJAWARE_UserLog] PRIMARY KEY CLUSTERED  ([IdUserLog]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[FILE_Califica_Homologacion]
(
[Campo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoBanco] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoBW] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL,
[FechaAlta] [datetime] NOT NULL CONSTRAINT [DF__FILE_Cali__Fecha__6657AA72] DEFAULT (getdate()),
[Tipo] [int] NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
